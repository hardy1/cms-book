FROM node:14.18.3-alpine

ARG username=moc-client
ARG workdir=/moc-client
ARG uid

RUN deluser node
RUN mkdir -p $workdir \
    && adduser -h $workdir -u 1000 -D $username

USER 1000
WORKDIR $workdir
CMD /bin/sh
