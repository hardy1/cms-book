module.exports = {
  stories: [
    "../packages/designsystem/src/**/*.stories.@(js|jsx|ts|tsx|mdx)",
    "../packages/client/src/**/*.stories.@(js|jsx|ts|tsx|mdx)",
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-a11y",
  ],
}
