# MOC

## Run the project locally

Clone the project and install the dependencies:

```sh
$ git clone git@bitbucket.org:coffeecup/moc.git
$ cd moc
```

## Scripts

### Development environment setup (Docker):

```sh
$ ./scripts/setup
$ source ~/.zshrc
```

### Client Development server (Docker):

```sh
$ moc dev
```

or

```sh
$ ./scripts/moc-client dev
```

#### Command `moc` options (Docker):

```
moc help            List commands
moc run [args]      Run application container, with optional args
moc setup           Run container setup
moc dev             Run client development mode
moc build           Build application (client e designsystem)
```

### Install the dependencies (Local):

```sh
$ yarn
```

### Client Development server (Local):

```sh
$ yarn dev
```

### Build client (Local):

```sh
$ yarn build
```

#### Storybook:

```sh
$ yarn run storybook
```

Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

#### Build Storybook:

```sh
$ npm run build-storybook
```

### Run package command:

```sh
$ yarn workspace <package name> <command>
```

### Branches

Choose short and descriptive names.

```sh
# Good
$ git checkout -b feature/oauth

# Bad
$ git checkout -b login_fix
```

Use dashes to separate words.

```sh
# Good
$ git checkout -b feature/add-webhook

# Bad
$ git checkout -b feature/add_webhook
```

Use default prefixes for new branches, to allow automated workflows and make branch types clearer.

```sh
# Bugfix
$ bugfix/foo-bar

# Feature
feature/foo-bar

# Hotfix
hotfix/foo-bar

# Release
release/foo-bar
```

## Merges

Merges should be always _squashed_ to turn the commit message into the pull request title.
