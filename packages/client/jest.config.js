module.exports = {
  roots: ["<rootDir>"],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  moduleFileExtensions: ["ts", "tsx", "js"],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupFilesAfterEnv: ["<rootDir>/config/setupEnzyme.ts"],
  moduleNameMapper: {
    "^components(.*)$": "<rootDir>/src/components$1",
    "^containers(.*)$": "<rootDir>/src/containers$1",
    "^hooks(.*)$": "<rootDir>/src/hooks$1",
    "^lib(.*)$": "<rootDir>/src/lib$1",
    "^pages(.*)$": "<rootDir>/src/pages$1",
    "^resources(.*)$": "<rootDir>/src/resources$1",
    "^services(.*)$": "<rootDir>/src/services$1",
    "^utils(.*)$": "<rootDir>/utils$1",
    "\\.(css|less|scss)$": "identity-obj-proxy",
  }
}
