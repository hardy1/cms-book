export enum ChannelSlug {
  VIDEOS = 'videos',
  NEWS = 'noticias',
  PODCASTS = 'podcasts',
  CONGRESSES = 'congressos',
  TIPS = 'dicas',
}

export enum ChannelName {
  VIDEOS = 'Vídeo-MOC',
  NEWS = 'Notícias',
  PODCASTS = 'Podcasts',
  CONGRESSES = 'Coberturas de Congressos',
  TIPS = 'MOC Dicas',
}

export enum ChannelSection {
  NAME = 'Canal MOC',
  SLUG = 'canais-moc',
}
