export enum paymentMethod {
  CREDIT_CARD = 'credit_card',
  BANK_SLIP = 'boleto',
}

export enum purchaseMethod {
  GROUP = 'group',
  SINGLE = 'single',
}
