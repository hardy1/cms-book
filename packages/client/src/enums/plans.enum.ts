export enum planCode {
  LIMITED = 'plano-limitado',
  MAX = 'plano-max',
  PREMIUM = 'plano-premium',
}
