export enum themeName {
  NAVYBLUE = 'navyBlue',
  RED = 'red',
  CHAMPAGNE = 'champagne',
  PURPLE = 'purple',
  DARKGREEN = 'darkGreen',
  UMBER = 'umber',
  BLACK = 'black',
  OCRE = 'ocre',
  FALLBACK = 'fallback',
}

export * from './channels.enum'
export * from './institution-type.enum'
export * from './login.enum'
export * from './payment-method.enum'
export * from './plans.enum'
export * from './storages.enum'
export * from './cookies.enum'
export * from './forms-entries.enum'
export * from './professions.enum'
export * from './errors.enum'
