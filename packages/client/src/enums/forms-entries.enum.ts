export enum FormPlaceholder {
  DATE = '__/__/__',
  SELECT = 'Selecione',
  FULLNAME = 'Nome e sobrenome',
  CPF_DOCUMENT = 'Ex: 111.222.333-44',
  EMAIL = 'meunome@email.com.br',
  COUNTRY = 'Selecione um país',
}

export enum PasswordFormLabel {
  OLD_PASSWORD = 'Senha atual',
  PASSWORD = 'Nova senha',
  CONFIRM_PASSWORD = 'Repetir Nova senha',
}

export enum PasswordFormId {
  OLD_PASSWORD = 'oldPassword',
  PASSWORD = 'password',
  CONFIRM_PASSWORD = 'confirmPassword',
}

export enum PersonalFormLabel {
  NAME = 'Nome Completo',
  CPF_DOCUMENT = 'CPF',
  PHONE = 'Telefone',
  EMAIL = 'Email',
  LANGUAGE = 'Idioma',
  COUNTRY = 'País',
  ZIPCODE = 'CEP',
  STATE = 'UF',
  CITY = 'Cidade',
  STREET = 'Endereço',
  NUMBER = 'Número',
  EXTENTION = 'Complemento (Opcional)',
}

export enum PersonalFormId {
  NAME = 'name',
  CPF_DOCUMENT = 'cpfDocument',
  PHONE = 'phone',
  EMAIL = 'email',
  LANGUAGE = 'language',
  COUNTRY = 'country',
  ZIPCODE = 'zipcode',
  STATE = 'state',
  CITY = 'city',
  STREET = 'street',
  NUMBER = 'number',
  EXTENTION = 'extention',
}

export enum ProfessionalFormLabel {
  PROFESSION = 'Profissão',
  SPECIALIZATION = 'Especialização',
  SPECIALIZATION_CUSTOM = 'Qual?',
  STATE = 'UF',
  FORMATION_DATE = 'Data de formação',
  FINISHED_RESIDENCY_DATE = 'Término da residência',
  PROFESSION_STATUS = 'Status',
}

export enum ProfessionalFormId {
  PROFESSION = 'profession',
  SPECIALIZATION = 'specialization',
  SPECIALIZATION_CUSTOM = 'specializationCustom',
  STATE = 'state',
  FORMATION_DATE = 'formationDate',
  FINISHED_RESIDENCY_DATE = 'finishedResidencyDate',
  PROFESSION_REGISTER = 'professionRegister',
  PROFESSION_STATUS = 'professionStatus',
}
