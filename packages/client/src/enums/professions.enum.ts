export enum ProfessionSlug {
  OTHER_HEALTH_PROFESSIONALS = 'outros-profissionais-da-area-da-saude',
  STUDENT = 'estudante',
  RESIDENT = 'residente',
}

export enum SpecializationSlug {
  OTHER = 'Outras',
}
