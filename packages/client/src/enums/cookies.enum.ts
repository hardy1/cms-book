export enum cookies {
  COUPON_VERIFICATION = 'verifiedCoupon',
  SELECTED_PLAN = 'selectedPlan',
  SELECTED_OFFER = 'selectedOffer',
  SELECTED_PERIOD = 'selectedPeriod',
  SELECTED_USER_PROFILE = 'selectedProfile',
  TOKEN = 'token',
  USER = 'user',
}
