export enum institutionTypeName {
  PUBLIC = 'Pública',
  PRIVATE = 'Privada',
}

export enum institutionTypeId {
  PUBLIC = 1,
  PRIVATE = 2,
}
