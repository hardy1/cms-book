import styled from 'styled-components'
import colors from '../../resources/colors'

export const NavBarContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100px;
  box-sizing: border-box;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;

  @media screen and (min-width: 768px) {
    position: fixed;
    display: flex;
    flex-direction: column;
    width: 300px;
    height: 100%;
  }
`

export const NavBarTopSection = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 100px;
  align-items: center;
  justify-content: center;
  background-color: ${colors.primaryColor};
  padding: 1.5rem 3rem 1rem;
  cursor: pointer;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    border-bottom: 0;
  }

  svg {
    display: inline-block;
  }

  .NavBarStyle {
    &__Button {
      position: absolute;
      top: calc(50% - 19px / 2);
      display: flex;
      align-items: center;
      justify-content: center;
      right: 24px;
      width: 22px;
      height: 19px;
      border-width: 0;
      background-color: transparent;
      padding: 0;
      cursor: pointer;

      @media screen and (min-width: 768px) {
        display: none;
      }

      svg {
        path {
          fill: #fff;
        }
      }
    }
  }
`

export const NavBarBottomSection = styled.div`
  position: absolute;
  top: 100px;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  width: 100%;
  height: calc(100vh - 100px);
  flex-direction: column;
  background-color: ${colors.primaryColor};
  color: ${colors.white};
  font-family: 'Montserrat', sans-serif;
  text-align: left;
  box-sizing: border-box;
  padding: 0;
  opacity: 0;
  pointer-events: none;
  transition: opacity 0.2s ease-out;
  z-index: 5;

  &.opened {
    opacity: 1;
    pointer-events: auto;
  }

  @media screen and (min-width: 768px) {
    padding-top: 3rem;
    opacity: 1;
    pointer-events: auto;
  }

  h6,
  h3 {
    color: ${colors.white};
  }

  h3 {
    font-size: 1rem;
    text-transform: uppercase;
    padding: 24px 48px 48px;
    margin-bottom: 0;
  }

  h6 {
    border-top: 1px solid #123e82;
    border-bottom: 1px solid #123e82;
    font-family: 'Lato', sans-serif;
    font-size: 18px;
    font-weight: 400;
    line-height: 32px;
    text-transform: none;
    padding: 24px 30px 21px 48px;
    margin-top: 0;
    margin-bottom: 0;
  }
`
