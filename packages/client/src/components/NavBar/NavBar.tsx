import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { Heading3, Heading6, LogoMoc, IconCloseMenu, IconMenu } from 'designsystem'
import { NavBarContainer, NavBarBottomSection, NavBarTopSection } from './NavBarStyle'
import NavBarSelector from './NavBarSelector'
import colors from '../../resources/colors'
import { IGenericItem } from '../../types'

const NavBar: React.FC<{
  userName?: string
  menuOptions?: IGenericItem[]
  onClick?: (e) => void
}> = ({ userName, menuOptions, onClick }) => {
  const router = useRouter()

  const [menuOpened, toggleMenu] = useState(false)

  const iconMenu = <IconMenu fill="#fff" />
  const iconClose = <IconCloseMenu fill="#fff" />

  const onClickNavBarItem = (e) => {
    onClick(e)
    toggleMenu(false)
  }

  return (
    <NavBarContainer>
      <NavBarTopSection>
        <LogoMoc
          fill={colors.white}
          onClick={() => {
            router.push('/')
          }}
        />

        <button type="button" className="NavBarStyle__Button" onClick={() => toggleMenu(!menuOpened)}>
          {menuOpened ? iconClose : iconMenu}
        </button>
      </NavBarTopSection>

      <NavBarBottomSection className={menuOpened ? 'opened' : ''}>
        <Heading6>{`Olá, ${userName ? userName : ''}`}</Heading6>
        <Heading3>Minha conta</Heading3>

        {menuOptions?.map((option: IGenericItem, index: number) => (
          <NavBarSelector key={index} option={option} onClick={onClickNavBarItem} />
        ))}
      </NavBarBottomSection>
    </NavBarContainer>
  )
}

export default NavBar
