import React from 'react'
import styled from 'styled-components'
import colors from '../../resources/colors'
import { IGenericItem } from '../../types'

const NavBarSelectorStyled = styled.button`
  position: relative;
  background-color: transparent;
  border: none;
  color: ${colors.white};
  font-family: 'Montserrat', sans-serif;
  font-size: 15px;
  font-weight: 700;
  text-align: left;
  cursor: pointer;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 48px;
  padding-right: 48px;
  margin: 0;
  transition: color 0.2s ease-out;

  &:nth-child(8) {
    margin-bottom: 10px;

    &::before {
      content: '';
      position: absolute;
      left: 0;
      bottom: -10px;
      width: 100%;
      height: 1px;
      background-color: #123e82;
    }
  }

  &:nth-child(9) {
    margin-top: 10px;
  }

  span {
    pointer-events: none;
  }

  &:hover,
  &:focus {
    color: #00e4b7;
  }

  &:active {
    color: ${colors.tertiaryColor};
  }
`

const NavBarSelector: React.FC<{
  option: IGenericItem
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void
}> = ({ option, onClick }) => {
  return (
    <NavBarSelectorStyled value={option?.value} onClick={onClick}>
      <span>{option?.name}</span>
    </NavBarSelectorStyled>
  )
}

export default NavBarSelector
