import * as React from 'react'
import { Input, IconSearch } from 'designsystem'

import { BooksSearchContainer } from './BooksSearch.styles'
import { useRouter } from 'next/router'

const BooksSearch: React.FC = () => {
  const [term, setTerm] = React.useState('')
  const router = useRouter()

  function handleSearch(e, query: string) {
    e.preventDefault()
    router.push(`/livro-pesquisa?q=${query}`)
  }

  return (
    <BooksSearchContainer>
      <form onSubmit={(e) => handleSearch(e, term)}>
        <Input
          id="inputId"
          type="search"
          placeholder="Pesquisar nos livros"
          icon={<IconSearch />}
          onChange={(e) => setTerm(e.target.value)}
        />
      </form>
    </BooksSearchContainer>
  )
}

export default BooksSearch
