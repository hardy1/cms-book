import styled from 'styled-components'

export const BooksSearchContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;

  width: 100%;

  form {
    width: 100%;
    height: 38px;

    input {
      width: 100%;
      height: 38px;
      font-size: 0.9em;
    }

    span {
      height: 38px;
      svg {
        width: 14px;
        height: 14px;
      }
    }
  }
`
