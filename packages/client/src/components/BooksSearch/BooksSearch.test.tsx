import * as React from 'react'
import { render } from '@testing-library/react'
import BooksSearch from './BooksSearch'

describe('BooksSearch', () => {
  it('SHOULD render correctly', () => {
    const component = render(<BooksSearch />)

    expect(component).toBeTruthy()
  })
})
