import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import BooksSearch from './BooksSearch'

export default {
  title: 'Books/BooksSearch',
  component: BooksSearch,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <BooksSearch />
