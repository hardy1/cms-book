import React, { useEffect, useState, useCallback } from 'react'
import { Heading2, MainContainer } from 'designsystem'
import {
  WrapperComponent,
  WrapperTitle,
  WrapperContainer,
  WrapperColumnLeft,
  WrapperColumnRight,
} from './BooksCollaborators.styles'
import { AuthorEditorType, AuthorsBooksType, CollaboratorNameType } from '../../types'

import {
  BooksCollaboratorsNavBar,
  BooksCollaboratorsSynopsis,
  BooksCollaboratorsContent,
  BooksCollaboratorsMobile,
} from '../index'

const BooksCollaborators: React.FC<AuthorEditorType> = (props) => {
  const [books, setBooks] = useState<AuthorsBooksType[] | undefined>(undefined)
  const [authors, setAuthors] = useState<string[]>([])
  const [editors, setEditors] = useState<string[]>([])
  const [currentBook, setCurrentBook] = useState<number>(0)
  const data = props.data

  const handleCurrentBook = (currentBook: number) => {
    setCurrentBook(currentBook)
  }

  const handleCollaboratorsData = useCallback((collaborator, coCollaborator, guestCollaborator, type) => {
    const filteredData = [collaborator, coCollaborator, guestCollaborator].map((data) => {
      if (data !== undefined && type === 'autores') {
        setAuthors((authors) => [...authors, ...data.collaborators.map((d: CollaboratorNameType) => d.fullname)])
      } else if (data !== undefined && type === 'editores') {
        setEditors((editors) => [...editors, ...data.collaborators.map((d: CollaboratorNameType) => d.fullname)])
      }
    })
    filteredData
  }, [])

  useEffect(() => {
    if (data) {
      const orderById = data.sort((a, b) => a.id - b.id)
      setBooks(orderById)
    }
  }, [data])

  useEffect(() => {
    if (books) {
      const {
        autores: author,
        'autores-convidados': guestAuthor,
        coautores: coAuthor,
        editores: editor,
        'editores-convidados': guestEditor,
        coeditores: coEditor,
      } = books[currentBook].collaborators_group

      setAuthors([])
      setEditors([])
      handleCollaboratorsData(author, guestAuthor, coAuthor, 'autores')
      handleCollaboratorsData(editor, guestEditor, coEditor, 'editores')
    }
  }, [currentBook, books, handleCollaboratorsData])

  return (
    <>
      {books && (
        <>
          <WrapperTitle>
            <Heading2>Editores e Autores dos Livros MOC</Heading2>
          </WrapperTitle>
          <WrapperComponent theme={books[currentBook].theme_color}>
            <MainContainer noSpacement>
              <WrapperContainer>
                <WrapperColumnLeft theme={books[currentBook].theme_color}>
                  <BooksCollaboratorsNavBar data={books} onClick={handleCurrentBook} currentBook={currentBook} />
                </WrapperColumnLeft>
                <WrapperColumnRight theme={books[currentBook].theme_color}>
                  <BooksCollaboratorsSynopsis
                    bookTitle={books[currentBook].name}
                    bookThumb={books[currentBook].cover.uri}
                    bookSynopsis={books[currentBook].synopsis}
                  />
                  <BooksCollaboratorsContent authors={authors} editors={editors} />
                </WrapperColumnRight>
              </WrapperContainer>

              <BooksCollaboratorsMobile
                data={books}
                currentBook={currentBook}
                authors={authors}
                editors={editors}
                onClick={handleCurrentBook}
              />
            </MainContainer>
          </WrapperComponent>
        </>
      )}
    </>
  )
}

export default BooksCollaborators
