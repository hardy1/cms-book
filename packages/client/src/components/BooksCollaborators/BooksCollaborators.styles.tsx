import styled from 'styled-components'
import colors from '../../resources/colors'
import theme from '../../lib/theme'
import media from '../../resources/media'

export const WrapperTitle = styled.div`
  width: 100%;
  margin: 28px 0;
  text-align: center;
`
export const WrapperComponent = styled.div`
  margin-bottom: -64px;
  background: linear-gradient(
    90deg,
    ${({ theme: themeColor }) => theme.bookColors[themeColor].secondary.hex} 50%,
    ${({ theme: themeColor }) => theme.bookColors[themeColor].hex} 50%
  );
`

export const WrapperContainer = styled.div`
  color: ${colors.white};
  width: 100%;
  max-height: 1176px;
  display: flex;

  @media ${media.tabletL} {
    display: none;
  }
`
export const WrapperColumnLeft = styled.div`
  background-color: ${({ theme: themeColor }) => theme.bookColors[themeColor].secondary.hex};
  width: 100%;
  max-width: 307px;
  padding: 32px 0;
  display: flex;
  flex-direction: column;

  .customButton {
    color: ${({ theme: themeColor }) => theme.bookColors[themeColor].secondary.hex};
    font-size: 1rem;
    margin: 20px 25px 20px 23px;
    padding: 9.75px 11.86px;
    width: 170px;
  }
`
export const WrapperColumnRight = styled.div`
  background-color: ${({ theme: themeColor }) => theme.bookColors[themeColor].hex};
  padding: 20px 20px 30px 40px;
  width: 100%;

  h2,
  h6 {
    color: ${colors.white};
  }

  h2 {
    text-align: left;
    margin: 28px 0 28px 188px;
  }

  h6 {
    font-size: 1.125rem;
    text-transform: initial;
  }
`
