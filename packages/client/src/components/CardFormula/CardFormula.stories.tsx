import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import CardFormula, { CardFormulaProps } from './CardFormula'

export default {
  title: 'Components/CardFormula',
  component: CardFormula,
} as Meta

const Template: Story<CardFormulaProps> = (args) => <CardFormula {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Cinética do PSA',
  category: 'Fórmulas médicas',
  url: '#',
}
