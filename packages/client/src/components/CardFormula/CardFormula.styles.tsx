import styled from 'styled-components'
import colors from '../../resources/colors'

export const Link = styled.a`
  display: flex;
  text-decoration: none;
`

export const Container = styled.div`
  background-color: ${colors.lightBlue};
  border-radius: 10px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  padding: 1.5rem 1rem;
  position: relative;
  width: 100%;
  min-height: 6.5rem;
`

export const IconContainer = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
`

export const Category = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.secondaryColor};
  margin-bottom: 5px;
  text-transform: uppercase;
  font-size: 0.75em;
  margin-top: 0;
`

export const Title = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  display: -webkit-box;
  font-size: 1.125em;
  margin: 1rem 0 0 0;
  overflow: hidden;
  flex: 1;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 4;
`
