import { IconFormula } from 'designsystem'
import * as React from 'react'
import { Link, Container, IconContainer, Category, Title } from './CardFormula.styles'

export type CardFormulaProps = {
  category?: string
  title?: string
  url?: string | undefined
}

const CardFormula: React.FC<CardFormulaProps> = ({ category = '', title = '', url = '' }: CardFormulaProps) => (
  <Link href={url}>
    <Container>
      <IconContainer>
        <IconFormula />
      </IconContainer>
      {category && <Category>{category}</Category>}
      {title && <Title title={title}>{title}</Title>}
    </Container>
  </Link>
)

export default CardFormula
