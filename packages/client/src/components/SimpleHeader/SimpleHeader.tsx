import React from 'react'
import { LogoMoc } from 'designsystem'

import { StyledSimpleHeader } from './SimpleHeader.styles'

const SimpleHeader: React.FC = () => {
  return (
    <StyledSimpleHeader>
      <LogoMoc />
    </StyledSimpleHeader>
  )
}

export default SimpleHeader
