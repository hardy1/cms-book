import * as React from 'react'
import { render } from '@testing-library/react'

import SimpleHeader from './SimpleHeader'

describe('SimpleHeader', () => {
  it('SHOULD render correctly', () => {
    const component = render(<SimpleHeader />)

    expect(component).toBeTruthy()
  })
})
