import styled from 'styled-components'

const StyledSimpleHeader = styled.header`
  display: none;

  @media screen and (min-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100px;
    background-color: #fff;
    border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: #f1f1f1;
    margin-bottom: 40px;
    box-sizing: border-box;
  }
`

export { StyledSimpleHeader }
