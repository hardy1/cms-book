import React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import SimpleHeader from './SimpleHeader'

export default {
  title: 'Client/SimpleHeader',
  component: SimpleHeader,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <SimpleHeader />
