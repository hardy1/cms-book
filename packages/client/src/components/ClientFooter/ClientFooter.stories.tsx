import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientFooter from './ClientFooter'
import { mockCommon } from '../../resources/mocks/common'

export default {
  title: 'Client/ClientFooter',
  component: ClientFooter,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

const footer = mockCommon.footer

export const Default: React.FC = () => <ClientFooter items={footer} />
