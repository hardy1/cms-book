import React from 'react'
import Link from 'next/link'
import { LogoMoc, LogoFera, ContactSection, MainContainer } from 'designsystem'

import ClientSocialMenu from '../../components/ClientSocialMenu'
import VerticalMenu from './VerticalMenu'

import {
  StyledFooter,
  Container,
  FooterContent,
  SocialContainer,
  ContactContainer,
  TermsContainer,
  Copyright,
  LogoFeraContainer,
} from './ClientFooter.styles'
import { IMenu } from '../../types'

const ClientFooter: React.FC<IMenu> = ({ items }: IMenu) => {
  const siteMapItems =
    items &&
    items
      .find((child) => child.title.toLowerCase() === 'sitemap')
      .children.map((item) => ({ id: item.id, value: item.title, url: item.url || '' }))

  const bookItems =
    items &&
    items
      .find((child) => child.title.toLowerCase() === 'livros')
      .children.map((item) => ({ id: item.id, value: item.title, url: item.url || '' }))

  const date = new Date()
  const currentYear = date.getFullYear()

  return (
    <StyledFooter>
      <MainContainer direction="column">
        <Container>
          <FooterContent>
            <LogoMoc />
            <div>
              <SocialContainer>
                <VerticalMenu label="Redes sociais" />
                <ClientSocialMenu />
              </SocialContainer>

              <ContactContainer>
                <VerticalMenu label="Entre em contato" />
                <ContactSection>
                  Para enviar sugestões, dúvidas ou solicitar informações, entre em contato conosco no e-mail:
                  <br />
                  <a href="mailto:contato@mocbrasil.com">contato@mocbrasil.com</a>
                </ContactSection>
              </ContactContainer>

              <TermsContainer>
                <Link href="/page/termo-de-uso-politica-de-privacidade-e-assinatura" passHref>
                  Termos de uso e Política de Privacidade
                </Link>
              </TermsContainer>
            </div>

            <VerticalMenu label="Sitemap" items={siteMapItems} />
            <VerticalMenu label="Livros" items={bookItems} />

            <div className="logosWrapper">
              <div className="logo">
                <a href="https://www.einstein.br" rel="noreferrer" target="_blank">
                  <img
                    src="/img/logo-einstein.png"
                    className="fullWidthLogo"
                    title="Albert Einstein - Sociedade Brasileira Israelita BrasileirA"
                  />
                </a>
              </div>
              <div className="logo">
                <a href="https://www.bp.org.br/" rel="noreferrer" target="_blank">
                  <img src="/img/bp.png" className="centeredLogo" title="A Beneficiência Portuguesa de São Paulo" />
                </a>
              </div>
            </div>
          </FooterContent>

          <Copyright>
            Atenção: O conteúdo deste site destina-se exclusivamente a profissionais de saúde. Nunca tome medicamentos
            tarjados por conta própria; siga sempre as orientações de seu médico. Os autores e editores desta obra
            fizeram todo esforço para assegurar que as doses e as indicações dos fármacos, bem como dos procedimentos
            apresentados no texto, estivessem de acordo com os padrões vigentes à época da publicação. Em virtude dos
            constantes avanços da Medicina e de possíveis modificações regulamentares referentes aos fármacos e
            procedimentos apresentados, recomendamos que o usuário consulte sempre outras fontes fidedignas, de modo a
            se certificar de que as informações contidas neste site estão corretas. Isso é particularmente importante no
            caso de fármacos ou procedimentos novos ou pouco usados. Este site, para uso exclusivo por profissionais de
            saúde, é editado com objetivos educacionais, estando em conformidade com a resolução no. 097/2001 do
            Conselho Regional de Medicina do Estado de São Paulo. Responsável técnico: Dr. Antonio Carlos Buzaid, CRM-SP
            45405. Nenhuma parte pode ser reproduzida ou transmitida sem a autorização dos autores. O conteúdo deste
            site é produzido de forma independente e autônoma, sem qualquer interferência das empresas/instituições
            apoiadoras ou patrocinadoras e sem que haja qualquer obrigação por parte de seus profissionais em relação à
            recomendação ou prescrição dos produtos/serviços eventualmente comercializados por quaisquer dessas
            empresas/instituições. Copyright ©&nbsp;
            {currentYear} Antonio Carlos Buzaid. Desenvolvimento: DENDRIX CNPJ 05.371.865/0001-88
          </Copyright>

          <LogoFeraContainer>
            <img src="/img/dendrix.png" className="dendrix" />
            <LogoFera />
          </LogoFeraContainer>
        </Container>
      </MainContainer>
    </StyledFooter>
  )
}

export default ClientFooter
