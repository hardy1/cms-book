import styled from 'styled-components'
import colors from '../../resources/colors'
import media from '../../resources/media'

export const StyledFooter = styled.div`
  align-items: center;
  background-color: ${colors.lightColor};
  display: flex;
  font-size: 0.75rem;
  justify-content: center;
  margin-top: 64px;
  padding: 4rem 0;
  text-align: center;

  & > span {
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 180px;
    margin: 0 auto;
  }

  @media ${media.tabletL} {
    padding: 26px 16px 65px;
  }

  .logosWrapper {
    width: 210px;

    .logo {
      margin-bottom: 25px;

      .fullWidthLogo {
        width: 100%;
      }

      .centeredLogo {
        width: 150px;
      }
    }
  }
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: 1280px;
  width: 100%;
`

export const FooterContent = styled.div`
  font-family: 'Lato', sans-serif;
  display: flex;
  justify-content: space-between;
  background-color: ${colors.lightColor};
  color: ${colors.neutralColor};
  padding: 1rem 0;
  align-items: start;
  width: 100%;

  @media ${media.tabletL} {
    display: none;
  }
`

export const SocialContainer = styled.div`
  & > *:not(:first-child) {
    margin-top: 1rem;
  }
`

export const ContactContainer = styled.div`
  margin-top: 40px;

  & > *:not(:first-child) {
    margin-top: 1rem;
  }
`

export const TermsContainer = styled.div`
  margin-top: 40px;
  text-align: left;

  a {
    display: inline-block;
    width: 170px;
    color: ${colors.neutralColor};
    font-family: 'Lato', sans-serif;
    font-size: 1rem;
    font-weight: 600;
    text-decoration: none;
  }
`

export const Copyright = styled.span`
  background-color: ${colors.lightColor};
  color: ${colors.disabledColor};
  padding: 4rem 0;
  font-size: 0.75rem;
  font-family: 'Lato', sans-serif;
  text-align: justify;
`

export const StyledVerticalMenu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

export const MenuLabel = styled.span`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 1rem;
  font-weight: 600;
`

export const Menu = styled.nav`
  display: flex;
  justify-content: center;
  max-width: 800px;
  width: 100%;
`

export const MenuList = styled.ul`
  color: ${colors.neutralColor};
  display: flex;
  font-size: 1rem;
  flex-direction: column;
  justify-content: space-between;
  line-height: 170%;
  list-style: none;
  margin: 0.875rem 0 0;
  padding: 0;
  width: 100%;
`

export const MenuItemContainer = styled.li`
  cursor: pointer;
  text-align: left;

  &:not(:first-child) {
    margin-top: 0.875rem;
  }
`

export const MenuItem = styled.a`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.875rem;
  line-height: 170%;
  text-decoration: none;
`

export const LogoFeraContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  font-family: 'Poppins', sans-serif;
  width: 200px;

  & > span {
    white-space: nowrap;
  }

  & > svg {
    margin-left: 20px;
    width: 110px;
  }
`
