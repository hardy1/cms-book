import * as React from 'react'
import Link from 'next/link'
import { StyledVerticalMenu, MenuLabel, Menu, MenuList, MenuItemContainer, MenuItem } from './ClientFooter.styles'

type MenuItemProps = {
  id: number
  value: string
  url: string
}

export type VerticalMenuProps = {
  label: string
  items?: MenuItemProps[] | undefined
}

const VerticalMenu: React.FC<VerticalMenuProps> = ({ label, items }: VerticalMenuProps) => (
  <StyledVerticalMenu>
    <MenuLabel>{label}</MenuLabel>
    {items && (
      <Menu>
        <MenuList>
          {items.map(({ id, value, url }) => (
            <MenuItemContainer key={id}>
              <Link href={url} passHref>
                <MenuItem>{value}</MenuItem>
              </Link>
            </MenuItemContainer>
          ))}
        </MenuList>
      </Menu>
    )}
  </StyledVerticalMenu>
)

export default VerticalMenu
