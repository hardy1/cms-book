import styled from 'styled-components'
import colors from '../../resources/colors'

const FixedPlanHeaderBox = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  flex-direction: column;
  max-height: 140px;
  max-width: 315px;

  @media screen and (max-width: 1919px) {
    max-width: 273px;
  }

  @media screen and (max-width: 1365px) {
    max-width: 232px;
  }

  .plan-name {
    font-weight: 600;
    font-size: 24px;
    margin-top: 12px;
    margin-bottom: 0;
  }

  .plan-price {
    margin-top: 8px;
    margin-bottom: 10px;
  }

  button {
    width: 150px;
    padding: 15px 10px;
  }

  :nth-child(1) {
    color: #66b8ff;

    button {
      background-color: #66b8ff;
      border-color: #66b8ff;
    }
  }

  :nth-child(2) {
    color: ${colors.primaryColor};

    button {
      background-color: #144590;
      border-color: #144590;
    }
  }

  :nth-child(3) {
    color: ${colors.secondaryColor}};

    button {
      background-color: #216de1;
      border-color: #216de1;
    }
  }
`

const FixedPlanHeaderContainer = styled.div`
  background-color: #fff;
  width: 100%;
  height: 140px;
  position: fixed;
  top: 0;
  left: 0;
  -webkit-box-shadow: 0px 4px 10px -1px rgba(0, 0, 0, 0.5);
  -moz-box-shadow: 0px 4px 10px -1px rgba(0, 0, 0, 0.5);
  box-shadow: 0px 4px 10px -1px rgba(0, 0, 0, 0.5);

  @media screen and (max-width: 1023px) {
    display: none;
  }

  > div {
    justify-content: flex-end;
  }
`

export { FixedPlanHeaderBox, FixedPlanHeaderContainer }
