import React from 'react'
import { Button } from 'designsystem'
import { IProduct, IPlanComparison } from '../../types'
import { handleDecimal } from '../../../utils/handleDecimal'

import StyledPlanBox from './StyledPlanBox'

interface PlanBoxProps {
  id?: number
  name?: string
  subtitle?: string
  price?: string
  monthlyPrice?: string
  details?: string
  moreInfo?: boolean
  recommended?: boolean
  plan?: IProduct
  periodicity?: string
  comparison?: IPlanComparison[]
  color?: string
  onClick?: () => void
}

const recurrency = {
  '360 days': {
    unit: 'ano',
  },
  '180 days': {
    unit: 'semestre',
  },
  '30 days': {
    unit: 'mês',
  },
}

const PlanBox: React.FC<PlanBoxProps> = ({
  id,
  name,
  subtitle,
  price,
  monthlyPrice,
  details,
  comparison,
  moreInfo,
  recommended,
  periodicity,
  color,
  onClick,
}) => {
  const isRecommended = () => {
    if ((recommended && name === 'Plano MAX') || (!recommended && name === 'Plano Premium')) {
      return <h4 className="recommended">O mais recomendado</h4>
    }
  }

  const hasComparsion =
    parseInt(monthlyPrice, 10) !== 0 &&
    comparison?.length &&
    comparison?.map((plan) => {
      return (
        <p key={plan.periodId} className="details__text">
          Plano {plan.name} R$
          {handleDecimal(plan.monthlyPriceValue)}
          /mês
        </p>
      )
    })

  return (
    <StyledPlanBox
      key={id}
      width="100%"
      className="box"
      color={color}
      titleColor={color}
      buttonColor={color}
      boxShadowMobile
    >
      {isRecommended()}
      <h3 className="Plan__Title">
        {name}
        <small>{subtitle}</small>
      </h3>
      {monthlyPrice && (
        <p className="price--monthly">
          R${handleDecimal(monthlyPrice)}
          <small>/mês</small>
        </p>
      )}
      {periodicity !== '30 days' && price && (
        <p className="price--full">{`(R$${handleDecimal(price)}/${recurrency[periodicity]?.unit})`}</p>
      )}
      {moreInfo && <p className="details__title">VEJA TAMBÉM</p>}
      {details && <p className="details__text">{details}</p>}
      {hasComparsion}
      <Button onClick={onClick}>Comece agora</Button>
    </StyledPlanBox>
  )
}

export default PlanBox
