import styled from 'styled-components'
import { Box } from 'designsystem'

import { size } from '../../resources/media'

const StyledPlanBox = styled(Box)`
  padding-bottom: 125px !important;
  min-height: 430px !important;

  @media screen and (min-width: ${size.tablet}) {
    min-height: 465px !important;
  }

  .Plan {
    &__Title {
      font-family: 'Lato', sans-serif;
      font-size: 22px;

      @media screen and (min-width: 1366px) {
        font-size: 24px;
      }
    }
  }

  .price {
    font-family: 'Montserrat', sans-serif;

    &--full {
      min-height: initial;
      margin: 0;

      + .details__text {
        margin-top: 46px;
      }
    }

    &--monthly {
      min-height: initial;
      font-family: 'Montserrat', sans-serif;
      font-size: 30px;
      font-weight: 600;
      line-height: 46px;
      margin: 0;

      @media screen and (min-width: ${size.laptopM})
        font-size: 38px;
      }

      small {
        font-size: 18px;

        @media screen and (min-width: ${size.tablet}) {
          min-height: 57px;
        }
      }

      + .details__text {
        margin-top: 46px;
      }
    }
  }

  .details {
    &__title {
      min-height: 0;
      font-weight: bold;
      font-size: 12px;
      line-height: 14px;
      margin-top: 24px;
      margin-bottom: 0.5rem;
    }

    &__text {
      font-size: 12px;
      min-height: 0;
      margin: 0;

      @media screen and (min-width: ${size.laptopM})
        font-size: 14px;
        line-height: 20px;
      }
    }
  }

  button {
    position: absolute;
    bottom: 47px;
    left: 50%;
    height: 55px;
    padding: 8.5px 10px !important;
    margin-top: 0 !important;
    transform: translateX(-50%);
  }
`

export default StyledPlanBox
