import React from 'react'
import styled from 'styled-components'
import colors from '../../resources/colors'

const RequiredFieldsStyled = styled.div`
  height: 1rem;

  p.required {
    color: ${colors.disabledColor};
    font-size: 0.8rem;
    margin: 0.8rem 0;
    line-height: 1rem;
    font-family: 'Lato';

    span {
      color: ${colors.secondaryColor};
      text-emphasis-position: over;
    }
  }
`

const RequiredFields: React.FC = () => (
  <RequiredFieldsStyled>
    <p className="required">* Todos os campos são obrigatórios</p>
  </RequiredFieldsStyled>
)

export default RequiredFields
