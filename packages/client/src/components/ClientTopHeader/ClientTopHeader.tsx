import React, { FC } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { IconLogin, LogoMoc } from 'designsystem'

import {
  TopHeaderContainer,
  Container,
  Editors,
  HeaderButton,
  LoginContainer,
  SignInButton,
  SignInText,
  LogoContainer,
} from './ClientTopHeader.styles'

const ClientTopHeader: FC<{ logged: boolean }> = ({ logged }) => {
  const router = useRouter()

  return (
    <TopHeaderContainer>
      <LogoContainer>
        <Link href="/">
          <a>
            <LogoMoc />
          </a>
        </Link>
      </LogoContainer>

      <Container>
        <Editors>
          <strong>Editores da série MOC:</strong> Antonio C. Buzaid - Fernando C. Maluf - William N. William Jr. -
          Carlos H. Barrios
        </Editors>
        {' | '}
        <Editors>
          <strong>Editor-convidado:</strong> Caio Max S. Rocha Lima
        </Editors>
        <LoginContainer onClick={() => router.push(logged ? '/minha-conta' : '/login')}>
          <SignInButton>
            <IconLogin />
          </SignInButton>
          <SignInText>{logged ? 'Minha conta' : 'Entrar'}</SignInText>
        </LoginContainer>
        {!logged && <HeaderButton onClick={() => router.push('/cadastro')}>Cadastrar-se</HeaderButton>}
      </Container>
    </TopHeaderContainer>
  )
}

export default ClientTopHeader
