import styled from 'styled-components'
import colors from '../../resources/colors'
import media from '../../resources/media'

export const TopHeaderContainer = styled.div`
  align-items: center;
  background-color: ${colors.lightColor};
  display: flex;
  height: 60px;
  justify-content: center;
  padding: 0 1.5rem;

  @media ${media.tabletL} {
    padding: 12px 0;
  }
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  max-width: 1150px;

  @media ${media.tabletL} {
    display: none;
  }
`

export const LogoContainer = styled.div`
  display: none;
  cursor: pointer;

  @media ${media.tabletL} {
    display: flex;
    margin: 0 auto;
  }
`

export const Editors = styled.span`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75rem;
  margin: 1rem;

  &:first-child {
    margin-left: 0;
  }

  @media ${media.laptop} {
    max-width: 215px;
  }
`

export const LoginContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: auto;
  cursor: pointer;
  margin: 0 20px;
`

export const SignInButton = styled.span`
  background-color: ${colors.primaryColor};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  width: 30px;
`

export const SignInText = styled.span`
  color: ${colors.neutralColor};
  margin-left: 10px;
  font-size: 1rem;
`

export const HeaderButton = styled.button`
  width: 120px;
  height: 30px;
  font-size: 1rem;
  background: ${colors.secondaryColor};
  border-radius: 30px;
  border-width: 2px;
  border-style: solid;
  color: ${colors.lightColor};
  outline: 0;
  cursor: pointer;
  border-color: ${colors.secondaryColor};
`
