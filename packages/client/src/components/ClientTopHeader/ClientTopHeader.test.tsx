import * as React from 'react'
import ClientTopHeader from './ClientTopHeader'
import { render, screen } from '@testing-library/react'
import { useSession } from 'next-auth/react'
import '@testing-library/jest-dom'

jest.mock('next-auth/react')

describe('ClientTopHeader', () => {
  it('SHOULD render correctly when logged out', async () => {
    const mockUseSession = useSession as jest.Mock
    mockUseSession.mockReturnValueOnce({ data: null, status: null, token: null })

    const component = render(<ClientTopHeader logged={false} />)

    expect(component).toBeTruthy()
    expect(screen.getByText('Entrar')).toBeInTheDocument()
  })

  it('SHOULD render correctly when logged in', async () => {
    const mockUseSession = useSession as jest.Mock
    mockUseSession.mockReturnValueOnce({
      data: {
        token:
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbW9jLnByb2QuYmFja29mZmljZS5mZXJhLmFnL2FwaS9hY2NvdW50L2xvZ2luIiwiaWF0IjoxNjQ2NzY4NjI1LCJleHAiOjE2NDY3NzIyMjUsIm5iZiI6MTY0Njc2ODYyNSwianRpIjoiVjFZck1vM2pnNk9hU1N4cCIsInN1YiI6MSwicHJ2IjoiNDExYzkxN2EwZmI1MWUwYTQyN2E3ZTNkZWFhNWE0OWUyOTJkZGI5YiJ9.P-LOjtjB4TYEFvpDeN5dKItKjgpa7jSWI0xNVkTMtjo',
        next_step: '/cadastro/dados-pessoais',
      },
      status: 'authenticated',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbW9jLnByb2QuYmFja29mZmljZS5mZXJhLmFnL2FwaS9hY2NvdW50L2xvZ2luIiwiaWF0IjoxNjQ2NzY4NjI1LCJleHAiOjE2NDY3NzIyMjUsIm5iZiI6MTY0Njc2ODYyNSwianRpIjoiVjFZck1vM2pnNk9hU1N4cCIsInN1YiI6MSwicHJ2IjoiNDExYzkxN2EwZmI1MWUwYTQyN2E3ZTNkZWFhNWE0OWUyOTJkZGI5YiJ9.P-LOjtjB4TYEFvpDeN5dKItKjgpa7jSWI0xNVkTMtjo',
    })

    const component = render(<ClientTopHeader logged={true} />)

    expect(component).toBeTruthy()
    expect(screen.getByText('Minha conta')).toBeInTheDocument()
  })
})
