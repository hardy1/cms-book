import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientTopHeader from './ClientTopHeader'

export default {
  title: 'Client/ClientTopHeader',
  component: ClientTopHeader,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <ClientTopHeader logged={false} />
