import * as React from 'react'
import StyledBox, { IStyledBoxProps } from './Box.styles'

interface BoxProps extends IStyledBoxProps {
  className?: string
  children?: React.ReactNode
}

const Box: React.FC<BoxProps> = (props) => <StyledBox {...props}>{props.children}</StyledBox>

export default Box
