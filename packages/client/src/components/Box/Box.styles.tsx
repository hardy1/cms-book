import styled from 'styled-components'
import colors from '../../resources/colors'

// TODO: Restructure Box from designsystem
export interface IStyledBoxProps {
  color?: string
  buttonColor?: string
  width?: string
  margin?: string
  padding?: string
  shadow?: boolean
  display?: string
  border?: string | 'white'
  overrideButton?: boolean
}

const StyledBox = styled.div<IStyledBoxProps>`
  position: ${({ display }) => display || 'relative'};
  display: block;
  width: ${({ width }) => width || '100%'};
  min-height: auto;
  background-color: ${colors.white};
  border: ${({ border }) => (border === 'white' ? `2px solid ${colors.white}` : border ? border : 'none')};
  border-radius: 10px;
  text-align: center;
  box-sizing: border-box;
  margin: ${({ margin }) => margin || 'auto'};
  grid-column: 1 / -1;

  @media screen and (min-width: 768px) {
    background-color: ${colors.white};
    padding: ${({ padding }) => (padding ? padding : '0')};
    box-shadow: ${({ shadow }) => (shadow ? '1px 1px 5px rgba(0, 0, 0, 0.25)' : 'none')};
  }

  :active {
    border-color: ${({ color }) => color || colors.white};
  }

  ${({ overrideButton }) =>
    overrideButton &&
    `
  button,
  a {
    margin-top: 45px;
    min-width: 150px;
    font-size: 18px;
    background-color: ${({ buttonColor }) => buttonColor};
    border-color: ${({ buttonColor }) => buttonColor};
    box-sizing: border-box;
    padding: 8.5px 15px;
  }
`}
`

export default StyledBox
