import React, { useContext } from 'react'
import { xml2json } from 'xml-js'
import { tableChildrenTagRemover } from '../../../utils'
import BookContext from '../../contexts/book'
import { StyledTable } from './Table.styles'

const spliceIntoChunks = (arr, chunkSize) => {
  const res = []
  let rowspan
  let colspan
  let currentChunkSize = chunkSize

  while (arr?.length > 0) {
    rowspan = parseInt(arr[0]?.attributes?.rowspan)
    colspan = parseInt(arr[0]?.attributes?.colspan)
    currentChunkSize = chunkSize - colspan + 1

    if (rowspan > 1) {
      res.push(arr?.splice(0, currentChunkSize))

      while (rowspan > 1) {
        res.push(arr?.splice(0, currentChunkSize - 1))
        rowspan--
      }
    } else {
      res.push(arr?.splice(0, currentChunkSize))
    }
  }
  return res
}

interface TableProps {
  tableRaw?: string
  colorTheme?: string
}

const Table: React.FC<TableProps> = ({ tableRaw, colorTheme }: TableProps) => {
  const { state } = useContext(BookContext)
  const tableTextOnly = tableChildrenTagRemover(tableRaw)

  // TODO - Include tags inside tables
  try {
    const tableStructureRaw = JSON.parse(
      xml2json(tableTextOnly, {
        compact: true,
        attributesKey: 'attributes',
        textKey: 'text',
        ignoreComment: true,
        ignoreInstruction: true,
      }),
    )

    const tableStructure = spliceIntoChunks(
      tableStructureRaw?.table?.td,
      parseInt(tableStructureRaw?.table?.attributes?.tcols),
    )

    const tableHeadCells = tableStructure?.filter((row) => row?.find((cell) => cell?.attributes?.th))
    const tableBodyCells = tableStructure?.filter((row) => row?.find((cell) => !cell?.attributes?.th))

    const tableHead = tableHeadCells.map((row, i) => (
      <tr key={i}>
        {row?.map(
          (cell, j) =>
            cell?.attributes?.th === '1' && (
              <th key={j} colSpan={parseInt(cell?.attributes?.colspan)}>
                {cell?.text}
              </th>
            ),
        )}
      </tr>
    ))

    const tableBody = tableBodyCells.map((row, i) => (
      <tr key={i}>
        {row?.map(
          (cell, j) =>
            !cell?.attributes?.th && (
              <td key={j} rowSpan={parseInt(cell?.attributes?.rowspan)} colSpan={parseInt(cell?.attributes?.colspan)}>
                {cell?.text}
              </td>
            ),
        )}
      </tr>
    ))

    return (
      <StyledTable colorTheme={colorTheme} nightMode={state?.nightMode}>
        <thead>{tableHead}</thead>
        <tbody>{tableBody}</tbody>
      </StyledTable>
    )
  } catch {
    return <></>
  }
}

export default Table
