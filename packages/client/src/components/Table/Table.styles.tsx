import styled from 'styled-components'
import setColorHex from '../../containers/Books/setColorHex'
import setColorRGBA from '../../containers/Books/setColorRGBA'
import colors from '../../resources/colors'

interface StyledTableProps {
  colorTheme?: string
  nightMode?: boolean
}

export const StyledTable = styled.table<StyledTableProps>`
  margin: 2rem 0;
  width: 100%;

  &,
  th,
  td {
    border: 1px solid
      ${({ colorTheme, theme, nightMode }) =>
        nightMode ? colors.disabledColor : setColorRGBA(colorTheme, theme, '0.2')};
    border-collapse: collapse;
  }

  tr {
    * {
      padding: 1rem;
      text-align: center;
    }
  }

  thead {
    background-color: ${({ colorTheme, theme, nightMode }) =>
      nightMode ? colors.white : setColorHex(colorTheme, theme)};
    color: ${({ nightMode }) => (nightMode ? colors.black : colors.white)};
  }

  tbody {
    tr {
      &:nth-child(even) {
        background: ${({ colorTheme, theme, nightMode }) =>
          nightMode ? colors.black : setColorRGBA(colorTheme, theme, '0.1')};
      }

      &:nth-child(odd) {
        background: ${({ nightMode }) => (nightMode ? colors.neutralColor : colors.white)};
      }
    }
  }
`
