import React, { FC, useEffect } from 'react'
import styled from 'styled-components'
import { AnimatedLoaderIcon } from './IconLoader'

type LoaderProps = {
  children?: React.ReactNode
  active: boolean
  fullscreen?: boolean
  size?: 'small' | 'medium' | 'large'
}

const Loader: FC<LoaderProps> = ({ active, fullscreen, size }: LoaderProps) => {
  const BlockedBackground = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(255, 255, 255, 0.9);
    margin: 0;
    padding: 0;
    position: fixed;
    width: 100vw;
    height: 100vh;
    z-index: 999;
    overflow: hidden;
    top: 0;
  `

  const LoaderContainer = styled.div`
    display: flex;
    justify-content: center;
    justify-self: center;
    align-items: center;
    width: 100%;
    max-height: ${size === 'small' ? '3rem' : '15rem'};
    overflow: hidden;
    grid-column: 1/-1;
  `
  useEffect(() => {
    if (active && fullscreen) {
      document.body.style.overflow = 'hidden'
      return () => {
        document.body.style.overflow = 'visible'
      }
    }
  }, [active, fullscreen])

  return active && fullscreen ? (
    <>
      <BlockedBackground>
        <AnimatedLoaderIcon size="large" />
      </BlockedBackground>
    </>
  ) : active ? (
    <LoaderContainer>
      <AnimatedLoaderIcon size={size} />
    </LoaderContainer>
  ) : (
    <></>
  )
}

export default Loader
