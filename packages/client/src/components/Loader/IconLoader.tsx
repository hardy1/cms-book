import * as React from 'react'
import styled, { keyframes } from 'styled-components'

type LoaderProps = {
  size?: string
  className?: string
}

const iconPan = (iconSize: number) => (iconSize / 100 - 1) * -50

const IconLoader = ({ className, size }: LoaderProps) => {
  const iconSize = {
    small: 2000,
    medium: 1000,
    large: 600,
  }
  const validSize = iconSize[size] || iconSize['small']

  return (
    <svg
      className={className}
      viewBox={`${iconPan(validSize)} ${iconPan(validSize)} ${validSize} ${validSize}`}
      xmlns="http://www.w3.org/2000/svg"
    >
      <defs>
        <linearGradient
          gradientUnits="userSpaceOnUse"
          x1="184.373"
          y1="115.645"
          x2="184.373"
          y2="355.645"
          id="gradient-0"
          gradientTransform="matrix(0.15286, -0.102078, 0.15709, 0.235242, -34.400682, 32.265637)"
        >
          <stop offset="0" style={{ stopColor: 'rgba(0, 105, 153, 1)' }}></stop>
          <stop offset="1" style={{ stopColor: 'rgba(33, 109, 225, 0.8)', stopOpacity: 0.9 }}></stop>
        </linearGradient>
      </defs>
      <linearGradient
        id="linearColors3"
        x1="1"
        y1="0"
        x2="0"
        y2="1"
        gradientTransform="matrix(1.024071, 0.027152, 0.036586, 1.032332, -0.036586, -0.032332)"
      >
        <stop offset="0" style={{ stopColor: 'rgb(146, 226, 220)', stopOpacity: 0.8 }}></stop>
        <stop offset="1" style={{ stopColor: 'rgba(33, 109, 225, 0.8)', stopOpacity: 0.9 }}></stop>
      </linearGradient>
      <linearGradient
        id="gradient-1"
        x1="1"
        y1="0"
        x2="0"
        y2="1"
        gradientTransform="matrix(0.475933, -0.557274, 1.310981, 0.398169, -0.446075, 0.601584)"
      >
        <stop offset="0" style={{ stopColor: 'rgba(0, 204, 164, 0.5)', stopOpacity: 0 }}></stop>
        <stop offset="0.451" style={{ stopColor: 'rgb(0, 205, 165)', stopOpacity: 0.34 }}></stop>
        <stop offset="1" style={{ stopOpacity: 0.8, stopColor: 'rgb(146, 226, 220)' }}></stop>
      </linearGradient>
      <g strokeWidth="5px" vectorEffect="non-scaling-stroke">
        <path
          d="M 91.136 73.768 C 82.652 88.465 66.97 97.518 50 97.518"
          fill="none"
          stroke="url(#linearColors3)"
          vectorEffect="non-scaling-stroke"
        ></path>
        <path
          d="M 91.136 26.268 C 99.621 40.965 99.621 59.072 91.136 73.768 M 50 2.518 C 66.97 2.518 82.652 11.572 91.136 26.268"
          fill="none"
          vectorEffect="non-scaling-stroke"
          style={{ stroke: 'url(#gradient-1)' }}
          stroke="url(#linearColors3)"
        ></path>
        <path
          d="M 8.864 73.731 C 0.379 59.035 0.379 40.928 8.864 26.231 M 8.864 26.231 C 17.349 11.535 33.03 2.481 50 2.481 M 50 97.481 C 33.03 97.481 17.349 88.428 8.864 73.731"
          fill="none"
          vectorEffect="non-scaling-stroke"
          style={{ stroke: 'url(#gradient-0)' }}
        ></path>
      </g>
    </svg>
  )
}

const rotate = keyframes`
  from {transform: rotate(0deg);}
  to {transform: rotate(360deg);}
`

const AnimatedLoaderIcon = styled(IconLoader)`
  margin: 0;
  padding: 0;
  z-index: 9999;
  animation: ${rotate} linear 5s infinite;
`

export { IconLoader, AnimatedLoaderIcon }
