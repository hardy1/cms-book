import * as React from 'react'
import theme from '../../lib/theme'
import { IMenu, IMenuItem, ISubmenuItem } from '../../types'
import ClientBooksDesktop from './ClientBooksDesktop'

const ClientBooks: React.FC<IMenu> = ({ items }: IMenu) => {
  const desktopItems: IMenuItem[] =
    items &&
    items.map((item) => {
      const submenu: IMenuItem['submenu'] = item.children.length ? mapSubmenuItems(item.children) : undefined
      return {
        id: item.id,
        color: theme.bookColors[item.theme_color]?.hex,
        image: item.image,
        value: item.title,
        year: item.year,
        url: item.url || '',
        collaborators: item.collaborators,
        submenu,
      }
    })

  return (
    <>
      <ClientBooksDesktop items={desktopItems} />
    </>
  )
}

const mapSubmenuItems = (items: IMenuItem[]): ISubmenuItem[] =>
  items && items.map((item) => ({ id: item.id, value: item.title, url: item.url || '' }))

export default ClientBooks
