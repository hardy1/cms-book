import * as React from 'react'
import { render } from '@testing-library/react'
import ClientBooks from './'
import { mockCommon } from '../../resources/mocks/common'

describe('ClientBooks', () => {
  it('SHOULD render correctly', () => {
    const component = render(<ClientBooks items={mockCommon.books} />)

    expect(component).toBeTruthy()
  })
})
