import * as React from 'react'
import Link from 'next/link'
import {
  Container,
  Image,
  Menu,
  MenuList,
  MenuItemContainer,
  MenuItem,
  SubmenuContainer,
  Submenu,
  SubMenuItemContainer,
  SubMenuItem,
  SubmenuArrow,
  MenuItemSubtitle,
  MenuItemTitle,
  SubmenuParts,
  SubmenuEditors,
  MenuHeading,
  MenuText,
} from './ClientBooks.styles'
import { useEffect } from 'react'
import { IMenu } from '../../types'

const ClientBooksDesktop: React.FC<IMenu> = ({ items }: IMenu) => {
  const menuNode = React.useRef<HTMLElement>()
  const itemNodes = []
  const [isActive, setIsActive] = React.useState(false)
  const [current, setCurrent] = React.useState<number>()

  const handleClick = (e: Event, index: number) => {
    if (current === index) {
      setIsActive(!isActive)
      return
    }

    setCurrent(index)

    if (itemNodes[index]?.contains(e.target)) {
      setIsActive(true)
      return
    }
    setIsActive(false)
  }

  const closeControl = (e: Event): void => {
    if (!menuNode?.current?.contains(e.target as Node)) {
      setIsActive(false)
      return
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', closeControl)
    return () => {
      document.removeEventListener('mousedown', closeControl)
    }
  }, [])

  return (
    <Container>
      <Menu ref={menuNode}>
        <MenuList>
          {items &&
            items.map(({ id, color, value, year, image, submenu, collaborators }, index) => (
              <MenuItemContainer
                key={id}
                color={color}
                selected={isActive && current === index}
                onClick={() => handleClick(window.event, index)}
                ref={(el) => (itemNodes[index] = el)}
              >
                {submenu && (
                  <>
                    <MenuItem color={color}>
                      <MenuItemTitle>{value}</MenuItemTitle>
                      <MenuItemSubtitle>{year}</MenuItemSubtitle>
                      <SubmenuArrow color={color} />
                    </MenuItem>
                    <SubmenuContainer className={isActive && current === index ? 'active' : null}>
                      <Submenu>
                        <SubmenuParts>
                          {submenu.map(({ id, value, url }) => (
                            <SubMenuItemContainer key={id}>
                              <Link href={url} passHref>
                                <SubMenuItem color={color}>{value}</SubMenuItem>
                              </Link>
                            </SubMenuItemContainer>
                          ))}
                        </SubmenuParts>
                        <Image src={image.uri} />
                        <SubmenuEditors>
                          {collaborators?.editores?.collaborators?.length && <MenuHeading>Editores</MenuHeading>}
                          {collaborators?.editores?.collaborators?.map(({ id, fullname }) => (
                            <MenuText key={id}>{fullname}</MenuText>
                          ))}
                          {collaborators?.['coeditores']?.collaborators?.length && (
                            <MenuHeading>Coeditores</MenuHeading>
                          )}
                          {collaborators?.['coeditores']?.collaborators?.map(({ id, fullname }) => (
                            <MenuText key={id}>{fullname}</MenuText>
                          ))}
                        </SubmenuEditors>
                      </Submenu>
                    </SubmenuContainer>
                  </>
                )}
              </MenuItemContainer>
            ))}
        </MenuList>
      </Menu>
    </Container>
  )
}

export default ClientBooksDesktop
