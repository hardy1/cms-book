import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientBooks from './'
import { mockCommon } from '../../resources/mocks/common'

export default {
  title: 'Client/ClientBooks',
  component: ClientBooks,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

const books = mockCommon.books

export const Default: React.FC = () => <ClientBooks items={books} />
