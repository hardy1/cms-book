import styled, { css } from 'styled-components'
import { IconArrowDown } from 'designsystem'

import colors from '../../resources/colors'
import media from '../../resources/media'

export const MenuHeading = styled.h2`
  font-family: 'Lato', sans-serif;
  font-size: 0.875rem;
`

export const MenuText = styled.p`
  font-family: 'Lato', sans-serif;
  font-size: 0.75rem;
  line-height: 1.2rem;
  margin: 0;
  padding: 0;
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;

  @media ${media.tabletL} {
    display: none;
  }
`

export const Menu = styled.nav`
  display: flex;
  position: relative;
  background-color: ${colors.lightColor};
  justify-content: center;
  width: 100%;
  height: 5rem;
  align-items: center;
  overflow-x: clip;
`

export const MenuList = styled.ul`
  display: grid;
  grid-auto-flow: column;
  grid-template-columns: repeat(8, calc(100% / 8));
  place-items: center;
  position: relative;
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;

  @media ${media.laptop} {
    transition: margin 200ms ease-in-out;
  }
`

export const MenuItemContainer = styled.li<{ color: string; selected?: boolean }>`
  --select-full-height: 100%;
  --select-max-height: calc((100% / 15) * 2);
  --select-min-height: calc(100% / 15);
  --select-max-width: calc(100% / 8);
  cursor: pointer;
  position: static;
  text-align: left;
  height: 100%;
  width: 100%;
  align-items: center;
  display: flex;
  justify-content: center;

  ${(props) =>
    props.selected
      ? css`
          & h3,
          h4 {
            color: ${colors.white};
            z-index: 1;
          }

          & > a > svg {
            z-index: 1;
            transform: rotate(180deg);

            & > path {
              stroke: ${colors.white};
            }
          }

          &::before {
            height: var(--select-full-height);
          }
        `
      : css`
          &:hover {
            & h3,
            h4 {
              z-index: 1;
            }

            & > a > svg {
              z-index: 1;
            }

            &::before {
              height: var(--select-max-height);
            }
          }
        `}

  &::before {
    content: '';
    height: ${(props) => (props.selected ? 'var(--select-full-height)' : 'var(--select-min-height)')};
    width: var(--select-max-width);
    max-width: var(--select-max-width);
    background-color: ${(props) => props.color};
    position: absolute;
    overflow: hidden;
    bottom: 0;
    transition: height 200ms ease-out;
  }
`

export const MenuItem = styled.a`
  display: grid;
  place-items: center;
  color: ${(props) => props.color || colors.neutralColor};
  font-family: 'Lato', sans-serif;
  line-height: 120%;
  text-decoration: none;
  min-width: min-content;
  height: min-content;
  grid-template-columns: 90% 1fr;
  grid-template-rows: repeat(2, auto);
  grid-auto-flow: column;
  grid-template-areas:
    'title arrow'
    'subtitle arrow';

  @media ${media.laptopM} {
    min-width: min-content;
    height: min-content;
  }
`

export const MenuItemTitle = styled.h3`
  font-size: 0.8em;
  font-weight: 700;
  text-align: center;
  margin: 0;
  grid-area: title;

  @media ${media.laptopM} {
    font-size: 1vw;
    transition: font-size 200ms ease-in-out;
  }
`

export const MenuItemSubtitle = styled.h4`
  font-size: 0.9em;
  font-weight: 700;
  white-space: normal;
  margin: 0;
  grid-area: subtitle;

  @media ${media.laptopM} {
    font-size: 1.1vw;
    transition: font-size 200ms ease-in-out;
  }
`

export const SubmenuArrow = styled(IconArrowDown)`
  color: ${(props) => props.color || colors.neutralColor};
  margin-left: 0.2rem;
  grid-area: arrow;

  path {
    stroke: ${(props) => props.color || colors.neutralColor};
  }
`

export const SubmenuContainer = styled.div`
  display: grid;
  height: 0;
  overflow: clip;

  &.active {
    ul {
      height: auto;
      opacity: 1;
      padding: 1.5rem;
      z-index: 9999;
      transition: opacity 200ms ease-in-out;

      li {
        opacity: 1;
        transition: opacity 200ms ease-in-out;
      }

      img {
        opacity: 1;
        transition: opacity 400ms ease-in-out;
      }
    }
  }
`

export const Submenu = styled.ul`
  box-sizing: border-box;
  align-items: flex-start;
  background-color: ${colors.white};
  border-color: ${colors.lightColor};
  border-style: solid;
  border-width: 1px;
  box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.25);
  display: grid;
  opacity: 0;
  flex-direction: column;
  flex-wrap: wrap;
  list-style: none;
  margin: 0;
  padding: 0;
  left: 0;
  top: 5rem;
  z-index: -1;
  position: absolute;
  width: 100vw;
  height: 0;
  grid-template-columns: repeat(2, 1fr) 20vw 15vw;
  grid-template-rows: 1fr;
  grid-template-areas: 'parts parts image editors';
  row-gap: 0.2rem;
  column-gap: 1rem;
  align-items: stretch;
  grid-auto-flow: column;
  grid-auto-rows: min-content max-content;
`

export const SubmenuParts = styled.div`
  grid-area: parts;
  display: flex;
  flex-direction: column;
  max-height: 400px;
  flex-wrap: wrap;
`

export const SubmenuEditors = styled.div`
  grid-area: editors;
  cursor: default;
`

export const SubMenuItemContainer = styled.li`
  align-self: stretch;
  opacity: 0;
  margin: 0;
  padding: 0 0 0.4rem 0;
  line-height: 150%;
`

export const SubMenuItem = styled.a`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.875rem;
  text-decoration: none;

  &:hover {
    color: ${(props) => props.color || colors.primaryColor};
    font-weight: bold;
  }
`

export const Image = styled.img`
  grid-area: image;
  max-width: 16rem;
  grid-column: 3 / -1;
  grid-row: 1 / -1;
  place-self: center start;
  opacity: 0;

  @media ${media.mobileL} {
    margin: 25px 0 0;
    padding: 0;
  }
`
