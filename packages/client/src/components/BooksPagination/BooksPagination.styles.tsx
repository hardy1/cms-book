import styled from 'styled-components'
import colors from '../../resources/colors'
import { IconArrowLeft, IconArrowRight } from 'designsystem'

export const BooksPaginationContainer = styled.div`
  align-items: center;
  display: flex;
  font-family: 'Montserrat';
  justify-content: space-between;
`

export const Title = styled.span`
  color: #8c8c8c;
  font-family: 'Montserrat';
  font-size: 1.125em;
  font-weight: bold;
  text-transform: uppercase;
`

export const Value = styled.span`
  color: ${colors.neutralColor};
  font-size: 1.625em;
`

export const PreviousContainer = styled.a`
  align-items: flex-end;
  background-color: #f3f3f3;
  border-color: #a5a5a5;
  border-width: 0 1px 0 0;
  border-style: solid;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding: 36px;
  position: relative;
  text-decoration: none;
  width: 50%;
`

export const NextContainer = styled.a`
  align-items: flex-start;
  background-color: #f3f3f3;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin-left: auto;
  padding: 36px;
  position: relative;
  text-decoration: none;
  width: 50%;
`

export const ArrowLeft = styled(IconArrowLeft)`
  bottom: 0;
  left: 36px;
  margin: auto;
  position: absolute;
  top: 0;

  & path {
    stroke: #ef4350;
  }
`

export const ArrowRight = styled(IconArrowRight)`
  bottom: 0;
  margin: auto;
  right: 36px;
  position: absolute;
  top: 0;

  & path {
    stroke: #ef4350;
  }
`
