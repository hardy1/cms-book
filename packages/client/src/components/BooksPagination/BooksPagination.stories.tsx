import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import BooksPagination, { BooksPaginationProps } from './BooksPagination'

export default {
  title: 'Books/BooksPagination',
  component: BooksPagination,
} as Meta

const Template: Story<BooksPaginationProps> = (args) => <BooksPagination {...args} />

export const Default = Template.bind({})
Default.args = {
  items: {
    previous: {
      value: 'Lorem Ipsum Sit Dolor Amet',
      url: '#',
    },
    next: {
      value: 'Lorem Ipsum Sit Dolor Amet',
      url: '#',
    },
  },
}
