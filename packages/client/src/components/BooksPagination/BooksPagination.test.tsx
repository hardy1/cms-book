import * as React from 'react'
import { render } from '@testing-library/react'
import BooksPagination from './BooksPagination'

describe('BooksPagination', () => {
  it('SHOULD render correctly', () => {
    const component = render(<BooksPagination />)

    expect(component).toBeTruthy()
  })
})
