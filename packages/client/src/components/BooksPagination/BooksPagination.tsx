import * as React from 'react'
import Link from 'next/link'
import {
  BooksPaginationContainer,
  PreviousContainer,
  NextContainer,
  Title,
  Value,
  ArrowLeft,
  ArrowRight,
} from './BooksPagination.styles'

type PaginationItemProps = {
  url?: string
  value?: string
}

export type BooksPaginationProps = {
  items?: {
    previous?: PaginationItemProps
    next?: PaginationItemProps
  }
}

const BooksPagination: React.FC<BooksPaginationProps> = ({ items }: BooksPaginationProps) => (
  <BooksPaginationContainer>
    {items && items.previous && (
      <Link href={items.previous.url} passHref>
        <PreviousContainer>
          <ArrowLeft />
          <Title>Anterior</Title>
          <Value>{items.previous.value}</Value>
        </PreviousContainer>
      </Link>
    )}
    {items && items.next && (
      <Link href={items.next.url} passHref>
        <NextContainer>
          <Title>Próximo</Title>
          <Value>{items.next.value}</Value>
          <ArrowRight />
        </NextContainer>
      </Link>
    )}
  </BooksPaginationContainer>
)

export default BooksPagination
