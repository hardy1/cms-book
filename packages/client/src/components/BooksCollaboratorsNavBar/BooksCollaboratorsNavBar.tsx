import React from 'react'
import { useRouter } from 'next/router'
import { Button } from 'designsystem'
import { VerticalMenu } from './BooksCollaboratorsNavBar.styles'
import colors from '../../resources/colors'
import { AuthorEditorType, AuthorsBooksType } from '../../types'
import { IconArrowRightAlternative } from '../../containers/Icons'

interface IProp extends AuthorEditorType {
  onClick: (value: number) => void
  currentBook: number
}

const BooksCollaboratorsNavbar: React.FC<IProp> = ({ data, onClick: handleCurrentBook, currentBook }) => {
  const router = useRouter()
  const books: AuthorsBooksType[] = data.sort((a, b) => a.id - b.id)

  const themeColor = books[currentBook].theme_color

  return (
    <>
      <VerticalMenu theme={themeColor}>
        {books.map((book, i) => (
          <a className={i === currentBook ? 'active' : ''} key={i} onClick={() => handleCurrentBook(i)}>
            <span>{book.name}</span>
            <IconArrowRightAlternative />
          </a>
        ))}
      </VerticalMenu>
      <Button className="customButton" color={colors.white} onClick={() => router.push('/cadastro')}>
        Quero me cadastrar
      </Button>
    </>
  )
}

export default BooksCollaboratorsNavbar
