import styled from 'styled-components'
import colors from '../../resources/colors'
import theme from '../../lib/theme'

export const VerticalMenu = styled.nav`
  width: 100%;

  a {
    color: ${colors.white};
    padding: 10.25px 14px 10.25px 23px;
    text-decoration: none;
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
      width: 96%;
    }

    svg {
      width: 4%;
    }
  }

  a:hover {
    background-color: ${({ theme: themeColor }) => theme.bookColors[themeColor].hex};
  }

  a.active {
    background-color: ${({ theme: themeColor }) => theme.bookColors[themeColor].hex};
  }
`
