import React from 'react'
import { useDropzone } from 'react-dropzone'
import styled from 'styled-components'
import colors from '../../resources/colors'

const getColor = (props) => {
  if (props.isDragAccept) {
    return colors.lightColor
  }
  if (props.isDragReject) {
    return colors.errorColor
  }
  if (props.isDragActive) {
    return colors.tertiaryColor
  }
  return colors.primaryColor
}

const DropzoneStyledContainer = styled.div`
  grid-column: 1 / -1;
  font-family: Lato, sans-serif;

  p.title,
  p.description {
    color: #9ea0a5 !important;
    min-height: 0 !important;
    margin-bottom: 10px !important;
    text-align: left;
    text-transform: uppercase;
  }

  p.description {
    font-size: 12px !important;
  }

  p.dropzone__text {
    cursor: pointer;
    font-family: Poppins, sans-serif;
    font-weight: bold;
  }

  .dropzone {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 50px;
    max-width: 570px;
    padding: 20px;
    border-width: 1px;
    border-radius: 10px;
    border-color: ${(props) => getColor(props)};
    border-style: dashed;
    background-color: #e6f3ff;
    color: ${(props) => getColor(props)};
    outline: none;
    transition: border 0.24s ease-in-out;

    &__text {
      margin: 0;
      min-height: 20px;
      color: ${(props) => getColor(props)};
      text-align: center;
    }

    &__filesContainer {
      display: flex;
      justify-content: flex-start;
    }

    &__file {
      font-weight: bold;
      line-height: 2rem;
      min-height: auto;
    }
  }
`

interface DropzoneWithFormikProps {
  id: string
  formikInput: any
  documentDescription: string
}

const Dropzone: React.FC<DropzoneWithFormikProps> = ({ id, formikInput, documentDescription }) => {
  const { acceptedFiles, getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
    accept: ['.pdf', '.doc', '.docx'],
    maxFiles: 1,
    onDrop: (acceptedUploads) => {
      formikInput.setFieldValue(id, acceptedUploads)
    },
  })

  const files = acceptedFiles.map((file) => {
    return (
      <p className="dropzone__file" key={file.name}>
        {file.name}
      </p>
    )
  })

  return (
    <DropzoneStyledContainer {...getRootProps({ isDragActive, isDragAccept, isDragReject })}>
      <p className="title">DOCUMENTO</p>
      <div className="dropzone">
        <input {...getInputProps()} name={id} id={id} />
        <p className="dropzone__text">Clique aqui ou arraste o arquivo nessa área</p>
      </div>
      {files && <div className="dropzone__filesContainer ">{files}</div>}
      <p className="description">{documentDescription}</p>
    </DropzoneStyledContainer>
  )
}

export default Dropzone
