import React from 'react'
import Head from 'next/head'

interface HeadWrapperProps {
  children?: React.ReactNode
  title?: string
}

const HeadWrapper: React.FC<HeadWrapperProps> = ({ children, title }: HeadWrapperProps) => (
  <Head>
    {title && <title>{title}</title>}
    {children}
  </Head>
)

export default HeadWrapper
