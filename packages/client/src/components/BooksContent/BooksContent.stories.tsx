import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import BooksContent, { BooksContentProps } from './BooksContent'

export default {
  title: 'Books/BooksContent',
  component: BooksContent,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC<BooksContentProps> = () => <BooksContent>Default</BooksContent>
