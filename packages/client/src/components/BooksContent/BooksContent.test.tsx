import * as React from 'react'
import { render } from '@testing-library/react'
import BooksContent from './BooksContent'

describe('BooksContent', () => {
  it('SHOULD render correctly', () => {
    const component = render(<BooksContent />)

    expect(component).toBeTruthy()
  })
})
