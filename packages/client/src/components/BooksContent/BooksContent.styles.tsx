import styled from 'styled-components'
import colors from '../../resources/colors'
import media from '../../resources/media'

interface BooksGridContainerProps {
  toolbarMaxWidth?: string
  toolbarMinWidth?: string
  fullScreen?: boolean
  nightMode?: boolean
}

export const BooksContentContainer = styled.div<BooksGridContainerProps>`
  position: relative;
  display: grid;
  justify-content: center;
  grid-template-columns: minmax(46px, auto) 1fr;
  grid-template-rows: auto 1fr;
  grid-template-areas:
    'toolbar title'
    'toolbar content';
  place-content: stretch;
  overflow: ${({ fullScreen }) => (fullScreen ? 'scroll' : 'visible')};
  background-color: ${({ nightMode }) => (nightMode ? colors.black : colors.white)};

  article {
    ${({ nightMode }) => (nightMode ? `color: ${colors.white}` : '')};
  }

  @media ${media.tabletL} {
    grid-template-columns: 1fr;
    grid-template-rows: auto 1fr;
    grid-template-areas:
      'title'
      'content';
  }
`
