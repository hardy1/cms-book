import * as React from 'react'
import BookContext from '../../contexts/book'
import { handleFullScreen } from '../../lib/tools'

import { BooksContentContainer } from './BooksContent.styles'

export interface BooksContentProps {
  children?: React.ReactNode
}

const BooksContent: React.FC<BooksContentProps> = ({ children }: BooksContentProps) => {
  const { state } = React.useContext(BookContext)
  const modifiableComponent = React.useRef(null)

  React.useEffect(() => {
    handleFullScreen(state.fullScreen, modifiableComponent)
  })

  return (
    <BooksContentContainer ref={modifiableComponent} fullScreen={state?.fullScreen} nightMode={state?.nightMode}>
      {children}
    </BooksContentContainer>
  )
}

export default BooksContent
