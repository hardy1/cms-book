import React from 'react'
import { Heading2, Heading6 } from 'designsystem'
import { WrapperTitle, WrapperBookSynopsis, BookSynopsis } from './BooksCollaboratorsSynopsis.styles'

interface IProps {
  bookTitle: string
  bookThumb: string
  bookSynopsis: string
}

const BooksCollaboratorsSynopsis: React.FC<IProps> = ({ bookTitle, bookThumb, bookSynopsis }) => {
  return (
    <>
      <WrapperTitle>
        <Heading2>{bookTitle}</Heading2>
      </WrapperTitle>
      <WrapperBookSynopsis>
        <img src={bookThumb} />
        <BookSynopsis>
          <Heading6>Sinopse</Heading6>
          <p>{bookSynopsis}</p>
        </BookSynopsis>
      </WrapperBookSynopsis>
    </>
  )
}

export default BooksCollaboratorsSynopsis
