import styled from 'styled-components'

export const WrapperTitle = styled.div`
  width: 100%;
  margin: 28px 0;
  text-align: center;
`
export const WrapperBookSynopsis = styled.div`
  display: flex;
  margin-bottom: 12px;

  img {
    max-width: 168px;
    width: 100%;
    height: 100%;
  }
`
export const BookSynopsis = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 22px;

  p {
    line-height: 23.4px;
  }
`
export const RowBookCollaboratos = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  h6 {
    margin: 48px 0 24px;
  }
`
export const WrapperColumnCollaborator = styled.div`
  display: flex;
  flex-direction: row;
`
export const ColumnBookCollaboratos = styled.div`
  display: flex;
  flex-direction: column;
  width: 33%;

  &:nth-child(2) {
    padding: 0 24px;
  }

  h6 {
    margin-bottom: 25px;
  }
`
export const Collaborator = styled.p`
  margin: 0;
  font-size: 13px;
  line-height: 23.4px;
`
