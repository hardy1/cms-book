import styled from 'styled-components'
import colors from '../../resources/colors'

const InfoBlockStyle = styled.div<{ title?: string; header?: string; columnFlow?: boolean }>`
  &.Infoblock {
    &__SubscriptionData,
    &__PersonalData,
    &__ProfessionalData,
    &__Privacy,
    &__SubscriptionDataDetails {
      .InfoBlockEntry {
        grid-column: 1 / -1;
      }
    }

    &__SubscriptionData {
      .InfoBlockEntry {
        @media screen and (min-width: 768px) {
          &:first-of-type,
          &:nth-child(3) {
            grid-column: 1 / 6;
          }

          &:nth-child(2),
          &:last-of-type {
            grid-column: 6 / -1;
          }
        }
      }
    }

    &__PersonalData {
      .InfoBlockEntry {
        @media screen and (min-width: 768px) {
          &:first-of-type {
            grid-column: 1 / 10;
          }

          &:nth-child(2),
          &:nth-child(5),
          &:nth-child(9),
          &:last-of-type {
            grid-column: 10 / -1;
          }

          &:nth-child(3),
          &:nth-child(10) {
            grid-column: 1 / 7;
          }

          &:nth-child(4),
          &:nth-child(8),
          &:nth-child(11) {
            grid-column: 7 / 10;
          }

          &:nth-child(6) {
            grid-column: 1 / 4;
          }

          &:nth-child(7) {
            grid-column: 4 / 7;
          }
        }
      }
    }

    &__ProfessionalData {
      .Infoblock__Content {
        &:first-of-type {
          .InfoBlockEntry {
            @media screen and (min-width: 768px) {
              &:first-of-type,
              &:nth-child(3) {
                grid-column: 1 / 6;
              }

              &:nth-child(2),
              &:nth-child(4) {
                grid-column: 6 / -1;
              }
            }
          }
        }

        &:nth-child(2) {
          .InfoBlockEntry {
            @media screen and (min-width: 768px) {
              &:first-of-type {
                grid-column: 1 / 9;
              }

              &:last-of-type {
                grid-column: 9 / -1;
              }
            }
          }
        }
      }
    }

    &__SubscriptionDataDetails {
      .InfoBlockEntry {
        @media screen and (min-width: 768px) {
          &:nth-child(odd) {
            grid-column: 1 / 6;
          }

          &:nth-child(even) {
            grid-column: 6 / -1;
          }
        }
      }
    }
  }

  .Infoblock {
    font-weight: bold;
    text-align: left;
    font-family: 'Montserrat', sans-serif;
    font-style: normal;

    &__TermsAndConitions {
      color: #3e3f42;
      font-family: 'Lato', sans-serif;
      font-size: 16px;
      font-weight: 400;
      line-height: 19px;

      a {
        color: #216de1;
        text-decoration: none;
      }
    }

    &__SubscriptionData {
      outline: 1px solid red;
    }

    &__Title {
      margin-top: 3.3rem;
      margin-bottom: 1.5rem;
    }

    &__Header {
      background-color: ${colors.primaryColor};
      color: ${colors.white};
      font-size: 24px;
      line-height: 130%;
      padding: 1.8rem 1.5rem;

      h4 {
        margin: 0;
      }
    }

    &__Body {
      background-color: ${colors.lighterGray};
      padding: 2.25rem;
      display: flex;
      flex-direction: column;
    }

    &__Content {
      display: grid;
      grid-template-columns: repeat(12, 1fr);
      column-gap: 1rem;
      row-gap: 48px;

      + .Infoblock__Content {
        padding-top: 48px;
      }
    }

    &__Key {
      margin: 0;
      grid-area: key;
      color: ${colors.disabledColor};
      text-transform: uppercase;
      align-self: flex-start;
    }

    &__Value {
      margin: 0;
      grid-area: value;
      color: ${colors.neutralColor};
      font-weight: normal;
      margin: ${(props) => (props.columnFlow ? '0 0 0 3rem' : '0')};
      line-height: 2rem;
      align-self: center;
    }

    &__Footer {
      padding-top: 48px;

      button {
        width: 190px;
        padding: 15px;
      }
    }
  }
`

const InfoBlockEntry = styled.div`
  .InfoBlockEntry {
    &__Key,
    &__Value {
      font-family: 'Lato', sans-serif;
      font-size: 16px;
      font-weight: 400;
      line-height: 19px;
      margin-top: 0;
      margin-bottom: 0;
    }

    &__Key {
      color: #9ea0a5;
      text-transform: uppercase;
      letter-spacing: 0.3px;
      padding-bottom: 16px;
    }

    &__Value {
      color: #3e3f42;
    }

    &__Values {
      .InfoBlockEntry {
        &__Value {
          &:not(:last-of-type) {
            margin-bottom: 16px;
          }
        }
      }
    }
  }
`

export { InfoBlockStyle, InfoBlockEntry }
