import React from 'react'
import { InfoBlockStyle, InfoBlockEntry } from './InfoBlock.style'
import { Button, Heading3 } from 'designsystem'
import { IBlockListValueSet, IBlockValueSet } from '../../types'

const InfoBlock: React.FC<{
  className?: string
  title?: string
  header?: string
  content?: IBlockValueSet[]
  contentList?: IBlockListValueSet[]
  onClickDetails?: () => void
  columnFlow?: boolean
  buttonText?: string
}> = (props) => {
  return (
    <>
      <InfoBlockStyle
        className={props.className}
        title={props.title}
        header={props.header}
        columnFlow={props.columnFlow}
      >
        {props.title && (
          <div className="Infoblock__Title">
            <Heading3>{props.title}</Heading3>
          </div>
        )}

        {props.header && (
          <div className="Infoblock Infoblock__Header">
            <h4>{props.header}</h4>
          </div>
        )}

        {props.content?.length && (
          <div className="Infoblock Infoblock__Body">
            <div className="Infoblock Infoblock__Content">
              {props.content?.map((item, index: number) => (
                <InfoBlockEntry key={index} className="InfoBlockEntry">
                  <p className="InfoBlockEntry__Key">{item.entryName}</p>
                  <p className="InfoBlockEntry__Value">{item.content || '-------'}</p>
                </InfoBlockEntry>
              ))}
            </div>

            {props.contentList && (
              <div className="Infoblock Infoblock__Content">
                {props?.contentList?.map((item, index: number) => (
                  <InfoBlockEntry key={index} className="InfoBlockEntry">
                    <p className="InfoBlockEntry__Key">{item.entryName}</p>
                    <div className="InfoBlockEntry__Values">
                      {item.content?.map((subitem, jIndex: number) => (
                        <p key={jIndex} className="InfoBlockEntry__Value">
                          {subitem.content}
                        </p>
                      ))}
                    </div>
                  </InfoBlockEntry>
                ))}
              </div>
            )}

            {props.onClickDetails && (
              <div className="Infoblock__Footer">
                <Button onClick={props.onClickDetails}>{props.buttonText ? props.buttonText : 'Ver detalhes'}</Button>
              </div>
            )}
          </div>
        )}
        {props.children}
      </InfoBlockStyle>
    </>
  )
}

export default InfoBlock
