import * as React from 'react'
import HeaderMobile from './HeaderMobile'
import HeaderDesktop from './HeaderDesktop'
import { MainContainer } from 'designsystem'
import { Session } from 'next-auth'

export type ClientImage = {
  uri: string
  type?: string
  title?: string
  description?: string
  thumb?: string
  thumb_square?: string
}

export type ClientHeaderChildren = {
  id?: number
  parent_id?: number
  title?: string
  description?: string
  url?: string
  is_external?: number
  link_target?: string
  color?: string
  children?: ClientHeaderChildren[]
  image?: ClientImage
}

export type ClientHeaderProps = {
  items: ClientHeaderChildren[]
  books?: ClientHeaderChildren[]
  hideDesktop?: boolean
  session: Session | undefined
}

const ClientHeader: React.FC<ClientHeaderProps> = ({ items, books, hideDesktop, session }: ClientHeaderProps) => {
  const desktopItems =
    items &&
    items.map((item) => {
      let submenu
      item.children.length ? (submenu = mapSubmenuItems(item.children)) : ''
      return { id: item.id, value: item.title, url: item.url || '', submenu }
    })

  const mobileBooksItems = mapSubmenuItems(books)

  const mobileItems = generateMobileItems(desktopItems)

  return (
    <MainContainer>
      {!hideDesktop && <HeaderDesktop items={desktopItems} />}
      <HeaderMobile items={mobileItems} books={mobileBooksItems} session={session} />
    </MainContainer>
  )
}

const mapSubmenuItems = (items: ClientHeaderChildren[]) =>
  items && items.map((item) => ({ id: item.id, value: item.title, url: item.url || '', color: item.color }))

const generateMobileItems = (desktopItems) => {
  let mobileItems = []

  desktopItems &&
    desktopItems.map((item) =>
      item.submenu ? (mobileItems = mobileItems.concat(item.submenu)) : mobileItems.push(item),
    )

  return mobileItems
}

export default ClientHeader
