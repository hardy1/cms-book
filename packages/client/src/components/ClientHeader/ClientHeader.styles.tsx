import styled from 'styled-components'
import { IconArrowDown, IconArrowRight } from 'designsystem'

import colors from '../../resources/colors'
import media from '../../resources/media'

export const HeaderContainer = styled.div`
  display: flex;
  color: ${colors.primaryColor};
  padding: 30px 0;
  align-items: center;
  justify-content: center;
  width: 100%;

  @media ${media.tabletL} {
    display: none;
  }
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 1280px;
  width: 100%;
`

export const LogoContainer = styled.div`
  cursor: pointer;
`

export const Menu = styled.nav`
  display: flex;
  justify-content: center;
  max-width: 900px;
  width: 100%;
`

export const MenuList = styled.ul`
  display: flex;
  justify-content: space-between;
  list-style: none;
  margin: 0;
  width: 100%;
`

export const MenuItemContainer = styled.li`
  cursor: pointer;
  padding: 5px 0 5px 6px;
  position: relative;
  text-align: left;
  max-width: 18rem;
  min-width: 4rem;

  &:hover ul {
    display: flex;
  }
`

export const MenuItem = styled.a`
  align-items: center;
  display: flex;
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 1em;
  line-height: 170%;
  text-decoration: none;

  &:hover {
    color: ${colors.primaryColor};
  }

  &:hover svg path {
    stroke: ${colors.primaryColor};
  }
`

export const SubmenuArrow = styled(IconArrowDown)`
  margin-left: 10px;
`

export const MobileSubmenuArrow = styled(IconArrowRight)`
  color: ${(props) => props.color || colors.neutralColor};
  justify-content: space-between;

  path {
    stroke: ${(props) => props.color || colors.neutralColor};
  }
`

export const Submenu = styled.ul`
  align-items: flex-start;
  background-color: ${colors.white};
  border-color: ${colors.lightColor};
  border-style: solid;
  border-width: 1px;
  box-shadow: 0px 4px 5px rgba(0, 0, 0, 0.25);
  display: none;
  flex-direction: column;
  left: -8px;
  list-style: none;
  margin: 0;
  padding: 0;
  position: absolute;
  top: 35px;
  width: auto;
  z-index: 10;
`

export const SubMenuItemContainer = styled.li`
  align-self: stretch;
  margin: 0;
  padding: 10px 20px 10px 16px;

  &:not(:first-child) {
    border-color: #e3e3e3;
    border-style: solid;
    border-width: 1px 0 0;
  }
`

export const SubMenuItem = styled.a`
  color: #888;
  font-family: 'Lato', sans-serif;
  font-size: 1rem;
  text-decoration: none;
  white-space: nowrap;

  &:hover {
    color: ${colors.primaryColor};
  }
`

export const InputContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 70px;
  height: 35px;
  padding-left: 30px;
  box-sizing: border-box;

  button {
    position: relative;
    display: inline-block;
    width: 100%;
    background-color: transparent;
    border-width: 0;
    padding: 0;
    cursor: pointer;

    &:hover {
      opacity: 0.8;
    }

    span {
      position: absolute;
      width: 0;
      height: 0;
      overflow: hidden;
    }
  }
`

export const FullSearchContainer = styled.div`
  align-items: center;
  background-color: rgb(255 255 255 / 85%);
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: flex-start;
  left: 0;
  padding: 200px 16px 26px;
  position: fixed;
  top: 0;
  width: 100vw;
  z-index: 10000;

  & > *:not(:first-child) {
    margin-top: 47px;
  }
`

export const FullSearchContent = styled.div`
  max-width: 846px;

  & form > *:not(:first-child) {
    margin-top: 32px;
  }
`

export const MobileHeaderContainer = styled.div`
  align-items: center;
  background-color: ${colors.lightColor};
  bottom: 0;
  display: none;
  flex: 1;
  height: 60px;
  justify-content: space-between;
  left: 0;
  padding: 0 16px;
  position: fixed;
  width: calc(100% - 32px);
  z-index: 10001;

  @media ${media.tabletL} {
    display: flex;
  }
`
export const MobileLink = styled.span`
  cursor: pointer;
  padding: 16px;

  & svg path {
    fill: ${colors.primaryColor};
  }
`

const FullContainer = styled.div`
  background-color: ${colors.white};
  display: none;
  height: 100vh;
  left: 0;
  padding: 26px 16px;
  position: fixed;
  top: 0;
  width: calc(100vw - 32px);
  z-index: 10000;
`

export const MobileMenuContainer = styled(FullContainer)`
  flex-direction: column;

  @media ${media.tabletL} {
    display: flex;
  }
`

export const MobileSearchContainer = styled(FullContainer)`
  align-items: flex-start;
  flex-direction: column;

  & a {
    border: 0;
    margin-top: 6px;
    padding-top: 12px;
  }

  @media ${media.tabletL} {
    display: flex;
  }
`

export const MobileMenu = styled.nav`
  width: 100%;
`

export const MobileMenuList = styled.ul`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
`

export const MobileMenuItemContainer = styled.li`
  cursor: pointer;
  padding: 12px 0 6px;
  text-align: left;

  &:not(:first-child) {
    border-top: 1px solid #eeeeee;
  }
`

export const MobileMenuItem = styled.a`
  color: ${(props) => props.color || colors.neutralColor};
  display: flex;
  justify-content: space-between;
  font-family: 'Lato', sans-serif;
  font-size: 1em;
  font-weight: ${(props) => (props.color ? 'bold' : 'normal')};
  line-height: 170%;
  text-decoration: none;
`

export const BackToHome = styled.a`
  border-top: 1px solid #eeeeee;
  color: ${colors.neutralColor};
  font-size: 1em;
  padding-top: 12px;
  text-decoration: none;
`
