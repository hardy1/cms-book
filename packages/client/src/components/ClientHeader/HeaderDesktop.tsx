import * as React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { IconSearch, LogoMoc, Input, Heading1 } from 'designsystem'

import {
  HeaderContainer,
  Container,
  LogoContainer,
  InputContainer,
  Menu,
  MenuList,
  MenuItemContainer,
  MenuItem,
  Submenu,
  SubMenuItemContainer,
  SubMenuItem,
  SubmenuArrow,
  FullSearchContainer,
  FullSearchContent,
} from './ClientHeader.styles'

type SubmenuItemProps = {
  id: number
  value: string
  url: string
}

type MenuItemProps = {
  id: number
  value: string
  url: string
  submenu?: SubmenuItemProps[] | undefined
}

export type HeaderProps = {
  items: MenuItemProps[] | undefined
}

const HeaderDesktop: React.FC<HeaderProps> = ({ items }: HeaderProps) => {
  const [searchModal, setSearchModal] = React.useState(false)
  const [searchTerm, setSearchTerm] = React.useState('')
  const router = useRouter()
  const inputRef = React.useRef<HTMLInputElement | null>(null)

  const handleSearch = (query: string) => {
    setSearchModal(!searchModal)
    router.push(`/pesquisa?q=${query}`)
  }

  React.useEffect(() => {
    if (searchModal) {
      inputRef.current?.focus()
    }
  }, [searchModal, inputRef])

  const modalSearch = searchModal && (
    <FullSearchContainer onClick={() => setSearchModal(!searchModal)}>
      <FullSearchContent onClick={(e) => e.stopPropagation()}>
        <form onSubmit={() => handleSearch(searchTerm)}>
          <Heading1>O que você deseja pesquisar no MOC?</Heading1>
          <Input
            onChange={(e) => setSearchTerm(e.target.value)}
            id="searchId"
            type="search"
            placeholder="Digite aqui sua pesquisa"
            ref={inputRef}
          />
        </form>
      </FullSearchContent>
    </FullSearchContainer>
  )

  return (
    <HeaderContainer>
      <Container>
        <LogoContainer>
          <Link href="/">
            <a>
              <LogoMoc />
            </a>
          </Link>
        </LogoContainer>
        <Menu>
          <MenuList>
            {items &&
              items.map(({ id, value, url, submenu }) => (
                <MenuItemContainer key={id}>
                  {!submenu && (
                    <Link href={url} passHref>
                      <MenuItem>{value}</MenuItem>
                    </Link>
                  )}
                  {submenu && (
                    <>
                      <MenuItem>
                        {value}
                        <SubmenuArrow />
                      </MenuItem>
                      <Submenu>
                        {submenu.map(({ id, value, url }) => (
                          <SubMenuItemContainer key={id}>
                            <Link href={url} passHref>
                              <SubMenuItem>{value}</SubMenuItem>
                            </Link>
                          </SubMenuItemContainer>
                        ))}
                      </Submenu>
                    </>
                  )}
                </MenuItemContainer>
              ))}
          </MenuList>
        </Menu>

        <InputContainer onClick={() => setSearchModal(!searchModal)}>
          <button>
            <span>Pesquisar</span>
            <IconSearch fill="#3e3f42" />
          </button>
        </InputContainer>

        {modalSearch}
      </Container>
    </HeaderContainer>
  )
}

export default HeaderDesktop
