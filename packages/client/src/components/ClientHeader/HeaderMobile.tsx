import * as React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { IconMenu, IconBook, IconSearch, IconPerson, Input } from 'designsystem'
import {
  MobileHeaderContainer,
  MobileLink,
  MobileMenuContainer,
  MobileMenu,
  MobileMenuList,
  MobileMenuItemContainer,
  MobileMenuItem,
  MobileSearchContainer,
  BackToHome,
  MobileSubmenuArrow,
} from './ClientHeader.styles'
import { signOut } from 'next-auth/react'
import { Session } from 'next-auth'

type MenuItemProps = {
  id: number
  value: string
  url: string
  color?: string
}

export type HeaderProps = {
  items?: MenuItemProps[] | undefined
  books?: MenuItemProps[] | undefined
  session: Session | undefined
}

const HeaderMobile: React.FC<HeaderProps> = ({ items, books, session }: HeaderProps) => {
  const [activeItem, setActiveItem] = React.useState('')
  const [searchTerm, setSearchTerm] = React.useState('')

  const router = useRouter()
  const inputRef = React.useRef<HTMLInputElement | null>(null)

  React.useEffect(() => {
    if (activeItem === 'searchContainer') {
      inputRef.current?.focus()
    }
  }, [activeItem])

  const handleSearch = (query: string) => {
    setActiveItem('')
    router.push(`/pesquisa?q=${query}`)
  }

  const toggleItem = (itemValue: string) => setActiveItem(activeItem === itemValue ? '' : itemValue)

  const MobileSessionMenu = () => {
    if (session) {
      return (
        <>
          <MobileMenuItemContainer onClick={() => setActiveItem('')}>
            <Link href="/minha-conta" passHref>
              <MobileMenuItem>Minha conta</MobileMenuItem>
            </Link>
          </MobileMenuItemContainer>
          <MobileMenuItemContainer onClick={() => setActiveItem('')}>
            <a onClick={() => signOut()}>
              <MobileMenuItem>Sair</MobileMenuItem>
            </a>
          </MobileMenuItemContainer>
        </>
      )
    }

    return (
      <MobileMenuItemContainer onClick={() => setActiveItem('')}>
        <Link href="/cadastro" passHref>
          <MobileMenuItem>Cadastre-se</MobileMenuItem>
        </Link>
      </MobileMenuItemContainer>
    )
  }

  if (activeItem == 'minhaConta') {
    setActiveItem('')
    router.push('/minha-conta')
  }
  return (
    <>
      <MobileHeaderContainer>
        <MobileLink onClick={() => toggleItem('menuContainer')}>
          <IconMenu />
        </MobileLink>
        <MobileLink onClick={() => toggleItem('bookContainer')}>
          <IconBook />
        </MobileLink>
        <MobileLink onClick={() => toggleItem('searchContainer')}>
          <IconSearch />
        </MobileLink>
        <MobileLink>
          <IconPerson onClick={() => toggleItem('minhaConta')} />
        </MobileLink>
      </MobileHeaderContainer>
      {activeItem === 'menuContainer' && (
        <MobileMenuContainer>
          <MobileMenu>
            <MobileMenuList>
              {items &&
                items.map(({ id, value, url }) => (
                  <MobileMenuItemContainer key={id} onClick={() => setActiveItem('')}>
                    <Link href={url} passHref>
                      <MobileMenuItem>{value}</MobileMenuItem>
                    </Link>
                  </MobileMenuItemContainer>
                ))}
              <MobileSessionMenu />
            </MobileMenuList>
          </MobileMenu>
          <Link href="/" passHref>
            <BackToHome onClick={() => setActiveItem('')}>Voltar para home</BackToHome>
          </Link>
        </MobileMenuContainer>
      )}

      {activeItem === 'bookContainer' && (
        <MobileMenuContainer>
          <MobileMenu>
            <MobileMenuList>
              {books &&
                books.map(({ id, value, url, color }) => (
                  <MobileMenuItemContainer key={id} onClick={() => setActiveItem('')}>
                    <Link href={url} passHref>
                      <MobileMenuItem color={color}>
                        {value}
                        <MobileSubmenuArrow color={color} />
                      </MobileMenuItem>
                    </Link>
                  </MobileMenuItemContainer>
                ))}
            </MobileMenuList>
          </MobileMenu>
          <Link href="/" passHref>
            <BackToHome onClick={() => setActiveItem('')}>Voltar para home</BackToHome>
          </Link>
        </MobileMenuContainer>
      )}

      {activeItem === 'searchContainer' && (
        <MobileSearchContainer>
          <form onSubmit={() => handleSearch(searchTerm)}>
            <Input
              onChange={(e) => setSearchTerm(e.target.value)}
              id="searchMobileId"
              type="text"
              placeholder="Pesquisar no MOC"
              icon={<IconSearch />}
              ref={inputRef}
            />
          </form>
          <Link href="/" passHref>
            <BackToHome onClick={() => setActiveItem('')}>Voltar para home</BackToHome>
          </Link>
        </MobileSearchContainer>
      )}
    </>
  )
}

export default HeaderMobile
