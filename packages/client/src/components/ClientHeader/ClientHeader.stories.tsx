import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientHeader from './ClientHeader'
import { mockCommon } from '../../resources/mocks/common'

export default {
  title: 'Client/ClientHeader',
  component: ClientHeader,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

const header = mockCommon.header

export const Default: React.FC = () => <ClientHeader items={header} session={null} />
