import React, { useState } from 'react'
import Link from 'next/link'
import { Container } from './CookiesPolicyTermsAndConditions.styles'
import { Button } from 'designsystem'
import Cookies from 'js-cookie'

const CookiesPolicyTermsAndConditions = () => {
  const [hasAcceptedCookiesUsage, setHasAcceptedCookiesUsage] = useState<boolean>(false)

  const handleCookieUsage = () => {
    Cookies.set('allowCookieUsage', 'yes')
    setHasAcceptedCookiesUsage(true)
  }
  return (
    <Container hidden={hasAcceptedCookiesUsage}>
      <p>
        Nós usamos cookies e outras tecnologias semelhantes para melhorar a sua experiência em nossos serviços,
        personalizar publicidade e recomendar conteúdo de seu interesse. Ao utilizar nossos serviços, você concorda com
        tal monitoramento. Informamos ainda que atualizamos nossa{' '}
        <Link href="/page/termo-de-uso-politica-de-privacidade-e-assinatura" passHref>
          Política de Privacidade
        </Link>
        . Conheça nosso Portal da Privacidade e veja a nossa nova Política.
      </p>
      <Button size="small" onClick={() => handleCookieUsage()}>
        Prosseguir
      </Button>
    </Container>
  )
}

export default CookiesPolicyTermsAndConditions
