import styled from 'styled-components'
import colors from '../../resources/colors'

export const Container = styled.div`
  display: ${(props) => (props.hidden ? 'none' : 'flex')};
  align-items: center;
  justify-content: space-between;
  position: fixed;
  height: 70px;
  width: 100%;
  left: 50%;
  bottom: 16px;
  background-color: ${colors.white};
  box-shadow: 0 2px 4px 0 rgb(0 0 0 / 40%);
  border: solid 1px ${colors.lighterGray};
  transition: 0.3s;

  @media screen and (max-width: 1024px) {
    flex-direction: column;
    left: 0;
    bottom: 80px;
    margin: 0 16px;
    width: calc(100% - 32px);
    height: auto;

    p {
      margin: 1em !important;
    }
    button {
      margin: 0 1em 1em;
      width: calc(100% - 2em);
    }
  }

  @media screen and (min-width: 1025px) {
    max-width: 958px;
    margin-left: -480px;
  }

  @media screen and (min-width: 1366px) {
    max-width: 1140px;
    margin-left: -571px;
  }

  p {
    margin: 0 1em;
    font-size: 0.75rem;
    color: ${colors.black};
  }

  button {
    margin-right: 1em;
  }
`
