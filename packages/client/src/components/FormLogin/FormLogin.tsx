import React from 'react'
import Link from 'next/link'
import { Button, FormGroup, Input } from 'designsystem'

import { Form, Label } from '../../containers/Form'
import { GenericFormSection } from '../../types'

interface FormLoginProps extends GenericFormSection {
  className?: string
  formGroupClassName?: string
}

const FormLogin: React.FC<FormLoginProps> = ({ className, formGroupClassName, formik }: FormLoginProps) => {
  return (
    <Form onSubmit={formik.handleSubmit} className={className} columnGap="0">
      <FormGroup gridColumn="1 / -1" className={formGroupClassName}>
        <Label>E-mail</Label>
        <Input
          type="email"
          id="login"
          autoComplete="email"
          placeholder="Digite seu e-mail"
          error={formik.touched.login && Boolean(formik.errors.login)}
          message={formik.touched.login && formik.errors.login}
          {...formik.getFieldProps('login')}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1" className={formGroupClassName}>
        <Label>Senha</Label>
        <Input
          type="password"
          id="password"
          autoComplete="password"
          error={formik.touched.password && Boolean(formik.errors.password)}
          message={formik.touched.password && formik.errors.password}
          {...formik.getFieldProps('password')}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1" className={formGroupClassName}>
        <p className="forgot-password">
          <Link href="/recuperar-senha">Esqueci minha senha</Link>
        </p>
      </FormGroup>

      <FormGroup gridColumn="1 / -1" textAlign="center" className={formGroupClassName}>
        <Button type="submit">Entrar</Button>
      </FormGroup>
    </Form>
  )
}

export default FormLogin
