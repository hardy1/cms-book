import styled from 'styled-components'
import media, { size } from '../../resources/media'
import colors from '../../resources/colors'

export const StyledDots = styled.div`
  position: absolute;
  top: -22px;
  display: flex;
  width: 100%;

  @media screen and (min-width: ${size.tablet}) {
    top: -30px;
  }

  .SliderDots {
    &__List {
      display: inline-flex;
      justify-content: flex-end;
      width: 100%;
      padding-left: 0;
      padding-bottom: 12px;
      margin-top: 0;
      margin-bottom: 0;

      @media screen and (min-width: ${size.tablet}) {
        padding-bottom: 0;
        padding-right: 32px;
      }

      li {
        display: inline-flex;

        :not(:last-of-type) {
          margin-right: 10px;
        }

        &.slick-active {
          button {
            background-color: ${colors.secondaryColor};
          }
        }

        button {
          width: 10px;
          height: 10px;
          background-color: ${colors.disabledColor};
          border-radius: 50%;
          border-width: 0;
          font-size: 0;
          padding: 0;
        }
      }
    }
  }
`

export const StyledSlider = styled.div`
  position: relative;
  padding-top: 2rem;
  width: auto;
  box-sizing: border-box;

  @media screen and (min-width: ${size.mobile}) and ${media.tablet} {
    width: calc(100vw - 64px);
  }

  @media screen and (min-width: ${size.tablet}) and ${media.tabletL} {
    padding-left: 20px;
    width: calc(100vw - 70px);
  }

  @media screen and (min-width: ${size.mobileL}) and ${media.tabletL} {
    .slick-slider {
      width: ${size.mobileM};
      margin: auto;

      @media screen and (min-width: ${size.mobileL}) {
        width: 65vw;
      }
    }
  }

  .slick-arrow {
    display: none;
    width: auto;
    height: auto;
    background-color: transparent;
    border-radius: 0;
    border-width: 0;
    z-index: 1;

    @media screen and (min-width: ${size.tablet}) {
      display: block;
    }

    &.slick-prev {
      @media screen and (min-width: ${size.tablet}) {
        left: -19px;
      }
    }

    &.slick-next {
      @media screen and (min-width: ${size.tablet}) {
        right: 5px;
      }
    }

    ::before {
      display: none;
    }
  }
`
