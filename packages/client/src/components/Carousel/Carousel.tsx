import React, { FC } from 'react'
import { Button, IconArrowLeft, IconArrowRight } from 'designsystem'
import Slider, { Settings } from 'react-slick'
import parse from 'html-react-parser'
import Box from '../Box'
import { IHomeItem } from '../../types'
import { StyledDots, StyledSlider } from './Carousel.styled'
import SliderBlade from './SliderBlade'

const Carousel: FC<{ items: IHomeItem[] } & Settings> = (props) => {
  const settings: Settings = {
    ...props,
    className: 'Slider',
    dotsClass: 'SliderDots__List__Item',
    nextArrow: <Button icon={<IconArrowRight />} />,
    prevArrow: <Button icon={<IconArrowLeft />} />,
    appendDots: function appendDots(dots) {
      return (
        <StyledDots>
          <ul className="SliderDots__List">{dots}</ul>
        </StyledDots>
      )
    },
  }

  return (
    <Box margin="0">
      <StyledSlider>
        <Slider {...settings}>
          {props.items.map(({ id, imagem, cta_url, cta_label, texto }) => (
            <SliderBlade key={id} src={imagem} url={cta_url} title={parse(texto)} label={cta_label} />
          ))}
        </Slider>
      </StyledSlider>
    </Box>
  )
}

export default Carousel
