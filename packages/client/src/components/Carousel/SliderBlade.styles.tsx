import styled from 'styled-components'
import colors from '../../resources/colors'
import media from '../../resources/media'

export const SliderBladeContainer = styled.div`
  position: relative;
  width: 100%;
  align-items: center;
  display: flex;

  a {
    display: flex;
  }

  @media ${media.mobileL} {
    flex-direction: column-reverse;
  }
`

export const Image = styled.div`
  margin: 1.5rem 0 1.5rem 1.5rem;
  overflow: hidden;
  width: 100%;
  img {
    object-fit: cover;
    &::before {
      content: '';
    }
  }

  @media ${media.mobileL} {
    margin: 0;
    padding: 0;
  }
`

export const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const ActionButton = styled.a`
  position: absolute;
  right: 10px;
  bottom: 10px;
  background-color: ${colors.primaryColor};
  border-radius: 4px;
  color: ${colors.lightColor};
  font-family: Lato, sans-serif;
  font-size: 1rem;
  text-decoration: none;
  padding: 1rem;

  @media screen and (min-width: 520px) {
    right: 20px;
    bottom: 40px;
  }

  @media screen and (min-width: 1440px) {
    font-size: 1.125rem;
    padding: 1rem 2rem;
  }

  @media ${media.mobileL} {
    right: 1rem;
    bottom: 1.5rem;
  }
`

export const Title = styled.div`
  position: relative;
  bottom: 0;
  width: 100%;
  height: 190px;
  background-color: ${colors.quaternaryColor};
  padding: 16px 16px 80px;
  margin: 0;
  box-sizing: border-box;

  p {
    display: -webkit-box;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    color: ${colors.primaryColor};
    font-family: 'Montserrat', sans-serif;
    font-size: 1.125rem;
    font-weight: bold;
    overflow: hidden;
    margin: 0;
    text-align: start;

    @media screen and (min-width: 570px) {
      -webkit-line-clamp: 9;
    }
  }

  @media screen and (min-width: 570px) {
    position: absolute;
    bottom: 15%;
    left: 0;
    width: 310px;
    height: fit-content;
    padding: 40px;
  }
`
