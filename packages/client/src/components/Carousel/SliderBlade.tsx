import * as React from 'react'
import { Image, ActionButton, Title, TextWrapper, SliderBladeContainer } from './SliderBlade.styles'

type Props = {
  src?: string
  title?: string | JSX.Element[] | JSX.Element
  url?: string
  label?: string
}

const SliderBlade: React.FC<Props> = ({ src = '', title = '', url = '', label = '' }: Props) => (
  <SliderBladeContainer>
    <TextWrapper>
      {title && <Title>{title}</Title>}

      {url && <ActionButton href={url}>{label || 'Confira agora'}</ActionButton>}
    </TextWrapper>

    {src && (
      <Image>
        <img src={src} />
      </Image>
    )}
  </SliderBladeContainer>
)

export default SliderBlade
