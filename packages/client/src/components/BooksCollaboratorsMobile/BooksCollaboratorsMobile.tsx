import React, { useState, useRef } from 'react'
import { useRouter } from 'next/router'
import {
  WrapperContainer,
  WrapperBookContent,
  BookContent,
  CollaboratorContent,
} from './BooksCollaboratorsMobile.styles'
import { AuthorEditorType, AuthorsBooksType } from '../../types'
import colors from '../../resources/colors'
import { Button, Heading2, Heading6, IconArrowDown } from 'designsystem'

interface IProp extends AuthorEditorType {
  onClick: (value: number) => void
  currentBook: number
  editors: string[]
  authors: string[]
}

const BooksCollaboratorsMobile: React.FC<IProp> = ({
  data,
  onClick: handleCurrentBook,
  currentBook,
  editors,
  authors,
}) => {
  const router = useRouter()
  const [isCollapseActive, setIsCollapseActive] = useState<boolean>(false)
  const books: AuthorsBooksType[] = data.sort((a, b) => a.id - b.id)

  const themeColor = books[currentBook].theme_color

  const scrollTop = useRef(null)
  const executeScroll = () => scrollTop.current.scrollIntoView()

  const handleCurrentBookMobile = (index: number) => {
    handleCurrentBook(index)
    !isCollapseActive && index === currentBook ? setIsCollapseActive(true) : setIsCollapseActive(false)
    isCollapseActive && index === currentBook ? setIsCollapseActive(false) : setIsCollapseActive(true)
    executeScroll()
  }

  const handleActiveItem = (index: number) => {
    return isCollapseActive && index === currentBook ? 'active' : ''
  }

  return (
    <>
      <WrapperContainer theme={themeColor}>
        <ul ref={scrollTop}>
          {books.map((book, i) => (
            <li className={handleActiveItem(i)} key={i}>
              <a onClick={() => handleCurrentBookMobile(i)}>
                <span>{book.name}</span>
                <IconArrowDown />
              </a>
              {isCollapseActive && i === currentBook && (
                <WrapperBookContent>
                  <img src={book.cover.uri} />
                  <BookContent>
                    <Heading2>{book.name}</Heading2>
                    <Heading6>Sinopse</Heading6>
                    <p>{book.synopsis}</p>

                    {editors.length > 0 && (
                      <CollaboratorContent>
                        <Heading6>Editores</Heading6>
                        {editors.map((editor) => (
                          <p key={editor}>{editor}</p>
                        ))}
                      </CollaboratorContent>
                    )}

                    {authors.length > 0 && (
                      <CollaboratorContent>
                        <Heading6>Autores</Heading6>
                        {authors.map((author) => (
                          <p key={author}>{author}</p>
                        ))}
                      </CollaboratorContent>
                    )}
                  </BookContent>
                </WrapperBookContent>
              )}
            </li>
          ))}
        </ul>
        <Button className="customButton" color={colors.white} onClick={() => router.push('/cadastro')}>
          Quero me cadastrar
        </Button>
      </WrapperContainer>
    </>
  )
}

export default BooksCollaboratorsMobile
