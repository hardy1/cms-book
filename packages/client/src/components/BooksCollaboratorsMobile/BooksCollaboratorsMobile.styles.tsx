import styled from 'styled-components'
import theme from '../../lib/theme'
import colors from '../../resources/colors'
import media from '../../resources/media'

export const WrapperContainer = styled.nav`
  display: none;
  width: 100%;
  background-color: ${theme.bookColors['navyBlue'].secondary.hex};
  padding: 10px 20px;

  @media ${media.tabletL} {
    display: block;
  }

  ul {
    padding: 0;
  }

  ul > li {
    list-style-type: none;
    color: ${colors.white};
    margin: 0 -20px;
    padding: 10px 20px;

    a {
      display: flex;
      justify-content: space-between;
      align-items: center;

      svg > path {
        stroke: ${colors.white};
      }
    }
  }

  ul > li.active {
    background-color: ${({ theme: themeColor }) => theme.bookColors[themeColor].hex};

    a > svg {
      transform: rotate(180deg);
    }
  }

  .customButton {
    color: ${theme.bookColors['navyBlue'].secondary.hex};
    font-size: 1rem;
    padding: 9.75px 11.86px;
    width: 170px;
  }
`
export const WrapperBookContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 20px 0 0;

  img {
    max-width: 168px;
    width: 100%;
    height: 100%;
  }
`
export const BookContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: left;
  padding-top: 20px;
  width: 100%;

  h2 {
    margin-bottom: 1.5rem;
  }

  h2,
  h6 {
    color: ${colors.white};
  }

  h6 {
    text-transform: initial;
  }
`
export const CollaboratorContent = styled.div`
  display: flex;
  flex-direction: column;
  margin: 15px 0;

  h6 {
    margin-bottom: 5px;
  }

  p {
    margin: 5px 0;
  }
`
