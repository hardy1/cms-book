import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import Head from 'next/head'
import parse from 'html-react-parser'
import {
  CardRelated,
  InternalHeader,
  Heading2,
  Heading1,
  HtmlWrapper,
  ArticleSharing,
  MainContainer,
} from 'designsystem'
import { IBreadcrumb, IChannelData, ISponsorData } from '../../types'
import { getFullUrl } from '../../lib/tools'
import {
  RelatedContainer,
  ArticleMiddleSection,
  TextContainer,
  AuthorContainer,
  By,
  AuthorName,
  ArticleInfoContainer,
  ArticleCategory,
  ArticleInfo,
  LastArticlesContainer,
  FlexContainer,
} from '../../containers/StylesContainer/single-artigo/_styles'

import { ClientBreadcrumbs } from '..'
import { ChannelSection } from '../../enums'

interface ChannelSingleContentProps {
  data?: IChannelData
  title?: string
  subtitle?: string
  channel?: string
  lastContent?: React.ReactNode
  breadcrumbs?: IBreadcrumb[]
}

const StyledHeadingWrapper = styled.div`
  margin-top: 24px;
`
const StyledAnchor = styled.a`
  color: rgb(33, 109, 225);
  text-decoration: none;
  font-size: 0.875em;
  font-weight: 400;
`
const StyledSponsorsWrapper = styled.div`
  display: flex;
  align-items: center;

  a {
    display: flex;
  }

  a > img {
    width: 100%;
    max-height: 60px;
    max-width: 100px;
  }

  a + a {
    margin-left: 8px;
  }
`
const ChannelSingleContent: React.FC<ChannelSingleContentProps> = ({
  data,
  title,
  channel,
  lastContent,
  breadcrumbs,
}) => {
  const router = useRouter()
  const [currentUrl, setCurrentUrl] = useState('')
  const date = data?.published_at && new Date(data.published_at)
  const relatedItems = data?.related_items?.map((item) => ({
    ...item,
    url: `/${ChannelSection.SLUG}/${channel}/${item.slug}`,
  }))

  useEffect(() => {
    setCurrentUrl(getFullUrl())
  }, [router.pathname])

  const RelatedItems = () => (
    <RelatedContainer>
      <CardRelated title="Você também pode gostar" items={relatedItems} />
    </RelatedContainer>
  )

  return (
    <>
      <Head>
        <meta property="og:image" content={data.image} />
        <meta name="twitter:image" content={data.image} />
      </Head>
      <MainContainer>
        <FlexContainer>
          <InternalHeader>
            <Heading2 color="primaryColor">{title}</Heading2>
          </InternalHeader>

          <ClientBreadcrumbs breadcrumbs={breadcrumbs} />
          <ArticleMiddleSection>
            <TextContainer>
              <StyledAnchor href={`${data?.slug}`}>
                <Heading1 color="primaryColor">{data?.title}</Heading1>
              </StyledAnchor>
              {data?.subtitle && (
                <StyledHeadingWrapper>
                  <Heading2 color="disabledColor">{data?.subtitle}</Heading2>
                </StyledHeadingWrapper>
              )}
              <AuthorContainer>
                <By>Por</By>
                <AuthorName>{`${data?.author_name}`}</AuthorName>
              </AuthorContainer>
              <ArticleInfoContainer>
                <ArticleInfo icon="calendar">
                  {date?.toLocaleDateString('pt-BR', { year: 'numeric', month: 'long', day: 'numeric' })}
                </ArticleInfo>
                <ArticleInfo icon="clock">
                  {!!data?.reading_time && `${data?.reading_time} min. de leitura`}
                </ArticleInfo>
                <ArticleCategory>
                  {data?.categories &&
                    data?.categories.map((value, i) => {
                      if (data.categories.length > i + 1) {
                        return (
                          <StyledAnchor key={value.id} href={`/canais-moc/noticias?category=${value.slug}`}>
                            <span>{value.name}, </span>
                          </StyledAnchor>
                        )
                      } else {
                        return (
                          <StyledAnchor key={value.id} href={`/canais-moc/noticias?category=${value.slug}`}>
                            <span>{value.name}</span>
                          </StyledAnchor>
                        )
                      }
                    })}
                </ArticleCategory>
              </ArticleInfoContainer>
              <HtmlWrapper>{data && parse(data.content || '')}</HtmlWrapper>

              {data.has_sponsor && data?.sponsors.length > 0 && (
                <>
                  <p>Apoio:</p>
                  <StyledSponsorsWrapper>
                    {data.sponsors.map((sponsor: ISponsorData) => (
                      <a key={sponsor.image_url} href={sponsor.image_cta} target="_blank" rel="noreferrer">
                        <img src={sponsor.image_url} />
                      </a>
                    ))}
                  </StyledSponsorsWrapper>
                  <HtmlWrapper>{data?.sponsors_description && parse(data?.sponsors_description || '')}</HtmlWrapper>
                </>
              )}

              <ArticleSharing title={data?.title} url={currentUrl} />
            </TextContainer>

            {data?.related_items?.length ? <RelatedItems /> : ''}
          </ArticleMiddleSection>

          {!!lastContent && <LastArticlesContainer>{lastContent}</LastArticlesContainer>}
        </FlexContainer>
      </MainContainer>
    </>
  )
}

export default ChannelSingleContent
