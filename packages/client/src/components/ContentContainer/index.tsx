import * as React from 'react'
import { StyledContentContainer, StyledContentContainerProps } from './ContentContainer.styles'

export interface ContentContainerProps extends StyledContentContainerProps {
  children: React.ReactNode
}

const ContentContainer: React.FC<ContentContainerProps> = ({
  children,
  gridColumns,
  type,
  margins,
  maxWidth,
  direction,
  gap,
}: ContentContainerProps) => (
  <StyledContentContainer
    type={type}
    gridColumns={gridColumns}
    maxWidth={maxWidth}
    direction={direction}
    margins={margins}
    gap={gap}
  >
    {children}
  </StyledContentContainer>
)

export default ContentContainer
