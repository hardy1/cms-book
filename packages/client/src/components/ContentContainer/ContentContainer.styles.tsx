import styled from 'styled-components'
import { size } from '../../resources/media'

// TODO: Restructure GridContainer from designsystem
export interface StyledContentContainerProps {
  type?: string
  margins?: string
  gridColumns?: number
  maxWidth?: string
  direction?: 'row' | 'column'
  gap?: string
  fullView?: boolean
}

export const StyledContentContainer = styled.div<StyledContentContainerProps>`
  display: ${(props) => props.type || 'grid'};
  grid-column: 1 / -1;
  column-gap: 1rem;
  grid-template-columns: repeat(${(props) => props.gridColumns || 1}, 1fr);
  margin: ${(props) => (props.margins ? props.margins : '0px auto')};
  max-width: ${(props) => props.maxWidth || size.laptopM};
  position: relative;

  ${(props) =>
    props.fullView &&
    `
    max-width: 100vw;
    max-height: 100vh;
    width: 100vw;
    height: 100vh;
    `}

  ${(props) => props.gap && `gap: ${props.gap};`}

  ${(props) =>
    props.type === 'flex' &&
    `
    flex-direction: ${props.direction === 'column' ? 'column;' : 'row;'}
    justify-content: center;
  `}
`
