import styled from 'styled-components'
import setColorHex from '../../containers/Books/setColorHex'
import colors from '../../resources/colors'

interface GenericListContainerProps {
  colorTheme?: string
  nightMode?: boolean
  fontSizeModifier?: number
}

const GenericListContainer = styled.div<GenericListContainerProps>`
  .Index {
    position: relative;
    color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
    font-family: 'Montserrat';
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 24}px`};
    font-weight: 700;
    line-height: 2rem;
    padding-bottom: 12px;
    margin-bottom: 12px;

    :hover,
    :focus {
      color: ${({ colorTheme, theme }) => setColorHex(colorTheme, theme)};
    }

    @media screen and (min-width: 960px) {
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 24}px`};
      line-height: 32px;
    }

    ::before {
      content: '';
      position: absolute;
      left: 0;
      bottom: 0;
      width: 100%;
      height: 1px;
      background-color: #eee;

      @media screen and (min-width: 960px) {
        display: none;
      }
    }

    &__Title {
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 28}px`};
      line-height: 34px;
      margin-bottom: 30px;

      @media screen and (min-width: 960px) {
        font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 28}px`};
        line-height: 46px;
      }
    }
  }
`

export default GenericListContainer
