import React from 'react'
import Link from 'next/link'
import { IGenericItem } from '../../types'
import GenericListContainer from './GenericList.style'

interface GenericListProps {
  items: IGenericItem[]
  baseUrl: string
  colorTheme?: string
  fontSizeModifier?: number
  nightMode?: boolean
}

const GenericList: React.FC<GenericListProps> = ({
  items,
  baseUrl,
  fontSizeModifier,
  nightMode,
  colorTheme,
}: GenericListProps) => {
  return (
    <GenericListContainer colorTheme={colorTheme} fontSizeModifier={fontSizeModifier} nightMode={nightMode}>
      {items.map(({ id, name, slug }) => (
        <Link key={id} href={`${baseUrl}/${slug}`}>
          <a>
            <h3 className="Index">{name}</h3>
          </a>
        </Link>
      ))}
    </GenericListContainer>
  )
}

export default GenericList
