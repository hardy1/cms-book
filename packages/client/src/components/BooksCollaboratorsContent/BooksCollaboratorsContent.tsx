import React from 'react'
import { Heading6 } from 'designsystem'
import {
  WrapperColumnCollaborator,
  ColumnBookCollaboratos,
  RowBookCollaboratos,
  Collaborator,
} from './BooksCollaboratorsContent.styles'

interface IProps {
  authors: string[]
  editors: string[]
}

const BooksCollaboratorsContent: React.FC<IProps> = ({ authors, editors }) => {
  const handleEditorsColumn = Math.ceil(editors.length / 3)
  const handleAuthorsColumn = Math.ceil(authors.length / 3)

  return (
    <>
      {editors.length > 0 && (
        <RowBookCollaboratos>
          <Heading6>Editores</Heading6>

          <WrapperColumnCollaborator>
            <ColumnBookCollaboratos>
              {editors.map((editor, i) => {
                if (i + 1 <= handleEditorsColumn) {
                  return <Collaborator key={i + 1}>{editor}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>
            <ColumnBookCollaboratos>
              {editors.map((editor, i) => {
                if (i + 1 > handleEditorsColumn && i + 1 <= handleEditorsColumn * 2) {
                  return <Collaborator key={i + 1}>{editor}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>
            <ColumnBookCollaboratos>
              {editors.map((editor, i) => {
                if (i + 1 > handleEditorsColumn * 2 && i + 1 <= handleEditorsColumn * 3) {
                  return <Collaborator key={i + 1}>{editor}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>
          </WrapperColumnCollaborator>
        </RowBookCollaboratos>
      )}

      {authors.length > 0 && (
        <RowBookCollaboratos>
          <Heading6>Autores</Heading6>

          <WrapperColumnCollaborator>
            <ColumnBookCollaboratos>
              {authors.map((author, i) => {
                if (i + 1 <= handleAuthorsColumn) {
                  return <Collaborator key={i + 1}>{author}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>

            <ColumnBookCollaboratos>
              {authors.map((author, i) => {
                if (i + 1 > handleAuthorsColumn && i + 1 <= handleAuthorsColumn * 2) {
                  return <Collaborator key={i + 1}>{author}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>

            <ColumnBookCollaboratos>
              {authors.map((author, i) => {
                if (i + 1 > handleAuthorsColumn * 2 && i + 1 <= handleAuthorsColumn * 3) {
                  return <Collaborator key={i + 1}>{author}</Collaborator>
                }
              })}
            </ColumnBookCollaboratos>
          </WrapperColumnCollaborator>
        </RowBookCollaboratos>
      )}
    </>
  )
}

export default BooksCollaboratorsContent
