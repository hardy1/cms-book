import styled from 'styled-components'

export const RowBookCollaboratos = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  h6 {
    margin: 48px 0 24px;
  }
`
export const WrapperColumnCollaborator = styled.div`
  display: flex;
  flex-direction: row;
`
export const ColumnBookCollaboratos = styled.div`
  display: flex;
  flex-direction: column;
  width: 33%;

  &:nth-child(2) {
    padding: 0 24px;
  }

  h6 {
    margin-bottom: 25px;
  }
`
export const Collaborator = styled.p`
  margin: 0;
  font-size: 13px;
  line-height: 23.4px;
`
