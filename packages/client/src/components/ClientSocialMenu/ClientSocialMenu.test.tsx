import * as React from 'react'
import { render } from '@testing-library/react'
import ClientSocialMenu from './ClientSocialMenu'

describe('ClientSocialMenu', () => {
  it('SHOULD render correctly', () => {
    const component = render(<ClientSocialMenu />)

    expect(component).toBeTruthy()
  })
})
