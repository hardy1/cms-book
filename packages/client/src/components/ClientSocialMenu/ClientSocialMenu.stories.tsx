import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientSocialMenu from './ClientSocialMenu'

export default {
  title: 'Client/ClientSocialMenu',
  component: ClientSocialMenu,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <ClientSocialMenu />
