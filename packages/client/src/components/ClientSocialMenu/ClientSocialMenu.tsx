import * as React from 'react'
import { IconFacebook, IconTwitter, IconLinkedin, IconPodcast } from 'designsystem'

import { SocialMenuContainer } from './ClientSocialMenu.styles'

const ClientSocialMenu: React.FC = () => (
  <SocialMenuContainer>
    <a href="https://www.facebook.com/MOCBrasil" target="_blank" rel="noreferrer" title="Facebook">
      <IconFacebook />
    </a>
    <a href="https://twitter.com/MOCBrasil" target="_blank" rel="noreferrer" title="Twitter">
      <IconTwitter />
    </a>
    <a href="https://www.linkedin.com/company/mocbrasil" target="_blank" rel="noreferrer" title="LinkedIn">
      <IconLinkedin />
    </a>
    <a href="/canais-moc/podcasts" target="_blank" rel="noreferrer" title="Podcast">
      <IconPodcast />
    </a>
  </SocialMenuContainer>
)

export default ClientSocialMenu
