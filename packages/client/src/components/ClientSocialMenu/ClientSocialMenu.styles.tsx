import styled from 'styled-components'

export const SocialMenuContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100px;
`
