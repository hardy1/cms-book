import React from 'react'
import { BookFullType } from '../../types'
import PresentationContainer from './Presentation.style'

interface PresentationProps {
  book: BookFullType
  fontSizeModifier?: number
  colorTheme: string
  nightMode?: boolean
}

const Presentation: React.FC<PresentationProps> = ({ book, fontSizeModifier, colorTheme }: PresentationProps) => {
  return (
    <PresentationContainer colorTheme={colorTheme} fontSizeModifier={fontSizeModifier}>
      <div className="HeaderBook__ContainerImage">
        <img
          src={book?.cover?.uri}
          className="HeaderBook__Image"
          alt={`Imagem do Livro de ${book?.cover?.description}`}
        />
      </div>

      <div className="HeaderBook__ContainerText">
        <h1 className="HeaderBook__Title">{book?.name}</h1>
        {book?.year ? <small className="HeaderBook__Subtitle">Ano {book?.year}</small> : ''}
      </div>
    </PresentationContainer>
  )
}

export default Presentation
