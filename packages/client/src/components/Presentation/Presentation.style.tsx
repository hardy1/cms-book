import styled from 'styled-components'
import setColorHex from '../../containers/Books/setColorHex'

interface PresentationContainerProps {
  colorTheme?: string
  nightMode?: boolean
  fontSizeModifier?: number
}

const PresentationContainer = styled.div<PresentationContainerProps>`
  display: flex;
  justify-content: flex-start;
  margin-bottom: 4rem;

  .HeaderBook {
    margin-bottom: 50px;

    @media screen and (min-width: 960px) {
      display: flex;
      flex-wrap: nowrap;
      margin-bottom: 60px;
    }

    &__ContainerImage {
      display: none;

      @media screen and (min-width: 960px) {
        display: inline-block;
        position: relative;
        display: inline-block;
        width: 250px;
        height: auto;
        padding-right: 45px;
        margin-right: 35px;

        &::before {
          content: '';
          position: absolute;
          top: 0;
          right: 0;
          width: 1px;
          height: 100%;
          background-color: #929292;
        }
      }
    }

    &__Image {
      @media screen and (min-width: 960px) {
        display: inline-block;
        width: 200px;
        height: auto;
      }
    }

    &__Title {
      position: relative;
      font-family: 'Merriweather';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 38}px`};
      line-height: 1;
      padding-bottom: 16px;

      ::before {
        content: '';
        position: absolute;
        bottom: 8px;
        left: 0;
        width: 138px;
        height: 2px;
        background-color: ${({ colorTheme, theme }) => setColorHex(colorTheme, theme)};
      }
    }

    &__Subtitle {
      color: #9ea0a5;
      font-family: 'Montserrat';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 18}px`};
      font-weight: 700;
      line-height: 24px;
    }
  }
`

export default PresentationContainer
