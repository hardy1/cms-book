import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import BooksTopHeader from './BooksTopHeader'

export default {
  title: 'Books/BooksTopHeader',
  component: BooksTopHeader,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <BooksTopHeader />
