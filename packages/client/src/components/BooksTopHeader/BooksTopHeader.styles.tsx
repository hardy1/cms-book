import styled from 'styled-components'
import colors from '../../resources/colors'
import { IconArrowLeft } from 'designsystem'

export const TopHeaderContainer = styled.div`
  align-items: center;
  background-color: ${colors.lightColor};
  display: flex;
  height: 60px;
  justify-content: space-between;
  align-items: center;
  padding: 0 40px;
  border-bottom: 1px solid #d6d0d0;
`

export const BackToHome = styled.a`
  color: ${colors.neutralColor};
  text-decoration: none;
  display: flex;
  align-items: center;
  width: 180px;
`

export const ArrowLeft = styled(IconArrowLeft)`
  margin-left: 10px;

  & path {
    stroke: ${colors.neutralColor};
  }
`

export const BackText = styled.span`
  display: inline-block;
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.875em;
  line-height: 1;
  margin-left: 8px;
  vertical-align: text-top;
`
