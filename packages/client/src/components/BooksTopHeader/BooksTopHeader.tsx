import React from 'react'
import BooksSearch from '../BooksSearch'
import { BackToHome, TopHeaderContainer, BackText, ArrowLeft } from './BooksTopHeader.styles'

const BooksTopHeader: React.FC = () => (
  <TopHeaderContainer>
    <BackToHome href="/">
      <ArrowLeft />
      <BackText>Voltar para o site</BackText>
    </BackToHome>
    <BooksSearch />
  </TopHeaderContainer>
)

export default BooksTopHeader
