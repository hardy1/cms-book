import * as React from 'react'
import { render } from '@testing-library/react'
import BooksTopHeader from './BooksTopHeader'

describe('BooksTopHeader', () => {
  it('SHOULD render correctly', () => {
    const component = render(<BooksTopHeader />)

    expect(component).toBeTruthy()
  })
})
