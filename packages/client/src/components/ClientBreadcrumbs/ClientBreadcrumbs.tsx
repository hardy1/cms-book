import * as React from 'react'
import { IBreadcrumb } from '../../types'

import { BreadcrumbsContainer, BreadcrumbsList, BreadcrumbsItem, BreadcrumbsLink } from './ClientBreadcrumbs.styles'

interface Props {
  as?: React.ElementType
  breadcrumbs: IBreadcrumb[]
}

export const ClientBreadcrumbs = ({ breadcrumbs = [], as = 'a' }: Props) => {
  return (
    <BreadcrumbsContainer aria-label="breadcrumbs">
      <BreadcrumbsList key="BC1">
        {breadcrumbs.map((breadcrumb: { href: string; content: string }) => {
          return (
            <BreadcrumbsItem key={breadcrumb.href}>
              <BreadcrumbsLink as={as} href={breadcrumb.href}>
                {breadcrumb.content}
              </BreadcrumbsLink>
            </BreadcrumbsItem>
          )
        })}
      </BreadcrumbsList>
    </BreadcrumbsContainer>
  )
}

export default ClientBreadcrumbs
