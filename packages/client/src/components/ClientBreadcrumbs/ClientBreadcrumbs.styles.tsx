import styled from 'styled-components'
import colors from '../../resources/colors'

export const BreadcrumbsContainer = styled.nav`
  text-align: center;
  display: flex;
  text-overflow: ellipsis;
  overflow: hidden;
`

export const BreadcrumbsList = styled.ul`
  display: flex;
  list-style: none;
  padding: 0;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  & a {
    margin: 0;
  }
`
export const BreadcrumbsItem = styled.li`
  white-space: nowrap;
  position: relative;
  margin: 0.5rem 1rem 0.5rem 0;

  &:not(:first-child) {
    color: ${colors.disabledColor};
    font-size: 0.875rem;

    & a {
      margin-left: 1rem;
    }

    &::before {
      content: '';
      border-style: solid;
      border-width: 0.25em 0.25em 0 0;
      display: inline-block;
      height: 0.45rem;
      position: relative;
      transform: rotate(45deg);
      width: 0.45rem;
      margin: 0;
    }
  }

  &:last-child a {
    color: ${colors.primaryColor};
    font-weight: 800;
    font-size: 0.875rem;
  }
`

export const BreadcrumbsLink = styled.a`
  font-weight: 100;
  color: ${colors.disabledColor};
  font-size: 0.875rem;
  text-decoration: none;
  margin-right: 10px;
  font-family: 'Montserrat';
  cursor: pointer;

  &:not(:first-child) {
    color: ${colors.disabledColor};
    text-decoration: none;
    margin: 5px;
  }
`
