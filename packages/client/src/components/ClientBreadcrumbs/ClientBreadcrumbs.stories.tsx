import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ClientBreadcrumbs from './ClientBreadcrumbs'
import { IBreadcrumb } from '../../types'

export default {
  title: 'Client/ClientBreadcrumbs',
  component: ClientBreadcrumbs,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

const links: IBreadcrumb[] = [
  { content: 'Pagina Inicial', href: '/' },
  { content: 'Livros', href: '/books' },
]

export const Default = () => <ClientBreadcrumbs breadcrumbs={links} />
