import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import BooksToolbar from './BooksToolbar'

export default {
  title: 'Books/BooksToolbar',
  component: BooksToolbar,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <BooksToolbar />
