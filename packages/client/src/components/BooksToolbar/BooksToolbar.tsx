import React, { FC, useContext, useState } from 'react'
import Sidebar from 'react-sidebar'
import BookContext from '../../contexts/book'
import { BooksToolbarContainer, SidebarContent, SidebarButton, BooksToolbarItens } from './BooksToolbar.styles'
import {
  IconBookPart,
  IconBookIndex,
  IconSearchIn,
  IconSetup,
  IconFullscreen,
  IconNightMode,
  IconFontSize,
} from '../../containers/Icons'
import { SidebarSearch } from './SidebarSearch'
import { SidebarSetup } from './SidebarSetup'
import { SidebarPart } from './SidebarPart'
import { SidebarIndex } from './SidebarIndex'
import { BookFullType, CurrentBookDataType } from '../../types'

const BooksToolbar: FC<{ book?: BookFullType; current?: CurrentBookDataType }> = (props) => {
  const { state, setState: setGlobalState } = useContext(BookContext)
  const { book, current } = props
  const [fullScreen, setFullScreen] = useState<boolean>(state.fullScreen)
  const [nightMode, setNightMode] = useState<boolean>(state.nightMode)
  const [sidebarOpen, setSidebarOpen] = useState<boolean>(false)
  const [bookIndex, setBookIndex] = useState<boolean>(state.bookIndex)
  const [searchIn, setSearchIn] = useState<boolean>(state.searchIn)
  const [currentMenu, setCurrentMenu] = useState<string>()

  const toggleFullScreen = (e) => {
    const toggle = !fullScreen
    e.preventDefault()
    setFullScreen(toggle)
    setGlobalState({ ...state, fullScreen: toggle })
  }

  const toggleNightMode = (e) => {
    const toggle = !nightMode
    e.preventDefault()
    setNightMode(toggle)
    setGlobalState({ ...state, nightMode: toggle })
  }

  const increaseFontSize = (e) => {
    e.preventDefault()
    setGlobalState({ ...state, fontModifier: state.fontModifier + 0.2 })
  }

  const decreaseFontSize = (e) => {
    e.preventDefault()
    setGlobalState({ ...state, fontModifier: state.fontModifier - 0.2 })
  }

  const resetFontSize = (e) => {
    e.preventDefault()
    setGlobalState({ ...state, fontModifier: 1 })
  }

  const openContent = (currentSelection) => {
    let selected

    switch (currentSelection) {
      case 'index':
        selected = <SidebarIndex book={book} setSidebarOpen={setSidebarOpen} setBookIndex={setBookIndex} />
        break
      case 'part':
        selected = <SidebarPart book={book} current={current} />
        break
      case 'search':
        selected = <SidebarSearch setSidebarOpen={setSidebarOpen} setSearchIn={setSearchIn} />
        break
      case 'setup':
        selected = (
          <SidebarSetup
            fullScreen={fullScreen}
            toggleFullScreen={toggleFullScreen}
            nightMode={nightMode}
            toggleNightMode={toggleNightMode}
            decreaseFont={decreaseFontSize}
            increaseFont={increaseFontSize}
            defaultFont={resetFontSize}
          />
        )
        break
      default:
        selected = <SidebarIndex book={book} />
        break
    }

    return <SidebarContent>{selected}</SidebarContent>
  }

  const toggleSidebarBookIndex = (menu: string) => {
    const toggle = !bookIndex
    setBookIndex(toggle)
    setSearchIn(false)
    setCurrentMenu(menu)
    setSidebarOpen(!!sidebarOpen && currentMenu === menu ? !sidebarOpen : true)
    setGlobalState({ ...state, bookIndex: toggle })
  }

  const toggleSidebarSearchIn = (menu: string) => {
    const toggle = !searchIn
    setSearchIn(toggle)
    setBookIndex(false)
    setCurrentMenu(menu)
    setSidebarOpen(!!sidebarOpen && currentMenu === menu ? !sidebarOpen : true)
    setGlobalState({ ...state, searchIn: toggle })
  }

  return (
    <BooksToolbarContainer minWidth={'46px'} maxWidth={'25vw'} opened={sidebarOpen}>
      <Sidebar
        sidebar={
          <BooksToolbarItens>
            <SidebarButton isActive={bookIndex} onClick={() => toggleSidebarBookIndex('index')}>
              <IconBookIndex />
              <span>Índice do Livro</span>
            </SidebarButton>
            {/* <button onClick={() => toggleSidebar('part')}>
              <IconBookPart />
              <span>Capítulos</span>
            </button> */}
            <SidebarButton isActive={searchIn} onClick={() => toggleSidebarSearchIn('search')}>
              <IconSearchIn />
              <span>Pesquisar no Livro</span>
            </SidebarButton>
            <IconFullscreen isActive={fullScreen} onClick={toggleFullScreen}></IconFullscreen>
            <IconNightMode isActive={nightMode} onClick={toggleNightMode}></IconNightMode>
            <IconFontSize increaseFont={true} onClick={increaseFontSize}></IconFontSize>
            <IconFontSize increaseFont={false} onClick={decreaseFontSize}></IconFontSize>
            {state?.fontModifier !== 1 && (
              <IconFontSize defaultSize className="active" onClick={resetFontSize}></IconFontSize>
            )}
          </BooksToolbarItens>
        }
        open={sidebarOpen}
        docked={true}
      >
        {openContent(currentMenu)}
      </Sidebar>
    </BooksToolbarContainer>
  )
}

export default BooksToolbar
