import { IconArrowDown } from 'designsystem'
import styled from 'styled-components'
import colors from '../../resources/colors'
import media from '../../resources/media'

type BooksToolbarContainerProps = {
  opened: boolean
  maxWidth: string
  minWidth: string
}

type SidebarButtonProps = {
  isActive: boolean
}

const BooksToolbarContainer = styled.div<BooksToolbarContainerProps>`
  position: relative;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  width: 100%;
  height: 60px;
  background-color: ${colors.lightColor};
  grid-area: toolbar;
  transition: width 150ms ease-in-out;

  > div {
    overflow: inherit !important;
    > div {
      &:first-of-type {
        overflow: inherit !important;
      }
    }
  }

  @media ${media.tabletL} {
    display: none;
  }

  @media screen and (min-width: 906px) {
    grid-area: toolbar;
    position: relative;
    left: 0;
    display: block;
    width: ${(props) => (props.opened ? props.maxWidth : props.minWidth)};
    height: 100%;
    bottom: unset;
    border-right: 1px solid #d6d0d0;
    height: 100vh;
    position: -webkit-sticky;
    position: sticky;
    top: -1px;
    overflow: visible;
    z-index: 10;
  }

  a,
  button {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 25%;
    border-width: 0;
    padding: 10px 15px;
    cursor: pointer;

    @media screen and (min-width: 768px) {
      width: 100%;
      height: 46px;
    }

    span {
      position: absolute;
      left: 39px;
      background-color: ${colors.neutralColor};
      color: ${colors.white};
      padding: 5px 10px;
      font-size: 12px;
      border-radius: 5px;
      opacity: 0;
      visibility: hidden;
      transition: all 0.2s linear;
      width: 100px;

      &:before {
        content: '';
        width: 10px;
        height: 10px;
        background-color: ${colors.neutralColor};
        position: absolute;
        left: -4px;
        top: 6px;
        transform: rotate(45deg);
      }
    }

    &:hover {
      span {
        left: 63px;
        opacity: 1;
        visibility: visible;
      }
    }
  }
`

const SidebarContent = styled.div`
  font-family: Montserrat;
  font-style: normal;
  font-weight: bold;
  font-size: 0.8rem;
  transition: opacity 2000ms ease-in-out, height 1500ms ease-in-out;

  h4 {
    font-size: 18px;
    font-weight: bold;
    padding: 1rem 1.2rem;
    margin: 0;
    border-bottom: 1px solid #d6d0d0;
  }

  p {
    font-weight: normal;
    padding: 1rem 1.2rem;
    margin: 0;
    cursor: pointer;
  }

  input {
    width: auto;
    padding: 1rem 1.2rem;
    margin: 0;
  }

  .Toolbar {
    &__Page,
    &__Part,
    &__Chapter {
      &--closed {
        height: 0;
        opacity: 0;
        -webkit-transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        -moz-transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        display: none;
      }

      &--open {
        height: 100%;
        opacity: 1;
        -webkit-transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        -moz-transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        transition: opacity 200ms ease-in-out, height 150ms ease-in-out;
        display: block;
      }

      &--active {
        color: ${colors.primaryBook};
        width: inherit;
        text-decoration: inherit;
      }

      &--inactive {
        color: ${colors.neutralColor};
        pointer-events: none;
        text-decoration: inherit;
      }
    }

    &__Part {
      text-decoration: none;
      color: inherit;
      width: inherit;

      &--active {
        pointer-events: auto;
      }
    }

    &__PartIndex {
      &--active {
        color: ${colors.neutralColor};
        width: inherit;
        text-decoration: inherit;

        &:hover {
          color: ${colors.primaryBook};
        }
      }
      &--inactive {
        color: ${colors.primaryBook};
        pointer-events: none;
        text-decoration: inherit;
      }
    }

    &__Chapter {
      text-decoration: none;
      width: auto;
      cursor: pointer;
      color: inherit;
      text-decoration: none;
      justify-content: flex-start;
      font-weight: normal;
      background-color: ${colors.white};

      &--active {
        justify-content: inherit;
        pointer-events: none;
        background-color: ${colors.white};
      }

      &:hover {
        color: ${colors.primaryBook};
      }
    }
  }
`

const SidebarChapterContent = styled.div`
  a {
    font-weight: bold;
    color: ${colors.primaryBook};
    text-align: right;
    font-size: 13px;
    text-transform: uppercase;
    cursor: pointer;
    width: initial;
    height: fit-content;
    justify-content: flex-end;
    text-decoration: none;
  }
`

const SidebarSearchBarContainer = styled.div`
  width: auto;
  padding: 1rem 1.2rem;
  margin: 0;

  input {
    align-items: center;
    background-color: ${colors.white};
    display: flex;
    height: auto;
    justify-content: flex-start;
    padding: 0;
    padding-left: 50px;
    height: 100%;
    width: 100%;
  }
`

const SidebarSearchContent = styled.div`
  h4 {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  mark {
    background-color: unset;
    font-weight: bold;
  }
`

const SubmenuArrow = styled(IconArrowDown)`
  color: ${colors.neutralColor};
  margin-left: 0.2rem;
  grid-area: arrow;
  min-width: max-content;
  transform: rotateZ(0);
  -webkit-transition: transform 200ms ease-in-out;
  -moz-transition: transform 200ms ease-in-out;
  transition: transform 200ms ease-in-out;

  path {
    stroke: ${colors.neutralColor};
  }

  &.TurnUp {
    transform: rotateZ(180deg);
    -webkit-transition: transform 200ms ease-in-out;
    -moz-transition: transform 200ms ease-in-out;
    transition: transform 200ms ease-in-out;
  }
`

const SidebarSetupContent = styled.div`
  div:not(*:first-child) {
    margin: 1rem 1.2rem;
  }
`

const SidebarButton = styled.button<SidebarButtonProps>`
  background-color: ${(props) => (props.isActive ? colors.disabledColor : 'transparent')};

  svg {
    path {
      fill: ${colors.black};
    }
  }

  &:hover {
    background-color: ${(props) => (props.isActive ? colors.disabledColor : '#C9CBCD')};
  }
`

const BooksToolbarItens = styled.div`
  border-bottom: 1px solid #d6d0d0;
  width: 47px;
`

const SidebarSearchCloseMenu = styled.button`
  width: auto !important;
  height: auto !important;
  padding: 0 !important;
`

export {
  BooksToolbarContainer,
  SidebarContent,
  SidebarChapterContent,
  SidebarSearchContent,
  SidebarSearchBarContainer,
  SidebarSetupContent,
  SubmenuArrow,
  SidebarButton,
  BooksToolbarItens,
  SidebarSearchCloseMenu,
}
