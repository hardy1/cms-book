import React, { useContext } from 'react'
import Link from 'next/link'
import { BookFullType, CurrentBookDataType } from '../../types'
import BookContext from '../../contexts/book'
import { SidebarChapterContent } from './BooksToolbar.styles'

type SidebarPartProps = {
  book: BookFullType
  current: CurrentBookDataType
}

const PartListItem = ({ book, current, chapter, active }) => (
  <Link key={chapter.id} href={`/livro/${book.slug}/${current.partSlug}/${chapter.slug}`}>
    <a className={`Toolbar__PartIndex${active ? '--inactive' : '--active'}`}>{chapter.name}</a>
  </Link>
)

export const SidebarPart = ({ book, current }: SidebarPartProps) => {
  const { state } = useContext(BookContext)
  const currentPartData = book.parts?.find((part) => part.slug === current?.partSlug)
  const currentChapterData = currentPartData?.chapters.map((chapter) => (
    <PartListItem
      key={chapter.id}
      book={book}
      current={current}
      chapter={chapter}
      active={state.current?.chapterSlug === chapter.slug}
    />
  ))

  return (
    <SidebarChapterContent>
      <h4>Índice da Parte</h4>
      {currentChapterData}
    </SidebarChapterContent>
  )
}
