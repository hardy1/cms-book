import React, { useContext } from 'react'
import parse from 'html-react-parser'
import { IconSearch, Input } from 'designsystem'
import { highlightSearch } from '../../lib/tools'
import { SidebarSearchBarContainer, SidebarSearchContent, SidebarSearchCloseMenu } from './BooksToolbar.styles'
import BookContext from '../../contexts/book'
import IconToolBarCloseMenu from '../../containers/Icons/IconToolBarCloseMenu'

const search = {
  id: 1,
  keyword: 'síndromes mielodisplásicas',
  occurences: [
    {
      id: 1,
      book_id: 1,
      chapter_id: 1,
      content:
        'As síndromes mielodisplásicas (SMDs) compreendem um grupo heterogêneo de doenças clonais caracterizadas por citopenia(s) e hematopoese ineficaz. Na maioria (≈ 85%), os casos são ... ',
      slug: 'capitulo-1-ou-pagina-tal',
    },
  ],
}

type SidebarSearchProps = {
  setSidebarOpen?: React.Dispatch<React.SetStateAction<boolean>>
  setSearchIn?: React.Dispatch<React.SetStateAction<boolean>>
}

export const SidebarSearch = ({ setSearchIn, setSidebarOpen }: SidebarSearchProps) => {
  const { state, setState: setGlobalState } = useContext(BookContext)

  function handlCloseSearch(e) {
    e.preventDefault()
    setSidebarOpen(false)
    setGlobalState({ ...state, searchIn: false })
    setSearchIn(false)
  }

  const searchResultList = parse(highlightSearch(search))

  return (
    <SidebarSearchContent>
      <h4>
        Pesquisar no livro
        <SidebarSearchCloseMenu onClick={handlCloseSearch}>
          <IconToolBarCloseMenu />
        </SidebarSearchCloseMenu>
      </h4>
      <SidebarSearchBarContainer>
        <Input
          id="inputId"
          type="text"
          placeholder="Pesquisar nos livros"
          icon={<IconSearch />}
          onChange={() => null}
        />
      </SidebarSearchBarContainer>
      {searchResultList}
    </SidebarSearchContent>
  )
}
