import React, { useState } from 'react'
import { SubmenuArrow } from '../BooksToolbar.styles'
import IndexChapterListItem from './ChapterItem'

type currentSubmenus = {
  1?: boolean
}

const IndexPartListItem = ({ part, currentBook, active, activeSubMenu }) => {
  const [currentSubmenu, setCurrentSubmenu] = useState<currentSubmenus>({})

  const togglePartSubmenu = (id: number) => () => {
    setCurrentSubmenu({ ...currentSubmenu, [id]: !currentSubmenu[id] })
  }

  return (
    <div key={part.id} onClick={togglePartSubmenu(part.id)}>
      <h5 className={`Toolbar__Part${active ? '--active' : ''}`}>
        <span>{part.name}</span>
        <SubmenuArrow className={`${currentSubmenu[part.id] ? 'TurnUp' : ''}`} />
      </h5>
      <div className={`Toolbar__Part${currentSubmenu[part.id] ? '--open' : '--closed'}`}>
        {part.chapters.map((chapter) => (
          <IndexChapterListItem
            key={chapter.id}
            chapter={chapter}
            currentBook={currentBook}
            currentPart={part.slug}
            active={activeSubMenu === chapter.slug}
          />
        ))}
      </div>
    </div>
  )
}

export default IndexPartListItem
