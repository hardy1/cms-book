import styled from 'styled-components'
import colors from '../../../resources/colors'

const SidebarIndexContent = styled.div`
  h5 {
    font-weight: bold;
    text-transform: uppercase;
    border-bottom: 1px solid ${colors.borderColor};
    padding: 1rem 1.2rem;
    margin: 0;
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  h4 {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  a {
    height: auto;
  }
`

const SidebarCloseMenu = styled.button`
  width: auto !important;
  height: auto !important;
  padding: 0 !important;
`

export { SidebarIndexContent, SidebarCloseMenu }
