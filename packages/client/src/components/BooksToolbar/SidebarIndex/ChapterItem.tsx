import React, { useContext } from 'react'
import BookContext from '../../../contexts/book'
import Link from 'next/link'

const IndexChapterListItem = ({ chapter, currentBook, currentPart, active }) => {
  const { state, setState: setGlobalState } = useContext(BookContext)

  const handleClick = () => {
    setGlobalState({
      ...state,
      current: { chapterSlug: chapter.slug },
    })
  }

  return (
    <Link key={chapter.id} href={`/livro/${currentBook}/${currentPart}/${chapter.slug}`}>
      <a className={`Toolbar__Chapter${active ? '--active' : ''}`} onClick={handleClick}>
        {chapter.name}
      </a>
    </Link>
  )
}

export default IndexChapterListItem
