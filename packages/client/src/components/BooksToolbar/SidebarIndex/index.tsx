import React, { useContext } from 'react'
import Link from 'next/link'
import BookContext from '../../../contexts/book'
import { BookFullType } from '../../../types'
import { SidebarIndexContent, SidebarCloseMenu } from './SidebarIndex.styles'
import IndexPartListItem from './PartItem'
import IconToolBarCloseMenu from '../../../containers/Icons/IconToolBarCloseMenu'

type SidebarIndexProps = {
  book: BookFullType
  setSidebarOpen?: React.Dispatch<React.SetStateAction<boolean>>
  setBookIndex?: React.Dispatch<React.SetStateAction<boolean>>
}

export const SidebarIndex = ({ book, setSidebarOpen, setBookIndex }: SidebarIndexProps) => {
  const { state, setState: setGlobalState } = useContext(BookContext)

  function handleCloseMenu(e) {
    e.preventDefault()
    setSidebarOpen(false)
    setGlobalState({ ...state, bookIndex: false })
    setBookIndex(false)
  }

  return (
    <SidebarIndexContent>
      <h4>
        Índice do livro
        <SidebarCloseMenu onClick={handleCloseMenu}>
          <IconToolBarCloseMenu />
        </SidebarCloseMenu>
      </h4>
      <Link href={`/livro/${book?.slug}`}>
        <h5 className="Toolbar__Part">Editores e Autores</h5>
      </Link>
      {book?.parts?.map((part) => (
        <IndexPartListItem
          key={part?.id}
          part={part}
          currentBook={book?.slug}
          active={state?.current?.partSlug === part?.slug}
          activeSubMenu={state?.current?.chapterSlug}
        />
      ))}
    </SidebarIndexContent>
  )
}

export default SidebarIndex
