import React, { useContext } from 'react'
import { SidebarSetupContent } from './BooksToolbar.styles'
import { IconNightMode, IconFontSize, IconFullscreen } from '../../containers/Icons'
import BookContext from '../../contexts/book'

type SiderbarSetupProps = {
  nightMode?: boolean
  toggleNightMode?: (e: React.MouseEvent<HTMLElement>) => void
  fullScreen?: boolean
  toggleFullScreen?: (e: React.MouseEvent<HTMLElement>) => void
  decreaseFont?: (e: React.MouseEvent<HTMLElement>) => void
  increaseFont?: (e: React.MouseEvent<HTMLElement>) => void
  defaultFont?: (e: React.MouseEvent<HTMLElement>) => void
}

export const SidebarSetup = (props: SiderbarSetupProps) => {
  const { state } = useContext(BookContext)

  return (
    <SidebarSetupContent>
      <h4>Configurações</h4>
      <IconFullscreen isActive={props.fullScreen} onClick={props.toggleFullScreen}></IconFullscreen>
      <IconNightMode isActive={props.nightMode} onClick={props.toggleNightMode}></IconNightMode>
      <IconFontSize increaseFont={true} onClick={props.increaseFont}></IconFontSize>
      <IconFontSize increaseFont={false} onClick={props.decreaseFont}></IconFontSize>
      {state?.fontModifier !== 1 && <IconFontSize defaultSize onClick={props.defaultFont}></IconFontSize>}
    </SidebarSetupContent>
  )
}
