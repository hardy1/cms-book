import * as React from 'react'
import { render } from '@testing-library/react'
import BooksToolbar from './BooksToolbar'
import { mockBook } from '../../resources/mocks/book'
import { mockCurrent } from '../../resources/mocks/current'

describe('BooksToolbar', () => {
  it('SHOULD render correctly', () => {
    const component = render(<BooksToolbar book={mockBook} current={mockCurrent} />)

    expect(component).toBeTruthy()
  })
})
