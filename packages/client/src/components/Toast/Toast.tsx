import React from 'react'
import { Alert } from 'designsystem'
import { render, unmountComponentAtNode } from 'react-dom'
import styled from 'styled-components'
import { AlertProps } from 'designsystem/dist/components/Alert/Alert'

const Toast: React.FC<AlertProps> = (props) => <Alert {...props} />

export default Toast

export const ToastContainer = styled.div`
  position: fixed;
  z-index: 2;
  inset: 2rem;
  height: fit-content;
  display: flex;
  justify-content: flex-end;
`

export const toastSetup = {
  containerId: 'toast-container',
  duration: 6,
}

interface ToastOptions {
  type?: 'success' | 'warning' | 'danger'
  close?: boolean
  target?: string
  duration?: number
}

export const toast = {
  notify: (message: string, options?: ToastOptions) => {
    const target: string = options?.target ? options?.target : toastSetup.containerId
    const type: 'success' | 'warning' | 'danger' = options?.type ? options?.type : 'success'
    const close: boolean = options?.close ? options?.close : undefined
    const duration: number = options?.duration || options?.duration === 0 ? options?.duration : toastSetup.duration

    if (typeof document !== 'undefined') {
      const instance = document.getElementById(target)
      if (close) {
        render(
          <Toast
            text={message}
            type={type}
            buttonClose
            onClick={() => {
              unmountComponentAtNode(instance)
            }}
          />,
          instance,
        )
      } else {
        render(<Toast text={message} type={type} />, instance)
      }

      if (duration) {
        setTimeout(() => {
          unmountComponentAtNode(instance)
        }, duration * 1000)
      }
    }
  },
  destroy: (containerId?: string) => {
    const target: string = containerId || toastSetup.containerId
    if (typeof document !== 'undefined') {
      const instance = document.getElementById(target)
      instance && unmountComponentAtNode(instance)
    }
  },
}
