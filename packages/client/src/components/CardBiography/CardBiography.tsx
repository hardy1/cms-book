import React from 'react'
import { CollaboratorType } from '../../types'
import CardBiographyContainer from './CardBiography.style'

interface CardBiographyProps {
  collaborators: CollaboratorType[]
  fontSizeModifier?: number
  nightMode?: boolean
}

const CardBiography: React.FC<CardBiographyProps> = ({
  collaborators,
  fontSizeModifier,
  nightMode,
}: CardBiographyProps) => {
  return (
    <CardBiographyContainer fontSizeModifier={fontSizeModifier} nightMode={nightMode}>
      {collaborators?.map((collaborator) => (
        <div key={collaborator?.info?.id} className={`Card ${collaborator?.info?.avatar?.uri ? '' : 'CardNoImage'}`}>
          {collaborator?.info?.avatar?.uri ? (
            <img src={collaborator?.info?.avatar?.uri} className="Card__Image" alt="Avatar" />
          ) : (
            ''
          )}
          <h3 className="Card__Title">
            {collaborator?.info?.first_name} {collaborator?.info?.last_name}
          </h3>
          {collaborator?.info?.biography ? <p className="Card__Text">{collaborator?.info?.biography}</p> : ''}
        </div>
      ))}
    </CardBiographyContainer>
  )
}

export default CardBiography
