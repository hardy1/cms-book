import styled from 'styled-components'
import colors from '../../resources/colors'

interface CardBiographyContainerProps {
  colorTheme?: string
  fontSizeModifier?: number
  nightMode?: boolean
}

const CardBiographyContainer = styled.div<CardBiographyContainerProps>`
  .Card {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    background-color: ${({ nightMode }) => (nightMode ? colors.neutralColor : colors.lightColor)};
    border-radius: 5px;
    box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.25);
    padding-top: 30px;
    padding-left: 20px;
    padding-bottom: 50px;
    padding-right: 30px;
    margin-bottom: 16px;

    + .CardTitle {
      padding-top: 16px;

      @media screen and (min-width: 960px) {
        padding-top: 50px;
      }
    }

    @media screen and (min-width: 960px) {
      padding-left: 155px;
    }

    &__Image {
      width: 100px;
      height: 100px;
      border-radius: 50%;
      object-fit: cover;

      @media screen and (min-width: 960px) {
        position: absolute;
        top: 40px;
        left: 30px;
      }
    }

    &.CardNoImage {
      flex-direction: column;

      @media screen and (min-width: 960px) {
        padding-left: 30px;
        padding-bottom: 30px;
      }
    }

    &__Title {
      min-height: 100px;
      display: inline-flex;
      align-items: center;
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 18}px`};
      line-height: 24px;
      padding-left: 16px;

      @media screen and (min-width: 960px) {
        min-height: initial;
        font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 24}px`};
        line-height: 32px;
        padding-left: 0;
      }
    }

    &__Text {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.black)};
      font-family: 'Montserrat';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 14}px`};
      font-weight: 400;
      line-height: 26px;
      letter-spacing: 5%;
      padding-top: 30px;
    }
  }
`

export default CardBiographyContainer
