import Cookies from 'js-cookie'

export const setCookiesKey = (key: string, value: any): void => {
  let currentValue
  clearCookiesByKey(key)

  if (typeof value === 'object') {
    currentValue = JSON.stringify(value)
  } else {
    currentValue = value
  }

  if (typeof window !== 'undefined') {
    Cookies.set(key, currentValue)
  } else {
    return null
  }
}

export const getCookiesKey = (key: string): any => {
  let selectedKey

  if (typeof window !== 'undefined') {
    selectedKey = Cookies.get(key)
  } else {
    return null
  }

  if (selectedKey?.includes('{"')) {
    return JSON.parse(selectedKey)
  } else {
    return selectedKey
  }
}

export const clearCookiesByKey = (key: string): void => {
  if (typeof window !== 'undefined') {
    Cookies.remove(key)
  } else {
    return null
  }
}

export const clearCookies = (cookiesToClear: string[]): void => {
  if (typeof window !== 'undefined') {
    cookiesToClear.forEach((cookie) => Cookies.remove(cookie))
  } else {
    return null
  }
}

export const setLocalKey = (key: string, value: any): void => {
  let currentValue
  clearLocalByKey(key)

  if (typeof value === 'object') {
    currentValue = JSON.stringify(value)
  } else {
    currentValue = value
  }

  if (typeof window !== 'undefined') {
    localStorage.setItem(key, currentValue)
  } else {
    return null
  }
}

export const getLocalKey = (key: string): any => {
  let selectedKey

  if (typeof window !== 'undefined') {
    selectedKey = localStorage.getItem(key)
  } else {
    return null
  }

  if (selectedKey?.includes('{"')) {
    return JSON.parse(selectedKey)
  } else {
    return selectedKey
  }
}

export const clearLocalByKey = (key: string): void => {
  if (typeof window !== 'undefined') {
    Cookies.remove(key)
  } else {
    return null
  }
}

export const clearLocal = (): void => {
  if (typeof window !== 'undefined') {
    localStorage.clear()
  } else {
    return null
  }
}
