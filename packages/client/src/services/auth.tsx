import { apiCall } from './api'
import {
  ILoginRequestBodyMap,
  ILogin,
  ILoginResponseMap,
  IRefreshToken,
  GenericResponse,
  IPasswordReset,
  IPasswordResetMap,
  IPasswordResetResponseMap,
} from '../types'
import { AxiosResponse } from 'axios'

export default async function login(body: ILoginRequestBodyMap): Promise<ILogin> {
  const source = '/api/account/login'

  const response: AxiosResponse<ILoginResponseMap> = await apiCall({
    method: 'POST',
    url: source,
    data: JSON.stringify(body),
  })

  if (!response?.data?.status) {
    throw new Error(response?.data?.message)
  }

  return {
    token: response?.data?.payload?.token,
    nextStep: response?.data?.payload?.next_step,
    user: response?.data?.payload?.user,
  }
}

export async function refreshToken(body: IRefreshToken): Promise<IRefreshToken> {
  const source = 'api/auth/refresh-token'

  try {
    const response: AxiosResponse<IRefreshToken> = await apiCall({
      method: 'POST',
      url: source,
      data: JSON.stringify(body),
    })

    return response?.data
  } catch (e) {
    if ([403].find((item) => item === e?.response?.status)) {
      await logout()
    }
    throw new Error(e)
  }
}

export async function logout(): Promise<GenericResponse> {
  const source = 'api/auth/logout'

  const response: AxiosResponse<GenericResponse> = await apiCall({
    method: 'POST',
    url: source,
  })

  if (typeof window !== undefined) {
    window.location.pathname = '/'
  }
  return response?.data
}

export const updatePassword = async (body: IPasswordReset) => {
  const { password, confirmPassword, oldPassword } = body
  const source = 'proxy/api/account/update-password'
  const mappedData: IPasswordResetMap = {
    new_password: password,
    confirm_password: confirmPassword,
    current_password: oldPassword,
  }

  const response: AxiosResponse<IPasswordResetResponseMap> = await apiCall({
    method: 'put',
    url: source,
    data: mappedData,
  })

  return response?.data?.payload
}

export const forgotPassword = async (body: { email: string }) => {
  const { email } = body
  const source = 'proxy/api/auth/forgot-password'
  const mappedData = { email }

  const response: AxiosResponse<IPasswordResetResponseMap> = await apiCall({
    method: 'POST',
    url: source,
    data: mappedData,
  })

  return response
}

export const resetPassword = async (body: { password: string; code: string | string[] }) => {
  const { password, code } = body
  const source = 'proxy/api/account/reset-password'
  const mappedData = {
    password,
    reset_password_code: code,
  }

  const response: AxiosResponse<IPasswordResetResponseMap> = await apiCall({
    method: 'POST',
    url: source,
    data: mappedData,
  })

  return response
}
