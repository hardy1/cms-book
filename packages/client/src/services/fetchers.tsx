import fetcher from '../hooks/api/fetcher'
import { serverURLPath } from '../lib/config'
import { BookFullType, ChapterType, PartType } from '../types'

type fetchBookProps = {
  bookSlug: string | string[]
  partSlug?: string | string[]
  chapterSlug?: string | string[]
}

const fetchBooks = async () => {
  const booksResponse = await fetcher(`${serverURLPath}/api/books`)
  const bookData: BookFullType[] = booksResponse
  return bookData
}

const fetchBook = async ({ bookSlug }: fetchBookProps) => {
  const booksResponse = await fetcher(`${serverURLPath}/api/books/${bookSlug}`)
  const bookData: BookFullType = booksResponse
  return { book: bookData, current: { bookSlug } }
}

const fetchPart = async ({ bookSlug, partSlug }: fetchBookProps) => {
  const data: PartType = await fetcher(`${serverURLPath}/api/books/${bookSlug}/${partSlug}`)
  const partData: PartType = data
  return { part: partData, current: { bookSlug, partSlug } }
}

const fetchChapter = async ({ bookSlug, partSlug, chapterSlug }: fetchBookProps) => {
  const data: ChapterType = await fetcher(`${serverURLPath}/api/books/${bookSlug}/${partSlug}/${chapterSlug}`)
  const chapterData: ChapterType = data

  return { chapter: chapterData, current: { bookSlug, partSlug, chapterSlug } }
}

const fetchToolbar = async ({ bookSlug }: fetchBookProps) => {
  const data = await fetchBook({ bookSlug })
  const toolbarData: BookFullType = data && data.book
  return toolbarData
}

const fetchBookCommons = async ({ bookSlug, partSlug, chapterSlug }: fetchBookProps) => {
  let bookCommonsData
  let bookCommonsToolbar

  if (bookSlug && partSlug && chapterSlug) {
    bookCommonsData = await fetchChapter({ bookSlug, partSlug, chapterSlug })
    bookCommonsToolbar = await fetchToolbar({ bookSlug })
    return { ...bookCommonsData, toolbarData: bookCommonsToolbar }
  } else if (bookSlug && partSlug) {
    bookCommonsData = await fetchPart({ bookSlug, partSlug })
    bookCommonsToolbar = await fetchToolbar({ bookSlug })
    return { ...bookCommonsData, toolbarData: bookCommonsToolbar }
  } else if (bookSlug) {
    bookCommonsData = await fetchBook({ bookSlug })
    return bookCommonsData
  }
}

const fetchCommon = async () => {
  const CommonData = await fetcher(`${serverURLPath}/api/common`)
  return CommonData
}

export { fetchBookCommons, fetchCommon, fetchBooks }
