export const setLocalKey = (key: string, value: any): void => {
  let currentValue
  clearLocalByKey(key)

  if (typeof value === 'object') {
    currentValue = JSON.stringify(value)
  } else {
    currentValue = value
  }

  if (typeof window !== 'undefined') {
    localStorage.setItem(key, currentValue)
  } else {
    return null
  }
}

export const getLocalKey = (key: string): any => {
  let selectedKey

  if (typeof window !== 'undefined') {
    selectedKey = localStorage.getItem(key)
  } else {
    return null
  }

  if (selectedKey?.includes('{"')) {
    return JSON.parse(selectedKey)
  } else {
    return selectedKey
  }
}

export const clearLocalByKey = (key: string): void => {
  if (typeof window !== 'undefined') {
    localStorage.removeItem(key)
  } else {
    return null
  }
}

export const clearLocal = (): void => {
  if (typeof window !== 'undefined') {
    localStorage.clear()
  } else {
    return null
  }
}
