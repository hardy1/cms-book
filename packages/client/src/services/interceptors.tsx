import { AxiosInstance } from 'axios'
import { getSession } from 'next-auth/react'

export const interceptedApi = async (httpResolver: AxiosInstance) => {
  const session = await getSession()
  const token = session?.user?.token

  httpResolver.interceptors.request.use(
    (config) => {
      return token
        ? {
            ...config,
            headers: {
              ...config.headers,
              common: {
                ...config.headers.common,
                Authorization: `Bearer ${token}`,
              },
            },
          }
        : config
    },
    (error) => {
      return Promise.reject(error.response?.data?.error)
    },
  )

  httpResolver.interceptors.response.use(
    (response) => {
      return response
    },
    async (error) => {
      let errorMessage = error.response?.data?.message || error.response?.data?.error || error.message
      if (error.response?.status === 401) {
        errorMessage = 'UNAUTHORIZED'
      }
      if (error.response?.data.code === 403 || error.response?.data.code === 4033) {
        return Promise.reject(error.response.data)
      }
      return Promise.reject(errorMessage)
    },
  )
}
