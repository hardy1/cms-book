import axios, { AxiosInstance } from 'axios'
import { serverURLPath } from '../lib/config'
import { interceptedApi } from './interceptors'

export const apiCall: AxiosInstance = axios.create({
  baseURL: serverURLPath,
})

apiCall.defaults.headers.post['Content-Type'] = 'application/json'

interceptedApi(apiCall)
