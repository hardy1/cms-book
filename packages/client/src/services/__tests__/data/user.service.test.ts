import { apiCall } from '../../api'
import MockAdapter from 'axios-mock-adapter'
import { userRegistration } from '../../data'

jest.mock('next-auth/react')
describe('User data service', () => {
  beforeAll(() => {
    process.env.NEXTAUTH_URL = 'http://localhost:3000'
  })

  const axiosMock = new MockAdapter(apiCall)
  it('userRegistration remaps data before call', () => {
    userRegistration({
      name: 'test name',
      email: 'test@test.com',
      phone: '+1199999999',
      password: '11223344',
      confirmPassword: '11223344',
      termsOfUse: true,
      language: 'pt_BR',
      country: 'BR',
      cpfDocument: '99988877733',
      optIn: true,
      city: 'Curitiba',
      state: 'HL',
    })
    axiosMock.onPost(
      '/proxy/api/account/register',
      {
        name: 'test name',
        email: 'test@test.com',
        phone: '+1199999999',
        password: '11223344',
        password_confirmation: '11223344',
        terms_and_conditions_accepted: 'true',
        language: 'pt_BR',
        document: '99988877733',
        optin_news: 'true',
        address: {
          city: 'Curitiba',
          state: 'HL',
          country: 'BR',
        },
      },
      expect.objectContaining({
        payload: null,
        status: true,
        code: 0,
        message: 'Registro realizado com sucesso.',
        timestamp: 1658074413,
      }),
    )
  })
})
