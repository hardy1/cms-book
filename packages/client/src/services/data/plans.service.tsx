import { internalServerURLPath } from '../../lib/config'
import { AxiosResponse } from 'axios'
import { apiCall } from '..'
import { IPlansResponseMap, IPlansByPeriodMap, IPeriodListMap } from '../../types/plan-profile.model'
import colors from '../../resources/colors'
import { formatCurrency } from '../../../utils/currecyParser'
import { IPlan, IPlanComparisonMap } from '../../types'

export const getPlans = async () => {
  const source = '/proxy/api/checkout/plans'

  try {
    const response: AxiosResponse<IPlansResponseMap> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const { payload } = response?.data

    const mappedResponse = payload?.map((plan) => ({
      id: plan?.id,
      name: plan?.name,
      code: plan?.code,
      active: plan?.active,
      categoryId: plan?.category_id,
      description: plan?.description,
      offer: plan?.offer,
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getPlansByPeriod = async (id: number) => {
  const source = `/proxy/api/checkout/periods/${id}`
  const colorsRemap = ['#66B8FF', '#144590', '#216DE1']

  try {
    const response: AxiosResponse<IPlansByPeriodMap> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const { payload } = response?.data

    const mappedResponse: IPlan[] = payload?.map((plan, index) => ({
      id: plan?.id,
      name: plan?.name,
      slug: plan?.slug,
      code: plan?.code,
      months: plan?.months,
      priceValue: formatCurrency(plan?.price_value),
      monthlyPriceValue: formatCurrency(plan?.monthly_price_value),
      offerId: plan.offer_id,
      product: plan?.product,
      period: plan?.period,
      color: plan?.color || colorsRemap[index] || colors.primaryColor,
      comparison: plan?.comparison?.map((item: IPlanComparisonMap) => ({
        name: item.name,
        periodId: item.period_id,
        months: item.months,
        monthlyPriceValue: formatCurrency(item.monthly_price_value),
        monthlyDiffPercent: item.monthly_diff_percent,
        priceValue: formatCurrency(item.price_value),
      })),
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getPeriods = async () => {
  const source = '/proxy/api/checkout/periods'

  try {
    const response: AxiosResponse<IPeriodListMap> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const { payload } = response?.data

    return payload
  } catch (e) {
    throw new Error(e.message)
  }
}
