import { AxiosResponse } from 'axios'
import { apiCall } from '..'
import { GenericResponse, IGenericItem, IContactUs } from '../../types'

export const getPost = async (slug: string, isPublic?: boolean, customHeaders?) => {
  const source = `/api/post/${slug}`

  try {
    let headers = isPublic ? { noAuth: 1 } : {}
    headers = { ...headers, ...customHeaders }

    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'get',
      url: source,
      headers: headers,
    })

    const mappedResponse: IGenericItem[] = response?.data?.payload

    return mappedResponse
  } catch (e) {
    throw {
      code: e.code,
    }
  }
}

export const getMedicalFormula = async (slug: string) => {
  const source = `/api/medical-formulas/${slug}`

  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'get',
      url: source,
    })

    const mappedResponse: IGenericItem[] = response?.data?.payload

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const sendContact = async (body: IContactUs) => {
  const source = 'proxy/api/contact-us'

  const response: AxiosResponse<GenericResponse> = await apiCall({
    method: 'post',
    url: source,
    data: body,
  })

  return response.data
}

export const getBooksSearchList = async (path, query, session, page?: number) => {
  let data = []
  await apiCall({
    method: 'get',
    url: `${path}/api/books/search?searchTerm=${query.q}&page=${page ? page : 1}`,
    headers: {
      Authorization: `Bearer ${session?.user?.token}`,
    },
  }).then((response) => {
    page ? (data = response?.data?.payload?.contents?.data) : (data = response.data?.payload)
  })

  return data
}
