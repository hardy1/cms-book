import { apiCall } from '..'
import { AxiosResponse } from 'axios'
import { internalServerURLPath } from '../../lib/config'
import {
  IGenericItem,
  NationalStatesResponse,
  NationalState,
  CountriesResponse,
  LanguagesResponse,
  IProfession,
  ICommons,
  ICommonsMap,
} from '../../types'
import { GenericResponse } from '../../types/generic-response.model'

export const getProfessions = async (useProxy = false): Promise<IProfession[]> => {
  const source = `${useProxy ? 'proxy/' : ''}api/common/professions`

  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'get',
      url: source,
    })

    const mappedResponse: IProfession[] = response?.data?.payload?.map((profession: IProfession) => ({
      ...profession,
      code: profession.code,
      finishedResidencyDate: profession.finishedResidencyDate ?? null,
      formationDate: profession.formationDate ?? null,
      id: profession.id,
      institutions: profession.institutions ?? null,
      name: profession.name,
      register: profession.register ?? null,
      slug: profession.slug,
      specialization: profession.specialization ?? null,
      specializationOther: profession.specializationOther ?? null,
      state: profession.state ?? null,
      status: profession.status ?? null,
      value: profession.id,
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getNationalStates = async () => {
  const source = '/proxy/api/common/states'

  try {
    const response: AxiosResponse<NationalStatesResponse> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const mappedResponse: NationalState[] = response?.data?.payload?.map((state) => ({
      initials: state.sigla,
      name: state.nome,
      capital: state.capital,
      cities: state.cidades,
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getCountries = async () => {
  const source = '/proxy/api/common/countries'

  try {
    const response: AxiosResponse<CountriesResponse> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const mappedResponse: IGenericItem[] = response?.data?.payload?.map((country) => ({
      code: country.code,
      name: country.name,
      value: country.code,
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getLanguages = async () => {
  const source = '/proxy/api/common/languages'

  try {
    const response: AxiosResponse<LanguagesResponse> = await apiCall({
      method: 'get',
      baseURL: internalServerURLPath,
      url: source,
    })

    const mappedResponse: IGenericItem[] = response?.data?.payload?.map((language) => ({
      code: language.code,
      name: language.name,
      value: language.code,
    }))

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}

export const getCommons = async () => {
  const source = 'proxy/api/common'

  try {
    const response: AxiosResponse<ICommonsMap> = await apiCall({
      method: 'get',
      url: source,
    })

    const mappedResponse: ICommons = response?.data?.payload

    return mappedResponse
  } catch (e) {
    throw new Error(e.message)
  }
}
