import pagarme from 'pagarme'
import { apiCall } from '../api'
import { CartMap, ICardInfo, IFormikDataPayment, ICart, GenericResponse } from '../../types'
import { AxiosResponse } from 'axios'

export async function addToCart(
  body: { documentType?: string; promoCode?: string; offerId: string | number },
  useProxy = false,
) {
  const source = `${useProxy ? 'proxy/' : ''}api/checkout/cart/add`
  const data = {}

  enum cartOffer {
    promoCode = 'promo_code',
    offerId = 'offer_id',
    documentType = 'document_type',
  }

  Object.entries(body).map(([key, value]) => (data[cartOffer[key]] = value))

  if (!data['document_type']) {
    data['document_type'] = 'CPF'
  }

  const response: AxiosResponse<CartMap> = await apiCall({
    method: 'POST',
    url: source,
    data: JSON.stringify(data),
  })

  const { payload } = response?.data

  const mappedResponse: ICart = {
    id: payload?.offer?.id,
    active: payload?.product?.active,
    name: payload?.product?.name,
    period: payload?.period?.name,
    description: payload?.product?.description,
    totalPrice: payload?.total_price,
    oldTotalPrice: payload?.old_total_price,
    discountTotalPrice: payload?.discount_total_price,
    isUnlimited: payload?.is_unlimited,
    status: response?.data.status,
    message: response?.data.message,
    discounts: payload?.discounts,
    offer: payload?.offer,
    price_data: payload?.price_data,
  }

  return mappedResponse
}

export async function postOfferCoupon(body: { promoCode: string }, useProxy = false) {
  const source = `${useProxy ? 'proxy/' : ''}api/checkout/coupon/offer`
  const data = { coupon_code: body.promoCode }

  const response: AxiosResponse = await apiCall({
    method: 'POST',
    url: source,
    data: JSON.stringify(data),
  })

  return response?.data?.payload
}

export async function postCouponValidate(body: { promoCode: string }, useProxy = false) {
  const source = `${useProxy ? 'proxy/' : ''}api/checkout/coupon/validate`
  const data = { coupon_code: body.promoCode }

  const response: AxiosResponse = await apiCall({
    method: 'POST',
    url: source,
    data: JSON.stringify(data),
  })

  return response?.data
}

export async function getCart(useProxy = false) {
  const source = `${useProxy ? 'proxy/' : ''}api/checkout/cart`

  const response: AxiosResponse<GenericResponse> = await apiCall({
    method: 'GET',
    url: source,
  })

  const { payload } = response?.data

  return {
    id: payload?.offer?.id,
    active: payload?.product?.active,
    name: payload?.product?.name,
    period: payload?.offer?.subscription_period?.name,
    description: payload?.product?.description,
    totalPrice: payload?.total_price,
    oldTotalPrice: payload?.old_total_price,
    discountTotalPrice: payload?.discount_total_price,
    offer: payload?.offer,
  }
}

export async function getOfferPeriod(offerId: string, useProxy = false) {
  const source = useProxy ? `proxy/api/checkout/offerperiod/${offerId}` : `api/checkout/offerperiod/${offerId}`

  const response: AxiosResponse<GenericResponse> = await apiCall({
    method: 'GET',
    url: source,
  })

  const { payload } = response?.data

  return {
    monthly_price: payload.comparison[0].monthly_price_value,
  }
}

export async function checkoutByBankSlip(data: IFormikDataPayment) {
  const source = 'proxy/api/checkout'
  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'POST',
      url: source,
      data: data,
    })

    return response
  } catch (error) {
    throw new Error(error.message)
  }
}

export async function checkoutByCreditCard(data: IFormikDataPayment, cardInfo: ICardInfo) {
  const { cardCVV, cardName, cardNumber, cardValidationMonth, cardValidationYear } = cardInfo

  const expirationDate = `${cardValidationMonth}${cardValidationYear.slice(-2)}`

  const card = {
    card_number: cardNumber,
    card_holder_name: cardName,
    card_expiration_date: expirationDate,
    card_cvv: cardCVV,
  }

  try {
    const pagarmeClient = await pagarme.client.connect({ encryption_key: 'ek_test_nRHaMyhpw5AI6t2ecT7CEGmG85NIQ8' })
    const card_hash = await pagarmeClient.security.encrypt(card)

    // payload call
    const source = 'proxy/api/checkout'
    const response = await apiCall({
      method: 'POST',
      url: source,
      data: {
        ...data,
        card_hash: card_hash,
      },
    })

    if (response?.data?.status === false) throw new Error(response?.data?.message)

    return response
  } catch (error) {
    throw new Error(error.message)
  }
}
