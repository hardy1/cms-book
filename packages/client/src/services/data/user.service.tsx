import { apiCall } from '..'
import { AxiosResponse } from 'axios'
import { internalServerURLPath } from '../../lib/config'
import { createFormData, destructureArrayToObject } from '../../../utils/formDataParser'
import { clearLocalByKey } from '../browser-storage'
import { storage } from '../../enums'
import {
  UserRegistration,
  UserRegistrationMap,
  GenericResponse,
  UserPersonalData,
  UserPersonalDataMap,
  IProfession,
  IUser,
  IUserProfessionalData,
  IUserProfessionalDataMap,
  IUserResponseMap,
  ISubscription,
  IAddress,
  IGateway,
  UserPreferences,
  UserPreferencesMap,
} from '../../types'
import { datePipe } from '../../../utils'

export const userRegistration = async ({
  name,
  email,
  phone,
  password,
  confirmPassword,
  termsOfUse,
  language,
  country,
  cpfDocument,
  optIn,
  city,
  state,
}: UserRegistration) => {
  const source = '/proxy/api/account/register'
  const mappedData: UserRegistrationMap = {
    name,
    email,
    phone,
    password,
    password_confirmation: confirmPassword,
    terms_and_conditions_accepted: termsOfUse.toString(),
    optin_news: optIn.toString(),
    language,
    document: cpfDocument,
    address: {
      country,
      city,
      state,
    },
  }

  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'post',
      baseURL: internalServerURLPath,
      url: source,
      data: mappedData,
    })

    return response?.data
  } catch (e) {
    throw new Error(e.message)
  }
}

export const userPersonalDataUpdate = async (body: UserPersonalData) => {
  const {
    name,
    email,
    phone,
    mobile,
    state,
    city,
    zipcode,
    country,
    street,
    number,
    extention,
    cpfDocument,
    language,
  } = body
  const source = '/proxy/api/account/update/personaldata'
  const mappedData: UserPersonalDataMap = {
    name,
    email,
    phone,
    cellphone: mobile,
    address: {
      country,
      postcode: zipcode,
      city,
      state,
      street,
      house: number,
      address1: extention,
    },
    document: cpfDocument,
    language,
  }

  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'put',
      baseURL: internalServerURLPath,
      url: source,
      data: mappedData,
    })

    clearLocalByKey(storage.USER)

    return response?.data
  } catch (e) {
    throw new Error(e.message)
  }
}

export const userProfessionalDataUpdate = async (body: IUserProfessionalData) => {
  const {
    profession,
    specialization,
    specializationCustom,
    state,
    formationDate,
    finishedResidencyDate,
    professionRegister,
    professionStatus,
    institutions,
    document,
  } = body
  const source = '/proxy/api/account/update/professiondata'

  const mappedData: IUserProfessionalDataMap = {
    profession_id: profession,
    specialization_id: specialization,
    specialization_other_value: specializationCustom,
    formation_date_at: formationDate,
    resident_finished_at: finishedResidencyDate,
    profession_state: state,
    profession_register: professionRegister,
    profession_status: professionStatus,
    document: document && document[0],
  }

  const handleMappedData = Object.keys(mappedData).reduce((acc, key) => {
    const _acc = acc
    if (mappedData[key] !== undefined) _acc[key] = mappedData[key]
    return _acc
  }, {})

  const mappedArrays = institutions?.map((institution) => ({
    name: institution.institution,
    institution_type_id: institution.institutionType,
  }))

  const parsedArrays = destructureArrayToObject(mappedArrays, 'institutions')

  const mappeDataParsedArrays = Object.assign(handleMappedData, parsedArrays)

  const parsedFormData = createFormData(mappeDataParsedArrays)

  try {
    const response: AxiosResponse<GenericResponse> = await apiCall({
      method: 'post',
      url: source,
      headers: {
        'Content-type': 'multipart/form-data',
      },
      data: parsedFormData,
    })

    clearLocalByKey(storage.USER)

    return response?.data
  } catch (e) {
    throw new Error(e.message)
  }
}

export const userProfileInfo = async () => {
  const source = '/api/account/me'
  const response: AxiosResponse<IUserResponseMap> = await apiCall({
    method: 'get',
    url: source,
  })

  const professionDataMapped: IProfession = response?.data?.payload?.profession_data?.profession && {
    id: response?.data?.payload?.profession_data?.profession?.id,
    slug: response?.data?.payload?.profession_data?.profession?.slug,
    name: response?.data?.payload?.profession_data?.profession?.name,
    code: response?.data?.payload?.profession_data?.profession?.code,
    specialization: {
      professionId: response?.data?.payload?.profession_data?.specialization?.profession_id || null,
      id: response?.data?.payload?.profession_data?.specialization?.id || null,
      name: response?.data?.payload?.profession_data?.specialization?.name || null,
      slug: response?.data?.payload?.profession_data?.specialization?.slug || null,
    },
    specializationOther: response?.data?.payload?.profession_data?.specialization_other_value,
    state: response?.data?.payload?.profession_data?.profession_state,
    register: response?.data?.payload?.profession_data?.profession_register,
    status: response?.data?.payload?.profession_data?.profession_status,
    formationDate: response?.data?.payload?.profession_data?.formation_date_at,
    finishedResidencyDate: response?.data?.payload?.profession_data?.resident_finished_at,
    institutions: response?.data?.payload?.profession_data?.institutions?.map((institution) => ({
      institution: institution.name?.toString(),
      institutionType: institution.institution_type_id?.toString(),
    })),
    proofDocument: {
      id: response?.data?.payload?.profession_data?.proof_document?.id,
    },
  }

  const gatewayMapped: IGateway = {
    plan: response?.data?.payload?.subscription?.gateway?.plan,
    paymentMethod: response?.data?.payload?.subscription?.gateway?.payment_method,
    cardLastDigits: response?.data?.payload?.subscription?.gateway?.card_last_digits,
    currentTransaction: {
      status: response?.data?.payload?.subscription?.gateway?.status,
      boletoUrl: response?.data?.payload?.subscription?.gateway?.current_transaction?.boleto_url || null,
    },
  }

  const subscriptionMapped: ISubscription = response.data?.payload?.subscription && {
    id: response?.data?.payload?.subscription?.id,
    expirationDate: datePipe(response?.data?.payload?.subscription?.expire_at),
    isActive: response?.data?.payload?.subscription?.is_active,
    gateway: gatewayMapped,
    product: response?.data?.payload?.subscription?.product,
    plan: response?.data?.payload?.subscription?.plan,
  }

  const addressMapped: IAddress = response?.data?.payload?.address && {
    id: response?.data?.payload?.address?.id,
    city: response?.data?.payload?.address?.city,
    state: response?.data?.payload?.address?.state,
    street: response?.data?.payload?.address?.street,
    number: response?.data?.payload?.address?.house,
    country: response?.data?.payload?.address?.country,
    zipcode: response?.data?.payload?.address?.postcode,
    extention: response?.data?.payload?.address?.address1,
  }

  const mappedResponse: IUser = {
    address: addressMapped,
    id: response?.data?.payload?.id,
    name: response?.data?.payload?.name,
    email: response?.data?.payload?.email,
    phone: response?.data?.payload?.phone,
    mobile: response?.data?.payload?.cellphone,
    language: response?.data?.payload?.language,
    isFirstaccess: response?.data?.payload?.is_firstaccess,
    cpfDocument: response?.data?.payload?.document,
    subscription: subscriptionMapped,
    profession: professionDataMapped,
    permissions: response?.data?.payload?.permissions,
    termsOfUse: response?.data?.payload?.terms_at,
    optIn: response?.data?.payload?.optin_news,
  }

  return mappedResponse
}

export const userPreferences = async (body: UserPreferences) => {
  const { optIn } = body
  const source = '/proxy/api/account/update/preferencesdata'
  const mappedData: UserPreferencesMap = {
    optin_news: optIn,
  }

  const response: AxiosResponse<GenericResponse> = await apiCall({
    method: 'put',
    baseURL: internalServerURLPath,
    url: source,
    data: mappedData,
  })

  return response?.data
}
