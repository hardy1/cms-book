export interface IAddress {
  id?: number
  type?: string
  country?: string
  zipcode?: string
  city?: string
  state?: string
  street?: string
  number?: string
  extention?: string
}

export interface IAddressMap {
  id?: number
  type?: string
  country?: string
  postcode?: string
  city?: string
  state?: string
  street?: string
  house?: string
  building?: string
  flat?: string
  floor?: string
  address1?: string
  address2?: string
}
