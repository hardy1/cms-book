export interface UserProfile {
  id: number
  name: string
  code: string
  description?: string
  color?: string
  icon?: string
}
