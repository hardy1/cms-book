export interface GenericFormSection {
  id?: number
  dynamicFormik?: any
  formik?: any
  formType?: string
}
