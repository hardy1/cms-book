export interface Avatar {
  id?: number
  title?: string
  description?: string
  disk_name?: string
  file_name?: string
  file_size?: number
  content_type?: string
  field?: string
  str_attachment_id?: string
  sort_order?: number
  created_at?: string
  updated_at?: string
  path?: string
  extension?: string
  uri?: string
}
