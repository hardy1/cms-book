import { NextPage } from 'next'

// export declare type NextPageWithAuth<P = {}, IP = P> = NextPage<P, IP> & {
//   auth: boolean
// }
// export declare type NextComponentType<C extends BaseContext = NextPageContext, IP = {}, P = {}> = ComponentType<P> & {
export declare type NextPageWithAuth<P = unknown, IP = P> = NextPage<P, IP> & {
  auth: boolean
}
