import { ErrorResponse } from './error-response'

export interface GenericResponse {
  payload: any
  status: boolean
  code: number
  message: string
  timestamp: number
  error?: ErrorResponse
}
