import NextAuth, { DefaultSession } from 'next-auth'
import { DefaultJWT } from 'next-auth/jwt'
import { IGenericItem } from './shared.model'

declare module 'next-auth' {
  interface Session {
    user: {
      role?: string | null
      token?: string | null
      permissions?: [] | null
    } & DefaultSession['user']
    expires: ISODateString
  }

  interface JWT extends DefaultJWT {
    jwt?: string | null
    error?: string | null
    role?: IGenericItem | string | null
    permissions?: [] | null
    accessTokenExpires?: string | null
  }
}
