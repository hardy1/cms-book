import { IPlan } from './plan.model'

export interface IPeriod {
  id?: number
  name?: string
  period?: string
}

export interface IPeriodMap {
  id: number
  name: string
  period: string
  plans: IPlan[]
}
