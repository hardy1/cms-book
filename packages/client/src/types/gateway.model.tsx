export interface IGatewayMap {
  object?: string
  plan?: {
    object?: string
    id?: number
    amount?: number
    days?: number
    name?: string
    trial_days?: number
    date_created?: string
    payment_methods?: string[]
    color?: string
    charges?: string
    installments?: number
    invoice_reminder?: string
    payment_deadline_charges_interval?: number
  }
  id?: number
  current_transaction?: {
    object?: string
    status?: string
    refuse_reason?: string
    status_reason?: string
    acquirer_response_code?: string
    acquirer_name?: string
    acquirer_id?: string
    authorization_code?: string
    soft_descriptor?: string
    tid?: number
    nsu?: number
    date_created?: string
    date_updated?: string
    amount?: number
    authorized_amount?: number
    paid_amount?: number
    refunded_amount?: number
    installments?: number
    id?: number
    cost?: number
    card_holder_name?: string
    card_last_digits?: string
    card_first_digits?: string
    card_brand?: string
    card_pin_mode?: string
    card_magstripe_fallback?: boolean
    cvm_pin?: boolean
    postback_url?: string
    payment_method?: string
    capture_method?: string
    antifraud_score?: number
    boleto_url?: string
    boleto_barcode?: string
    boleto_expiration_date?: string
    referer?: string
    ip?: string
    subscription_id?: number
    metadata?: {
      id?: number
    }
    //antifraud_metadata?: {}
    reference_key?: string
    device?: string
    local_transaction_id?: number
    local_time?: string
    fraud_covered?: boolean
    fraud_reimbursed?: string
    order_id?: number
    risk_level?: string
    receipt_url?: string
    payment?: string
    addition?: string
    discount?: string
    private_label?: string
    pix_qr_code?: string
    pix_expiration_date?: string
  }
  postback_url?: string
  payment_method?: string
  card_brand?: string
  card_last_digits?: string
  current_period_start?: string
  current_period_end?: string
  charges?: number
  soft_descriptor?: string
  status?: string
  date_created?: string
  date_updated?: string
  phone?: {
    object?: string
    ddi?: string
    ddd?: string
    number?: string
    id?: number
  }
  address?: string
  customer?: {
    object?: string
    id?: number
    external_id?: string
    type?: string
    country?: string
    document_number?: string
    document_type?: string
    name?: string
    email?: string
    phone_numbers?: string
    born_at?: string
    birthday?: string
    gender?: string
    date_created?: string
    documents?: []
  }
  card?: string
  metadata?: {
    id?: number
  }
  //fine?: {}
  //interest?: {}
  settled_charges?: string
  manage_token?: string
  manage_url?: string
}

export interface IGateway {
  object?: string
  plan?: {
    trial_days?: number
    object?: string
    id?: number
    amount?: number
    days?: number
    name?: string
    trialDays?: number
    dateCreated?: string
    paymentMethods?: [string]
    color?: string
    charges?: string
    installments?: number
    invoiceReminder?: string
    paymentDeadlineChargesInterval?: number
  }
  id?: number
  currentTransaction?: {
    object?: string
    status?: string
    refuseReason?: string
    statusReason?: string
    acquirerResponseCode?: string
    acquirerName?: string
    acquirerId?: string
    authorizationCode?: string
    softDescriptor?: string
    tid?: number
    nsu?: number
    dateCreated?: string
    dateUpdated?: string
    amount?: number
    authorizedAmount?: number
    paidAmount?: number
    refundedAmount?: number
    installments?: number
    id?: number
    cost?: number
    cardHolderName?: string
    cardLastDigits?: string
    cardFirstDigits?: string
    cardBrand?: string
    cardPinMode?: string
    cardMagstripeFallback?: boolean
    cvmPin?: boolean
    postbackUrl?: string
    paymentMethod?: string
    captureMethod?: string
    antifraudScore?: number
    boletoUrl?: string
    boletoBarcode?: string
    boletoExpirationDate?: string
    referer?: string
    ip?: string
    subscriptionId?: number
    metadata?: {
      id?: number
    }
    //antifraudMetadata?: {}
    referenceKey?: string
    device?: string
    localTransactionId?: number
    localTime?: string
    fraudCovered?: boolean
    fraudReimbursed?: string
    orderId?: number
    riskLevel?: string
    receiptUrl?: string
    payment?: string
    addition?: string
    discount?: string
    privateLabel?: string
    pixQrCode?: string
    pixExpirationDate?: string
  }
  postbackUrl?: string
  paymentMethod?: string
  cardBrand?: string
  cardLastDigits?: string
  currentPeriodStart?: string
  currentPeriodEnd?: string
  charges?: number
  softDescriptor?: string
  status?: string
  dateCreated?: string
  dateUpdated?: string
  phone?: {
    object?: string
    ddi?: string
    ddd?: string
    number?: string
    id?: number
  }
  address?: string
  customer?: {
    object?: string
    id?: number
    externalId?: string
    type?: string
    country?: string
    documentNumber?: string
    documentType?: string
    name?: string
    email?: string
    phoneNumbers?: string
    bornAt?: string
    birthday?: string
    gender?: string
    dateCreated?: string
    documents?: []
  }
  card?: string
  metadata?: {
    id?: number
  }
  //fine?: {}
  //interest?: {}
  settledCharges?: string
  manageToken?: string
  manageUrl?: string
}
