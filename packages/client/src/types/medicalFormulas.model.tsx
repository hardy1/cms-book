export interface MedicalFormula {
  id: number
  title: string
  created_at: string
  updated_at: string
  content: string
  slug: string
  external_url: string
}
