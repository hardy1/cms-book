import { GenericResponse } from './generic-response.model'
import { IProductMap } from './product.model'
import { IPeriodMap } from './period.model'
import { IPlanMap } from '.'

export interface IPlansResponseMap extends GenericResponse {
  payload: IPlanProfileMap[]
}

export interface IPlanProfileMap {
  id: number
  active?: boolean
  name?: string
  slug?: string
  brand_id?: number
  category_id?: number
  external_id?: string
  code?: string
  preview_text?: string
  description?: string
  deleted_at?: string
  created_at?: string
  updated_at?: string
  is_subscription?: boolean
  vdomah_role_id?: number
  offer?: IProductMap[]
  role?: string
}

export interface IPlansByPeriodMap extends GenericResponse {
  payload: IPlanMap[]
}

export interface IPeriodListMap extends GenericResponse {
  payload: IPeriodMap[]
}
