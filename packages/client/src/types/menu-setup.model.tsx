import { IUser } from '../types'
export interface IInfoBlock {
  [key: string]: IBlockStructure
}

export interface IBlockStructure {
  title?: string
  body?: IBlockValueSet[]
}

export interface IBlockValueSet {
  entryName?: string
  content?: string
  code?: string
}

export interface IBlockListValueSet {
  entryName?: string
  content?: IBlockValueSet[]
  code?: string
}
