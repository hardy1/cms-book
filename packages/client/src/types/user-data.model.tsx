import { GenericResponse } from './generic-response.model'
import { IPermission } from './permission.model'
import { IProfessionMap, IProfession } from '.'
import { ISubscription, ISubscriptionMap } from './subscription.model'
import { IAddress, IAddressMap } from './address.model'

export interface IUserResponseMap extends GenericResponse {
  payload: IUserMap
}

export interface IUserMap {
  id?: number
  name?: string
  email?: string
  phone?: string
  cellphone?: string
  language?: string
  is_firstaccess?: boolean
  document?: string
  address?: IAddressMap
  subscription?: ISubscriptionMap
  profession_data?: IProfessionMap
  permissions?: IPermission[]
  terms_at?: string
  optin_news?: boolean
  is_activated?: boolean
  activated_at?: string
  last_login?: string
  created_at?: string
  updated_at?: string
  username?: string
  surname?: string
  deleted_at?: string
  last_seen?: string
  is_guest?: boolean
  is_superuser?: boolean
  created_ip_address?: string
  last_ip_address?: string
  vdomah_role_id?: number
  biography?: string
  active_currency_code?: string
  coupon?: string
  renovation_at?: string
  resident_finished_at?: string
  trial_ends_at?: string
  subscription_stated_at?: string
  subscription_ends_at?: string
  is_subscriber?: boolean
  eadbox_user_id?: string
  eadbox_authentication_token?: string
  profession_id?: number
  specialization_id?: number
  specialization_other_value?: string
  profession_register?: string
  profession_state?: string
  profession_status?: string
  formation_date_at?: string
  primestart_user_id?: string
}

export interface IUser {
  id?: number
  name?: string
  email?: string
  phone?: string
  mobile?: string
  language?: string
  isFirstaccess?: boolean
  document?: string
  cpfDocument?: string
  address?: IAddress
  subscription?: ISubscription
  profession?: IProfession
  permissions?: IPermission[]
  termsOfUse?: string
  optIn?: boolean
  isActivated?: boolean
  activatedAt?: string
  lastLogin?: string
  createdAt?: string
  updatedAt?: string
  username?: string
  surname?: string
  deletedAt?: string
  lastSeen?: string
  isGuest?: boolean
  isSuperuser?: boolean
  createdIpAddress?: string
  lastIpAddress?: string
  vdomahRoleId?: number
  biography?: string
  activeCurrencyCode?: string
  coupon?: string
  renovationAt?: string
  residentFinishedAt?: string
  trialEndsAt?: string
  subscriptionStatedAt?: string
  subscriptionEndsAt?: string
  isSubscriber?: boolean
  eadboxUserId?: string
  eadboxAuthenticationToken?: string
  professionId?: number
  specializationId?: number
  specializationOtherValue?: string
  professionRegister?: string
  professionState?: string
  professionStatus?: string
  formationDateAt?: string
  primestartUserId?: string
}
