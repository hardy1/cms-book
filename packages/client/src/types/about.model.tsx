import { ICollaboratorByRole } from './collaborator.model'
import { CoverType } from './cover.model'

export interface AboutType {
  id: number
  title: string
  slug?: string
  html_content?: string
  created_at?: string
  updated_at?: string
  cta_text: string
  cta_url: string
  content: string
}

export interface AuthorsBooksType {
  id: number
  name: string
  synopsis: string
  theme_color: string
  cover: CoverType
  collaborators_group: ICollaboratorByRole
}

export interface CollaboratorNameType {
  fullname: string
}

export interface AboutPageType {
  data: AboutType
  books: AuthorsBooksType[]
}

export interface AuthorEditorType {
  data: AuthorsBooksType[]
}
