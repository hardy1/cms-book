import { IUser, IUserMap } from './user-data.model'
import { GenericResponse } from './generic-response.model'

export interface ILoginRequestBodyMap {
  login: string
  password: string
}

export interface ILoginResponseMap extends GenericResponse {
  payload: {
    token: string
    next_step?: string
    user: IUserMap
  }
}

export interface ILogin {
  token?: string
  nextStep?: string
  user: IUser
}

export interface IRefreshToken {
  token: string
}
