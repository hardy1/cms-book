import { IInstitutionMap, IInstitution } from './institution.model'

export interface IUserProfessionalData {
  profession?: string
  specialization?: string
  specializationCustom?: string
  state?: string
  formationDate?: string
  finishedResidencyDate?: string
  professionRegister?: string
  professionStatus?: string
  institutions?: IInstitution[]
  document?: File[]
  proofDocument?: {
    id: number
  }
}

export interface IUserProfessionalDataMap {
  profession_id?: string
  specialization_id?: string
  specialization_other_value?: string
  profession_state?: string
  profession_register?: string
  profession_status?: string
  formation_date_at?: string
  resident_finished_at?: string
  institutions?: IInstitutionMap[]
  document?: File
}
