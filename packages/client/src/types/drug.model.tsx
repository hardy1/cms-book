import { IGenericItem } from './shared.model'

interface DrugPivot {
  category_id: number
  drug_id: number
}

interface DrugCategory extends IGenericItem {
  pivot?: DrugPivot
}

interface DrugTabEntry extends IGenericItem {
  content?: string
  parsedContent?: string | Element | Element[] | JSX.Element | JSX.Element[]
}

interface DrugTab extends IGenericItem {
  entries?: DrugTabEntry[]
}

export interface Drug extends IGenericItem {
  short_name: string
  tabs?: DrugTab[]
  categories?: DrugCategory[]
}
