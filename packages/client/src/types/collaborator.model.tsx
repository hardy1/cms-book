import { Avatar } from './avatar.model'

export interface CollaboratorInfoType {
  id: number
  first_name?: string
  last_name?: string
  firstName?: string
  lastName?: string
  login?: string
  is_superuser?: boolean
  biography?: string
  avatar?: Avatar
  email: string
  permissions: string
  is_activated: boolean
  role_id: number
  activated_at: string
  last_login: string
  created_at: string
  updated_at: string
  deleted_at: string
}

export interface CollaboratorType {
  id?: number
  role?: string
  fullname?: string
  info?: CollaboratorInfoType
}

export interface CollaboratorsGroupType {
  group?: string
  name?: string
  collaborators?: CollaboratorType[]
}

export interface ICollaboratorByRole {
  [key: string]: CollaboratorsGroupType
}
