import { IGenericItem } from '.'
import { IInstitutionMap, IInstitution } from './institution.model'

export interface IProfession extends IGenericItem, IProfessionFormContent {
  specialization?: ISpecialization
  specializationOther?: string
  state?: string
  register?: string
  status?: string
  formationDate?: string
  finishedResidencyDate?: string
  institutions?: IInstitution[]
  specializations?: ISpecialization[]
  document?: File[]
  proofDocument?: {
    id: number
  }
}

export interface IProfessionMap extends IGenericItem, IProfessionFormContentMap {
  profession?: IGenericItem
  specialization?: ISpecializationMap
  specialization_other_value?: string
  profession_state?: string
  profession_register?: string
  profession_status?: string
  formation_date_at?: string
  resident_finished_at?: string
  finishedResidencyDate?: string
  institutions?: IInstitutionMap[]
  document?: File[]
  proof_document?: {
    id?: number
  }
}

interface ISpecializationMap extends IGenericItem {
  profession_id?: number
  pivot?: IProfessionPivotMap
}

export interface ISpecialization extends IGenericItem {
  professionId?: number
  pivot?: IProfessionPivot
}

interface IProfessionFormContent {
  documentDescription?: string
  hasCode?: boolean
  hasDocument?: boolean
  hasEndDate?: boolean
  hasInstitutions?: boolean
  hasState?: boolean
  hasStatusCode?: boolean
  reference?: string
}

interface IProfessionFormContentMap {
  document_description?: string
  has_code?: boolean
  has_document?: boolean
  has_end_date?: boolean
  has_institutions?: boolean
  has_state?: boolean
  has_status_code?: boolean
  reference?: string
}

interface IProfessionPivotMap {
  profession_id?: string
  specialization_id?: string
}

interface IProfessionPivot {
  professionId?: string
  specializationId?: string
}
