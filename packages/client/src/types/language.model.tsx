import { GenericResponse } from './generic-response.model'
import { IGenericItem } from './shared.model'

export interface LanguagesResponse extends GenericResponse {
  payload: IGenericItem[]
}
