import { GenericResponse } from './generic-response.model'

export interface IPasswordReset {
  password: string
  confirmPassword: string
  oldPassword: string
}

export interface IPasswordResetMap {
  new_password: string
  confirm_password: string
  current_password: string
}

export interface IPasswordResetResponseMap extends GenericResponse {
  payload: {
    message?: string
    data?: number
    error?: string
  }
}
