export interface IInstitution {
  id?: number
  userId?: number
  institution: string
  institutionType: string
}

export interface IInstitutionMap {
  name: string
  institution_type_id: number
  id: number
  user_id: number
  created_at: string
  updated_at: string
  deleted_at: string
}
