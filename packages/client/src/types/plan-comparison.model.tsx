export interface IPlanComparison {
  name?: string
  periodId?: number
  months?: number
  monthlyPriceValue?: string
  monthlyDiffPercent?: number
  priceValue?: string
}

export interface IPlanComparisonMap {
  name: string
  period_id: number
  months: number
  monthly_price_value: number
  monthly_diff_percent: number
  price_value: number
}
