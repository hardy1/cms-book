import { GenericResponse } from './generic-response.model'
import { IProductMap } from './product.model'

export interface CartMap extends GenericResponse {
  payload: {
    discounts: unknown
    is_unlimited: boolean
    total_price: number
    old_total_price: number
    discount_total_price: number
    product: IProductMap
    price_data: {
      price_with_tax_value?: number
    }
    offer: {
      id: number
      product_id: number
    }
    period: {
      name: string
    }
  }
}

export interface ICart extends Pick<GenericResponse, 'status' | 'message'> {
  id?: number
  active?: boolean
  name?: string
  period?: string
  description?: string
  monthly_price?: number
  totalPrice?: number
  oldTotalPrice?: number
  discountTotalPrice?: number
  isUnlimited?: boolean
  error?: string

  discounts?: {
    ir?: {
      value?: unknown
    }
    retencao_unificada?: {
      value?: unknown
    }
  }
  offer?: {
    id: number
    product_id: number
  }
  price_data: {
    price_with_tax_value?: unknown
  }
}
