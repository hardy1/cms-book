export interface ErrorResponse {
  code: string
  http_code: number
  message: string
}
