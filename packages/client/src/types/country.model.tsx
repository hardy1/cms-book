import { GenericResponse } from './generic-response.model'
import { IGenericItem } from './shared.model'

export interface CountriesResponse extends GenericResponse {
  payload: IGenericItem[]
}
