export interface ICardInfo {
  cardName?: string
  cardCVV?: string
  cardNumber?: string
  cardValidationMonth?: string
  cardValidationYear?: string
}

export interface IFormikDataPayment {
  payment_method: string
  billing_data: {
    document_type: string
    document: string
    name: string
    email: string
    phone: string
    address: {
      country: string
      state: string
      city: string
      neighborhood: string
      street: string
      street_number: string
      postcode: string
      complement?: string
    }
  }
}
