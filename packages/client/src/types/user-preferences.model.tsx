export interface UserPreferences {
  optIn: boolean
}

export interface UserPreferencesMap {
  optin_news: boolean
}
