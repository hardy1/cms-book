export interface ITag {
  tag: string
  slug: string
}
