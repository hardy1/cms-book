export interface UserRegistrationMap {
  name: string
  email: string
  phone: string
  password: string
  password_confirmation: string
  terms_and_conditions_accepted: string
  language: string
  document: string
  optin_news: string
  address: {
    country: string
    city: string
    state: string
  }
}

export interface UserRegistration {
  name: string
  email: string
  phone: string
  password: string
  confirmPassword: string
  termsOfUse: boolean
  language: string
  country: string
  city: string
  state: string
  cpfDocument?: string
  optIn: boolean
}
