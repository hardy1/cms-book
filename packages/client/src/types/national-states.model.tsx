import { GenericResponse } from './generic-response.model'

export interface NationalStateMap {
  sigla?: string
  nome?: string
  capital?: string
  cidades?: string[]
}

export interface NationalStatesResponse extends GenericResponse {
  payload: NationalStateMap[]
}

export interface NationalState {
  initials?: string
  name?: string
  capital?: string
  cities?: string[]
}
