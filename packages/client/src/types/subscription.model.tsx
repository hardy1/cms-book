import { IProductMap, IProduct } from './product.model'
import { IPlan, IPlanMap } from './plan.model'
import { IGatewayMap, IGateway } from '.'

export interface ISubscriptionMap {
  id: number
  expire_at: string
  is_active: boolean
  gateway?: IGatewayMap
  product: IProductMap
  plan: IPlanMap
}

export interface ISubscription {
  id?: number
  expirationDate?: string
  isActive?: boolean
  gateway?: IGateway
  product?: IProduct
  plan?: IPlan
}
