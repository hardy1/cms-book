interface ICidPivot {
  moc_book_chapter_id: number
  moc_cid_id: number
}

export interface ICidChapter {
  id?: number
  created_at?: string
  updated_at?: string
  deleted_at?: string
  name?: string
  slug?: string
  content?: string
  code?: string
  pivot?: ICidPivot
}
