export interface Pricing {
  id?: number
  item_id?: number
  item_type?: string
  price?: string
  old_price?: string
  price_type_id?: number
  created_at?: string
  updated_at?: string
}

export interface PriceListMap {
  [key: number]: {
    price: string
    old_price: string
  }
}

export interface MainPriceMap {
  id: number
  item_id: number
  item_type: string
  price: string
  old_price: string
  price_type_id: number
  created_at: string
  updated_at: string
}

export interface PriceLinkMap {
  id: number
  item_id: number
  item_type: string
  price: string
  old_price: string
  price_type_id: number
  created_at: string
  updated_at: string
}
