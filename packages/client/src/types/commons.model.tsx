import { GenericResponse } from './generic-response.model'
import { IGenericItem, IGenericEntry } from './shared.model'

export interface ICommonsMap extends GenericResponse {
  payload: ICommons
}

export interface ICommons {
  [key: string]: IGenericEntry[]
}

export interface IMenu {
  items: IMenuItem[] | undefined
}

export interface IMenuItem extends IGenericEntry {
  submenu?: ISubmenuItem[] | undefined
}

export interface ISubmenuItem extends Omit<IGenericItem, 'name'> {
  url?: string
}
