import { CoverType } from './cover.model'
import { ICollaboratorByRole } from './collaborator.model'

export interface IGenericItem {
  id?: number
  name: string
  slug?: string
  value?: any
  code?: string
}

export interface IGenericEntry extends Omit<IGenericItem, 'name'> {
  parent_id?: number
  title?: string
  description?: string
  url?: string
  is_external?: number
  link_target?: string
  year?: string
  children?: IGenericEntry[]
  image?: CoverType
  theme_color?: string
  color?: string
  value?: string
  collaborators?: ICollaboratorByRole
}
