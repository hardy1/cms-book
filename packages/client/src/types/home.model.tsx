import { ISponsorData } from './channel.model'
import { IGenericItem } from './shared.model'

interface IFeaturedImages {
  id?: number
  disk_name?: string
  file_name?: string
  file_size?: number
  content_type?: string
  title?: string
  description?: string
  field?: string
  str_attachment_id?: string
  sort_order?: number
  created_at?: string
  updated_at?: string
  path?: string
  extension?: string
}

interface IFeaturedUser {
  id?: number
  first_name?: string
  last_name?: string
  login?: string
  email?: string
  permissions?: string
  is_activated?: boolean
  role_id?: number
  activated_at?: string
  last_login?: string
  created_at?: string
  updated_at?: string
  deleted_at?: string
  is_superuser?: boolean
  biography?: string
  personal_title?: string
}

export interface IHomeItem {
  id: number
  title?: string
  slug?: string
  published_at?: string
  channel?: string
  reading_time?: number
  private?: boolean
  subtitle?: string
  custom_repeater?: string
  sponsor_image_path?: string
  unpublished_at?: string
  has_sponsor?: boolean
  sponsors?: ISponsorData[]
  sponsors_description?: string
  featured_images?: IFeaturedImages[]
  user?: IFeaturedUser
  categories?: IGenericItem[] | undefined
  author_name?: string
  image?: string
  imagem?: string
  sponsor?: string
  cta_url?: string
  cta_label?: string
  texto?: string
}

export interface IHome {
  featuring?: IHomeItem[]
  news_featured?: IHomeItem[]
  news_latest?: IHomeItem[]
  tip?: IHomeItem[]
  video?: IHomeItem[]
  podcasts?: IHomeItem[]
  congressos?: IHomeItem[]
}
