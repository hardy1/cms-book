import { ICidChapter } from './cid.model'
import { CollaboratorInfoType, CollaboratorsGroupType } from './collaborator.model'
import { CoverType } from './cover.model'

interface SubtitleType {
  id: string
  title: string
}

export interface ContentType {
  id: number
  title: string
  slug: string
  subtitles: SubtitleType[]
  formatted_content: string
}

export interface ChapterType {
  id: number
  name: string
  slug: string
  last_update?: string
  collaborators: CollaboratorInfoType[]
  cids?: ICidChapter[]
  part?: PartType
  content: ContentType[]
}

export interface PartType {
  id: number
  book_id: number
  name: string
  slug: string
  last_update?: string
  book?: BookType
  chapters?: ChapterType[]
}

export interface CurrentBookDataType {
  bookSlug?: string
  partSlug?: string
  chapterSlug?: string
}

export interface BookScopeDataType {
  fullScreen?: boolean
  nightMode?: boolean
  bookIndex: boolean
  searchIn: boolean
  fontModifier: number
  book?: BookFullType
  current?: CurrentBookDataType
}

export interface BookType {
  id?: number
  name?: string
  slug?: string
}

export interface BookFullType extends BookType {
  created_at?: string
  updated_at?: string
  year?: number
  cover?: CoverType
  collaborators_groups?: CollaboratorsGroupType[]
  parts?: PartType[]
  theme_color?: string
}
