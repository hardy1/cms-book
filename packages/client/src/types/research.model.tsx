interface ResearchCategoryDataVM {
  id: number
  name: string
  slug: string
}
export interface ResearchUserDataVM {
  id: number
  name: string
  avatar_uri: string
}
export interface ResearchDataVM {
  id: number
  user_id: number
  title: string
  slug: string
  excerpt: string
  content: string
  content_html: string
  published_at: string
  published: boolean
  created_at: string
  updated_at: string
  metadata: string
  channel: string
  featured: boolean
  reading_time: number
  private: boolean
  subtitle: string
  external_url: string
  cta_text: string
  cta_url: string
  summary: string
  has_summary: boolean
  sponsor: string
  categories: ResearchCategoryDataVM[]
  tags: []
  user: ResearchUserDataVM
  image: string
}

export interface ResearchesDataVM {
  current_page: number
  data: ResearchDataVM[]
  first_page_url: string
  from: number
  last_page: number
  last_page_url: string
  next_page_url: string
  path: string
  per_page: number
  prev_page_url: string
  to: number
  total: number
}
