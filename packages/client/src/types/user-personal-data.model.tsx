import { IAddress, IAddressMap } from './address.model'
import { IUser, IUserMap } from './user-data.model'

export interface UserPersonalDataMap extends IUserMap {
  phone?: string
  cellphone?: string
  document?: string
  address?: IAddressMap
}

export interface UserPersonalData extends IUser, IAddress {}
