import { IGenericItem } from './shared.model'
import { ITag } from './tag.model'
import { CollaboratorInfoType } from './collaborator.model'

export interface IChannel {
  current_page?: number
  data?: IChannelData[]
  first_page_url?: string
  from?: number
  last_page?: number
  last_page_url?: string
  next_page_url?: number
  path?: string
  per_page?: number
  prev_page_url?: number
  to?: number
  total: number
}

interface ILatestItems {
  id?: number
  title?: string
  slug?: string
  published_at?: string
  reading_time?: number
  private?: boolean
  subtitle?: string
  custom_repeater?: string
  sponsor?: string
  sponsors?: ISponsorData[]
  categories?: [
    {
      id: number
      name: string
      slug: string
    },
  ]
  players?: number
  image?: string
}

interface IRelatedItems {
  id?: number
  title?: string
  slug?: string
  published_at?: string
  reading_time?: number
  private?: boolean
  subtitle?: string
  custom_repeater?: string
  pivot?: {
    post_id?: number
    related_id?: number
  }
  sponsor?: string
  categories?: [
    {
      id: number
      name: string
      slug: string
    },
  ]
  image?: string
}

export interface IChannelData {
  id?: number
  user_id?: number
  title?: string
  slug?: string
  excerpt?: string
  content?: string
  content_html?: string
  published_at?: string
  published?: boolean
  created_at?: string
  updated_at?: string
  metadata?: string
  channel?: string
  featured?: boolean
  reading_time?: number
  private?: boolean
  subtitle?: string
  external_url?: string
  cta_text?: string
  cta_url?: string
  summary?: string
  has_summary?: false
  sponsor?: string
  sponsors?: ISponsorData[]
  sponsors_description?: string
  has_sponsor?: boolean
  image?: string
  categories?: IGenericItem[]
  tags?: ITag[]
  user?: CollaboratorInfoType
  author_name: string
  latest_items?: ILatestItems[]
  related_items?: IRelatedItems[]
}

export interface ISponsorData {
  image_url?: string
  image_cta?: string
}
