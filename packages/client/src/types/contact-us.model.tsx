export interface ContactUsData {
  content: string
}

export interface IContactUs {
  subject: string
  name: string
  message: string
  email: string
  phone: string
  termsOfUse: boolean
  totalAccess?: string
  cpfDocument?: string
  institutionName?: string
}
