import { ParsedUrlQuery } from 'querystring'

export interface PathsVM {
  params: PathsSlugsVM
}

interface PathsSlugsVM extends ParsedUrlQuery {
  book?: string
  part?: string
  chapter?: string
  all?: string[]
  slug?: string
}
