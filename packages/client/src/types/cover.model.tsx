export interface CoverType {
  uri: string
  type?: string
  title?: string
  description?: string
  thumb_square?: string
}
