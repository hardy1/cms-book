import { PriceListMap, MainPriceMap, PriceLinkMap } from '.'

export interface IProductMap {
  id?: number
  active?: boolean
  product_id?: number
  name?: string
  code?: string
  slug?: string
  external_id?: number
  quantity?: number
  preview_text?: string
  description?: string
  deleted_at?: string
  created_at?: string
  updated_at?: string
  quantity_in_unit?: string
  measure_of_unit_id?: number
  width?: string
  length?: string
  height?: string
  weight?: string
  measure_id?: number
  discount_id?: number
  discount_value?: string
  discount_type?: string
  subscription_period_id?: number
  price?: string
  price_value?: string
  old_price?: string
  old_price_value?: string
  discount_price?: string
  discount_price_value?: number
  price_list?: PriceListMap
  main_price?: MainPriceMap
  price_link?: PriceLinkMap[]
}

export interface IProduct {
  id?: number
  name?: string
  code?: string
  description?: string
}
