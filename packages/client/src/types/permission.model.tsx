export interface IPermission {
  name: string
  code: string
  enabled: boolean
}
