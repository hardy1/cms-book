import { IPlan } from './plan.model'

export interface SubscriptionPeriod {
  id: number
  name: string
  period: string
  plans: IPlan[]
}
