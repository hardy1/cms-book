import { IProduct } from './product.model'
import { IPeriod } from './period.model'
import { IPlanComparison, IPlanComparisonMap } from './plan-comparison.model'
import { SubscriptionPeriod } from './subscription-period.model'

export interface IPlan {
  id?: number
  name?: string
  slug?: string
  code?: string
  months?: number
  priceValue?: string
  monthlyPriceValue?: string
  offerId?: number
  product?: IProduct
  period?: IPeriod
  comparison?: IPlanComparison[]
  color?: string
  subscriptionPeriod?: string
}

export interface IPlanMap {
  id: number
  name: string
  slug: null
  code: string
  months: number
  price_value: number
  monthly_price_value: number
  offer_id?: number
  product: IProduct
  period: IPeriod
  comparison: IPlanComparisonMap[]
  color?: string
}

export interface PlansPageType {
  planPeriods: SubscriptionPeriod[]
}
