import * as Yup from 'yup'
import { regex } from './regex'
import valid from 'card-validator'

const documentsSchema = {
  cpfDocument: Yup.string().required('Campo obrigatório').matches(regex.cpf, 'CPF inválido'),
  cnpjDocument: Yup.string().required('Campo obrigatório').matches(regex.cnpj, 'CNPJ inválido'),
}

const addressSchema = {
  country: Yup.string().required('Campo obrigatório'),
  zipcode: Yup.string().required('Campo obrigatório').matches(regex.cep, 'CEP inválido'),
  city: Yup.string().required('Campo obrigatório'),
  state: Yup.string().required('Campo obrigatório').matches(regex.state, 'UF inválido'),
  neighborhood: Yup.string().required('Campo obrigatório'),
  street: Yup.string().required('Campo obrigatório'),
  number: Yup.string().required('Campo obrigatório'),
  extention: Yup.string(),
}

const UserRegistrationSchema = {
  name: Yup.string().required('Campo obrigatório').matches(regex.name, 'Nome inválido'),
  email: Yup.string().required('Campo obrigatório').email('E-mail inválido'),
  country: addressSchema.country,
  language: Yup.string().required('Campo obrigatório'),
  cpfDocument: documentsSchema.cpfDocument,
}

const OldPasswordSchema = {
  oldPassword: Yup.string().required('Campo obrigatório'),
}

const PasswordSchema = {
  password: Yup.string()
    .required('Campo obrigatório')
    .min(8, 'Deve ter no mínimo 8 caracteres')
    .matches(regex.password, 'Senha inválida'),
  confirmPassword: Yup.string()
    .required('Campo obrigatório')
    .oneOf([Yup.ref('password'), null], 'As senhas devem ser iguais'),
}

const UserOptionsSchema = {
  termsOfUse: Yup.bool().oneOf([true], 'Campo obrigatório'),
  optIn: Yup.boolean(),
}

const UserPersonalSchema = {
  phone: Yup.string().required('Campo obrigatório').matches(regex.phone, 'Telefone inválido'),
  mobile: Yup.string(),
}

const UserDadosPessoaisSchema = {
  phone: UserPersonalSchema.phone,
  mobile: UserPersonalSchema.mobile,
  zipcode: addressSchema.zipcode,
  city: addressSchema.city,
  number: addressSchema.number,
  extention: addressSchema.extention,
  state: addressSchema.state,
}

const InstitutionSchema = Yup.object().shape({
  institutionType: Yup.string().required('Campo obrigatório'),
  institution: Yup.string().required('Campo obrigatório'),
})

const professionsWithoutSpecialization = ['6', '7', '8', '10']
const professionsRegister = ['6', '7', '8', '9']

const UserProfessionalSchema = {
  profession: Yup.string(),
  specialization: Yup.string()
    .when('profession', {
      is: (profession: string) => professionsWithoutSpecialization.find((el) => el === profession),
      then: Yup.string(),
      otherwise: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  specializationCustom: Yup.string()
    .when('specialization', {
      is: (specialization: string) => specialization === 'Outras',
      then: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  formationDate: Yup.string()
    .when('profession', {
      is: (profession: string) => profession === '11',
      then: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  finishedResidencyDate: Yup.string()
    .when('profession', {
      is: (profession: string) => profession === '10',
      then: Yup.string().required('Campo obrigatório').nullable(),
    })
    .nullable(),
  state: Yup.string()
    .matches(regex.state, 'UF inválido')
    .when('profession', {
      is: (profession: string) => professionsRegister.find((el) => el === profession),
      then: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  professionRegister: Yup.string()
    .when('profession', {
      is: (profession: string) => professionsRegister.find((el) => el === profession),
      then: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  professionStatus: Yup.string()
    .when('profession', {
      is: (profession: string) => professionsRegister.find((el) => el === profession),
      then: Yup.string().required('Campo obrigatório'),
    })
    .nullable(),
  institutions: Yup.array().of(InstitutionSchema),
  document: Yup.mixed().when('profession', {
    is: '9',
    then: Yup.mixed(),
    otherwise: Yup.mixed().required('Campo obrigatório'),
  }),
}

const CheckoutSchema = {
  promoCode: Yup.string(),
}

const CreditCardSchema = {
  cardNumber: Yup.string()
    .required('Campo obrigatório')
    .test(
      'test-number', // this is used internally by yup
      'O número do cartão de crédito é inválido', //validation message
      (value) => valid.number(value).isValid,
    ), // return true false based on validation,
  cardName: Yup.string().required('Campo obrigatório'),
  cardValidationMonth: Yup.string().required('Campo obrigatório'),
  cardValidationYear: Yup.string().required('Campo obrigatório'),
  cardCVV: Yup.string()
    .required('Campo obrigatório')
    .test('len', 'Mínimo 3 dígitos', (val) => val?.toString().trim().length > 2),
}

const ContactUsSchema = {
  name: Yup.string().required('Campo obrigatório').matches(regex.name, 'Nome inválido'),
  subject: Yup.string().required('Campo obrigatório'),
  message: Yup.string().required('Campo obrigatório'),
  email: Yup.string().required('Campo obrigatório').email('E-mail inválido'),
  phone: Yup.string().required('Campo obrigatório'),
  termsOfUse: Yup.bool().oneOf([true], 'Campo obrigatório'),
}

const groupOrLegalEntitySchema = {
  name: Yup.string().required('Campo obrigatório').matches(regex.name, 'Nome inválido'),
  email: Yup.string().required('Campo obrigatório').email('E-mail inválido'),
  phone: Yup.string().required('Campo obrigatório'),
  cnpjDocument: Yup.string().required('Campo obrigatório').matches(regex.cnpj, 'CNPJ inválido'),
  institutionName: Yup.string().required('Campo obrigatório'),
  totalAccess: Yup.string().required('Campo obrigatório'),
  message: Yup.string().required('Campo obrigatório'),
  termsOfUse: Yup.bool().oneOf([true], 'Campo obrigatório'),
}

const pharmaceuticalIndustrySchema = {
  name: Yup.string().required('Campo obrigatório').matches(regex.name, 'Nome inválido'),
  email: Yup.string().required('Campo obrigatório').email('E-mail inválido'),
  cpfDocument: Yup.string().required('Campo obrigatório').matches(regex.cpf, 'CPF inválido'),
  phone: Yup.string().required('Campo obrigatório'),
  institutionName: Yup.string().required('Campo obrigatório'),
  totalAccess: Yup.string().required('Campo obrigatório'),
  message: Yup.string().required('Campo obrigatório'),
  termsOfUse: Yup.bool().oneOf([true], 'Campo obrigatório'),
}

export {
  addressSchema,
  CheckoutSchema,
  ContactUsSchema,
  CreditCardSchema,
  documentsSchema,
  groupOrLegalEntitySchema,
  InstitutionSchema,
  OldPasswordSchema,
  PasswordSchema,
  pharmaceuticalIndustrySchema,
  professionsWithoutSpecialization,
  professionsRegister,
  UserDadosPessoaisSchema,
  UserOptionsSchema,
  UserPersonalSchema,
  UserProfessionalSchema,
  UserRegistrationSchema,
}
