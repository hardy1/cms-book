import { v4 as uuidv4 } from 'uuid'
import { IGenericItem } from '../types'

const statusList: IGenericItem[] = [
  {
    id: uuidv4(),
    name: 'Ativo',
    value: 'ativo',
  },
  {
    id: uuidv4(),
    name: 'Inativo',
    value: 'inativo',
  },
]

export default statusList
