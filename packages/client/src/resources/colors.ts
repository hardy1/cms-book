const colors = {
  disabledColor: '#9ea0a5',
  neutralColor: '#3e3f42',
  primaryColor: '#144590',
  secondaryColor: '#216de1',
  tertiaryColor: '#00e4b7',
  quaternaryColor: '#b1dbff',
  successColor: '#2ecc71',
  warningColor: '#f1c40f',
  errorColor: '#fe4140',
  lightColor: '#f1f1f1',
  lightBlue: '#b3dcff',
  white: '#fff',
  black: '#000',
  mediumGray: '#656D79',
  lightGray: '#c4c4c4',
  lighterGray: '#eee',
  primaryBook: '#ef4350',
  quaternaryBook: '#ffeaec',
  borderColor: '#d6d0d0',
}

export default colors
