enum MASK {
  cpf = '###.###.###-##',
  cnpj = '##.###.###/####-##',
  zipcodeBr = '#####-###',
  phone = '+55 (##) ####-####',
  genericPhone = '+#############',
  mobile = '+55 (##) #####-####',
  professionCode = '###############',
  date = '##/##/####',
  number = '###############',
}

export default MASK
