export const regex = {
  name: new RegExp(/^[a-zA-Z\u00c0-\u00ff]{2,}(\s[a-zA-Z\u00c0-\u00ff]+)*/),
  phone: new RegExp(/^\+([0-9]){0,3}\s?(\(?[0-9]{2}\)?)\s?([0-9]{4,5}-?[0-9]{4,5})$/gi),
  password: new RegExp(/^([a-zA-Z0-9@*#!_=]{8,25})$/),
  cpf: new RegExp(/^\d{3}\.?\d{3}\.?\d{3}-?\d{2}$/),
  cep: new RegExp(/^([0-9]){5}[-\s]?([0-9]){3}$/),
  state: new RegExp(/^([A-Z]){2}$/),
  cnpj: new RegExp(/^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/),
}
