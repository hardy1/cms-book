import { IInfoBlock, IGenericItem, IBlockListValueSet, IUser } from '../types'
import { institutionTypeId, institutionTypeName } from '../enums'

import { formatDate } from '../resources/pipes'

export enum dashboardMenu {
  INITIAL = 'initial',
  CHANGE_PASSWORD = 'changePassword',
  PERSONAL = 'personal',
  PROFESSIONAL = 'professional',
  PLANSIGNUP = 'planSignUp',
  PREFERENCES = 'preferences',
  PLAN = 'plan',
  LOGOUT = 'logout',
  GOBACKTOSITE = 'goBackToSite',
}

enum activationStatus {
  ACTIVE = 'Ativo',
  INACTIVE = 'Inativo',
}

const paymentStatus = {
  done: 'Efetuado',
  failed: 'Falha',
  waiting_payment: 'Aguardando pagamento',
  no_status: 'Sem status disponível',
  trialing: 'Degustação',
}

const institutionContext = {
  [institutionTypeId.PUBLIC]: institutionTypeName.PUBLIC,
  [institutionTypeId.PRIVATE]: institutionTypeName.PRIVATE,
}

export const menuOptions: IGenericItem[] = [
  { name: 'Visão geral', value: dashboardMenu.INITIAL },
  { name: 'Alterar senha', value: dashboardMenu.CHANGE_PASSWORD },
  { name: 'Dados pessoais', value: dashboardMenu.PERSONAL },
  { name: 'Dados profissionais', value: dashboardMenu.PROFESSIONAL },
  { name: 'Dados de assinatura', value: dashboardMenu.PLANSIGNUP },
  { name: 'Preferências', value: dashboardMenu.PREFERENCES },
  { name: 'Voltar ao site', value: dashboardMenu.GOBACKTOSITE },
  { name: 'Sair', value: dashboardMenu.LOGOUT },
]

// Code should be an User type key (check interface)
export const dashboardInfo = (user?: IUser): IInfoBlock => {
  return {
    plan: {
      title: user?.subscription?.product?.name || '',
      body: [
        { entryName: 'Plano', content: user?.subscription?.plan?.name || '' },
        {
          entryName: 'Status da assinatura',
          content: user?.subscription?.isActive ? activationStatus.ACTIVE : activationStatus.INACTIVE,
        },
        {
          entryName: 'Status do pagamento',
          content: user?.subscription?.gateway?.currentTransaction?.status
            ? paymentStatus[user?.subscription?.gateway?.currentTransaction?.status]
            : paymentStatus.no_status,
        },
        {
          entryName: 'Validade',
          content: user?.subscription?.expirationDate ? formatDate(user.subscription.expirationDate) : '',
        },
      ],
    },
    personal: {
      title: '',
      body: [
        { entryName: 'Nome completo', content: user?.name || '' },
        { entryName: 'CPF', content: user?.cpfDocument || '' },
        { entryName: 'Email', content: user?.email || '' },
        { entryName: 'Telefone', content: user?.phone || '' },
        { entryName: 'Idioma', content: user?.language || '' },
        { entryName: 'País', content: user?.address?.country || '' },
        { entryName: 'CEP', content: user?.address?.zipcode || '' },
        { entryName: 'UF', content: user?.address?.state || '' },
        { entryName: 'Cidade', content: user?.address?.city || '' },
        { entryName: 'Endereço', content: user?.address?.street || '' },
        { entryName: 'Número', content: user?.address?.number || '' },
        { entryName: 'Complemento', content: user?.address?.extention || '' },
      ],
    },
    professional: {
      title: '',
      body: [
        { entryName: 'Profissão', content: user?.profession?.name || '' },
        {
          entryName: 'Especialidade',
          content: user?.profession?.specialization?.name || user?.profession?.specializationOther || '',
        },
        { entryName: 'Registro', content: user?.profession?.register || '' },
        { entryName: 'Status do registro', content: user?.profession?.status || '' },
      ],
    },
    privacy: {
      title: '',
      body: [{ entryName: 'Você aceitou os termos em', content: user?.termsOfUse ? formatDate(user.termsOfUse) : '' }],
    },
  }
}

export const institutionsInfo = (user: IUser): IBlockListValueSet[] => {
  return [
    {
      entryName: 'Instituições',
      content: user?.profession?.institutions?.map(({ institution }) => ({
        content: institution,
      })),
    },
    {
      entryName: 'Tipo de instituição',
      content: user?.profession?.institutions?.map(({ institutionType }) => ({
        content: institutionContext[institutionType],
      })),
    },
  ]
}

export const dashboardPreferences = (user: IUser): IInfoBlock => {
  return {
    privacy: {
      title: '',
      body: [
        {
          entryName: 'Você aceitou os termos em',
          content: user?.termsOfUse ? formatDate(user.termsOfUse) : '',
        },
      ],
    },
  }
}

export const dashboardSubscription = (user: IUser): IInfoBlock => {
  const mappedDataBody = [
    ...dashboardInfo(user).plan.body,
    { entryName: 'Tipo de pagamento', content: user?.subscription?.gateway?.paymentMethod || '' },
    { entryName: 'Número do cartão', content: user?.subscription?.gateway?.cardLastDigits || '' },
  ]

  const mappedData = {
    subscription: {
      title: user?.subscription?.product?.name || '',
      body:
        user?.subscription?.gateway?.paymentMethod === 'boleto'
          ? mappedDataBody.filter((item) => item.entryName !== 'Número do cartão')
          : mappedDataBody,
    },
  }

  return mappedData
}
