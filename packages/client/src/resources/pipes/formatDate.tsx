import { format } from 'date-fns'

const formatDate = (date: string): string => {
  const isValidDate = Boolean(date.match(/\d{4}-\d{2}-\d{2}/g)?.length)
  return isValidDate ? format(new Date(date), 'dd/MM/yyyy') : date
}

export { formatDate }
