const UserRegistrationInitialValues = {
  name: '',
  email: '',
  phone: '',
  country: 'BR',
  cpfDocument: '',
  language: 'pt_BR',
}

const OldPasswordInitialValues = {
  oldPassword: '',
}

const PasswordInitialValues = {
  password: '',
  confirmPassword: '',
}

const UserOptionsInitialValues = {
  termsOfUse: false,
  optIn: true,
}

const addressSchema = {
  country: 'BR',
  zipcode: '',
  city: '',
  state: '',
  neighborhood: '',
  street: '',
  number: '',
}

const UserPersonalInitialValues = {
  phone: '',
  mobile: '',
  ...addressSchema,
}

const InstitutionInitialValues = {
  institutionType: '',
  institution: '',
}

const UserProfessionalInitialValues = {
  profession: '',
  institutions: [InstitutionInitialValues],
}

const ContactUsInitalValues = {
  subject: '',
  name: '',
  message: '',
  email: '',
  phone: '',
  termsOfUse: false,
}

const CheckoutInitialValues = {
  promoCode: '',
}

const CreditCardInitialValues = {
  cardNumber: '',
  cardName: '',
  cardValidationMonth: '',
  cardValidationYear: '',
  cardCVV: '',
}

const groupOrLegalEntityInitialValues = {
  name: '',
  email: '',
  phone: '',
  cnpjDocument: '',
  institutionName: '',
  totalAccess: '',
  message: '',
  termsOfUse: false,
}

const pharmaceuticalIndustryInitialValues = {
  name: '',
  email: '',
  cpfDocument: '',
  phone: '',
  institutionName: '',
  totalAccess: '',
  message: '',
  termsOfUse: false,
}

export {
  addressSchema,
  CheckoutInitialValues,
  ContactUsInitalValues,
  CreditCardInitialValues,
  InstitutionInitialValues,
  groupOrLegalEntityInitialValues,
  OldPasswordInitialValues,
  PasswordInitialValues,
  pharmaceuticalIndustryInitialValues,
  UserOptionsInitialValues,
  UserPersonalInitialValues,
  UserProfessionalInitialValues,
  UserRegistrationInitialValues,
}
