import { v4 as uuidv4 } from 'uuid'

export const countries = [
  {
    id: uuidv4(),
    name: 'Brasil',
    value: 'BR',
  },
]
