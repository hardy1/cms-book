import { v4 as uuidv4 } from 'uuid'

export const languages = [
  {
    id: uuidv4(),
    value: 'pt_BR',
    name: 'Português',
  },
]
