export const handleProfessionRegister = async (professionRegister: string) => {
  if (professionRegister) {
    return professionRegister?.replaceAll(/\D/g, '')
  }
  return undefined
}
