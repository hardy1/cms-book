import { IProfession } from '../../types'
import { professionsWithoutSpecialization } from '../formValidationSchemas'

export const handleSpecializationId = async ({
  profession,
  specialization,
  professions,
}: {
  profession: number
  specialization: string
  professions: IProfession[]
}) => {
  const findProfession: IProfession = professions?.find((p) => p?.id === profession)
  const professionId = findProfession.id.toString()
  const findSpecializationId = professionsWithoutSpecialization.find((el) => el !== professionId)
    ? findProfession?.specializations.find((s) => s?.name === specialization)?.id.toString()
    : undefined
  return findSpecializationId
}
