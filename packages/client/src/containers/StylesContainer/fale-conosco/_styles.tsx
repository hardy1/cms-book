import styled from 'styled-components'
import theme from '../../../lib/theme'
import media from '../../../resources/media'

export const TextContainer = styled.div`
  grid-column: 3 / 11;
  margin-top: 72px;

  p {
    font-size: ${theme.font.size.md};
    line-height: ${theme.font.lineHeight.md};
    color: ${theme.color.grey[500]};
  }

  & > *:not(:first-child) {
    margin-top: 1rem;
  }

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const FormContainer = styled.form`
  grid-column: 3 / 11;
  margin-bottom: 80px;
  margin-top: 64px;
  width: 100%;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const InputContainer = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
  height: 7.8rem;
  flex: 1;

  & > *:not(:first-child) {
    margin-left: 60px;
  }

  & > div {
    width: 100%;
  }

  @media ${media.mobileL} {
    flex-direction: column;
    height: 100%;

    & > *:not(:first-child) {
      margin-left: 0;
    }

    & > div {
      margin-bottom: 1rem;
    }
  }
`

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  flex: 1;
  margin-top: 1.2rem;
`
