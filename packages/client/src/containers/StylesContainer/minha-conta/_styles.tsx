import styled from 'styled-components'

const DashboardContainer = styled.div`
  position: absolute;
  inset: 0;
  display: grid;
  margin: 0;

  @media screen and (min-width: 768px) {
    grid-template-columns: 300px 1fr;
  }

  @media screen and (max-width: 768px) {
    grid-template-rows: 100px 1fr;
  }
`

const SelectedContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100vw - 300);
  padding-top: 24px;
  padding-left: 16px;
  padding-bottom: 24px;
  padding-right: 16px;
  box-sizing: border-box;

  @media screen and (min-width: 360px) {
    padding-left: 24px;
    padding-right: 24px;
  }

  @media screen and (min-width: 768px) {
    min-width: calc(100vw);
    padding-top: 4.5rem;
    padding-right: 4.5rem;
    padding-bottom: 4.5rem;
    padding-left: calc(4.5rem + 300px);
  }

  .Infoblock {
    &__Title {
      margin-top: 48px;
      margin-bottom: 24px;

      @media screen and (min-width: 768px) {
        margin-top: 3.3rem;
        margin-bottom: 1.5rem;
      }

      h3 {
        font-size: 20px;

        @media screen and (min-width: 768px) {
          font-size: 1.5rem;
        }
      }
    }

    &__Header {
      padding: 20px;

      @media screen and (min-width: 768px) {
        padding: 1.8rem 1.5rem;
      }

      h4 {
        font-size: 20px;
        line-height: 20px;

        @media screen and (min-width: 768px) {
          line-height: 1.3;
        }
      }
    }

    &__Body {
      padding: 20px;

      @media screen and (min-width: 768px) {
        padding: 2.2rem;
      }
    }

    &__ContentList {
      grid-template-columns: 1fr;

      @media screen and (min-width: 768px) {
        grid-template-columns: repeat(2, 1fr);
      }
    }

    &__Entry {
      padding-top: 0;
      grid-template-rows: 2fr;
      grid-auto-columns: 1fr;
      grid-template-areas: unset;

      @media screen and (min-width: 768px) {
        grid-template-areas: 'key key' 'value value';
      }
    }

    &__Key,
    &__Value {
      grid-column: 1 / -1 !important;
      grid-area: unset;
    }

    &__Key {
      @media screen and (min-width: 768px) {
        grid-area: key;
      }
    }

    &__Value {
      margin-left: 0;

      @media screen and (min-width: 768px) {
        grid-area: value;
      }
    }
  }
`

export { DashboardContainer, SelectedContentContainer }
