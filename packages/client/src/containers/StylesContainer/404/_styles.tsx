import styled from 'styled-components'

export const TitleContainer = styled.div`
  display: block;
  grid-column: 1 / -1;
  margin-top: 50px;
  text-align: center;

  & > *:not(:first-child) {
    margin-top: 16px;
  }
`

export const ButtonContainer = styled.div`
  display: flex;
  grid-column: 1 / -1;
  justify-content: center;
  margin-top: 60px;
`
