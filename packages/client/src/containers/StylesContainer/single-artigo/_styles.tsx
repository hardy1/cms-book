import styled from 'styled-components'
import colors from '../../../resources/colors'
import media from '../../../resources/media'

export const FlexContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media ${media.tabletM} {
    flex-direction: column;
  }
`

export const TextContainer = styled.div`
  & .SingleArticle--imported {
    & * {
      max-width: 100%;
    }

    & iframe {
      height: 100%;

      &[src*='video'] {
        aspect-ratio: 16 / 9;
      }
    }

    & img {
      margin-top: 2rem;
    }
  }

  @media ${media.laptop} {
    width: 100%;
  }
`

export const ArticleMiddleSection = styled.div`
  grid-column: 1 / -1;
  display: flex;

  @media screen and (min-width: 1024px) {
    justify-content: space-between;
  }

  @media ${media.laptop} {
    flex-direction: column;
  }
`

export const AuthorContainer = styled.div`
  align-items: center;
  display: flex;
  margin-top: 24px;
`

export const By = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 1em;
  font-weight: 400;
  letter-spacing: 0.05em;
  margin-right: 0.5rem;
`

export const AuthorName = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 1em;
  font-weight: 700;
  letter-spacing: 0.05em;
  text-decoration-line: underline;
`

export const ArticleInfoContainer = styled.div`
  align-items: center;
  display: flex;
  margin-top: 18px;
  margin-bottom: 3rem;
`

export const ArticleCategory = styled.span`
  color: ${colors.secondaryColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 0.875em;
  font-weight: 400;
`

export const ArticleInfo = styled.span<{ icon?: string }>`
  color: ${colors.disabledColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 0.875em;
  font-weight: 400;
  margin-right: 24px;

  &::before {
    content: '';
    height: 1rem;
    width: 1rem;
    position: relative;
    display: inline-block;
    margin-right: 0.4rem;
    vertical-align: middle;
    background-image: ${({ icon }) => `url(/img/${icon}.svg)`};
  }
`

export const ParagraphContainer = styled.div`
  width: 100%;
  margin-top: 47px;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;

  & > *:not(:first-child) {
    margin-top: 2em;
  }

  p,
  ul,
  ol {
    color: #656d79;
    font-family: 'Lato', sans-serif;
    font-size: 1rem;
    line-height: 1.75rem;
  }

  ul,
  ol {
    list-style-position: inside;
    padding-left: 0;

    li {
      &:not(:last-of-type) {
        padding-bottom: 24px;
      }

      > ol,
      ul {
        list-style-type: upper-roman;
        padding-top: 16px;
        padding-left: 24px;

        li {
          padding-bottom: 16px !important;
        }
      }
    }
  }

  a {
    color: #216de1;
    text-decoration: none;
  }
`

export const RelatedContainer = styled.div`
  min-width: 30%;
  max-width: 30%;
  margin-top: 64px;
  margin-left: 1.5rem;

  @media ${media.laptop} {
    max-width: unset;
    width: 100%;
    margin-left: 0;
  }
`

export const ArticleSharingContainer = styled.div`
  grid-column: 1 / -1;
  margin-top: 75px;
`

export const LastArticlesContainer = styled.div`
  grid-column: 1 / -1;
  margin-top: 75px;
`

export const LastArticles = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 2em;
  margin: 32px 0 0;

  @media ${media.laptop} {
    grid-template-columns: repeat(3, 1fr);
  }

  @media ${media.tabletM} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${media.mobileL} {
    grid-template-columns: auto;
  }
`
