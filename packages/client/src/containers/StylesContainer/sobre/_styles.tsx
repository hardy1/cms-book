import styled from 'styled-components'
import colors from '../../../resources/colors'
import media from '../../../resources/media'

export const TextContainer = styled.div`
  grid-column: 3 / 11;
  margin-top: 64px;

  h4 {
    color: ${colors.primaryColor};
  }

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const Title = styled.h4`
  color: ${colors.primaryColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 1.125em;
  font-weight: 700;
  margin: 0;
`

export const ParagraphContainer = styled.div`
  &:not(:first-child) {
    margin-top: 32px;
  }

  & > *:not(:first-child) {
    margin-top: 2em;
  }
`

export const ButtonContainer = styled.div`
  grid-column: 3 / 11;
  display: flex;
  justify-content: center;
  margin: 62px auto 0;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`
