import styled from 'styled-components'
import colors from '../../../resources/colors'
import media from '../../../resources/media'

export const InternalHeader = styled.div`
  background-color: ${colors.disabledColor};
  display: flex;
  align-items: center;
  width: 100%;
`
export const Heading2 = styled.h2`
  color: ${colors.white};
`

export const LeftContainer = styled.div`
  grid-column: 1 / 7;
  margin-top: 32px;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const RightContainer = styled.div`
  grid-column: 7 / -1;
  margin-top: 32px;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const TitlesContainer = styled.div`
  display: flex;
  flex-direction: column;

  & > *:not(:first-child) {
    margin-top: 40px;
  }
`
export const Title = styled.h2`
  color: #000;
  font-family: 'Montserrat', sans-serif;
  font-size: 1.875em;
  font-weight: 700;
  margin: 0;
`

export const SubTitle = styled.span`
  color: #9ea0a5;
  font-family: 'Montserrat', sans-serif;
  font-size: 1em;
  margin: 0;

  & strong {
    color: ${colors.neutralColor};
    font-weight: 700;
  }
`

export const SelectContainer = styled.div`
  margin-top: 20px;
`

export const ResultsContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 50px;

  & > *:not(:first-child) {
    margin-top: 3em;
  }
`

export const ButtonContainer = styled.div`
  margin-top: 72px;
`
export const BookTitle = styled.h1`
  color: ${colors.black};
  font-size: 30px;
  font-weight: bold;
`

export const PartTitle = styled.h2`
  color: ${colors.black};
  font-size: 20px;
  font-weight: bold;
`

export const ChapterTitle = styled.h3`
  color: ${colors.black};
  font-size: 16px;
  font-weight: bold;
  text-transform: capitalize;
`

export const Part = styled.div`
  margin-left: 20px;
`

export const Chapter = styled.div`
  margin-left: 20px;
`

export const ChapterContent = styled.div`
  color: #656d79;
`
