import styled from 'styled-components'

export const TitleContainer = styled.div`
  display: block;
  grid-column: 1 / -1;
  margin-top: 50px;
`

export const CanaisMocContainer = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 1em;
  margin-top: 60px;
`

export const ButtonContainer = styled.div`
  grid-column: 1 / -1;
  display: flex;
  justify-content: center;
  margin-top: 60px;
  margin-bottom: 100px;
`

export const ResultTextContainer = styled.div`
  text-align: center;
  grid-column: 1/-1;
`
