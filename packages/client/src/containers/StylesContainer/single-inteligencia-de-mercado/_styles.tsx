import styled from 'styled-components'
import media from '../../../resources/media'

export const TextContainer = styled.div`
  grid-column: 3 / 11;
  margin-top: 64px;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const ParagraphContainer = styled.div`
  &:not(:first-child) {
    margin-top: 32px;
  }

  & > *:not(:first-child) {
    margin-top: 1em;
  }
`

export const ButtonContainer = styled.div`
  grid-column: 3 / 11;
  display: flex;
  justify-content: flex-start;
  margin-top: 48px;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`
