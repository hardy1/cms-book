import styled from 'styled-components'
import colors from '../../../resources/colors'
import media from '../../../resources/media'

export const TextContainer = styled.div`
  grid-column: 3 / 11;
  margin-top: 64px;

  h3 {
    color: ${colors.primaryColor};
    text-transform: capitalize;
  }

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`
export const ButtonContainer = styled.div`
  grid-column: 3 / 11;
  display: flex;
  justify-content: center;
  margin: 62px auto 0;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`
