import styled from 'styled-components'
import media from '../../../resources/media'

export const TextContainer = styled.div`
  grid-column: 1 / 9;
  margin-top: 64px;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const ParagraphContainer = styled.div`
  margin-top: 47px;

  & > *:not(:first-child) {
    margin-top: 2em;
  }
`

export const AsideContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  grid-column: 9 / -1;
  margin-top: 64px;
  background-color: #fafafa;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
  }
`

export const ResearchesContainer = styled.div`
  grid-column: 1 / -1;
  margin-top: 75px;
  width: 100%;
`

export const Researches = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(3, minmax(auto, 33%));
  row-gap: 2em;
  margin: 32px 0 60px;

  @media ${media.tabletL} {
    grid-template-columns: repeat(2, minmax(auto, 50%));
  }

  @media ${media.tablet} {
    grid-template-columns: 100%;
  }
`
