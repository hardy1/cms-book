import styled from 'styled-components'
import colors from '../../../resources/colors'
import media from '../../../resources/media'

export const ContainerWrapper = styled.div`
  display: grid;
  margin: 75px auto 0 auto;

  column-gap: 1rem;
  grid-template-columns: repeat(12, 1fr);
  position: relative;
`

export const TextContainer = styled.div`
  grid-column: 1 / 9;
  text-align: left;
  margin-bottom: 2rem;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
    grid-row: 1 / 2;
    text-align: center;
  }
`

export const ParagraphContainer = styled.div`
  margin-top: 32px;

  & > *:not(:first-child) {
    margin-top: 2em;
  }
`
export const RelatedFormulaContainer = styled.div`
  grid-column: 9 / -1;
  grid-row: 1 / 3;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
    grid-row: 3 / 4;
    margin-top: 4rem;
  }
`

export const FormulasContainer = styled.div`
  grid-column: 1 / -1;
  width: 100%;
`

export const FormulaContainer = styled.div`
  grid-column: 1 / 9;
  border-top: 1px solid ${colors.quaternaryColor};
  padding-top: 1rem;

  @media ${media.tabletM} {
    grid-column: 1 / -1;
    grid-row: 2 / 3;
  }
`

export const Formulas = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 2em;
  margin: 0;

  p {
    grid-column: 1 / -1;
    margin: 0;
  }

  iframe {
    height: 50vh;
    border: none;
  }

  @media ${media.laptop} {
    grid-template-columns: repeat(3, 1fr);
  }

  @media ${media.tabletM} {
    grid-template-columns: repeat(2, 1fr);
    grid-column: 1 / -1;
  }

  @media ${media.mobileL} {
    grid-template-columns: auto;
  }
`
