import styled from 'styled-components'
import media from '../../../resources/media'

export const TitleContainer = styled.div`
  display: block;
  grid-column: 1 / -1;
  margin-top: 50px;
`

export const SelectContainer = styled.div`
  display: block;
  grid-column: 1 / -1;
  margin-top: 16px;
`

export const CongressoContainer = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(3, 1fr);
  row-gap: 2em;
  margin-top: 60px;

  @media ${media.laptop} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${media.tabletM} {
    grid-template-columns: auto;
  }
`

export const ButtonContainer = styled.div`
  grid-column: 1 / -1;
  display: flex;
  justify-content: flex-start;
  margin-top: 60px;
  margin-bottom: 100px;
`
