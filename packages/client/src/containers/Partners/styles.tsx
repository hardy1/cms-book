import styled from 'styled-components'
import media from '../../resources/media'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  grid-column: 2 / -2;
  justify-content: flex-end;
  margin-top: 60px;
`

const LogosContainer = styled.div`
  display: grid;
  column-gap: 1rem;
  grid-column: 1 / -1;
  grid-template-columns: repeat(2, 1fr);
  width: 100%;

  @media ${media.tabletM} {
    grid-template-columns: auto;
  }
`

const ContentPartners = styled.div`
  width: 100%;

  @media ${media.tabletL} {
    &:not(:first-child) {
      margin-top: 64px;
    }
  }
`

const Title = styled.h6`
  color: #000;
  display: block;
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  text-transform: uppercase;
  margin: 0;
`

const Logos = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-directions: row;
  align-items: flex-start;
  justify-content: space-between;
  width: 100%;
  margin-top: 30px;

  @media screen and (min-width: 768px) {
    align-items: center;
    justify-content: flex-start;
  }

  a {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 50%;
    height: 80px;
    box-sizing: border-box;
    padding-right: 8px;
    padding-left: 8px;
    margin-bottom: 32px;

    @media screen and (min-width: 768px) {
      width: auto;
      height: auto;
      padding-right: 0;
      padding-left: 0;
    }

    &.link-block-mobile {
      width: 100%;
      justify-content: center;

      @media screen and (min-width: 768px) {
        width: auto;
      }
    }

    img {
      @media screen and (min-width: 768px) {
        margin-right: 60px;
        margin-bottom: 40px;
      }

      &.astellas-oncologia {
        width: 100%;

        @media screen and (min-width: 768px) {
          width: 150px;
        }
      }

      &.daiichi {
        width: auto;
        height: 70px;

        @media screen and (min-width: 768px) {
          width: 95px;
          height: auto;
        }
      }

      &.libbs {
        width: auto;
        height: 40px;

        @media screen and (min-width: 768px) {
          width: 90px;
          height: auto;
        }
      }

      &.lilly-oncologia {
        width: auto;
        height: 70px;

        @media screen and (min-width: 768px) {
          width: 95px;
          height: auto;
        }
      }

      &.pfizer {
        width: 100%;

        @media screen and (min-width: 768px) {
          width: 150px;
        }
      }

      &.albert-einstein {
        max-width: 100%;
        max-height: 70px;

        @media screen and (min-width: 768px) {
          width: 220px;
        }
      }

      &.bp {
        width: 100%;
      }

      &.instituto-vencer {
        max-width: 100%;
        max-height: 60px;

        @media screen and (min-width: 768px) {
          width: 120px;
        }
      }
    }
  }
`

const PartnersContainer = styled.section`
  width: 100%;
  padding-top: 40px;
  box-sizing: border-box;
`

export { PartnersContainer, Logos, Title, ContentPartners, LogosContainer, Container }
