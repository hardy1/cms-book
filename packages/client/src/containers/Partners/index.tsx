import React from 'react'
import { LogosContainer, ContentPartners, Title, Logos, PartnersContainer } from './styles'

const Partners: React.FC = () => {
  return (
    <PartnersContainer>
      <LogosContainer>
        <ContentPartners>
          <Title>Apoio</Title>

          <Logos>
            <a href="https://www.astellas.com" rel="noreferrer" target="_blank">
              <img src="/img/partner-1.png" className="astellas-oncologia" />
            </a>

            <a href="https://www.daiichisankyo.com.br/site/" rel="noreferrer" target="_blank">
              <img src="/img/daiichi.png" className="daiichi" />
            </a>

            <a href="https://www.libbs.com.br/" rel="noreferrer" target="_blank">
              <img src="/img/partner-2.png" className="libbs" />
            </a>

            <a href="https://www.lilly.com.br/" rel="noreferrer" target="_blank">
              <img src="/img/lilly-oncologia.png" className="lilly-oncologia" />
            </a>

            <a href="https://www.pfizer.com.br/" rel="noreferrer" target="_blank">
              <img src="/img/pfizer.png" className="pfizer" />
            </a>
          </Logos>
        </ContentPartners>

        <ContentPartners>
          <Title>Parceiros</Title>

          <Logos>
            <a className="link-block-mobile" href="https://www.einstein.br" rel="noreferrer" target="_blank">
              <img src="/img/partner-3.png" className="albert-einstein" />
            </a>

            <a href="https://www.bp.org.br/" rel="noreferrer" target="_blank">
              <img src="/img/bp.png" className="bp" />
            </a>

            <a href="https://vencerocancer.org.br/" rel="noreferrer" target="_blank">
              <img src="/img/partner-4.png" className="instituto-vencer" />
            </a>
          </Logos>
        </ContentPartners>
      </LogosContainer>
    </PartnersContainer>
  )
}

export default Partners
