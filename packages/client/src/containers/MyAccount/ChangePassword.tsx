import React, { useState, useEffect } from 'react'
import * as Yup from 'yup'
import { Heading1, Input, Button, FormGroup } from 'designsystem'
import { PasswordSchema, OldPasswordSchema } from '../../resources/formValidationSchemas'
import { useFormik } from 'formik'
import { toast } from '../../components/Toast'
import { Form } from '../Form'
import { updatePassword } from '../../services/auth'
import { IPasswordReset } from '../../types'
import { PasswordInitialValues, OldPasswordInitialValues } from '../../resources/formInitialValues'
import { ContentContainer } from '../../components'
import { size } from '../../resources/media'
import { PasswordFormId, PasswordFormLabel } from '../../enums'

const validationSchema = Yup.object({
  ...PasswordSchema,
  ...OldPasswordSchema,
})

const initialValues = {
  ...PasswordInitialValues,
  ...OldPasswordInitialValues,
}

const ChangePassword: React.FC<{ title?: string }> = ({ title }) => {
  const [error, setError] = useState<string>('')

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values: IPasswordReset) => {
      setError('')

      try {
        const response = await updatePassword(values)
        if (response?.message) {
          toast.notify(
            response?.message?.toLowerCase()?.includes('success') ? 'Senha alterada com sucesso' : response?.message,
          )
          formik.resetForm()
        }
      } catch (e) {
        setError(e)
      }
    },
  })

  useEffect(() => {
    error &&
      toast.notify(error?.toLowerCase()?.includes('error') ? 'Falha na alteração da senha' : error, { type: 'danger' })
  }, [error])

  return (
    <>
      <Heading1>{title}</Heading1>

      <ContentContainer type="flex" margins="3rem 0 0 0" maxWidth={size.mobile}>
        <Form onSubmit={formik.handleSubmit} style={{ margin: 0 }}>
          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps(PasswordFormId.OLD_PASSWORD)}
              id={PasswordFormId.OLD_PASSWORD}
              label={PasswordFormLabel.OLD_PASSWORD}
              type="password"
              autoComplete="password"
              required
              error={formik.touched.oldPassword && Boolean(formik.errors.oldPassword)}
              message={formik.touched.oldPassword && formik.errors.oldPassword}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps(PasswordFormId.PASSWORD)}
              id={PasswordFormId.PASSWORD}
              label={PasswordFormLabel.PASSWORD}
              type="password"
              required
              error={formik.touched.password && Boolean(formik.errors.password)}
              message={formik.touched.password && formik.errors.password}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps(PasswordFormId.CONFIRM_PASSWORD)}
              id={PasswordFormId.CONFIRM_PASSWORD}
              label={PasswordFormLabel.CONFIRM_PASSWORD}
              type="password"
              required
              error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
              message={formik.touched.confirmPassword && formik.errors.confirmPassword}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1" marginTop="3rem">
            <Button type="submit">Salvar alterações</Button>
          </FormGroup>
        </Form>
      </ContentContainer>
    </>
  )
}

export default ChangePassword
