import React from 'react'
import { Heading1 } from 'designsystem'
import { InfoBlock } from '../../components'
import { useRouter } from 'next/router'

import NotificationAlert from './NotificationAlert'
import { IInfoBlock, IUser } from '../../types'
import WrapperTittleNotification from './WrapperTittleNotification'

const PlanSignUpData: React.FC<{ user?: IUser; title?: string; boards?: IInfoBlock }> = ({ user, title, boards }) => {
  const router = useRouter()

  return (
    <>
      <WrapperTittleNotification className="SpaceBottom">
        <Heading1>{title}</Heading1>
        <NotificationAlert user={user} />
      </WrapperTittleNotification>

      <InfoBlock
        className="Infoblock__SubscriptionDataDetails"
        header={boards?.subscription?.title}
        content={boards?.subscription?.body}
        buttonText="Alterar plano"
        onClickDetails={() => {
          router.push('/cadastro/selecionar-plano')
        }}
        columnFlow
      />
    </>
  )
}

export default PlanSignUpData
