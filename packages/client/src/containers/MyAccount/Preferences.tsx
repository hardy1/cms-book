import React from 'react'
import { Heading1, Button } from 'designsystem'
import { InfoBlock } from '../../components'
import { IInfoBlock, IUser } from '../../types'
import styled from 'styled-components'
import Link from 'next/link'

const TermsOfConditionsWrapper = styled.div`
  margin-top: 2rem;
`

const Preferences: React.FC<{ title?: string; boards?: IInfoBlock; user?: IUser }> = ({ title, boards, user }) => {
  return (
    <>
      <Heading1>{title}</Heading1>

      <InfoBlock className="Infoblock__Privacy" title="Privacidade" content={boards?.privacy?.body} columnFlow>
        <TermsOfConditionsWrapper>
          <p className="Infoblock__TermsAndConitions">
            Veja aqui os&nbsp;
            <Link passHref href="/page/termo-de-uso-politica-de-privacidade-e-assinatura">
              <a>termos e condições</a>
            </Link>
            &nbsp; de uso deste site.
          </p>
        </TermsOfConditionsWrapper>
      </InfoBlock>

      <InfoBlock title="Notificações" columnFlow>
        <Button
          onClick={() =>
            window.open('https://conteudo.mocbrasil.com/email/unsubscribe/62e59fcdeb5b5134641999', '_blank')
          }
          type="button"
        >
          Gerenciar preferências
        </Button>
      </InfoBlock>
    </>
  )
}

export default Preferences
