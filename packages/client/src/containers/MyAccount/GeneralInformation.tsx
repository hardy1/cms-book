import React from 'react'
import { Heading1 } from 'designsystem'
import { InfoBlock } from '../../components'
import { IUser, IInfoBlock, IBlockListValueSet } from '../../types'
import { DashboardContext } from '../../contexts'
import { dashboardMenu } from '../../resources/dashboardSetup'
import NotificationAlert from './NotificationAlert'
import WrapperTittleNotification from './WrapperTittleNotification'

const GeneralInformation: React.FC<{
  title?: string
  boards?: IInfoBlock
  institutions?: IBlockListValueSet[]
  user?: IUser
}> = ({ title, boards, institutions, user }) => {
  return (
    <DashboardContext.Consumer>
      {({ state, setState }) => (
        <>
          <WrapperTittleNotification>
            <Heading1>{title}</Heading1>
            <NotificationAlert user={user} />
          </WrapperTittleNotification>

          <InfoBlock
            className="Infoblock__SubscriptionData"
            title="Dados da Assinatura"
            header={boards?.plan?.title}
            content={boards?.plan?.body}
            onClickDetails={() => {
              setState({ ...state, currentMenu: dashboardMenu.PLANSIGNUP })
            }}
            columnFlow
          />

          <InfoBlock
            className="Infoblock__PersonalData"
            title="Dados Pessoais"
            content={boards?.personal?.body}
            onClickDetails={() => {
              setState({ ...state, currentMenu: dashboardMenu.PERSONAL })
            }}
            buttonText="Editar Informações"
          />

          <InfoBlock
            className="Infoblock__ProfessionalData"
            title="Dados Profissionais"
            content={boards?.professional?.body}
            contentList={institutions}
            onClickDetails={() => {
              setState({ ...state, currentMenu: dashboardMenu.PROFESSIONAL })
            }}
            buttonText="Editar Informações"
          />

          <InfoBlock className="Infoblock__Privacy" title="Privacidade" content={boards?.privacy?.body} columnFlow />
        </>
      )}
    </DashboardContext.Consumer>
  )
}

export default GeneralInformation
