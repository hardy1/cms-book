import React, { useState, useEffect, useContext } from 'react'
import * as Yup from 'yup'
import { Heading1, Button, FormGroup } from 'designsystem'
import { Formik } from 'formik'
import { ProfessionForm, Form } from '../Form'
import { IUserProfessionalData, IProfession } from '../../types'
import { UserProfessionalSchema } from '../../resources/formValidationSchemas'
import { userProfessionalDataUpdate } from '../../services/data/user.service'
import { toast } from '../../components/Toast'
import { handleSpecializationId, handleProfessionRegister } from '../../resources'
import { DashboardContext } from '../../contexts'
import { ContentContainer } from '../../components'
import { size } from '../../resources/media'

const ProfessionalData: React.FC<{ title?: string; userProfession?: IProfession; professions?: IProfession[] }> = ({
  title,
  userProfession,
  professions,
}) => {
  const { state } = useContext(DashboardContext)
  const [error, setError] = useState()
  const userProfessionData: IUserProfessionalData = {
    profession: userProfession?.id?.toString(),
    specialization: userProfession?.specialization?.id?.toString(),
    specializationCustom: userProfession?.specializationOther,
    state: userProfession?.state,
    professionRegister: userProfession?.register,
    professionStatus: userProfession?.status,
    institutions: userProfession?.institutions,
    document: userProfession?.document,
    proofDocument: {
      id: userProfession?.proofDocument?.id,
    },
    formationDate: userProfession?.formationDate,
    finishedResidencyDate: userProfession?.finishedResidencyDate,
  }

  useEffect(() => {
    error && toast.notify(error, { type: 'danger' })
  }, [error])

  const formikProps = {
    enableReinitialize: true,
    initialValues: userProfessionData,
    validationSchema: Yup.object({
      ...UserProfessionalSchema,
      document: userProfessionData.proofDocument.id === undefined ? UserProfessionalSchema.document : Yup.mixed(),
    }),
    onSubmit: async (values: IUserProfessionalData) => {
      const specializationId = await handleSpecializationId({
        profession: Number(values.profession),
        specialization: values.specialization,
        professions: state.professions || professions,
      })
      const professionRegister = await handleProfessionRegister(values.professionRegister)

      try {
        const response = await userProfessionalDataUpdate({
          ...values,
          specialization: specializationId,
          professionRegister: professionRegister,
        })

        if (response?.status) {
          toast.notify(response?.message)
          if (typeof window !== undefined) {
            window.location.reload()
          }
        } else {
          throw new Error(response?.message)
        }
      } catch (e) {
        setError(e.message)
      }
    },
  }

  return (
    <Formik {...formikProps}>
      {(formik) => (
        <>
          <Heading1>{title}</Heading1>
          <ContentContainer type="flex" margins="3rem 0 0 0" maxWidth={size.laptop}>
            <Form onReset={formik.handleReset} onSubmit={formik.handleSubmit}>
              <ProfessionForm formik={formik} professions={state.professions || professions} />
              <FormGroup gridColumn="1 / -1" marginTop="3rem">
                <Button type="submit" disabled={!formik.isValid}>
                  Salvar alterações
                </Button>
              </FormGroup>
            </Form>
          </ContentContainer>
        </>
      )}
    </Formik>
  )
}

export default ProfessionalData
