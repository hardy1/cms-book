import GeneralInformation from './GeneralInformation'
import PersonalData from './PersonalData'
import ProfessionalData from './ProfessionalData'
import PlanSignUpData from './PlanSignUpData'
import Preferences from './Preferences'
import ChangePassword from './ChangePassword'

export { GeneralInformation, PersonalData, ProfessionalData, PlanSignUpData, Preferences, ChangePassword }
