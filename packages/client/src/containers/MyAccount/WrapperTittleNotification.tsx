import styled from 'styled-components'

const WrapperTittleNotification = styled.div`
  position: relative;
  margin-bottom: 0;

  @media screen and (min-width: 1024px) {
    margin-bottom: 24px;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  &.SpaceBottom {
    margin-bottom: 24px;

    @media screen and (min-width: 1024px) {
      margin-bottom: 48px;
    }
  }

  h1,
  h2,
  h3,
  h4,
  h5 {
    margin-bottom: 16px;

    @media screen and (min-width: 1024px) {
      margin-bottom: 0;
    }
  }

  .Dashboard {
    &__Alert {
      background-color: #9ea0a5;
      margin-bottom: 0;
    }
  }
`

export default WrapperTittleNotification
