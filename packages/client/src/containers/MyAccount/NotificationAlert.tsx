import React from 'react'
import { Alert } from 'designsystem'

import { IUser } from '../../types'

const NotificationAlert: React.FC<{ user?: IUser }> = ({ user }) => {
  const trialDays = user.subscription?.gateway?.plan?.trial_days || 0

  const isPlural = trialDays > 1 ? 's' : ''

  if (trialDays <= 0) {
    return null
  }

  return (
    <Alert
      type="warning"
      text={`${trialDays} dia${isPlural} restante${isPlural} na avaliação`}
      className="Dashboard__Alert"
    />
  )
}

export default NotificationAlert
