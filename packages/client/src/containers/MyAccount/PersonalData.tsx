import React, { useState, useEffect } from 'react'
import * as Yup from 'yup'
import { Heading1, Button, Input, Select, FormGroup } from 'designsystem'
import { Form, FormDataContentField } from '../Form'
import { useFormik } from 'formik'
import { UserPersonalSchema, UserRegistrationSchema, addressSchema } from '../../resources/formValidationSchemas'
import { IUser, IAddress } from '../../types'
import { userPersonalDataUpdate } from '../../services/data/user.service'
import { toast } from '../../components/Toast'
import { languages } from '../../resources/mocks/common/index'
import UserPersonalForm from '../User/UserPersonalForm'
import MASK from '../../resources/mask'
import { ContentContainer } from '../../components'
import { size } from '../../resources/media'
import { PersonalFormId, PersonalFormLabel, FormPlaceholder } from '../../enums'

const validationSchema = Yup.object({
  ...UserRegistrationSchema,
  phone: UserPersonalSchema.phone,
  zipcode: addressSchema.zipcode,
  country: addressSchema.country,
  state: addressSchema.state,
  street: addressSchema.street,
  number: addressSchema.number,
  extention: addressSchema.extention,
})

interface UserPersonalDataProps extends IUser, IAddress {}

const PersonalData: React.FC<{ title?: string; user?: IUser }> = ({ title, user }) => {
  const [error, setError] = useState()
  const [userData] = useState<UserPersonalDataProps>({
    name: user?.name || '',
    email: user?.email || '',
    language: user?.language || '',
    cpfDocument: user?.cpfDocument || '',
    phone: user?.phone || '',
    zipcode: user?.address?.zipcode || '',
    city: user?.address?.city || '',
    state: user?.address?.state || '',
    country: 'BR',
    street: user?.address?.street || '',
    number: user?.address?.number || '',
    extention: user?.address?.extention || '',
  })

  const formik = useFormik({
    initialValues: userData,
    validationSchema,
    onSubmit: async (values) => {
      try {
        const response = await userPersonalDataUpdate({
          ...values,
          cpfDocument: values.cpfDocument?.replace(/\D/g, ''),
          zipcode: values.zipcode?.replaceAll(/\D/g, ''),
          phone: values.phone?.replaceAll(/[\s()-]/g, ''),
        })
        if (response?.status) {
          toast.notify(response?.message)
          if (typeof window !== undefined) {
            window.location.reload()
          }
        } else {
          throw new Error(response?.message)
        }
      } catch (e) {
        setError(e.message)
      }
    },
  })

  useEffect(() => {
    error && toast.notify(error, { type: 'danger' })
  }, [error])

  return (
    <>
      <Heading1>{title}</Heading1>

      <ContentContainer type="flex" margins="3rem 0 0 0" maxWidth={size.laptop}>
        <Form onSubmit={formik.handleSubmit} style={{ width: '100%' }}>
          <FormGroup gridColumn="1 / 9">
            <FormDataContentField size={100}>
              <Input
                {...formik.getFieldProps(PersonalFormId.NAME)}
                id={PersonalFormId.NAME}
                label={PersonalFormLabel.NAME}
                placeholder={FormPlaceholder.FULLNAME}
                type="text"
                autoComplete="name"
                required
                error={formik.touched.name && Boolean(formik.errors.name)}
                message={formik.touched.name && formik.errors.name}
              />
            </FormDataContentField>
          </FormGroup>

          <FormGroup gridColumn="9 / -1">
            <Input
              {...formik.getFieldProps(PersonalFormId.CPF_DOCUMENT)}
              mask={MASK.cpf}
              id={PersonalFormId.CPF_DOCUMENT}
              label={PersonalFormLabel.CPF_DOCUMENT}
              placeholder={FormPlaceholder.CPF_DOCUMENT}
              type="text"
              required
              error={formik.touched.cpfDocument && Boolean(formik.errors.cpfDocument)}
              message={formik.touched.cpfDocument && formik.errors.cpfDocument}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / 4">
            <Input
              {...formik.getFieldProps(PersonalFormId.PHONE)}
              mask={MASK.genericPhone}
              id={PersonalFormId.PHONE}
              label={PersonalFormLabel.PHONE}
              type="text"
              autoComplete="tel"
              required
              error={formik.touched.phone && Boolean(formik.errors.phone)}
              message={formik.touched.phone && formik.errors.phone}
            />
          </FormGroup>

          <FormGroup gridColumn="4 / 8">
            <Input
              {...formik.getFieldProps(PersonalFormId.EMAIL)}
              id={PersonalFormId.EMAIL}
              label={PersonalFormLabel.EMAIL}
              placeholder={FormPlaceholder.EMAIL}
              autoComplete="email"
              type="email"
              required
              error={formik.touched.email && Boolean(formik.errors.email)}
              message={formik.touched.email && formik.errors.email}
            />
          </FormGroup>

          <FormGroup gridColumn="8 / -1">
            <Select
              {...formik.getFieldProps(PersonalFormId.LANGUAGE)}
              id={PersonalFormId.LANGUAGE}
              label={PersonalFormLabel.LANGUAGE}
              placeholder={FormPlaceholder.SELECT}
              items={languages}
              disabled
            />
          </FormGroup>

          <UserPersonalForm formik={formik} />

          <FormGroup gridColumn="1 / -1" marginTop="3rem">
            <Button type="submit" disabled={!formik.isValid}>
              Salvar alterações
            </Button>
          </FormGroup>
        </Form>
      </ContentContainer>
    </>
  )
}

export default PersonalData
