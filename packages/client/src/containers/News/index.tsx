import React from 'react'
import Link from 'next/link'
import { CardNews, Button, GridContainer, Heading2 } from 'designsystem'
import { Container, Header, NewsContainer } from './styles'
import { ChannelSection, ChannelSlug } from '../../enums'
import { IHomeItem } from '../../types'

export interface NewsProps {
  news: IHomeItem[] | undefined
}

const News: React.FC<NewsProps> = ({ news }: NewsProps) => {
  return (
    <GridContainer>
      <Container>
        <Header>
          <Link href="/canais-moc/noticias" passHref>
            <Button as="a" fill="text">
              <Heading2 color="primaryColor">Últimas notícias</Heading2>
            </Button>
          </Link>
        </Header>

        <NewsContainer>
          {news &&
            news.map(({ id, image, categories, title, reading_time, published_at, slug }) => (
              <CardNews
                key={id}
                imgSrc={image}
                imgAlt={title}
                title={title}
                category={categories.map((category) => category?.name).join(', ')}
                readingTime={reading_time.toString()}
                date={new Date(published_at)}
                thumbnailSize="md"
                url={`${ChannelSection.SLUG}/${ChannelSlug.NEWS}/${slug}`}
              />
            ))}
        </NewsContainer>
      </Container>
    </GridContainer>
  )
}

export default News
