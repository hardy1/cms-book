import styled from 'styled-components'
import media from '../../resources/media'

export const Container = styled.div`
  grid-column: 1 / -1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-top: 120px;
`

export const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;

  @media ${media.tabletM} {
    flex-direction: column;
    align-items: flex-start;
  }
`

export const NewsContainer = styled.div`
  column-gap: 1em;
  row-gap: 40px;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(3, 1fr);
  margin-top: 20px;

  @media ${media.laptopL} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${media.tabletM} {
    grid-template-columns: auto;
  }
`
