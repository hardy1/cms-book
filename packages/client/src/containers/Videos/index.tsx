import React from 'react'
import styled from 'styled-components'
import { CardThumbVideo, Button, Heading6 } from 'designsystem'

const Container = styled.div`
  grid-column: 7 / 10;
  display: flex;
  flex-direction: column;
  height: 800px;
  justify-content: center;
`

const VideosContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-around;
`

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  justify-self: flex-end;
`

const Videos: React.FC = () => {
  return (
    <Container>
      <Heading6>Vídeo-MOC</Heading6>
      <VideosContainer>
        <CardThumbVideo
          url="https://picsum.photos/305/140"
          title="Inibidores de ICDK4/6 não são todos iguais (foco em Abemaciclibe)."
          subtitle="Volume 11 - Número 06"
          date={1594609200000}
        />
        <CardThumbVideo
          url="https://picsum.photos/305/140"
          title="Inibidores de ICDK4/6 não são todos iguais (foco em Abemaciclibe)."
          subtitle="Volume 11 - Número 06"
          date={1594609200000}
        />
      </VideosContainer>
      <ButtonContainer>
        <Button fill="text" onClick={() => null}>
          Veja todas os vídeos
        </Button>
      </ButtonContainer>
    </Container>
  )
}

export default Videos
