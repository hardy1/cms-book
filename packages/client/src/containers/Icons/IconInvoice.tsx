import React from 'react'

interface IconInvoiceProps {
  width?: number
  height?: number
  fill?: string
}

const IconInvoice = ({ width, height, fill }: IconInvoiceProps) => (
  <svg
    width={width || 15}
    height={height || 13}
    viewBox={`0 0 ${width ? width : 15} ${height ? height : 13}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 0.5H1.5V12.5H0V0.5ZM3 0.5H4.5V12.5H3V0.5ZM5.25 0.5H7.5V12.5H5.25V0.5ZM8.25 0.5H9.75V12.5H8.25V0.5ZM10.5 0.5H12V12.5H10.5V0.5ZM12.75 0.5H15V12.5H12.75V0.5Z"
      fill={fill || '#ffffff'}
    />
  </svg>
)

export default IconInvoice
