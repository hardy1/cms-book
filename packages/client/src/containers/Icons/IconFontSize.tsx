import React from 'react'
import styled from 'styled-components'
import colors from '../../resources/colors'

interface IconFontSizeProps {
  size?: number
  fill?: string
  active?: string
  isActive?: boolean
  className?: string
  increaseFont?: boolean
  defaultSize?: boolean
  onClick?: (e: React.MouseEvent<HTMLElement>) => void
}

const IconFontSizeBase = ({ defaultSize, increaseFont, className, onClick }: IconFontSizeProps) => {
  const increaseFontPath =
    'M4.12 9 6.5 2.67 8.87 9H4.12ZM5.5 0 0 14H2.25L3.37 11H9.62L10.75 14H13L7.5 0H5.5ZM17 2 12 7.07 13.41 8.5 16 5.9V12H18V5.9L20.59 8.5 22 7.07 17 2Z'
  const decreaseFontPath =
    'M4.12 9L6.5 2.67L8.87 9H4.12ZM5.5 0L0 14H2.25L3.37 11H9.62L10.75 14H13L7.5 0H5.5ZM17 12L22 6.93L20.59 5.5L18 8.1V2H16V8.1L13.41 5.5L12 6.93L17 12Z'
  const defaultFontPath = 'M4.12 9L6.5 2.67L8.87 9H4.12ZM5.5 0L0 14H2.25L3.37 11H9.62L10.75 14H13L7.5 0H5.5Z'

  return (
    <div className={className} onClick={onClick}>
      <div className="fontSize">
        <svg viewBox={defaultSize ? '0 0 13 13' : '0 0 20 12'} fill="none" xmlns="http://www.w3.org/2000/svg">
          {defaultSize ? <path d={defaultFontPath} /> : <path d={increaseFont ? increaseFontPath : decreaseFontPath} />}
        </svg>
        <span>{defaultSize ? 'Font Padrão' : increaseFont ? 'Aumentar Fonte' : 'Diminuir Fonte'}</span>
      </div>
    </div>
  )
}

const IconFontSize = styled(IconFontSizeBase) <IconFontSizeProps>`
  display: flex;
  justify-content: flex-start;
  align-content: center;
  cursor: pointer;
  position: relative;
  &.active {
    .fontSize {
      background-color: ${colors.disabledColor};

      path {
        fill: ${colors.black};
      }
    }
  }

  .fontSize {
    display: inline-flex;
    position: relative;
    justify-content: center;
    align-items: center;
    background-color: ${(props) => (props.isActive ? colors.disabledColor : 'transparent')};
    width: 100%;
    height: 46px;

    svg {
      position: absolute;
      width: 40%;
      height: 40%;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      padding-right: 2px;

      path {
        fill: ${colors.black};
      }
    }

    span {
      position: absolute;
      left: 39px;
      background-color: ${colors.neutralColor};
      color: ${colors.white};
      padding: 5px 10px;
      font-size: 12px;
      border-radius: 5px;
      opacity: 0;
      visibility: hidden;
      transition: all 0.2s linear;
      width: ${(props) => (props.defaultSize ? '70px' : '90px')};

      &:before {
        content: '';
        width: 10px;
        height: 10px;
        background-color: ${colors.neutralColor};
        position: absolute;
        left: -4px;
        top: 6px;
        transform: rotate(45deg);
      }
    }

    &:hover {
      span {
        left: 63px;
        opacity: 1;
        visibility: visible;
      }
    }
  }

  :hover,
  :active {
    .fontSize {
      background-color: ${(props) => (props.active ? props.active : '#C9CBCD')};
    }
  }
`

export default IconFontSize
