import IconArrowRightAlternative from './IconArrowRightAlternative'
import IconArrowRight from './IconArrowRight'
import IconBook from './IconBook'
import IconBookPart from './IconBookPart'
import IconBookIndex from './IconBookIndex'
import IconBookmark from './IconBookmark'
import IconCalculator from './IconCalculator'
import IconNightMode from './IconNightMode'
import IconFontSize from './IconFontSize'
import IconFullscreen from './IconFullscreen'
import IconImage from './IconImage'
import IconInvoice from './IconInvoice'
import IconLeave from './IconLeave'
import IconPDF from './IconPDF'
import IconSearchIn from './IconSearchIn'
import IconSetup from './IconSetup'
import IconVideo from './IconVideo'
import IconCheck from './IconCheck'
import IconDanger from './IconDanger'
import IconFacebookSquare from './IconFacebookSquare'
import IconGmail from './IconGmail'
import IconGoogle from './IconGoogle'
import IconInformation from './IconInformation'
import IconOutlook from './IconOutlook'
import IconYahoo from './IconYahoo'

export {
  IconArrowRightAlternative,
  IconArrowRight,
  IconBook,
  IconBookPart,
  IconBookIndex,
  IconBookmark,
  IconCalculator,
  IconNightMode,
  IconFontSize,
  IconFullscreen,
  IconImage,
  IconInvoice,
  IconLeave,
  IconPDF,
  IconSearchIn,
  IconSetup,
  IconVideo,
  IconCheck,
  IconDanger,
  IconFacebookSquare,
  IconGoogle,
  IconInformation,
  IconOutlook,
  IconYahoo,
  IconGmail,
}
