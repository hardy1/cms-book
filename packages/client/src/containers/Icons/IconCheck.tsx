import React from 'react'

interface IconCheckProps {
  width?: number
  height?: number
  fill?: string
}

const IconCheck = ({ width, height, fill }: IconCheckProps) => (
  <svg
    width={width ? width : 11}
    height={height ? height : 9}
    viewBox={`0 0 ${width ? width : 11} ${height ? height : 9}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.8668 1.0848L10.0041 0.146569C9.82548 -0.047743 9.53464 -0.0490228 9.35454 0.143761L3.94382 5.93454L1.65508 3.44516C1.47643 3.25085 1.18559 3.24957 1.00549 3.44231L0.135849 4.37303C-0.0442507 4.56577 -0.0454375 4.87956 0.133246 5.07391L3.60819 8.85343C3.78684 9.04774 4.07768 9.04902 4.25778 8.85624L10.8642 1.78569C11.0443 1.5929 11.0454 1.27911 10.8668 1.0848Z"
      fill={fill ? fill : '#fff'}
    />
  </svg>
)

export default IconCheck
