import React from 'react'
import colors from '../../resources/colors'

interface IconBookPartProps {
  width?: number
  height?: number
  fill?: string
}

const IconBookPart = ({ width, height, fill }: IconBookPartProps) => (
  <svg
    width={width ? width : 20}
    height={height ? height : 20}
    viewBox={`0 0 ${width ? width : 20} ${height ? height : 20}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M18 9.61L16.59 11L11.58 6L16.59 1L18 2.39L14.44 6L18 9.61ZM0 0H13V2H0V0ZM0 7V5H10V7H0ZM0 12V10H13V12H0Z"
      fill={fill ? fill : colors.neutralColor}
    />
  </svg>
)

export default IconBookPart
