import React from 'react'
import colors from '../../resources/colors'

interface IconLeaveProps {
  width?: number
  height?: number
  fill?: string
}

const IconLeave = ({ width, height, fill }: IconLeaveProps) => (
  <svg
    width={width ? width : 20}
    height={height ? height : 20}
    viewBox={`0 0 ${width ? width : 20} ${height ? height : 20}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M15 12.5V10H8.75V7.5H15V5L18.75 8.75L15 12.5ZM13.75 11.25V16.25H7.5V20L0 16.25V0H13.75V6.25H12.5V1.25H2.5L7.5 3.75V15H12.5V11.25H13.75Z"
      fill={fill ? fill : colors.neutralColor}
    />
  </svg>
)

export default IconLeave
