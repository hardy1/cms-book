import React from 'react'

interface IconArrowRightProps {
  width?: number
  height?: number
  fill?: string
}

const IconArrowRight = ({ width, height, fill }: IconArrowRightProps) => (
  <svg
    width={width || 18}
    height={height || 18}
    viewBox={`0 0 ${width ? width : 18} ${height ? height : 18}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.37054 1.52332L8.22946 0.656128C8.59315 0.28894 9.18125 0.28894 9.54107 0.656128L17.0625 8.24597C17.4262 8.61316 17.4262 9.20691 17.0625 9.57019L9.54107 17.1639C9.17738 17.5311 8.58928 17.5311 8.22946 17.1639L7.37054 16.2968C7.00298 15.9257 7.01071 15.3202 7.38601 14.9569L12.0482 10.4725H0.928571C0.413988 10.4725 0 10.0546 0 9.53503V8.28503C0 7.7655 0.413988 7.34753 0.928571 7.34753H12.0482L7.38601 2.86316C7.00684 2.49988 6.99911 1.89441 7.37054 1.52332Z"
      fill={fill || '#000'}
    />
  </svg>
)

export default IconArrowRight
