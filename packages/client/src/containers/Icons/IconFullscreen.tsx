import React from 'react'
import styled from 'styled-components'
import colors from '../../resources/colors'

interface IconFullscreenProps {
  size?: number
  className?: string
  active?: string
  isActive?: boolean
  onClick?: (e: React.MouseEvent<HTMLElement>) => void
}

const IconFullscreenBase = ({ className, isActive, onClick }: IconFullscreenProps) => (
  <div className={className} onClick={onClick}>
    <div className="fullScreen">
      <svg viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M6.5 10.09L7.91 11.5L3.41 16H7V18H0V11H2V14.59L6.5 10.09ZM7.91 6.5L6.5 7.91L2 3.41V7H0V0H7V2H3.41L7.91 6.5ZM11.5 10.09L16 14.59V11H18V18H11V16H14.59L10.09 11.5L11.5 10.09ZM10.09 6.5L14.59 2H11V0H18V7H16V3.41L11.5 7.91L10.09 6.5Z" />
      </svg>
      <span>{isActive ? `Sair da Tela Cheia` : 'Tela Cheia'}</span>
    </div>
  </div>
)

const IconFullscreen = styled(IconFullscreenBase) <IconFullscreenProps>`
  display: flex;
  justify-content: flex-start;
  align-content: center;
  cursor: pointer;
  position: relative;

  .fullScreen {
    display: inline-flex;
    position: relative;
    justify-content: center;
    align-items: center;
    background-color: ${(props) => (props.isActive ? colors.disabledColor : 'transparent')};
    width: 100%;
    height: 46px;

    svg {
      position: absolute;
      width: 35%;
      height: 35%;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      padding-right: 2px;

      path {
        fill: ${colors.black};
        width: 100%;
        height: 100%;
      }
    }

    span {
      position: absolute;
      left: 39px;
      background-color: ${colors.neutralColor};
      color: ${colors.white};
      padding: 5px 10px;
      font-size: 12px;
      border-radius: 5px;
      opacity: 0;
      visibility: hidden;
      transition: all 0.2s linear;
      width: ${(props) => (props.isActive ? '95px' : '56px')};

      &:before {
        content: '';
        width: 10px;
        height: 10px;
        background-color: ${colors.neutralColor};
        position: absolute;
        left: -4px;
        top: 6px;
        transform: rotate(45deg);
      }
    }

    &:hover {
      span {
        left: 63px;
        opacity: 1;
        visibility: visible;
      }
    }
  }

  :hover,
  :active {
    .fullScreen {
      background-color: ${(props) => (props.active ? props.active : '#C9CBCD')};
    }
  }
`

export default IconFullscreen
