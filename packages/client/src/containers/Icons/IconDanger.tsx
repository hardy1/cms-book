import React from 'react'

interface IconDangerProps {
  width?: number
  height?: number
  fill?: string
}

const IconDanger = ({ width, height, fill }: IconDangerProps) => (
  <svg
    width={width ? width : 18}
    height={height ? height : 19}
    viewBox={`0 0 ${width ? width : 18} ${height ? height : 19}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M17.6949 3.7136L12.3808 9.07236L17.6949 14.4311C18.093 14.8326 18.093 15.4872 17.6949 15.8974L15.7649 17.8436C15.3668 18.2451 14.7176 18.2451 14.3109 17.8436L8.99675 12.4849L3.68266 17.8436C3.28453 18.2451 2.63541 18.2451 2.22863 17.8436L0.298594 15.8974C-0.0995312 15.4959 -0.0995312 14.8413 0.298594 14.4311L5.61269 9.07236L0.298594 3.7136C-0.0995312 3.31213 -0.0995312 2.65756 0.298594 2.24736L2.22863 0.301103C2.62676 -0.100368 3.27588 -0.100368 3.68266 0.301103L8.99675 5.65986L14.3109 0.301103C14.709 -0.100368 15.3581 -0.100368 15.7649 0.301103L17.6949 2.24736C18.1017 2.64883 18.1017 3.3034 17.6949 3.7136Z"
      fill={fill ? fill : '#fe4140'}
    />
  </svg>
)

export default IconDanger
