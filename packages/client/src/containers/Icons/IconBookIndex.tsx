import React from 'react'
import colors from '../../resources/colors'

interface IconBookIndexProps {
  width?: number
  height?: number
  fill?: string
}

const IconBookIndex = ({ width, height, fill }: IconBookIndexProps) => (
  <svg
    width={width ? width : 20}
    height={height ? height : 17}
    viewBox={`0 0 ${width ? width : 20} ${height ? height : 17}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 7.63625C1.55228 7.63625 2 7.16662 2 6.5873C2 6.00799 1.55228 5.53836 1 5.53836C0.447715 5.53836 0 6.00799 0 6.5873C0 7.16662 0.447715 7.63625 1 7.63625Z"
      fill={fill ? fill : colors.neutralColor}
    />

    <path
      d="M17.06 5.53836H4.94C4.42085 5.53836 4 5.97981 4 6.52437V6.65024C4 7.1948 4.42085 7.63625 4.94 7.63625H17.06C17.5791 7.63625 18 7.1948 18 6.65024V6.52437C18 5.97981 17.5791 5.53836 17.06 5.53836Z"
      fill={fill ? fill : colors.neutralColor}
    />

    <path
      d="M17.06 10.7831H0.94C0.420852 10.7831 0 11.2245 0 11.7691V11.895C0 12.4395 0.420852 12.881 0.94 12.881H17.06C17.5791 12.881 18 12.4395 18 11.895V11.7691C18 11.2245 17.5791 10.7831 17.06 10.7831Z"
      fill={fill ? fill : colors.neutralColor}
    />

    <path
      d="M17.06 0.29364H0.94C0.420852 0.29364 0 0.73509 0 1.27965V1.40552C0 1.95008 0.420852 2.39153 0.94 2.39153H17.06C17.5791 2.39153 18 1.95008 18 1.40552V1.27965C18 0.73509 17.5791 0.29364 17.06 0.29364Z"
      fill={fill ? fill : colors.neutralColor}
    />
  </svg>
)

export default IconBookIndex
