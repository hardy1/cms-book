import React from 'react'

const IconArrowRightAlternative = () => (
  <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1.9502 1L6.89993 5.94976L1.9502 10.8995" stroke="white" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
)

export default IconArrowRightAlternative
