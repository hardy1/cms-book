import React from 'react'
import Link from 'next/link'
import { Button, GridContainer, Heading2 } from 'designsystem'
import { Container, Header, LogosContainer } from './styles'
import { ChannelName, ChannelSection, ChannelSlug } from '../../enums'
import { IHomeItem } from '../../types'

interface CongressesItemProps extends IHomeItem {
  encodedTitle?: string
}

export type CongressesProps = {
  congresses: CongressesItemProps[] | undefined
}

const Congresses: React.FC<CongressesProps> = ({ congresses }: CongressesProps) => {
  const congressList = congresses.map((congress) => ({
    ...congress,
    encodedTitle: encodeURIComponent(encodeURIComponent(congress.title)),
  }))

  return (
    <GridContainer>
      <Container>
        <Header>
          <Link href="/canais-moc/congressos" passHref>
            <Button as="a" fill="text">
              <Heading2 color="primaryColor">{ChannelName.CONGRESSES}</Heading2>
            </Button>
          </Link>
        </Header>

        <LogosContainer>
          {congressList &&
            congressList.map(({ id, image, title, slug }) => {
              return (
                <Link key={id} href={`/${ChannelSection.SLUG}/${ChannelSlug.CONGRESSES}/${slug}`} passHref>
                  <a>
                    <img key={id} src={image} alt={title} title={title} />
                  </a>
                </Link>
              )
            })}
        </LogosContainer>
      </Container>
    </GridContainer>
  )
}

export default Congresses
