import styled from 'styled-components'
import media from '../../resources/media'

export const Container = styled.div`
  grid-column: 1 / -1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin: 100px 0;
`

export const Header = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`

export const Hr = styled.hr`
  background-color: #eeeeee;
  border: 0;
  flex: 1;
  height: 1px;
  margin-left: 14px;
`

export const LogosContainer = styled.div`
  column-gap: 1em;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(3, 1fr);
  margin-top: 40px;
  row-gap: 2em;

  & img {
    cursor: pointer;
    max-width: 100%;
  }

  @media ${media.laptop} {
    grid-template-columns: repeat(2, 1fr);
  }

  @media ${media.mobileL} {
    grid-template-columns: auto;
  }

  & > * {
    align-self: center;
    margin: 0 auto;
  }
`
