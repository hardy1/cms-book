import styled from 'styled-components'

export const Container = styled.div`
  grid-column: 1 / -1;
  display: flex;
  justify-content: center;
  margin-top: 80px;
`

export const Link = styled.a`
  cursor: pointer;
`

export const Img = styled.img`
  height: auto;
  width: 100%;
`
