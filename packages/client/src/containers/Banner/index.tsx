import React from 'react'
import { GridContainer } from 'designsystem'
import { Container, Link, Img } from './styles'

const Banner: React.FC = () => {
  return (
    <GridContainer>
      <Container>
        <Link href="#">
          <Img src="/img/banner-moc.png" height={90} width={728} />
        </Link>
      </Container>
    </GridContainer>
  )
}

export default Banner
