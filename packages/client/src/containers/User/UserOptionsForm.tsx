import React from 'react'
import { GenericFormSection } from '../../types/generic-form-section.model'
import { FormGroup, CheckBox } from 'designsystem'
import { Label } from '../Form'
import styled from 'styled-components'

const HyperLink = styled.a`
  padding: 0 !important;
  font-size: inherit !important;
  font-family: inherit;
  font-weight: inherit;
  text-decoration: inherit;
`

const UserOptionsForm: React.FC<GenericFormSection> = ({ formik }) => {
  return (
    <>
      <FormGroup gridColumn="1 / -1">
        <CheckBox
          {...formik.getFieldProps('optIn')}
          checkImage="/img/Check.svg"
          id="optIn"
          name="optIn"
          checked={Boolean(formik.values.optIn)}
          error={formik.touched.optIn && Boolean(formik.errors.optIn)}
        />

        <Label htmlFor="optIn">Desejo acompanhar os assuntos em evidência na Oncologia por e-mail</Label>
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <CheckBox
          {...formik.getFieldProps('termsOfUse')}
          checkImage="/img/Check.svg"
          id="termsOfUse"
          name="termsOfUse"
          checked={Boolean(formik.values.termsOfUse)}
          error={formik.touched.termsOfUse && Boolean(formik.errors.termsOfUse)}
        />

        <Label htmlFor="termsOfUse">
          Li e aceito os
          <span>
            <HyperLink target="_blank" href="/page/termo-de-uso-politica-de-privacidade-e-assinatura">
              {` termos e condições `}
            </HyperLink>
          </span>
          de uso deste site. *
        </Label>
      </FormGroup>
    </>
  )
}

export default UserOptionsForm
