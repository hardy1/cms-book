import React from 'react'

import { FormGroup, Input, Select } from 'designsystem'
import MASK from '../../resources/mask'
import { GenericFormSection } from '../../types/generic-form-section.model'
import { countries, languages } from '../../resources/mocks/common/index'
import federativeStates from '../../resources/states-BR'

const UserRegistrationForm: React.FC<GenericFormSection> = ({ formik }) => {
  return (
    <>
      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('name')}
          id="name"
          label="Nome Completo"
          placeholder="Nome e sobrenome"
          type="text"
          autoComplete="name"
          required
          error={formik.touched.name && Boolean(formik.errors.name)}
          message={formik.touched.name && formik.errors.name}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('email')}
          id="email"
          label="Email"
          placeholder="Digite seu email"
          autoComplete="email"
          type="email"
          required
          error={formik.touched.email && Boolean(formik.errors.email)}
          message={formik.touched.email && formik.errors.email}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('password')}
          id="password"
          label="Senha"
          type="password"
          autoComplete="new-password"
          required
          error={formik.touched.password && Boolean(formik.errors.password)}
          message={formik.touched.password && formik.errors.password}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('confirmPassword')}
          id="confirmPassword"
          label="Confirme sua senha"
          type="password"
          autoComplete="off"
          required
          error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
          message={formik.touched.confirmPassword && formik.errors.confirmPassword}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Select
          {...formik.getFieldProps('country')}
          id="country"
          label="País"
          placeholder="Selecione um país"
          items={countries}
          disabled
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Select
          {...formik.getFieldProps('language')}
          id="language"
          label="Idioma"
          placeholder="Selecione"
          items={languages}
          disabled
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Select
          {...formik.getFieldProps('state')}
          id="state"
          label="Estado"
          autoComplete="address-level2"
          placeholder="Selecionar"
          items={federativeStates}
          required
          error={Boolean(formik.errors.state)}
          message={formik.errors.state}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('city')}
          type="text"
          id="city"
          label="Cidade"
          autoComplete="address-level2"
          required
          error={Boolean(formik.errors.city)}
          message={formik.errors.city}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('phone')}
          mask={MASK.genericPhone}
          id="phone"
          label="Telefone"
          placeholder="Ex: +551199999999"
          type="text"
          required
          error={formik.touched.phone && Boolean(formik.errors.phone)}
          message={formik.touched.phone && formik.errors.phone}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          mask={MASK.cpf}
          id="cpfDocument"
          label="CPF"
          placeholder="Ex: 111.222.333-44"
          type="text"
          required
          error={formik.touched.cpfDocument && Boolean(formik.errors.cpfDocument)}
          message={formik.touched.cpfDocument && formik.errors.cpfDocument}
          {...formik.getFieldProps('cpfDocument')}
        />
      </FormGroup>
    </>
  )
}

export default UserRegistrationForm
