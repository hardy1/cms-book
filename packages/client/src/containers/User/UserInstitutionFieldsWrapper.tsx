import styled from 'styled-components'

export const UserInstitutionContainer = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-column: 1 / -1;
  column-gap: 2.188rem;
  margin-top: 1rem;
`
