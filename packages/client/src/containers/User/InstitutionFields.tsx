import React from 'react'
import { FormGroup, Input, Select } from 'designsystem'
import { GenericFormSection } from '../../types/generic-form-section.model'
import { institutionTypeName, institutionTypeId } from '../../enums'

const intitutionType = [
  {
    id: 1,
    name: institutionTypeName.PUBLIC,
    value: institutionTypeId.PUBLIC,
  },
  {
    id: 2,
    name: institutionTypeName.PRIVATE,
    value: institutionTypeId.PRIVATE,
  },
]

const UserInstitutionField: React.FC<GenericFormSection> = ({ id, formik, dynamicFormik }) => {
  return (
    <>
      <FormGroup gridColumn="1 / 2">
        <Input
          {...formik.getFieldProps(`institutions.${id}.institution`)}
          required
          id={`institutions.${id}.institution`}
          type="text"
          label="Instituição"
          error={dynamicFormik?.errors?.institution}
          touched={dynamicFormik?.touched?.institution}
          message={dynamicFormik?.errors?.institution}
        />
      </FormGroup>

      <FormGroup gridColumn="2 / -1">
        <Select
          {...formik.getFieldProps(`institutions.${id}.institutionType`)}
          required
          id={`institutions.${id}.institutionType`}
          label="Tipo de instituição"
          placeholder="Selecione"
          items={intitutionType}
          error={dynamicFormik?.errors?.institutionType}
          touched={dynamicFormik?.touched?.institutionType}
          message={dynamicFormik?.errors?.institutionType}
        />
      </FormGroup>
    </>
  )
}

export default UserInstitutionField
