import React from 'react'
import styled from 'styled-components'
import { GenericFormSection } from '../../types/generic-form-section.model'
import UserInstitutionField from './InstitutionFields'
import { FieldArray, ArrayHelpers } from 'formik'
import { FormGroup, Button, Heading2 } from 'designsystem'
import { InstitutionInitialValues } from '../../resources/formInitialValues'
import { UserInstitutionContainer } from './UserInstitutionFieldsWrapper'
import colors from '../../resources/colors'
import UserInstitutionFormWrapper from './UserInstitutionFormWrapper'
import { IInstitution } from '../../types'

const StyledButton = styled(Button)`
  margin-left: -6.5px !important;
`
const StyledFormGroup = styled(FormGroup)`
  margin-bottom: 0 !important;

  .institution--add {
    margin: 0.9rem 0;
  }
`

const UserInstitutionForm: React.FC<GenericFormSection> = ({ formik }) => {
  const addInstitution = (push: ArrayHelpers['push']): void => {
    push(InstitutionInitialValues)
  }

  const removeInstitution = (id: number, remove: ArrayHelpers['remove']): void => {
    if (id === 0 && formik?.values?.institutions.length === 1) {
      alert('Você precisa preencher pelo menos uma instituição!')
    } else {
      remove(id)
    }
  }

  return (
    <>
      <Heading2 color="disabledColor">Instituições</Heading2>
      <FieldArray name="institutions">
        {({ remove, push }) => (
          <UserInstitutionFormWrapper>
            {formik?.values?.institutions?.map((institution: IInstitution, index: number) => {
              return (
                <UserInstitutionContainer key={index}>
                  <UserInstitutionField
                    id={index}
                    formik={formik}
                    dynamicFormik={{
                      errors: formik.errors.institutions?.length && formik.errors.institutions[index],
                      touched: formik.touched.institutions?.length && formik.touched.institutions[index],
                      message: formik.erros?.institutions,
                    }}
                  />
                  {formik?.values?.institutions?.length > 1 && (
                    <FormGroup key={index} gridColumn="1 / -1">
                      <StyledButton
                        type="button"
                        size="small"
                        fill="text"
                        color={colors.neutralColor}
                        className="institution--remove"
                        onClick={() => removeInstitution(index, remove)}
                      >
                        Remover instituição
                      </StyledButton>
                    </FormGroup>
                  )}
                </UserInstitutionContainer>
              )
            })}
            <StyledFormGroup gridColumn="1 / -1">
              <Button
                type="button"
                fill="text"
                color={colors.lightGray}
                className="institution--add"
                onClick={() => addInstitution(push)}
              >
                Adicionar instituição +
              </Button>
            </StyledFormGroup>
          </UserInstitutionFormWrapper>
        )}
      </FieldArray>
    </>
  )
}

export default UserInstitutionForm
