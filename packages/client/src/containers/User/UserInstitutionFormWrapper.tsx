import styled from 'styled-components'
import colors from '../../resources/colors'

const UserInstitutionFormWrapper = styled.div`
  grid-column: 1 / -1;

  .institution {
    &--add {
      margin: 1rem 0;
      text-decoration: underline;
      font-weight: normal;
    }
    &--remove {
      font-size: 0.8rem;
      font-weight: normal;
      margin: 0;

      &::before {
        content: '×';
        width: 1rem;
        height: 1rem;
        margin-right: 5px;
        padding: 2px;
        border-radius: 2px;
        color: #fff;
        background-color: ${colors.errorColor};
      }
    }
  }
`

export default UserInstitutionFormWrapper
