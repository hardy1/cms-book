import React, { useEffect, useState } from 'react'
import { FormGroup, Input, Select } from 'designsystem'
import { GenericFormSection } from '../../types/generic-form-section.model'
import cep from 'cep-promise'
import { toast } from '../../components/Toast'
import MASK from '../../resources/mask'
import { countries } from '../../resources/mocks/common/index'
import { PersonalFormId, PersonalFormLabel, FormPlaceholder } from '../../enums'

interface IAddressByCep {
  city: string
  country: string
  state: string
  street: string
  zipcode: string
}

const UserPersonalForm: React.FC<GenericFormSection> = ({ formik }) => {
  const [currentAddress, setCurrentAddress] = useState<IAddressByCep>()

  const addressByCep = async (postalCode: string) => {
    try {
      const address = postalCode ? await cep(postalCode, { timeout: 2000, providers: ['brasilapi'] }) : {}
      setCurrentAddress(address)
      return address
    } catch (error) {
      formik.setFieldValue('city', '', true)
      formik.setFieldValue('state', '', true)
      formik.setFieldValue('street', '', true)
      toast.notify(error.errors[0]?.message.split(/\s/).slice(0, 3).join(' '), { type: 'danger' })
    }
  }

  const customHandleBlur = async (e) => {
    const { value, id } = e.target
    if (!formik.errors[id] && formik.values[id]) {
      await addressByCep(value)
    }

    if (!formik.values[id]) {
      formik.setFieldValue('city', '', false)
      formik.setFieldValue('state', '', false)
    }
  }

  useEffect(() => {
    formik.setFieldError('zipcode', '')
  }, [])

  useEffect(() => {
    if (currentAddress?.city) {
      formik.setFieldValue('city', currentAddress?.city, false)
      formik.setFieldTouched('city', true, false)
      formik.setFieldError('city', undefined)
    }

    if (currentAddress?.state) {
      formik.setFieldValue('state', currentAddress?.state, false)
      formik.setFieldTouched('state', true, false)
      formik.setFieldError('state', undefined)
    }

    if (currentAddress?.street) {
      formik.setFieldValue('street', currentAddress?.street, false)
      formik.setFieldTouched('street', true, false)
      formik.setFieldError('street', undefined)
    }
  }, [currentAddress])

  return (
    <>
      <FormGroup gridColumn="1 / 4">
        <Select
          {...formik.getFieldProps(PersonalFormId.COUNTRY)}
          id={PersonalFormId.COUNTRY}
          label={PersonalFormLabel.COUNTRY}
          placeholder={FormPlaceholder.COUNTRY}
          items={countries}
          disabled
        />
      </FormGroup>

      <FormGroup gridColumn="4 / 7">
        <Input
          {...formik.getFieldProps(PersonalFormId.ZIPCODE)}
          mask={MASK.zipcodeBr}
          id={PersonalFormId.ZIPCODE}
          label={PersonalFormLabel.ZIPCODE}
          type="text"
          autoComplete="zipcode"
          required
          onBlur={customHandleBlur}
          error={formik.touched.zipcode && Boolean(formik.errors.zipcode)}
          message={formik.touched.zipcode && formik.errors.zipcode}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / 8">
        <Input
          {...formik.getFieldProps(PersonalFormId.STATE)}
          id={PersonalFormId.STATE}
          label={PersonalFormLabel.STATE}
          type="text"
          autoComplete="address-level2"
          required
          disabled
          error={formik.touched.state && Boolean(formik.errors.state)}
          message={formik.touched.state && formik.errors.state}
        />
      </FormGroup>

      <FormGroup gridColumn="8 / -1">
        <Input
          {...formik.getFieldProps(PersonalFormId.CITY)}
          id={PersonalFormId.CITY}
          label={PersonalFormLabel.CITY}
          type="text"
          autoComplete="address-level2"
          required
          disabled
          error={formik.touched.city && Boolean(formik.errors.city)}
          message={formik.touched.city && formik.errors.city}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 8">
        <Input
          {...formik.getFieldProps(PersonalFormId.STREET)}
          id={PersonalFormId.STREET}
          label={PersonalFormLabel.STREET}
          type="text"
          autoComplete="address-level1"
          required
          disabled
          error={formik.touched.street && Boolean(formik.errors.street)}
          message={formik.touched.street && formik.errors.street}
        />
      </FormGroup>

      <FormGroup gridColumn="8 / 9">
        <Input
          {...formik.getFieldProps(PersonalFormId.NUMBER)}
          mask={MASK.number}
          id={PersonalFormId.NUMBER}
          label={PersonalFormLabel.NUMBER}
          type="text"
          autoComplete="address-level1"
          required
          error={formik.touched.number && Boolean(formik.errors.number)}
          message={formik.touched.number && formik.errors.number}
        />
      </FormGroup>

      <FormGroup gridColumn="9 / -1">
        <Input
          {...formik.getFieldProps(PersonalFormId.EXTENTION)}
          id={PersonalFormId.EXTENTION}
          label={PersonalFormLabel.EXTENTION}
          type="text"
          autoComplete="address-level2"
          required
          error={formik.touched.extention && Boolean(formik.errors.extention)}
          message={formik.touched.extention && formik.errors.extention}
        />
      </FormGroup>
    </>
  )
}

export default UserPersonalForm
