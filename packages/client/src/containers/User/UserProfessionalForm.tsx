import React, { useState, useEffect } from 'react'
import { FormGroup, Input, Select } from 'designsystem'
import DatePicker, { registerLocale } from 'react-datepicker'
import styled from 'styled-components'
import ptBR from 'date-fns/locale/pt-BR'
import statusList from '../../resources/statusList'
import { GenericFormSection } from '../../types/generic-form-section.model'
import { IGenericItem, ISpecialization, IProfession } from '../../types'
import federativeStates from '../../resources/states-BR'
import {
  ProfessionalFormId,
  ProfessionalFormLabel,
  FormPlaceholder,
  ProfessionSlug,
  SpecializationSlug,
} from '../../enums'
import MASK from '../../resources/mask'

registerLocale('pt-BR', ptBR)
import 'react-datepicker/dist/react-datepicker.css'

interface UserProfessionalForm extends GenericFormSection {
  professions: IProfession[]
}

const StyledWrapperDatePicker = styled.div`
  .react-datepicker__navigation--previous,
  .react-datepicker__navigation--next {
    margin-top: 0 !important;
    min-width: 35px !important;
  }
`

const UserProfessionalForm: React.FC<UserProfessionalForm> = ({ formik, professions }) => {
  const [specializations, setSpecializations] = useState<ISpecialization[]>()
  const [activeCustomSpecialization, setActiveCustomSpecialization] = useState<boolean>(false)
  const [startDate, setStartDate] = useState<Date>()

  const forceUpdate: () => void = React.useState()[1].bind(null, {})

  const findProfessionById = (id: string): IProfession => {
    const foundProfession: IProfession = professions?.find((profession) => profession?.id === parseInt(id))

    return foundProfession
  }

  const findSpecializationById = (id: string) => {
    const foundSpecialization: ISpecialization = specializations?.find(
      (specialization) => specialization?.id === parseInt(id),
    )
    return foundSpecialization?.name
  }

  const findItemById = (id: string, items: IGenericItem[]) => items?.find((item) => item?.id === parseInt(id))

  const fetchSpecializations = async (value: string) => {
    const professionSpecialization: ISpecialization[] = findProfessionById(value).specializations
    setSpecializations(professionSpecialization)
  }

  const handleCustomSpecialization = (value: string) => {
    if (value !== SpecializationSlug.OTHER) {
      formik.setFieldValue(ProfessionalFormId.SPECIALIZATION_CUSTOM, undefined)
      setActiveCustomSpecialization(false)
    } else {
      setActiveCustomSpecialization(true)
    }
  }

  const handleSpecialization = (value: string) => {
    forceUpdate()

    fetchSpecializations(value)
  }

  const handleDateValue = (date: Date, inputName: string) => {
    const formatYmd = (date: Date) => date?.toISOString().slice(0, 10)
    setStartDate(date)
    formik.setFieldValue(inputName, formatYmd(date))
  }

  useEffect(() => {
    if (formik.values.profession && professions) {
      fetchSpecializations(formik.values.profession)
    }
  }, [professions])

  useEffect(() => {
    if (formik.values.profession === '11' && formik.values.formationDate) {
      setStartDate(new Date(formik.values.formationDate))
      formik.setFieldValue('formationDate', formik.values.formationDate)
    } else if (formik.values.profession === '10' && formik.values.finishedResidencyDate) {
      setStartDate(new Date(formik.values.finishedResidencyDate))
      formik.setFieldValue('finishedResidencyDate', formik.values.finishedResidencyDate)
    }
  }, [formik.values])

  return (
    <>
      <FormGroup gridColumn="1 / 9">
        <Select
          {...formik.getFieldProps(ProfessionalFormId.PROFESSION)}
          id={ProfessionalFormId.PROFESSION}
          label={ProfessionalFormLabel.PROFESSION}
          placeholder={FormPlaceholder.SELECT}
          items={professions}
          required
          disabled={!professions}
          onChange={(e) => {
            formik.resetForm({
              values: {
                profession: e.target.value,
                institutions: [
                  {
                    institution: '',
                    institutionType: '',
                  },
                ],
              },
            })
            formik.handleChange(e)
            setActiveCustomSpecialization(false)
            handleSpecialization(e.target.value)
          }}
          error={Boolean(formik.errors.profession)}
          message={formik.errors.profession}
        />
      </FormGroup>
      {specializations?.length > 0 && (
        <FormGroup gridColumn="9 / -1">
          <Select
            {...formik.getFieldProps(ProfessionalFormId.SPECIALIZATION)}
            id={ProfessionalFormId.SPECIALIZATION}
            label={ProfessionalFormLabel.SPECIALIZATION}
            placeholder={FormPlaceholder.SELECT}
            value={findSpecializationById(formik.values?.specialization)}
            items={specializations}
            onChange={(e) => {
              formik.handleChange(e)
              handleCustomSpecialization(e.target.value)
            }}
            error={Boolean(formik.errors.specialization)}
            message={formik.errors.specialization}
          />
        </FormGroup>
      )}

      {activeCustomSpecialization && (
        <FormGroup gridColumn="1 / 9">
          <Input
            {...formik.getFieldProps(ProfessionalFormId.SPECIALIZATION_CUSTOM)}
            id={ProfessionalFormId.SPECIALIZATION_CUSTOM}
            label={ProfessionalFormLabel.SPECIALIZATION_CUSTOM}
            type="text"
            required
            placeholder={FormPlaceholder.SELECT}
            error={Boolean(formik.errors.specializationCustom)}
            message={formik.errors.specializationCustom}
          />
        </FormGroup>
      )}

      {findItemById(formik.values.profession, professions)?.slug !== ProfessionSlug.OTHER_HEALTH_PROFESSIONALS &&
        findItemById(formik.values.profession, professions)?.slug !== ProfessionSlug.STUDENT &&
        findItemById(formik.values.profession, professions)?.slug !== undefined && (
          <FormGroup gridColumn="1 / 4">
            <Select
              {...formik.getFieldProps(ProfessionalFormId.STATE)}
              id={ProfessionalFormId.STATE}
              label={ProfessionalFormLabel.STATE}
              placeholder={FormPlaceholder.SELECT}
              items={federativeStates}
              required
              error={Boolean(formik.errors.state)}
              message={formik.errors.state}
            />
          </FormGroup>
        )}

      {findProfessionById(formik.values.profession)?.code && (
        <FormGroup gridColumn="4 / 9">
          <Input
            {...formik.getFieldProps(ProfessionalFormId.PROFESSION_REGISTER)}
            mask={MASK.professionCode}
            type="text"
            id={ProfessionalFormId.PROFESSION_REGISTER}
            label={findProfessionById(formik.values.profession)?.code}
            required
            error={Boolean(formik.errors.professionRegister)}
            message={formik.errors.professionRegister}
          />
        </FormGroup>
      )}

      {findItemById(formik.values.profession, professions)?.slug !== ProfessionSlug.OTHER_HEALTH_PROFESSIONALS &&
        findItemById(formik.values.profession, professions)?.slug !== ProfessionSlug.STUDENT &&
        findItemById(formik.values.profession, professions)?.slug !== undefined && (
          <FormGroup gridColumn="9 / -1">
            <Select
              {...formik.getFieldProps(ProfessionalFormId.PROFESSION_STATUS)}
              id={ProfessionalFormId.PROFESSION_STATUS}
              label={
                findProfessionById(formik.values.profession)?.code
                  ? `${ProfessionalFormLabel.PROFESSION_STATUS} do ${
                      findProfessionById(formik.values.profession)?.code
                    }`
                  : ProfessionalFormLabel.PROFESSION_STATUS
              }
              placeholder={FormPlaceholder.SELECT}
              items={statusList}
              required
              error={Boolean(formik.errors.professionStatus)}
              message={formik.errors.professionStatus}
            />
          </FormGroup>
        )}

      {findItemById(formik.values.profession, professions)?.slug === ProfessionSlug.STUDENT && (
        <FormGroup gridColumn="1 / 5">
          <StyledWrapperDatePicker>
            <DatePicker
              dateFormat="dd/MM/yyyy"
              minDate={new Date()}
              selected={startDate ? startDate : new Date()}
              locale="pt-BR"
              onChange={(date: Date) => handleDateValue(date, ProfessionalFormId.FORMATION_DATE)}
              className="fix-arrow-calendar"
              customInput={
                <div>
                  <Input
                    {...formik.getFieldProps(ProfessionalFormId.FORMATION_DATE)}
                    value={startDate?.toLocaleDateString('pt-BR')}
                    id={ProfessionalFormId.FORMATION_DATE}
                    label={ProfessionalFormLabel.FORMATION_DATE}
                    required
                    isDatePicker
                    autocomplete="off"
                    error={Boolean(formik.errors.formationDate)}
                    message={formik.errors.formationDate}
                  />
                </div>
              }
            />
          </StyledWrapperDatePicker>
        </FormGroup>
      )}
      {findItemById(formik.values.profession, professions)?.slug === ProfessionSlug.RESIDENT && (
        <FormGroup gridColumn="1 / 4">
          <StyledWrapperDatePicker>
            <DatePicker
              dateFormat="dd/MM/yyyy"
              minDate={new Date()}
              selected={startDate ? startDate : new Date()}
              locale="pt-BR"
              onChange={(date: Date) => handleDateValue(date, ProfessionalFormId.FINISHED_RESIDENCY_DATE)}
              className="fix-arrow-calendar"
              customInput={
                <div>
                  <Input
                    {...formik.getFieldProps(ProfessionalFormId.FINISHED_RESIDENCY_DATE)}
                    value={startDate?.toLocaleDateString('pt-BR')}
                    id={ProfessionalFormId.FINISHED_RESIDENCY_DATE}
                    label={ProfessionalFormLabel.FINISHED_RESIDENCY_DATE}
                    required
                    isDatePicker
                    autocomplete="off"
                    error={Boolean(formik.errors.finishedResidencyDate)}
                    message={formik.errors.finishedResidencyDate}
                  />
                </div>
              }
            />
          </StyledWrapperDatePicker>
        </FormGroup>
      )}
    </>
  )
}

export default UserProfessionalForm
