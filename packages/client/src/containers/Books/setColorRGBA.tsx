const setColorRGBA = (colorTheme, theme, alpha) => {
  const fallbackColor = theme.bookColors.fallback.rgb
  const hasThemeColor = theme.bookColors[colorTheme] ? true : false

  if (hasThemeColor) {
    return `rgba(${theme.bookColors[colorTheme].rgb}, ${alpha})`
  }

  return `rgba(${fallbackColor}, ${alpha})`
}

export default setColorRGBA
