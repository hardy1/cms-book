import styled from 'styled-components'

type BookChapterWrapperProps = {
  backgroundColor?: string
  fontSizeModifier?: number
}

const BookChapterWrapper = styled.span<BookChapterWrapperProps>`
  font-family: Montserrat;
  font-style: normal;
  line-height: 200%;

  h1 {
    font-style: inherit;
    font-weight: normal;
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 26}px`};
  }

  h4 {
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
  }

  p {
    margin: 2rem 0;
    font-style: inherit;
    font-weight: normal;
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
    line-height: 180%;
    text-align: justify;
    letter-spacing: 0.05em;
  }
`

export default BookChapterWrapper
