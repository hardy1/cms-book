import styled from 'styled-components'
import colors from '../../resources/colors'

interface BookPageTitleProps {
  nightMode?: boolean
  fontSizeModifier?: number
}

const BookPageTitle = styled.h1<BookPageTitleProps>`
  display: flex;
  align-items: center;
  margin-top: 4rem;
  width: 100%;
  color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
  font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 28}px`};
  font-weight: bold;
  font-style: normal;
  text-transform: uppercase;
  box-sizing: border-box;

  em {
    font-style: inherit;
    font-weight: 400;
  }
`

export default BookPageTitle
