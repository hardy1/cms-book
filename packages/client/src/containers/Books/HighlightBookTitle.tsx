import React from 'react'
import styled from 'styled-components'
import colors from '../../resources/colors'
import setColorHex from './setColorHex'

type HighlightBookTitleProps = {
  colorTheme?: string
  title: string
  part?: string
}

const HighlightBookTitleContents = (props) => (
  <div className={props.className}>
    <h2>
      <em>Você está lendo</em>
      &nbsp; MOC - {props.title}
    </h2>
    <h3>{props.part}</h3>
  </div>
)

const HighlightBookTitle = styled(HighlightBookTitleContents) <HighlightBookTitleProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  grid-area: title;
  width: 100%;
  height: 50px;
  background-color: ${({ colorTheme, theme }) => setColorHex(colorTheme, theme)};
  color: ${colors.white};
  font-style: normal;
  font-family: 'Lato';
  padding: 5px 16px;
  margin-top: 0;
  margin-bottom: 0;
  box-sizing: border-box;
  position: sticky;
  top: 0;
  z-index: 0;

  h2 {
    font-size: 1rem;
    font-weight: 900;
    margin: 0;
  }

  h3 {
    font-size: 0.8rem;
    font-weight: normal;
    margin: 0;
  }

  @media screen and (min-width: 768px) {
    height: 78px;
    padding: 16px 46px;
  }

  em {
    font-style: inherit;
    font-weight: 400;
  }
`

export default HighlightBookTitle
