const setColorHex = (colorTheme, theme) => {
  const hasThemeColor = theme.bookColors[colorTheme] ? true : false

  if (hasThemeColor) {
    return theme.bookColors[colorTheme].hex
  }

  return theme.bookColors.fallback.hex
}

export default setColorHex
