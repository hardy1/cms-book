import styled from 'styled-components'

const ChapterSectionContainer = styled.section`
  display: flex;
  align-items: flex-start;
  width: 100%;
  text-transform: uppercase;
  box-sizing: border-box;
  margin-bottom: 1.2rem;
  flex-wrap: wrap;
`

export default ChapterSectionContainer
