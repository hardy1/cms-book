const setImage = (image, colorTheme, theme) => {
  const hasThemeColor = theme.bookColors[colorTheme] ? true : false

  if (hasThemeColor) {
    return `url(/img/${image}-${colorTheme}.svg)`
  }

  return `url(/img/${image}-fallback.svg)`
}

export default setImage
