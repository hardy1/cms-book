import styled from 'styled-components'
import colors from '../../resources/colors'

import setColorHex from './setColorHex'
import setImage from './setImage'
import setColorRGBA from './setColorRGBA'

interface BooksContainerProps {
  colorTheme?: string
  nightMode?: boolean
  fontSizeModifier?: number
}

const BooksContainer = styled.article<BooksContainerProps>`
  grid-area: content;
  padding: 48px 15px 100px;
  margin: 0;
  box-sizing: border-box;

  @media screen and (min-width: 960px) {
    margin: 0 9rem;
  }

  * {
    box-sizing: border-box;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    ::after,
    ::before {
      box-sizing: inherit;
    }
  }

  a {
    text-decoration: none;

    h3 {
      &:hover,
      &:active {
        color: ${colors.primaryBook};
      }
    }
  }

  .CardMainTitle {
    font-family: 'Montserrat';
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 28}px`};
    font-weight: 800;
    line-height: 34px;
    margin-bottom: 32px;

    @media screen and (min-width: 960px) {
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 38}px`};
      line-height: 46px;
      padding-top: 30px;
      margin-bottom: 4rem;
    }
  }

  .CardTitle {
    color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
    font-family: Montserrat;
    font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 21}px`};
    font-weight: 700;
    line-height: 26px;
    margin-bottom: 16px;

    @media screen and (min-width: 960px) {
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 30}px`};
      line-height: 36px;
      margin-bottom: 50px;
    }
  }

  .Part {
    &__Title {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
      font-family: 'Merriweather';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 38}px`};
      font-weight: 700;
      line-height: 48px;
      margin-bottom: 20px;
    }

    &__Date {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.disabledColor)};
      font-family: 'Lato';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0.8px;
      margin-bottom: 55px;
    }

    &__List {
      display: block;
      width: 100%;
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
      font-family: 'Montserrat';
      list-style: none;
      transition: color 0.2s ease-out;

      a {
        color: inherit;

        &:hover,
        &:active {
          color: ${({ colorTheme, theme }) => setColorHex(colorTheme, theme)};
        }
      }

      &__Item {
        display: inherit;
        font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 22}px`};
        font-weight: 700;
        line-height: 28px;
        letter-spacing: 5%;
      }

      &__Sublist {
        list-style: inherit;
        padding-left: 30px;

        :empty {
          display: none;
        }

        &__Item {
          font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
          font-weight: 400;
          line-height: 30px;
          text-transform: uppercase;
        }
      }
    }
  }

  .Chapter {
    &__Title {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
      font-family: 'Merriweather';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 38}px`};
      font-weight: 700;
      line-height: 48px;
      margin-bottom: 20px;
    }

    &__Date {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.disabledColor)};
      font-family: 'Lato';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0.8px;
      margin-bottom: 55px;
    }

    &__Subtitle {
      color: ${({ nightMode }) => (nightMode ? colors.white : colors.neutralColor)};
      font-family: 'Montserrat';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 24}px`};
      font-weight: 700;
      line-height: 1.8rem;
      margin: 0;
      padding: 0;
    }

    &__CID {
      color: ${({ colorTheme, theme, nightMode }) => (nightMode ? colors.white : setColorHex(colorTheme, theme))};
      font-family: 'Montserrat';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 24}px`};
      font-weight: 700;
      font-style: normal;
      line-height: 30px;
      text-decoration: underline;
      text-transform: uppercase;
      margin-right: 1rem;
      cursor: pointer;
    }

    &__Recommendation {
      display: block;
      background-color: ${({ colorTheme, theme }) => setColorRGBA(colorTheme, theme, 0.2)};
      border-radius: 10px;
      font-family: 'Montserrat';
      font-size: ${({ fontSizeModifier }) => `${fontSizeModifier * 16}px`};
      font-weight: 400;
      line-height: 30px;
      letter-spacing: 5%;
      text-align: justify;
      padding: 21px 25px;
      margin-bottom: 45px;

      strong {
        color: ${({ colorTheme, theme, nightMode }) => (nightMode ? colors.white : setColorHex(colorTheme, theme))};
        font-weight: 700;
      }
    }

    &__Drug,
    &__Link,
    &__Link--Calculator,
    &__Link--PDF,
    &__Link--Image,
    &__Link--Video {
      display: inline-block;
      color: ${({ colorTheme, theme, nightMode }) => (nightMode ? colors.white : setColorHex(colorTheme, theme))};
      font-family: 'Montserrat';
      font-size: inherit;
      line-height: inherit;
      text-decoration: underline;
      letter-spacing: 5%;
      padding-right: 5px;
      padding-top: 2px;
      padding-bottom: 2px;
      cursor: pointer;
    }

    &__Drug {
      font-weight: 700;
    }

    &__Link--Calculator,
    &__Link--Image,
    &__Link--PDF,
    &__Link--Video {
      position: relative;
      font-weight: 400;

      ::before {
        content: '';
        position: absolute;
        right: 0;
      }
    }

    &__Link--PDF {
      padding-right: 47px;

      ::before {
        top: calc(50% - 26px / 2);
        width: 42px;
        height: 26px;
        background-image: ${({ colorTheme, theme }) => setImage('PDF', colorTheme, theme)};
      }
    }

    &__Link--Video {
      padding-right: 30px;

      ::before {
        top: calc(50% - 17px / 2);
        width: 25px;
        height: 17px;
        background-image: ${({ colorTheme, theme }) => setImage('Video', colorTheme, theme)};
      }
    }

    &__Link--Calculator {
      padding-right: 23px;

      ::before {
        top: calc(50% - 24px / 2);
        width: 18px;
        height: 24px;
        background-image: ${({ colorTheme, theme }) => setImage('Calculator', colorTheme, theme)};
      }
    }

    &__Link--Image {
      padding-right: 30px;

      ::before {
        top: calc(50% - 22px / 2);
        width: 25px;
        height: 22px;
        background-image: ${({ colorTheme, theme }) => setImage('Image', colorTheme, theme)};
      }
    }
  }
`

export default BooksContainer
