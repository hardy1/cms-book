import React from 'react'
import {
  Heading1,
  Heading2,
  CardThumbVideo,
  Button,
  Select,
  CardTips,
  CardNews,
  CardPodcast,
  IconClose,
} from 'designsystem'
import {
  TitleContainer,
  SelectContainer,
  VideosContainer,
  ButtonContainer,
} from '../../containers/StylesContainer/videos-moc/_styles'
import Loader from '../../components/Loader'
import { ResultTextContainer } from '../StylesContainer/canais-moc/_styles'
import { IChannelData, IGenericItem } from '../../types'
import { DicasContainer } from '../StylesContainer/moc-dicas/_styles'
import { NewsContainer } from '../StylesContainer/noticias/_styles'
import { PodcastsContainer } from '../Podcasts/styles'

export interface ChannelContainerProps {
  title?: string
  subtitle?: string
  channel?: string
  categories?: IGenericItem[]
  handleSelect?: (e: React.MouseEvent<HTMLElement> | React.ChangeEvent<HTMLSelectElement>) => void
  activeFilter?: string
  clearFilter?: () => void
  loading?: boolean
  items?: IChannelData[]
  handleLoadMore?: () => void
  loadMore?: boolean
  itemUrlSuffix?: string
  notFoundText?: string
  value?: unknown
}

const ChannelContainer: React.FC<ChannelContainerProps> = ({
  title = '',
  subtitle = '',
  channel = 'dicas',
  categories = [],
  handleSelect = () => '',
  activeFilter = '',
  clearFilter = () => '',
  loading = false,
  items = [],
  handleLoadMore = () => '',
  loadMore = false,
  itemUrlSuffix = '',
  notFoundText = 'Nada encontrado',
  value,
}) => {
  const stringifyCategories = (categories: IGenericItem[]) => {
    const categoryNames = categories.map((category) => category?.name)
    return categoryNames.join(', ')
  }

  const ChannelContent = (type: string) => {
    switch (type.toLowerCase()) {
      case 'dicas':
        return (
          <DicasContainer>
            {items &&
              items.map(({ id, title, published_at, slug, categories }) => (
                <CardTips
                  key={id}
                  category={categories && categories[0]?.name}
                  title={title}
                  date={new Date(published_at)}
                  url={`${itemUrlSuffix}/${slug}`}
                />
              ))}
            {items?.length ? (
              ''
            ) : (
              <ResultTextContainer>
                <Heading2 color="primaryColor">{notFoundText}</Heading2>
              </ResultTextContainer>
            )}
          </DicasContainer>
        )

      case 'videos':
        return (
          <VideosContainer>
            {items &&
              items.map(({ id, image, title, subtitle, published_at, slug, sponsor }) => (
                <CardThumbVideo
                  key={id}
                  imgSrc={image}
                  title={title}
                  subtitle={subtitle}
                  date={new Date(published_at)}
                  footerImg={sponsor}
                  url={`${itemUrlSuffix}/${slug}`}
                />
              ))}
            {items.length ? (
              ''
            ) : (
              <ResultTextContainer>
                <Heading2 color="primaryColor">{notFoundText}</Heading2>
              </ResultTextContainer>
            )}
          </VideosContainer>
        )

      case 'podcasts':
        return (
          <PodcastsContainer>
            {items &&
              items.map(({ id, image, title, published_at, slug }) => (
                <CardPodcast
                  key={id}
                  imgSrc={image}
                  title={title}
                  date={new Date(published_at)}
                  url={`${itemUrlSuffix}/${slug}`}
                />
              ))}
            {items.length ? (
              ''
            ) : (
              <ResultTextContainer>
                <Heading2 color="primaryColor">{notFoundText}</Heading2>
              </ResultTextContainer>
            )}
          </PodcastsContainer>
        )

      default:
        return (
          <NewsContainer>
            {items &&
              items.map(({ id, image, title, reading_time, published_at, categories, slug }) => (
                <CardNews
                  key={id}
                  imgSrc={image}
                  title={title}
                  align="row"
                  category={stringifyCategories(categories) || 'Sem categoria'}
                  date={new Date(published_at)}
                  imgAlt="Image alternative text"
                  readingTime={reading_time.toString()}
                  thumbnailSize="md"
                  url={`${itemUrlSuffix}/${slug}`}
                />
              ))}
            {items.length ? (
              ''
            ) : (
              <ResultTextContainer>
                <Heading2 color="primaryColor">{notFoundText}</Heading2>
              </ResultTextContainer>
            )}
          </NewsContainer>
        )
    }
  }

  return (
    <>
      <TitleContainer>
        <Heading1 color="primaryColor">{title}</Heading1>
        <Heading2 color="disabledColor">{subtitle}</Heading2>
      </TitleContainer>
      <SelectContainer>
        <Select
          id="selectId"
          placeholder="Selecione"
          items={categories}
          handleChange={handleSelect}
          value={value as string}
        />
        {activeFilter && <Button icon={<IconClose />} fill="text" onClick={() => clearFilter()} />}
      </SelectContainer>

      {loading ? <Loader active={loading} size="medium"></Loader> : ChannelContent(channel)}

      <ButtonContainer>{loadMore && <Button onClick={handleLoadMore}>Veja mais</Button>}</ButtonContainer>
    </>
  )
}

export default ChannelContainer
