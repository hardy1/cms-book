import { useRouter } from 'next/router'
import React, { useState, useEffect, useCallback } from 'react'
import { v4 as uuidv4 } from 'uuid'
import fetcher from '../../hooks/api/fetcher'
import { internalServerURLPath, serverURLPath } from '../../lib/config'
import { IChannel, IGenericItem } from '../../types'
import ChannelContainer, { ChannelContainerProps } from './ChannelContainer'

interface ChannelWrapperProps {
  title?: string
  subtitle?: string
  channel?: 'videos' | 'dicas' | 'noticias' | 'podcasts' | 'congressos'
  notFoundText?: string
  categories?: IGenericItem[]
  content?: IChannel
  container?: typeof ChannelContainer
}

const ChannelWrapper: React.FC<ChannelWrapperProps> = ({
  title,
  subtitle,
  channel,
  notFoundText,
  categories,
  content,
}) => {
  const [mappedCategories, setMappedCategories] = useState<IGenericItem[]>()
  const urlSuffix = `/canais-moc/${channel}`
  const router = useRouter()
  const categoria: string = (
    router.query?.categoria || router.asPath.match(new RegExp(`categoria=(?<categoria>.*)`))?.groups?.categoria
  )?.toString()

  const [results, setResults] = useState([])
  const [page, setPage] = useState<number>(1)
  const [selectedFilter, setSelectedFilter] = useState<string>()
  const [isloading, setIsLoading] = useState<boolean>(false)

  const filterContent = async ({ channel, category }) => {
    setIsLoading(true)
    const filteredData: IChannel = await fetcher(
      `${internalServerURLPath}/api/search?channel=${channel}&category=${category}`,
    )
    setIsLoading(false)

    if (filteredData) {
      setResults(filteredData.data)
    }
  }

  const loadMoreButtonAction = useCallback(async ({ channel, page }) => {
    const fetchApi = await fetch(`${serverURLPath}/proxy/api/content/${channel}?p=${page}`, {
      method: 'GET',
    })

    const data = await fetchApi.json()

    if (data) {
      setResults((results) => [...results, ...data.payload.data])
    }
  }, [])

  useEffect(() => {
    setSelectedFilter(categoria)
    selectedFilter || categoria ? filterContent({ channel, category: selectedFilter || categoria }) : ''
    setResults(content?.data)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const loadMore = () => {
    if (page < content.last_page) {
      setPage(page + 1)
    }
  }

  useEffect(() => {
    if (page !== 1) {
      loadMoreButtonAction({ channel, page: page })
    }
  }, [page, channel, loadMoreButtonAction])

  const clearFilters = () => {
    setSelectedFilter('')
    filterContent({ channel, category: '' })
  }

  const handleSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selected = e.target.value
    setSelectedFilter(selected)
    filterContent({ channel, category: selected === 'all' ? '' : selected })
  }

  useEffect(() => {
    if (categories) {
      const resetItem = {
        id: uuidv4(),
        name: 'Todos',
        slug: 'all',
      }

      const changedCategories = [resetItem, ...categories]
      setMappedCategories(changedCategories?.map(({ id, name, slug }) => ({ id, name, value: slug })))
    }
  }, [categories])

  const containerProps: ChannelContainerProps = {
    title,
    subtitle,
    channel,
    notFoundText,
    categories: mappedCategories,
    handleSelect,
    value: selectedFilter,
    activeFilter: selectedFilter,
    clearFilter: () => clearFilters(),
    loading: isloading,
    items: results,
    handleLoadMore: loadMore,
    loadMore: page < content?.last_page,
    itemUrlSuffix: urlSuffix,
  }

  return (
    <>
      <ChannelContainer {...containerProps}></ChannelContainer>
    </>
  )
}

export default ChannelWrapper
