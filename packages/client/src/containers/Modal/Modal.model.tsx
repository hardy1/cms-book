export interface IModalState {
  isOpen: boolean
  content: {
    title?: string
    body: string | Element | Element[] | JSX.Element | JSX.Element[]
    type: string
  }
}
