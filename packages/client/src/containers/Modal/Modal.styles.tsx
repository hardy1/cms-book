import React from 'react'
import styled from 'styled-components'

interface ICustomModalProps {
  children?: React.ReactNode | string
  className?: string
}

export const StyledModalContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;

  img,
  iframe {
    object-fit: contain;
    object-position: center;
    width: 100%;
    height: 100%;
    border: none;
  }
`

export const ModalContainer = ({ children, className }: ICustomModalProps) => (
  <StyledModalContainer className={className}>{children}</StyledModalContainer>
)

export const CustomModalContainer = styled(ModalContainer)`
  margin-top: 2rem;
  flex-direction: column;
  align-items: unset;
  justify-content: unset;
  width: fit-content;
  height: fit-content;
`
