import React from 'react'
import styled from 'styled-components'
import { CardTips, Button, Heading6 } from 'designsystem'

const Container = styled.div`
  grid-column: 10 / -1;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const TipsContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-around;
`

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  justify-self: flex-end;
`
const Tips: React.FC = () => {
  return (
    <Container>
      <Heading6>MOC Dicas</Heading6>
      <TipsContainer>
        <CardTips
          category="Dica de oncologia"
          title="Dica – Acesso ao tratamento para dor do paciente oncológico"
          date={1594609200000}
        />
        <CardTips
          category="Dica de oncologia"
          title="Dica – Acesso ao tratamento para dor do paciente oncológico"
          date={1594609200000}
        />
        <CardTips
          category="Dica de oncologia"
          title="Dica – Acesso ao tratamento para dor do paciente oncológico"
          date={1594609200000}
        />
      </TipsContainer>
      <ButtonContainer>
        <Button fill="text" onClick={() => null}>
          Veja todas as MOC Dicas
        </Button>
      </ButtonContainer>
    </Container>
  )
}

export default Tips
