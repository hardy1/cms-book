import styled from 'styled-components'

type TooltipProps = {
  show: boolean
}

const Tooltip = styled.div<TooltipProps>`
  position: absolute;
  top: calc(-55px - 18px);
  left: calc(50% - 160px / 2);
  width: 160px;
  height: 55px;
  background-color: #9ea0a5;
  border-radius: 4px;
  color: #fff;
  font-family: 'Lato';
  font-size: 12px;
  font-weight: 600;
  line-height: 16px;
  padding: 12px;
  box-sizing: border-box;
  text-align: left;
  pointer-events: ${({ show }) => (show ? 'auto' : 'none')};
  opacity: ${({ show }) => (show ? 1 : 0)};
  transition: opacity 0.2s ease-out;

  ::before {
    content: '';
    position: absolute;
    bottom: -9px;
    left: calc(50% - 16px / 2);
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 10px 8px 0 8px;
    border-color: #9ea0a5 transparent transparent transparent;
  }
`

export default Tooltip
