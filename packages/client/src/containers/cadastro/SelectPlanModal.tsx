import styled from 'styled-components'

const SelectPlanModal = styled.div`
  position: absolute;
  inset: 0;
  z-index: 10001;
  background-color: #fff;
  overflow: auto;
  box-sizing: border-box;

  * {
    box-sizing: inherit;
  }

  .Modal {
    background-color: rgba(244, 255, 255, 0.9);

    &__Body {
      position: relative;
      width: 100%;
      min-height: 100vh;
      background-color: #fff;
      padding: 4rem 0 1rem;

      @media screen and (min-width: 1024px) {
        width: 930px;
        min-height: initial;
        border-radius: 10px;
        box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
        padding: 4rem 3rem;
        margin: 40px auto;
      }
    }

    &__Close {
      position: absolute;
      top: 8px;
      right: 8px;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      width: 40px;
      height: 40px;
      padding: 10px;
      border-radius: 50%;
      font-size: 30px;

      @media screen and (min-width: 1024px) {
        top: 20px;
        right: 20px;
        width: 55px;
        height: 55px;
        font-size: 40px;
      }

      span {
        width: inherit;
        height: inherit;
      }
    }

    &__SwitchContainer {
      width: 200px;
      margin-right: auto;
      margin-left: auto;
      padding-bottom: 20px;
    }

    &__PlansList {
      display: flex;
      flex-flow: row nowrap;
      width: 100%;
      scroll-snap-type: x mandatory;
      padding-top: 50px;
      padding-bottom: 20px;
      overflow: auto;
      scrollbar-width: none;

      @media screen and (min-width: 1024px) {
        justify-content: space-between;
        flex-flow: unset;
        scroll-snap-type: unset;
        padding-right: 10px;
        padding-left: 10px;
      }

      &__Item {
        display: inline-block;
        width: calc(100% - 48px);
        min-height: 300px;
        flex: none;
        scroll-snap-align: center;
        padding: 30px 16px;
        margin-right: 1rem;

        @media screen and (min-width: 1024px) {
          width: 260px;
          scroll-snap-align: unset;
          margin-right: 0;
          margin-left: 0;
        }

        &:first-of-type {
          margin-left: 1rem;

          @media screen and (min-width: 1024px) {
            margin-left: 0;
          }
        }

        &__Title {
          padding-bottom: 20px;
          margin-bottom: 0;
        }

        &__Price {
          min-height: 50px;
          margin-top: 0;
        }

        &__Details {
          margin-bottom: 20px;
        }
      }
    }

    &__FreePlan {
      padding: 0 16px 16px;

      @media screen and (min-width: 1024px) {
        display: flex;
        align-items: flex-start;
        padding: 0;
      }

      &__Item {
        @media screen and (min-width: 1024px) {
          display: flex;
          flex-wrap: wrap;
          min-height: initial;
          padding: 30px 29px 15px;
        }

        &__Title {
          @media screen and (min-width: 1024px) {
            display: inline-block;
            text-align: left;
            margin-bottom: 0;
          }

          small {
            @media screen and (min-width: 1024px) {
              padding-top: 22px;
            }
          }

          &__Container {
            @media screen and (min-width: 1024px) {
              display: inline-block;
              width: 50%;
            }
          }
        }

        &__Price {
          @media screen and (min-width: 1024px) {
            min-height: initial;
            margin-top: 0;
            margin-bottom: 0;
          }
        }

        &__PriceAndAction {
          @media screen and (min-width: 1024px) {
            width: 50%;
            text-align: right;
          }
        }

        &__Info {
          color: #9ea0a5;
          text-transform: uppercase;
          margin-bottom: 0;

          @media screen and (min-width: 1024px) {
            width: 100%;
            min-height: initial;
            padding-top: 32px;
          }

          em {
            color: #3e3f42;
            font-style: normal;
          }
        }
      }
    }
  }
`

export default SelectPlanModal
