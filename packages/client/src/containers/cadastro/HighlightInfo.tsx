import styled, { css } from 'styled-components'

export type HighlightInfoProps = {
  as?: React.ElementType
  alignItems?: string
  bottomSeparator?: boolean
  gridColumn?: string
  marginBottom?: string
  padding?: string
}

const HighlightInfo = styled.div<HighlightInfoProps>`
  position: relative;
  grid-column: ${({ gridColumn }) => gridColumn || '1 / -1'};
  background-color: rgba(177, 219, 255, 0.2);
  padding: ${({ padding }) => padding || '18px 29px'};
  text-align: left;
  margin-bottom: ${({ marginBottom }) => marginBottom || 0};
  box-sizing: border-box;
  margin-top: 30px;

  @media screen and (min-width: 768px) {
    display: flex;
    align-items: ${({ alignItems }) => alignItems || 'flex-start'};
    justify-content: space-between;
    flex-wrap: wrap;
  }

  * {
    box-sizing: inherit;
  }

  ${({ bottomSeparator }) =>
    bottomSeparator &&
    css`
      margin-bottom: 48px;

      ::after {
        content: '';
        position: absolute;
        bottom: -24px;
        left: 0;
        width: 100%;
        height: 1px;
        background-color: #eee;
      }
    `}

  .HighlightInfo {
    &__Title {
      width: 100%;
      min-height: initial;
      color: #144590;
      font-family: 'Montserrat';
      font-weight: normal;
      font-size: 18px;
      line-height: 22px;
      text-transform: uppercase;
      margin-bottom: 24px;
    }

    &__Details {
      @media screen and (min-width: 768px) {
        padding-right: 20px;
        width: calc(100% - 150px);
      }

      &__Values {
        @media screen and (min-width: 768px) {
          widith: 100%;
          justify-content: flex-start;
        }

        + .HighlightInfo {
          &__Details {
            &__Values {
              padding-top: 30px;
            }
          }
        }
      }

      &__Plan,
      &__Values {
        list-style: none;
        padding-left: 0;
        margin-top: 0;
        margin-bottom: 20px;

        @media screen and (min-width: 768px) {
          display: flex;
          align-items: flex-start;
          flex-wrap: nowrap;
        }

        li {
          width: 100%;
          font-family: 'Lato', sans-serif;
          /* outline: 1px solid red; */

          @media screen and (min-width: 768px) {
            width: auto;
            /* min-width: 175px; */
          }

          &:first-of-type {
            @media screen and (min-width: 768px) {
              min-width: 145px;
            }
          }

          &:nth-child(2) {
            @media screen and (min-width: 768px) {
              min-width: 175px;
            }
          }

          &:only-child {
            p {
              font-size: 20px;
              line-height: 24px;

              @media screen and (min-width: 768px) {
                font-size: 32px;
                line-height: 38px;
              }
            }
          }

          &:not(:last-of-type) {
            margin-bottom: 20px;

            @media screen and (min-width: 768px) {
              margin-bottom: 0;
              margin-right: 20px;
            }
          }

          h4 {
            color: #216de1;
            font-size: 14px;
            font-weight: 400;
            line-height: 18px;
            letter-spacing: 1px;
            text-transform: uppercase;
            margin-top: 0;
            margin-bottom: 8px;
          }

          p {
            min-height: initial;
            color: #144590;
            font-size: 20px;
            font-weight: 700;
            line-height: 24px;
            margin-top: 0;
            margin-bottom: 0;

            @media screen and (min-width: 768px) {
              font-size: 18px;
              line-height: 29px;
            }
          }

          p.highLight {
            font-size: 24px;
          }
        }
      }

      &__Plan {
        &__Type {
          p {
            @media screen and (min-width: 768px) {
              white-space: nowrap;
              text-overflow: ellipsis;
              overflow: hidden;
            }
          }
        }
      }

      &__Values {
        @media screen and (min-width: 768px) {
          margin-bottom: 0;
        }
      }
    }

    &__Actions {
      &__Button {
        width: 100%;
        height: 42px;
        font-size: 16px;
        margin-top: 0;

        &.LargeButton {
          height: 50px;
          font-size: 16px;

          @media screen and (min-width: 768px) {
            height: 55px;
            min-width: 165px;
            font-size: 18px;
          }
        }

        @media screen and (min-width: 768px) {
          width: auto;
        }
      }
    }
  }
`

export default HighlightInfo
