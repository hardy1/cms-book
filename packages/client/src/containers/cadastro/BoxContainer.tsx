import styled from 'styled-components'

const BoxContainer = styled.div`
  column-gap: 1.125rem;
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  row-gap: 3em;

  .TogglePlan {
    padding: 0 40px;

    .HighlightAnnualPlan {
      background-color: #f1f1f1;
      color: #216de1;
      font-family: 'Lato';
      font-size: 1rem;
      line-height: 19px;
      padding: 15px 25px;
      text-align: center;
    }
  }
`

export default BoxContainer
