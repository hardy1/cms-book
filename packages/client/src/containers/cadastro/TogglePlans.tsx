import styled from 'styled-components'

const TogglePlans = styled.div`
  font-size: 'Lato';
  text-align: left;

  .TogglePlans {
    &__BuyWithCupom {
      position: relative;
      min-width: initial;
      width: 100%;
      height: 60px;
      background-color: #144590;
      border-color: #144590;
      padding-top: 16px;
      padding-left: 16px;
      padding-bottom: 16px;
      padding-right: 16px;
      margin-top: 16px;

      span {
        width: 100%;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 700;
        line-height: 16px;
        padding-left: 30px;
        text-align: left;
        margin-left: 12px;

        @media screen and (min-width: 1280px) {
          font-size: 16px;
          line-height: 18px;
        }
      }

      svg {
        position: absolute;
        left: 19px;
        width: 18px;
        height: 18px;
        margin-left: 0;
      }
    }
  }

  .TogglePlansTittle {
    color: #656d79;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.25rem;
    margin-bottom: 24px;
  }

  .TogglePlansList {
    list-style: none;
    padding-left: 0;

    &__Item {
      width: 100%;

      &:not(:last-of-type) {
        margin-bottom: 12px;
      }

      &__Button {
        position: relative;
        display: inline-flex;
        flex-wrap: wrap;
        align-items: center;
        width: inherit;
        height: 60px;
        background-color: transparent;
        border-width: 0;
        border-radius: 4px;
        color: #000;
        font-family: inherit;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.125rem;
        text-align: left;
        padding: 10px 26px;
        cursor: pointer;
        transition-property: background-color, color;
        transition-duration: 0.2s;
        transition-timing-function: ease-out;

        &--Active,
        :hover,
        :focus {
          background-color: #216de1;
          color: #fff;

          ::before {
            border-left-color: #fff !important;
          }
        }

        ::before {
          content: '';
          position: absolute;
          top: calc(50% - 10px / 2);
          right: 22px;
          width: 0;
          height: 0;
          border-style: solid;
          border-width: 5px 0 5px 7.5px;
          border-top-color: transparent;
          border-right-color: transparent;
          border-bottom-color: transparent;
          border-left-color: #000;
          transform: rotate(360deg);
        }

        small {
          display: inline-block;
          width: 100%;
        }
      }
    }
  }
`

export default TogglePlans
