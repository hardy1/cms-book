import styled from 'styled-components'
import colors from '../../resources/colors'

const SimpleContainer = styled.div`
  display: grid;
  grid-column: 1 / -1;
  padding: 0 2rem;

  .no-login {
    color: ${colors.mediumGray};
    font-family: 'Lato';
    font-size: 18px;
    font-weight: bold;
    line-height: 21px;
    text-align: center;
    display: flex;
    justify-content: center;

    .link-with-icon {
      display: flex;
      align-items: center;
    }

    button,
    a,
    span {
      margin-left: 8px;
      color: ${colors.secondaryColor};
      cursor: pointer;
    }
  }
`

export default SimpleContainer
