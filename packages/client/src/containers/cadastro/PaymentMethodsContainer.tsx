import styled from 'styled-components'

const PaymentMethodsContainer = styled.div`
  max-width: 765px;
  grid-column: 1 / -1;
  display: flex;
  flex-wrap: wrap;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 40px;
  margin: 0 auto;

  @media screen and (min-width: 768px) {
    padding-left: 0;
    padding-right: 0;
    flex-wrap: nowrap;
  }

  .Container {
    &__Title {
      width: 100%;
      color: #9ea0a5;
      font-size: 12px;
      font-weight: normal;
      line-height: 14px;
      text-transform: uppercase;
      margin-top: 0;
      margin-bottom: 22px;
    }

    &__Cards {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      width: 100%;
      margin-bottom: 40px;
      box-sizing: border-box;

      @media screen and (min-width: 768px) {
        padding-right: 190px;
        margin-bottom: 0;
      }

      > div {
        @media screen and (min-width: 768px) {
          box-sizing: border-box;
          margin-bottom: 10px !important;
        }
      }

      &__Image {
        padding: 5px !important;
        box-sizing: border-box;
      }
    }

    &__SecureSite {
      h6 {
        @media screen and (min-width: 768px) {
          text-align: right;
        }
      }

      &__Image {
        padding: 5px !important;
        box-sizing: border-box;
      }
    }
  }
`

export default PaymentMethodsContainer
