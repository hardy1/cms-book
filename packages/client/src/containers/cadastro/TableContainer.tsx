import styled from 'styled-components'

const TableContainer = styled.div`
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  max-width: 1280px;
  margin-top: 40px;
  margin-right: auto;
  margin-left: auto;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    width: 100%;
    overflow: none;
  }
`

export default TableContainer
