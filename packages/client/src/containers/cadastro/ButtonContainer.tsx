import styled from 'styled-components'

const ButtonContainer = styled.div`
  grid-column: 1 / -1;
  grid-template-columns: repeat(4, 1fr);
  text-align: right;
  padding: 32px 0 0;
`

export default ButtonContainer
