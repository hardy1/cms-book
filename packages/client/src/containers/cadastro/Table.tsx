import styled from 'styled-components'

const Table = styled.table`
  width: 100%;
  grid-column: 1 / -1;
  border-spacing: 0;

  @media screen and (min-width: 768px) {
    width: 100%;
  }

  tr {
    &.row-action {
      td {
        padding-top: 20px;
        padding-bottom: 20px;
        text-align: center;

        &:first-of-type {
          width: calc(100% - 75px);

          @media screen and (min-width: 768px) {
            opacity: 0;
            pointer-events: none;
          }
        }

        &:not(:first-of-type) {
          display: none;

          @media screen and (min-width: 768px) {
            display: table-cell;
          }
        }

        &:not(:last-of-type) {
          border-right-width: 0;
        }

        button {
          width: 150px;
          padding: 15px 10px;
        }
      }
    }

    &:nth-child(odd) {
      background-color: #f4f8fe;
    }

    td {
      &.description {
        padding: 16px 14px 13px;
        width: calc(100vw - 76px);

        @media screen and (min-width: 768px) {
          width: 295px;
        }

        p {
          display: flex;
          align-items: center;
          min-height: 50px;
          color: #3e3f42;
          font-family: 'Lato';
          font-size: 14px;
          line-height: 17px;
          margin: 0;
        }
      }

      &.options {
        display: none;
        text-align: center;
        width: 76px;

        @media screen and (min-width: 768px) {
          display: table-cell;
        }

        &.show {
          display: table-cell;
        }

        @media screen and (min-width: 768px) {
          width: 310px;
        }
      }

      :not(:last-of-type) {
        border-right: 1px solid #eee;
      }
    }
  }
`

export default Table
