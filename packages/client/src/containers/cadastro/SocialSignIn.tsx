import styled from 'styled-components'

import SocialLogin from './SocialLogin'

const SocialSignIn = styled(SocialLogin)`
  display: flex;
  flex-wrap: wrap;
  grid-column: 1 / -1;
  text-align: center;
  padding-top: 20px;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    justify-content: center;
    flex-wrap: nowrap;
    padding-top: 40px;
  }

  button {
    width: 100%;
    height: 50px;
    padding: 10px 20px;
    box-sizing: border-box;

    @media screen and (min-width: 768px) {
      width: 290px;
    }

    :first-of-type {
      @media screen and (min-width: 768px) {
        margin-right: 60px;
      }
    }
  }
`

export default SocialSignIn
