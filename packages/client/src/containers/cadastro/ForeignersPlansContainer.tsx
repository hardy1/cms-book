import styled from 'styled-components'

const ForeignersPlansContainer = styled.div`
  text-align: center;
  padding-top: 32px;
  padding-bottom: 20px;

  .ForeignersPlansContainerButton {
    color: #000;
    background-color: #eee;
    border-color: #eee;
    box-shadow: 0px 4px 5px rgba(0, 105, 153, 0.25);
  }
`

export default ForeignersPlansContainer
