import styled from 'styled-components'

type togglePlayAndBoxContainerProps = {
  sliderPlans: number
}

const setSlickSlideWidth = (sliderPlans) => {
  switch (sliderPlans) {
    case 2:
      return '50%'
    case 3:
      return '33.333%'
    default:
      return '50%'
  }
}

const TogglePlayAndBoxContainer = styled.div<togglePlayAndBoxContainerProps>`
  display: grid;
  column-gap: 1rem;
  grid-template-columns: repeat(12, 1fr);
  max-width: 1280px;
  width: 100%;
  grid-column: 1 / -1;
  box-sizing: border-box;
  margin-top: 50px;
  margin-right: auto;
  margin-left: auto;

  .HighlightAnnualPlan {
    position: relative;
    background-color: #eee;
    font-family: 'Lato', sans-serif;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.125rem;
    text-align: left;
    padding: 11px 24px 11px 40px;
    margin-top: 10px;
    margin-bottom: 16px;

    img {
      position: absolute;
      top: 17px;
      left: 8px;
    }
  }

  .Slider {
    grid-column: 1 / -1;
    width: 100%;

    .slick-track {
      @media screen and (min-width: 1024px) {
        width: 100% !important;
        transform: translate3d(0, 0, 0) !important;
      }
    }

    .slick-slide {
      @media screen and (min-width: 1024px) {
        width: ${({ sliderPlans }) => `${setSlickSlideWidth(sliderPlans)} !important`};
      }
    }

    .slick-list {
      padding: 50px 0 10px;
    }

    .slick-dots {
      li {
        display: inline-block;
        width: 10px;
        height: 10px;

        &.slick-active {
          button {
            background-color: #216de1;
          }
        }

        button {
          display: inherit;
          width: inherit;
          height: inherit;
          background-color: #9ea0a5;
          border-radius: 50%;
          font-size: 0;
          padding: 0;
          transition: background-color 0.2s ease-out;

          ::before {
            display: none;
          }
        }
      }
    }
  }

  .boxes {
    grid-column: 1 / -1;
    display: flex;
    flex-flow: row nowrap;
    width: 100%;
    padding-bottom: 20px;

    @media screen and (min-width: 1024px) {
      grid-column: 4 / -1;
      display: flex;
      overflow: none;
      padding-left: 4px;
      padding-right: 4px;
      box-sizing: border-box;
    }

    .box {
      display: block;
      width: calc(100% - 40px);
      min-height: 325px;
      margin: 0 auto;
      box-sizing: border-box;
      padding: 28px 18px 26px;

      @media screen and (min-width: 1024px) {
        width: calc(100% - 20px);
      }
    }
  }

  .toggle-plan-container {
    grid-column: 1 / -1;
    text-align: center;

    @media screen and (min-width: 1024px) {
      grid-column: 1 / 4;
      padding-top: 50px;
    }
  }

  .container-button {
    grid-column: 1 / -1;
    padding-right: 16px;
    padding-left: 16px;
    text-align: right;
  }
`

export default TogglePlayAndBoxContainer
