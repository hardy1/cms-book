import styled from 'styled-components'

type BoxAccessContainerProps = {
  className?: string
  dir?: string
}

const BoxAccessContainer = styled.div<BoxAccessContainerProps>`
  display: flex;
  width: calc(100vw + 32px);
  flex-wrap: wrap;
  justify-content: center;
  padding: 1rem;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    display: grid;
    grid-gap: 1rem;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: auto;
    width: 100%;
    padding-right: 16px;
    padding-left: 16px;
    margin-right: auto;
    margin-left: auto;
  }

  @media screen and (min-width: 1024px) {
    max-width: 1200px;
    grid-template-columns: repeat(4, 1fr);
  }

  .BoxItem {
    flex: none;
    width: calc(100% - 48px);
    scroll-snap-align: center;
    margin-right: 16px;
    padding: 30px 25px;

    &:hover {
      box-shadow: 2px 2px 10px rgb(0 0 0 / 25%);
    }

    @media screen and (min-width: 768px) {
      width: 100%;
      margin-right: 0;
      min-height: 410px;
    }

    @media screen and (min-width: 1024px) {
      width: 100%;
      min-height: 450px;
      margin-right: 0;
    }

    p {
      display: flex;
      align-items: center;

      @media screen and (min-width: 1024px) {
        min-height: 115px;
      }
    }

    &:not(:last-of-type) {
      margin-bottom: 32px;

      @media screen and (min-width: 768px) {
        margin-bottom: 0;
      }
    }
  }
`

export default BoxAccessContainer
