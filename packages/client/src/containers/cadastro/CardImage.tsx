import styled, { css } from 'styled-components'

type CardImageProps = {
  className?: string
  imagePosition?: 'left' | 'right'
  imageURL?: string
  gridColumn?: string
}

const CardImage = styled.div<CardImageProps>`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  min-height: 475px;
  background-color: #fff;
  border-radius: 10px;
  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
  padding-top: 65px;
  padding-bottom: 65px;
  margin: 0 auto;
  box-sizing: border-box;

  @media screen and (min-width: 768px) {
    width: 750px;
  }

  ${({ gridColumn }) =>
    gridColumn &&
    css`
      grid-column: ${gridColumn};
    `}

  ${({ imagePosition }) =>
    (imagePosition === 'left' || imagePosition === 'right') &&
    css`
      padding-top: 240px;
      padding-right: 24px;
      padding-left: 24px;

      @media screen and (min-width: 768px) {
        padding-top: 65px;
      }
    `}

  ${({ imagePosition }) =>
    imagePosition === 'left' &&
    css`
      @media screen and (min-width: 768px) {
        padding-left: 330px;
        padding-right: 35px;
      }
    `}

  ${({ imagePosition }) =>
    imagePosition === 'right' &&
    css`
      @media screen and (min-width: 768px) {
        padding-right: 370px;
        padding-left: 35px;
      }
    `}

  ${({ imageURL, imagePosition }) =>
    imageURL &&
    css`
      ::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 215px;
        background-image: url(${imageURL});
        background-repeat: no-repeat;
        background-size: cover;
        border-radius: 10px 10px 0 0;
        overflow: hidden;

        @media screen and (min-width: 768px) {
          width: 309px;
          height: 100%;
          background-position: -250px 0;

          ${imagePosition === 'left' &&
          css`
            left: 0;
            border-radius: 10px 0 0 10px;
          `}

          ${imagePosition === 'right' &&
          css`
            right: 0;
            left: unset;
            border-radius: 0 10px 10px 0;
          `}
        }
      }
    `}

  .Card__Title {
    color: #216de1;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 37px;
    margin-top: 0;
    margin-bottom: 40px;

    span {
      display: block;
      color: #656d79;
    }
  }

  .Card__Message {
    + .Card__ButtonPrintInvoice {
      margin-top: 24px;
      margin-bottom: 8px;

      svg {
        margin-right: 10px;
      }
    }

    + .Card__Links {
      padding-top: 48px;
    }

    p {
      &:not(:last-of-type) {
        margin-bottom: 20px;
      }
    }
  }

  .Card__Links {
    p {
      &:not(:last-of-type) {
        margin-bottom: 8px;
      }

      a {
        position: relative;
        color: #3e3f42;
        line-height: 22px;
        text-decoration: none;

        ::before {
          content: '';
          position: absolute;
          right: 0;
          bottom: 0;
          left: 0;
          width: 100%;
          height: 1px;
          background-color: #3e3f42;
        }
      }
    }
  }

  p {
    color: #3e3f42;
    font-family: 'Lato';
    font-size: 16px;
    line-height: 21px;
    margin-top: 0;

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  .Card__Actions {
    padding-top: 40px;

    p {
      color: #3e3f42;
      font-size: 16px;
      line-height: 30px;
      margin-bottom: 7px;
    }

    button,
    a {
      position: relative;
      width: 55px;
      height: 55px;
      border-radius: 50%;
      text-align: center;
      padding: 5px;
      vertical-align: top;

      &:not(:last-of-type) {
        margin-right: 30px;
      }

      span {
        display: inline-block;
        vertical-align: top;
      }

      svg {
        display: inline-block;
        vertical-align: inherit;
        margin: 0;
      }

      .sr-only {
        position: absolute;
        width: 0;
        height: 0;
        overflow: hidden;
      }
    }
  }
`

export default CardImage
