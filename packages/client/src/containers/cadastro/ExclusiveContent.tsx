import styled from 'styled-components'

type ExclusiveContentProps = {
  highlightImage: string
}

const ExclusiveContent = styled.div<ExclusiveContentProps>`
  * {
    box-sizing: border-box;
  }

  .ExclusiveContent {
    &__Title {
      font-size: 18px;
      line-height: 22px;
      margin-bottom: 12px;

      @media screen and (min-width: 1024px) {
        min-height: 72px;
        font-size: 30px;
        line-height: 36px;
        margin-bottom: 24px;
      }
    }

    &__Iframe {
      width: 100%;
      height: 400px;
    }

    &__Column {
      min-height: 200px;
      grid-column: 1 / -1;

      &:first-of-type {
        margin-bottom: 40px;

        @media screen and (min-width: 1024px) {
          grid-column: 1 / 8;
          padding-right: 25px;
        }
      }

      &:last-of-type {
        @media screen and (min-width: 1024px) {
          grid-column: 8 / -1;
          padding-left: 25px;
        }
      }
    }

    &__FormLogin {
      padding-top: 20px;

      &__FormGroup {
        text-align: left;

        &:nth-child(3) {
          margin-bottom: 50px;

          .forgot-password {
            padding-bottom: 0;
            margin-bottom: 0;
          }
        }

        label {
          font-size: 12px;
          line-height: 16px;
          margin-bottom: 8px;

          @media screen and (min-width: 768px) {
            font-size: 1rem;
            line-height: 19px;
            margin-bottom: 12px;
          }
        }

        input {
          max-width: 325px;
        }
      }
    }

    &__Highlight {
      display: flex;
      align-items: center;
      min-height: 150px;
      background-image: linear-gradient(90deg, #eee 50%, rgba(250, 250, 250, 0) 92.93%, rgba(250, 250, 250, 0) 141.66%);
      padding: 0 16px;
      margin-bottom: 42px;

      @media screen and (min-width: 768px) {
        position: relative;
        display: block;
        min-height: 200px;
        justify-content: flex-start;
      }

      @media screen and (min-width: 1024px) {
        min-height: 295px;
      }

      ::before {
        @media screen and (min-width: 768px) {
          content: '';
          position: absolute;
          top: 0;
          right: 0;
          width: 50%;
          height: 100%;
          background-image: ${({ highlightImage }) => `url(${highlightImage})`};
          background-position: left center;
          background-repeat: no-repeat;
          background-size: cover;
        }
      }

      &__Container {
        max-width: 1280px;
        height: inherit;
        margin-left: auto;
        margin-right: auto;

        @media screen and (min-width: 1024px) {
          display: flex;
          align-items: center;
          height: 295px;
        }
      }

      &__Title {
        font-family: 'Montserrat', sans-serif;
        font-size: 20px;
        line-height: 24px;

        @media screen and (min-width: 768px) {
          width: 450px;
          font-size: 28px;
          line-height: 32px;
        }

        @media screen and (min-width: 1024px) {
          width: 620px;
          font-size: 38px;
          line-height: 46px;
        }
      }
    }

    &__Content {
      p {
        color: #656d79;
        font-family: 'Lato' sans-serif;
        font-size: 16px;
        line-height: 29px;
        margin-bottom: 12px;

        @media screen and (min-width: 1024px) {
          margin-bottom: 38px;
        }
      }

      &__Highlight {
        grid-column: 1 / -1;
      }

      &__Highlight,
      &__Normal {
        margin-bottom: 20px;
      }

      &__Highlight {
        position: relative;
        padding-bottom: 20px;

        @media screen and (min-width: 768px) {
          display: flex;
          flex-wrap: nowrap;
        }

        @media screen and (min-width: 1024px) {
          padding-bottom: 42px;
          margin-bottom: 72px;
        }

        ::before {
          content: '';
          position: absolute;
          bottom: 0;
          left: 20px;
          width: calc(100% - 40px);
          height: 1px;
          background-color: #eee;

          @media screen and (min-width: 1024px) {
            left: 100px;
            width: calc(100% - 200px);
          }
        }

        &__Wrapper {
          @media screen and (min-width: 768px) {
            width: calc(100% - 190px);
            padding-left: 24px;
          }

          @media screen and (min-width: 1024px) {
            width: calc(100% - 520px);
          }
        }

        &__Video {
          width: 100%;
          height: 180px;
          background-color: #000;
          margin-bottom: 16px;

          @media screen and (min-width: 768px) {
            width: 420px;
            height: 220px;
          }

          @media screen and (min-width: 1024px) {
            width: 520px;
            height: 295px;
          }
        }
      }

      &__SeeMore {
        padding-top: 32px;

        &__Title {
          color: #656d79;
          font-family: 'Lato', sans-serif;
          font-size: 16px;
          font-weight: 400;
          line-height: 18px;
          margin-bottom: 20px;
        }

        &__List {
          padding-left: 0;
          list-style: none;
          padding-bottom: 20px;
          margin-top: 0;

          &__Item {
            &--Videos,
            &--Calc,
            &--News,
            &--Education {
              margin-bottom: 16px;

              @media screen and (min-width: 768px) {
                display: inline-block;
                width: 50%;
                margin-bottom: 24px;
              }

              a {
                position: relative;
                color: #144590;
                font-family: 'Lato', sans-serif;
                font-size: 16px;
                line-height: 20px;
                text-decoration: none;
                padding-left: 40px;
                transition: opacity 0.2s ease-out;

                &:hover,
                &:focus {
                  opacity: 0.8;
                }

                &:active {
                  opacity: 0.7;
                }

                svg {
                  position: absolute;
                  top: 2px;
                  left: 0;
                }
              }
            }
          }
        }
      }
    }
  }
`

export default ExclusiveContent
