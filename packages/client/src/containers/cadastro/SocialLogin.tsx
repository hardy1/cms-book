import styled from 'styled-components'

const SocialLogin = styled.div`
  button {
    width: 294px;
    padding: 10px 35px;

    :first-of-type {
      background-color: #348bf0;
      border-color: #348bf0;
      margin-bottom: 12px;
    }

    :last-of-type {
      background-color: #ff5f72;
      border-color: #ff5f72;
    }

    span {
      min-width: 215px;
      text-align: left;
    }

    svg {
      width: 22px;
      height: 22px;
      display: inline-block;
      vertical-align: text-bottom;
      margin-right: 20px !important;
    }
  }
`

export default SocialLogin
