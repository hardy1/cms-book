import styled from 'styled-components'

type HeadingContainerProps = {
  marginBottomDesktop?: string
  marginBottomMobile?: string
}

const HeadingContainer = styled.div<HeadingContainerProps>`
  display: block;
  grid-column: 1 / -1;
  text-align: center;
  margin-top: 90px;
  margin-bottom: ${({ marginBottomMobile }) => marginBottomMobile || '50px'};

  @media screen and (min-width: 768px) {
    margin-top: 30px;
    margin-bottom: 20px;
    margin-bottom: ${({ marginBottomDesktop }) => marginBottomDesktop || '20px'};
  }
`

export default HeadingContainer
