import React from 'react'
import Link from 'next/link'
import { Heading2, CardPodcast, Button, GridContainer } from 'designsystem'
import { Container, Header, PodcastsContainer } from './styles'
import { ChannelName, ChannelSection, ChannelSlug } from '../../enums'
import { IHomeItem } from '../../types'

export type PodcastsProps = {
  podcasts: IHomeItem[] | undefined
}

const Podcasts: React.FC<PodcastsProps> = ({ podcasts }: PodcastsProps) => {
  return (
    <GridContainer>
      <Container>
        <Header>
          <Link href={`/${ChannelSection.SLUG}/${ChannelSlug.PODCASTS}`} passHref>
            <Button as="a" fill="text">
              <Heading2 color="primaryColor">{ChannelName.PODCASTS}</Heading2>
            </Button>
          </Link>
        </Header>
        <PodcastsContainer>
          {podcasts &&
            podcasts.map(({ id, image, title, published_at, slug }) => (
              <CardPodcast
                key={id}
                imgSrc={image}
                title={title}
                date={new Date(published_at)}
                url={`/${ChannelSection.SLUG}/${ChannelSlug.PODCASTS}/${slug}`}
              />
            ))}
        </PodcastsContainer>
      </Container>
    </GridContainer>
  )
}

export default Podcasts
