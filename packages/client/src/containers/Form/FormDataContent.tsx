import styled from 'styled-components'

export const FormDataContentRow = styled.div<{ columns?: number }>`
  display: grid;
  grid-template-columns: ${({ columns }) => (columns ? `repeat(${columns}, 1fr)` : `${1}fr`)};
  grid-gap: 1rem;
`

export const FormDataContentField = styled.div<{ size?: number }>`
  width: ${({ size }) => (size ? `${size}%` : `${100}%`)};
`

export const FormDataContentStyled = styled.div<{ spanColumns?: number }>`
  display: grid;
  grid-gap: 1rem;
  grid-column: ${({ spanColumns }) => (spanColumns ? `span ${spanColumns}` : '1 / -1')};
  margin-top: 3rem;
`
