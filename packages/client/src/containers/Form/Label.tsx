import styled, { css } from 'styled-components'

type LabelProps = {
  required?: boolean
}

const setRequired = () => `
  color: #144590;
  margin-left: 5px;
`

const Label = styled.label<LabelProps>`
  display: inline-block;
  width: 100%;
  color: #9ea0a5;
  font-family: 'Lato';
  font-size: 1rem;
  font-weight: 100;
  line-height: 19px;
  text-align: left;
  text-transform: uppercase;
  margin-bottom: 12px;

  .required {
    display: inline-block;
    ${setRequired()}
  }

  ${({ required }) =>
    required &&
    css`
      ::after {
        content: '*';
        position: relative;
        ${setRequired()}
      }
    `}
`

export default Label
