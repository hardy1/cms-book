import React from 'react'
import styled from 'styled-components'
import { GenericFormSection, IGenericItem } from '../../types'
import { FormGroup } from 'designsystem'
import Dropzone from '../../components/Dropzone'
import UserProfessionalForm from '../User/UserProfessionalForm'
import UserInstitutionForm from '../User/UserInstitutionForm'

interface ProfessionFormProps extends GenericFormSection {
  professions?: IGenericItem[]
}

const StyledHr = styled.hr`
  border: 0.5px solid #c2c4c7;
  margin: 1.5rem 0;
`

const ProfessionForm: React.FC<ProfessionFormProps> = ({ formik, professions }) => {
  const findProfessionById = (id: string) => professions?.find((profession) => profession.id === parseInt(id))
  const currentProfession: any = findProfessionById(formik.values.profession)

  return (
    <>
      <UserProfessionalForm formik={formik} professions={professions} />
      <FormGroup gridColumn="1 / -1" marginBottom="0">
        <StyledHr />
      </FormGroup>
      {<UserInstitutionForm formik={formik} />}
      <Dropzone documentDescription={currentProfession?.document_description} id="document" formikInput={formik} />
    </>
  )
}

export default ProfessionForm
