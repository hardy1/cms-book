import styled from 'styled-components'

type FormProps = {
  columnGap?: string
  display?: string
  marginTop?: string
}

const Form = styled.form<FormProps>`
  position: relative;
  display: ${({ display }) => display || 'grid'};
  width: 100%;
  column-gap: ${({ columnGap }) => columnGap || '1rem'};
  grid-template-columns: repeat(12, 1fr);
  margin-top: ${({ marginTop }) => marginTop || 0};
  margin-right: auto;
  margin-left: auto;

  fieldset {
    border-width: 0;
  }
`

export default Form
