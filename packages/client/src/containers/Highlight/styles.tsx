import styled from 'styled-components'
import media from '../../resources/media'

export const GridContainer = styled.div`
  display: grid;
  column-gap: 1em;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: repeat(2, 1fr);
  margin: 0px auto;
  max-width: 1280px;
  position: relative;

  @media ${media.laptopM} {
    margin-top: 64px;
    max-width: 1200px;
    padding: 0 20px;
  }

  @media ${media.tabletL} {
    margin: 0;
    display: grid;
    row-gap: 2.5rem;
    grid-template-columns: 100%;
    grid-template-rows: repeat(4, auto);
    grid-auto-flow: column;

    & h6 {
      font-size: 1.8rem;
    }
  }
`

export const CarouselContainer = styled.div`
  grid-column: 1 / 7;
  grid-row: 1 / 2;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  max-width: 100%;

  @media ${media.tabletL} {
    order: 1;
    margin-top: 1.2rem;
    grid-column: auto;
    grid-row: auto;

    & h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      display: none;
    }
  }
`

export const CarouselListContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: flex-start;
  margin-top: 20px;

  @media ${media.tabletL} {
    margin: 0;
  }
`

export const NewsContainer = styled.div`
  grid-column: 1 / 7;
  grid-row: 2 / 3;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-top: 20px;

  @media ${media.tabletL} {
    margin: 0;
    padding: 1.2rem 0;
    order: 3;
    grid-column: auto;
    grid-row: auto;
  }
`

export const NewsListContainer = styled.div`
  display: grid;
  grid-column: 1 / -1;
  grid-row: 2 / 2;
  grid-template-columns: repeat(3, calc(93% / 3));
  column-gap: 1em;
  margin-top: 20px;
  row-gap: 1em;

  @media ${media.mobileL} {
    grid-template-columns: auto;
  }
`

export const VideosContainer = styled.div`
  grid-column: 7 / 10;
  grid-row: 1 / 3;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media ${media.tabletL} {
    margin: 0;
    order: 2;
    grid-column: auto;
    grid-row: auto;
  }
`

export const VideosListContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: flex-start;
  margin-top: 20px;

  > a {
    margin-bottom: 24px;
  }

  @media ${media.tabletL} {
    & > *:not(:first-child) {
      margin-top: 1rem;
    }
  }
`

export const TipsContainer = styled.div`
  grid-column: 10 / -1;
  grid-row: 1 / 3;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media ${media.tabletL} {
    margin: 0;
    order: 4;
    grid-column: auto;
    grid-row: auto;
  }
`

export const TipsListContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: flex-start;
  margin-top: 20px;

  > a {
    margin-bottom: 24px;
  }

  @media ${media.tabletL} {
    & > * {
      margin-top: 1em;
    }
  }
`

export const SectionButtonContainer = styled.div`
  align-items: flex-start;
  display: flex;
  justify-content: flex-start;
  justify-self: flex-start;
`

export const ButtonContainer = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: flex-end;
  justify-self: flex-end;
  margin-top: 15px;
`
