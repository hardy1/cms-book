import React from 'react'
import Link from 'next/link'

import { Heading6, CardNews, CardThumbVideo, CardTips, Button } from 'designsystem'

import {
  GridContainer,
  CarouselContainer,
  CarouselListContainer,
  NewsContainer,
  NewsListContainer,
  VideosContainer,
  VideosListContainer,
  TipsContainer,
  TipsListContainer,
  SectionButtonContainer,
} from './styles'

import { ChannelName, ChannelSection, ChannelSlug } from '../../enums'
import { Spacing } from '../../pages/_app'
import { IHomeItem } from '../../types'
import { Carousel } from '../../components'

interface HighlightProps {
  featuring: IHomeItem[] | undefined
  news: IHomeItem[] | undefined
  tip: IHomeItem[] | undefined
  video: IHomeItem[] | undefined
}

const Highlight: React.FC<HighlightProps> = ({ featuring, news, tip, video }: HighlightProps) => {
  return (
    <>
      <Spacing />
      <GridContainer>
        <CarouselContainer>
          <Heading6>Publicações em destaque</Heading6>

          <CarouselListContainer>
            {featuring && (
              <Carousel
                items={featuring}
                autoplaySpeed={5000}
                speed={1000}
                cssEase="ease-in-out"
                dots
                infinite
                autoplay
                pauseOnHover
              ></Carousel>
            )}
          </CarouselListContainer>
        </CarouselContainer>

        <NewsContainer>
          <SectionButtonContainer>
            <Link href={`/${ChannelSection.SLUG}/${ChannelSlug.NEWS}`} passHref>
              <Button as="a" fill="text" onClick={() => null}>
                <Heading6>{ChannelName.NEWS}</Heading6>
              </Button>
            </Link>
          </SectionButtonContainer>

          <NewsListContainer>
            {news &&
              news.map(({ id, image, categories, title, reading_time, published_at, slug }) => (
                <CardNews
                  key={id}
                  imgSrc={image}
                  imgAlt={title}
                  title={title}
                  category={categories.map((category) => category?.name).join(', ')}
                  readingTime={reading_time.toString()}
                  date={new Date(published_at)}
                  url={`${ChannelSection.SLUG}/${ChannelSlug.NEWS}/${slug}`}
                  align="column"
                  imgContainerHeight="75px"
                />
              ))}
          </NewsListContainer>
        </NewsContainer>

        <VideosContainer>
          <SectionButtonContainer>
            <Link href={`/${ChannelSection.SLUG}/${ChannelSlug.VIDEOS}`} passHref>
              <Button as="a" fill="text" onClick={() => null}>
                <Heading6>{ChannelName.VIDEOS}</Heading6>
              </Button>
            </Link>
          </SectionButtonContainer>
          <VideosListContainer>
            {video &&
              video.map(({ id, image, title, subtitle, published_at, slug, sponsor }) => (
                <CardThumbVideo
                  key={id}
                  imgSrc={image}
                  title={title}
                  subtitle={subtitle}
                  date={new Date(published_at)}
                  footerImg={sponsor}
                  url={`${ChannelSection.SLUG}/${ChannelSlug.VIDEOS}/${slug}`}
                />
              ))}
          </VideosListContainer>
        </VideosContainer>

        <TipsContainer>
          <SectionButtonContainer>
            <Link href={`/${ChannelSection.SLUG}/${ChannelSlug.TIPS}`} passHref>
              <Button as="a" fill="text" onClick={() => null}>
                <Heading6>{ChannelName.TIPS}</Heading6>
              </Button>
            </Link>
          </SectionButtonContainer>
          <TipsListContainer>
            {tip &&
              tip.map(({ id, title, published_at, slug, categories }) => (
                <CardTips
                  key={id}
                  category={categories.map((category) => category.name).join(', ')}
                  title={title}
                  date={new Date(published_at)}
                  url={`${ChannelSection.SLUG}/${ChannelSlug.TIPS}/${slug}`}
                />
              ))}
          </TipsListContainer>
        </TipsContainer>
      </GridContainer>
    </>
  )
}

export default Highlight
