import { themeName } from '../enums'

// TODO: Reestructure design tokens to material design model.
const theme = {
  font: {
    family: {
      base: '"Lato", sans-serif',
      alt: '"Montserrat", sans-serif',
    },
    size: {
      sm: '0.85rem',
      md: '1rem',
      lg: '1.5rem',
    },
    lineHeight: {
      sm: '1rem',
      md: '1.8rem',
      lg: '2.3rem',
    },
  },
  color: {
    primary: {
      500: '#144590',
    },
    grey: {
      500: '#3e3f42',
    },
  },
  bookColors: {
    navyBlue: {
      hex: '#1a3563',
      rgb: '26, 53, 99',
      secondary: {
        hex: '#216DE1',
        rgb: '33, 109, 225',
      },
    },
    red: {
      hex: '#f44054',
      rgb: '244, 64, 84',
      secondary: {
        hex: '#CC3646',
        rgb: '204, 54, 70',
      },
    },
    champagne: {
      hex: '#cca367',
      rgb: '204, 163, 103',
      secondary: {
        hex: '#80693B',
        rgb: '128, 105, 59',
      },
    },
    purple: {
      hex: '#a94c86',
      rgb: '169, 76, 134',
      secondary: {
        hex: '#4B1649',
        rgb: '75, 22, 73',
      },
    },
    darkGreen: {
      hex: '#295351',
      rgb: '41, 83, 81',
      secondary: {
        hex: '#21332B',
        rgb: '33, 51, 43',
      },
    },
    umber: {
      hex: '#8b4d4b',
      rgb: '139, 77, 75',
      secondary: {
        hex: '#57312E',
        rgb: '87, 49, 46',
      },
    },
    black: {
      hex: '#151618',
      rgb: '21, 22, 24',
      secondary: {
        hex: '#4D4D4D',
        rgb: '77, 77, 77',
      },
    },
    ocre: {
      hex: '#b34c14',
      rgb: '179, 76, 20',
      secondary: {
        hex: '#80360E',
        rgb: '128, 54, 14',
      },
    },
    fallback: {
      hex: '#216de1',
      rgb: '33, 109, 225',
      secondary: {
        hex: '#1a3563',
        rgb: '26, 53, 99',
      },
    },
  },
}

export const validateTheme = (theme?): string => {
  const isTheme: boolean = Object.values(themeName).includes(theme)
  return isTheme ? theme : themeName.FALLBACK
}

export default theme
