import { createGlobalStyle } from 'styled-components'
import media from '../resources/media'
import theme from './theme'

const GlobalStyle = createGlobalStyle`
  body {
    font-family: ${theme.font.family.base};
    margin: 0;

    @media ${media.mobileL} {
      font-size: ${theme.font.size.sm};
    }

    @media ${media.tabletL} {
      display: flex;

      .grecaptcha-badge{
        bottom: 65px!important;
      }
    }
  }
`

export default GlobalStyle
