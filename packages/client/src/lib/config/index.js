import serverURLPath from './serverURLPath'
import internalServerURLPath from './internalServerURLPath'

export { serverURLPath, internalServerURLPath }
