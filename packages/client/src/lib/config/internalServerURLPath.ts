let internalServerURLPath = process.env.NEXT_PUBLIC_BASE_LOCAL_API_URL

if (typeof window !== 'undefined') {
  internalServerURLPath = window.location.origin
}

export default internalServerURLPath
