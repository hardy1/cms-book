let serverURLPath = process.env.BASE_API_URL

if (typeof window !== 'undefined') serverURLPath = window.location.origin

export default serverURLPath
