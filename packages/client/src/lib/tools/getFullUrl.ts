export const getFullUrl = (req?, fallback = 'https://moc.com.br') => {
  if (req) {
    return req.protocol + '://' + req.get('host') + req.originalUrl
  } else if (typeof window !== 'undefined') {
    return window.location.href
  } else {
    return fallback
  }
}
