export const highlightSearch = (result) => {
  const fullContent = result.occurences.map((occurence) => {
    const formattedKeyword = `<mark>${result.keyword}</mark>`
    const content = occurence.content.split(result.keyword)
    const formattedContent = `<p>${content.join(formattedKeyword)}</p>`

    return formattedContent
  })
  return fullContent.join()
}
