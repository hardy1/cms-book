const isFullscreenEnabled = () => document.fullscreenEnabled

const handleFullScreen = (fullScreen: boolean, elementRef) => {
  try {
    fullScreen && isFullscreenEnabled()
      ? elementRef.current.requestFullscreen()
      : document.fullscreenElement
      ? document.exitFullscreen()
      : ''
  } catch (e) {
    throw console.error('O modo tela cheia não é suportado por este browser', e)
  }
}

export { isFullscreenEnabled, handleFullScreen }
