export { isFullscreenEnabled, handleFullScreen } from './fullscreen'
export { highlightSearch } from './highlightText'
export { getFullUrl } from './getFullUrl'
