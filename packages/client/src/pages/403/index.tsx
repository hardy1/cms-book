import React from 'react'
import Link from 'next/link'
import { MainContainer, Heading2, Paragraph, Button } from 'designsystem'
import { TitleContainer, ButtonContainer } from '../../containers/StylesContainer/404/_styles'

import { HeadWrapper } from '../../components'

const Page403: React.FC = () => {
  return (
    <>
      <HeadWrapper title="MOC Brasil - Acesso restrito" />

      <MainContainer direction="column" align="center">
        <TitleContainer>
          <Heading2 color="primaryColor">Acesso Restrito</Heading2>
          <Paragraph>
            Clique no botão abaixo para conferir os planos disponíveis para realizar o upgrade e tenha acesso a este e
            outros conteúdos exclusivos.
          </Paragraph>
        </TitleContainer>

        <ButtonContainer>
          <Link href="/minha-conta" passHref>
            <Button as="a" onClick={() => null}>
              Minha conta
            </Button>
          </Link>
        </ButtonContainer>
      </MainContainer>
    </>
  )
}

export default Page403
