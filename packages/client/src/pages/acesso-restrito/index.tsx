import React from 'react'
import Link from 'next/link'
import { GridContainer, Heading2, Paragraph, Button } from 'designsystem'
import { TitleContainer, ButtonContainer } from '../../containers/StylesContainer/404/_styles'

import { HeadWrapper } from '../../components'

const AcessoRestrito: React.FC = () => {
  return (
    <>
      <HeadWrapper title="MOC Brasil - Acesso restrito" />
      <GridContainer>
        <TitleContainer>
          <Heading2 color="primaryColor">Acesso restrito!</Heading2>
          <Paragraph>
            Você não tem permissão para acessar o conteúdo pois seu perfil profissional não é correspondente ao
            conteúdo.
          </Paragraph>
        </TitleContainer>
        <ButtonContainer>
          <Link href="/" passHref>
            <Button as="a" onClick={() => null}>
              Voltar para página inicial
            </Button>
          </Link>
        </ButtonContainer>
      </GridContainer>
    </>
  )
}

export default AcessoRestrito
