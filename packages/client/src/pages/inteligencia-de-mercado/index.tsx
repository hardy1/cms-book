import React from 'react'
import parse from 'html-react-parser'
import { NextPage } from 'next'
import {
  GridContainer,
  Heading2,
  Heading3,
  Heading6,
  CardResearch,
  InternalHeader,
  HtmlWrapper,
  MainContainer,
} from 'designsystem'

import fetcher from '../../hooks/api/fetcher'
import {
  TextContainer,
  AsideContainer,
  ResearchesContainer,
  Researches,
} from '../../containers/StylesContainer/inteligencia-de-mercado/_styles'

import { serverURLPath } from '../../lib/config'
import { ResearchesDataVM } from '../../types'

import { HeadWrapper } from '../../components'

const Inteligencia: NextPage<{ data; researches: ResearchesDataVM }> = (props) => {
  const { data, researches } = props

  return (
    <>
      <HeadWrapper title="MOC Brasil - Inteligência de Mercado" />
      <InternalHeader>
        <Heading2 color="primaryColor">{data?.title}</Heading2>
      </InternalHeader>
      <MainContainer direction="column">
        <GridContainer>
          <TextContainer>
            <Heading3 color="primaryColor">Bem-vindo à seção Inteligência de Mercado!</Heading3>
            <HtmlWrapper>{data && parse(data?.content)}</HtmlWrapper>
          </TextContainer>
          <AsideContainer>
            <img src="/img/logo-moc.png" />
            <img src="/img/logo-iqvia.png" />
          </AsideContainer>
          <ResearchesContainer>
            <Heading6>Pesquisas</Heading6>
            <Researches>
              {researches?.data?.map(({ id, image, title, subtitle, slug, reading_time }) => (
                <CardResearch
                  key={id}
                  imgSrc={image}
                  imgAlt={title}
                  title={title}
                  subtitle={subtitle}
                  readingTime={reading_time}
                  url={`/pesquisas/${slug}`}
                />
              ))}
            </Researches>
          </ResearchesContainer>
        </GridContainer>
      </MainContainer>
    </>
  )
}

export const getStaticProps = async () => {
  try {
    const data = await fetcher(`${serverURLPath}/api/pages/inteligencia-de-mercado`)
    const researches = await fetcher(`${serverURLPath}/api/content/pesquisas`)

    if (!data) {
      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
        notFound: true,
      }
    }

    return {
      props: { data, researches },
      revalidate: 60,
    }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Inteligencia
