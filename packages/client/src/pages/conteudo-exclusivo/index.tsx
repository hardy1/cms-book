import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import * as yup from 'yup'

import {
  Button,
  GridContainer,
  Heading1,
  IconBook,
  IconCalculatorSquare,
  IconNewsSquare,
  IconPlaySquare,
  Paragraph,
} from 'designsystem'

import { HeadWrapper, FormLogin } from '../../components'

import { ExclusiveContent as StyledExclusiveContent } from '../../containers/cadastro'
import { useRouter } from 'next/router'
import { toast } from '../../components/Toast'
import { useFormik } from 'formik'
import { signIn, getProviders, getSession } from 'next-auth/react'
import { GetServerSideProps } from 'next'
import { AuthError, AuthErrorCallback } from '../../enums'

const validationSchema = yup.object({
  login: yup.string().required('Campo obrigatório').email('E-mail inválido'),
  password: yup.string().required('Campo obrigatório'),
})

const ExclusiveContent: React.FC<{ providers }> = ({ providers }) => {
  const router = useRouter()
  const { query } = router
  const [error] = useState<string | string[]>(query?.error)

  useEffect(() => {
    if (error) {
      if (error !== AuthErrorCallback.SESSION_REQUIRED) {
        toast.notify(`${AuthError[error as string] ? AuthError[error as string] : error}`, {
          type: 'danger',
        })
      }
    }
  }, [error])

  const formik = useFormik({
    initialValues: {
      login: '',
      password: '',
    },
    validationSchema,
    onSubmit: async (values) => {
      signIn(providers.credentials.id, { ...values, callbackUrl: `${query?.callbackUrl}` })
    },
  })

  return (
    <>
      <HeadWrapper title="MOC Brasil - Conteúdo exclusivo" />

      <StyledExclusiveContent highlightImage="/img/exclusive-content-bg.png">
        <div className="ExclusiveContent__Highlight">
          <div className="ExclusiveContent__Highlight__Container">
            <Heading1 as="h3" className="ExclusiveContent__Highlight__Title">
              Conteúdo exclusivo para usuários cadastrados
            </Heading1>
          </div>
        </div>

        <div className="ExclusiveContent__Content">
          <GridContainer>
            <div className="ExclusiveContent__Column">
              <Heading1 as="h3" className="ExclusiveContent__Title">
                Tenha acesso a todo conteúdo que é referência em Oncologia no Brasil.
              </Heading1>
              <Paragraph>
                O MOC é maior plataforma digital de informação oncológica em português, dividido em 8 especialidades na
                área de oncologia, em um formato conciso, rápido e extremamente atualizado.
              </Paragraph>

              <iframe
                src="https://player.vimeo.com/video/378664724?h=84aa1161f2"
                className="ExclusiveContent__Iframe"
                frameBorder="0"
                allow="autoplay; fullscreen; picture-in-picture"
                allowFullScreen
              ></iframe>

              <div className="ExclusiveContent__Content__SeeMore">
                <Heading1 as="h4" className="ExclusiveContent__Content__SeeMore__Title">
                  E ainda mais ...
                </Heading1>

                <ul className="ExclusiveContent__Content__SeeMore__List">
                  <li className="ExclusiveContent__Content__SeeMore__List__Item--Videos">
                    <Link href="/canais-moc/videos" passHref>
                      <a>
                        <IconPlaySquare />
                        Vídeos exclusivos
                      </a>
                    </Link>
                  </li>

                  <li className="ExclusiveContent__Content__SeeMore__List__Item--Calc">
                    <Link href="/formulas-medicas" passHref>
                      <a>
                        <IconCalculatorSquare />
                        Calculadoras médicas
                      </a>
                    </Link>
                  </li>

                  <li className="ExclusiveContent__Content__SeeMore__List__Item--News">
                    <Link href="/canais-moc/noticias" passHref>
                      <a>
                        <IconNewsSquare />
                        Notícias atualizadas
                      </a>
                    </Link>
                  </li>

                  <li className="ExclusiveContent__Content__SeeMore__List__Item--Education">
                    <Link href="/cursos" passHref>
                      <a>
                        <IconBook />
                        Educação médica continuada
                      </a>
                    </Link>
                  </li>
                </ul>

                <Link href="/cadastro" passHref>
                  <Button as="a">Quero me cadastrar</Button>
                </Link>
              </div>
            </div>

            <div className="ExclusiveContent__Column">
              <div className="ExclusiveContent__Content__Normal">
                <Heading1 as="h3" className="ExclusiveContent__Title">
                  Faça seu login
                </Heading1>

                <Paragraph>Se você já é cadastrado, entre com seus dados de login e senha para acessar agora</Paragraph>

                <FormLogin
                  formik={formik}
                  className="ExclusiveContent__FormLogin"
                  formGroupClassName="ExclusiveContent__FormLogin__FormGroup"
                />
              </div>
            </div>
          </GridContainer>
        </div>
      </StyledExclusiveContent>
    </>
  )
}

export default ExclusiveContent

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context
  const session = await getSession({ req })

  return {
    props: {
      session,
      providers: await getProviders(),
    },
  }
}
