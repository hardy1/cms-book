/* eslint-disable */
import React from 'react'
import parse from 'html-react-parser'
import { MainContainer, Heading2, InternalHeader } from 'designsystem'
import { TextContainer, ParagraphContainer } from '../../containers/StylesContainer/single-artigo/_styles'

import { serverURLPath } from '../../lib/config'
import fetcher from '../../hooks/api/fetcher'
import { GetServerSideProps, NextPage } from 'next'
import styled from 'styled-components'
import media, { size } from '../../resources/media'

import { HeadWrapper } from '../../components'

const Container = styled.div`
  display: flex;
  margin: auto;
  max-width: ${size.laptopM};
  justify-content: center;

  @media ${media.laptopM} {
    margin: 0 3rem;
  }
`

const SinglePage: NextPage<{ data }> = (props) => {
  const { data } = props
  const payload = data

  return (
    <>
      <HeadWrapper title={payload && payload.title} />

      <InternalHeader>
        <Heading2 color="primaryColor">{payload && payload.title}</Heading2>
      </InternalHeader>

      <MainContainer direction="column">
        <TextContainer>
          <ParagraphContainer>{payload && parse(payload.content)}</ParagraphContainer>
        </TextContainer>
      </MainContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ res, params }) => {
  try {
    const data = await fetcher(`${serverURLPath}/api/pages/${params['all'][0]}`)

    if (!data) {
      return {
        notFound: true,
        props: {},
      }
    }

    return { props: { data } }
  } catch {
    res.statusCode = 404
    return {
      props: {},
    }
  }
}

export default SinglePage
