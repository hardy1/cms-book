import React from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { Heading6, CardNews, Divider } from 'designsystem'
import { LastArticles } from '../../../containers/StylesContainer/single-artigo/_styles'
import { IBreadcrumb, IChannelData } from '../../../types'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { getPost } from '../../../services/data/moc-content.service'
import { HeadWrapper, ChannelSingleContent } from '../../../components'
import { getSession } from 'next-auth/react'

const SingleNoticia: NextPage<{ data: IChannelData }> = ({ data }) => {
  const title = ChannelName.CONGRESSES
  const breadcrumbs: IBreadcrumb[] = [
    { content: ChannelSection.NAME, href: '/' },
    { content: title, href: `/${ChannelSection.SLUG}/${ChannelSlug.CONGRESSES}` },
    { content: data?.title },
  ]

  const LastArticlesContent = () => (
    <>
      <Heading6>Últimos congressos</Heading6>
      <Divider />
      <LastArticles>
        {data?.latest_items?.map(({ id, image, title, reading_time, published_at, slug }) => (
          <CardNews
            key={id}
            imgSrc={image}
            imgAlt={`Imagem de ${title}`}
            title={title}
            category={title}
            readingTime={reading_time?.toString()}
            date={new Date(published_at)}
            align="column"
            url={`/${ChannelSection.SLUG}/${ChannelSlug.CONGRESSES}/${slug}`}
          />
        ))}
      </LastArticles>
    </>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${title} - ${data.title}`} />

      <ChannelSingleContent
        data={data}
        title={title}
        channel={ChannelSlug.CONGRESSES}
        lastContent={<LastArticlesContent />}
        breadcrumbs={breadcrumbs}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const { params } = context
    const session = await getSession(context)
    const bearerToken = session?.user?.token

    const data = await getPost(params['all'][0], false, { Authorization: `Bearer ${bearerToken}` })

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination:
          error.code === 4033 ? '/acesso-restrito' : `/conteudo-exclusivo?callbackUrl=${context.resolvedUrl}`,
      },
    }
  }
}

export default SingleNoticia
