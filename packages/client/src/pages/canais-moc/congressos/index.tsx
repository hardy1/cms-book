import React from 'react'

import { serverURLPath } from '../../../lib/config'
import fetcher from '../../../hooks/api/fetcher'
import { GetStaticProps, NextPage } from 'next'
import { IChannel, IGenericItem } from '../../../types'
import ChannelWrapper from '../../../containers/Channel/ChannelWrapper'
import { MainContainer } from 'designsystem'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { ClientBreadcrumbs } from '../../../components'

import { HeadWrapper } from '../../../components'

const Congresses: NextPage<{ videos: IChannel; categories: IGenericItem[] }> = (props) => {
  const { videos, categories } = props
  const breadcrumbs = [
    { content: ChannelSection.NAME, href: '/' },
    { content: ChannelName.CONGRESSES, href: `/${ChannelSection.SLUG}/${ChannelSlug.CONGRESSES}` },
  ]

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${ChannelName.CONGRESSES}`} />

      <MainContainer direction="column">
        <ClientBreadcrumbs breadcrumbs={breadcrumbs} />
        <ChannelWrapper
          title={ChannelName.CONGRESSES}
          channel={ChannelSlug.CONGRESSES}
          notFoundText="Nenhum congresso encontrado"
          categories={categories}
          content={videos}
        />
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const videos: IChannel = await fetcher(`${serverURLPath}/api/content/${ChannelSlug.CONGRESSES}`)
    const categories: IGenericItem[] = await fetcher(
      `${serverURLPath}/api/categories?channel=${ChannelSlug.CONGRESSES}`,
    )

    return {
      props: {
        videos,
        categories,
      },
      revalidate: 60 * 15,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Congresses
