import React from 'react'

import { serverURLPath } from '../../../lib/config'
import fetcher from '../../../hooks/api/fetcher'
import { GetStaticProps, NextPage } from 'next'
import { IChannel, IGenericItem } from '../../../types'
import ChannelWrapper from '../../../containers/Channel/ChannelWrapper'
import { MainContainer } from 'designsystem'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { ClientBreadcrumbs } from '../../../components'

import { HeadWrapper } from '../../../components'

const Podcasts: NextPage<{ podcasts: IChannel; categories: IGenericItem[] }> = (props) => {
  const { podcasts, categories } = props
  const breadcrumbs = [
    { content: ChannelSection.NAME, href: '/' },
    { content: ChannelName.PODCASTS, href: `/${ChannelSection.SLUG}/${ChannelSlug.PODCASTS}` },
  ]

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${ChannelName.PODCASTS}`} />

      <MainContainer direction="column">
        <ClientBreadcrumbs breadcrumbs={breadcrumbs} />
        <ChannelWrapper
          title={ChannelName.PODCASTS}
          subtitle="Ouça as últimas novidades em áudio"
          channel={ChannelSlug.PODCASTS}
          notFoundText="Nenhum podcast encontrado"
          categories={categories}
          content={podcasts}
        />
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const podcasts: IChannel = await fetcher(`${serverURLPath}/api/content/${ChannelSlug.PODCASTS}`)
    const categories: IGenericItem[] = await fetcher(`${serverURLPath}/api/categories?channel=${ChannelSlug.PODCASTS}`)

    return {
      props: {
        podcasts,
        categories,
      },
      revalidate: 60 * 15,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Podcasts
