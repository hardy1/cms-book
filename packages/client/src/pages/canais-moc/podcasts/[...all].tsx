import React from 'react'
import { NextPage, GetServerSideProps } from 'next'
import { Heading6, CardPodcast, Divider } from 'designsystem'
import { LastArticles } from '../../../containers/StylesContainer/single-artigo/_styles'
import { IBreadcrumb, IChannelData } from '../../../types'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { getPost } from '../../../services/data/moc-content.service'
import { HeadWrapper, ChannelSingleContent } from '../../../components'
import { getSession } from 'next-auth/react'

const SinglePodcast: NextPage<{ data: IChannelData }> = ({ data }) => {
  const title = ChannelName.PODCASTS
  const breadcrumbs: IBreadcrumb[] = [
    { content: ChannelSection.NAME, href: '/' },
    { content: title, href: `/${ChannelSection.SLUG}/${ChannelSlug.PODCASTS}` },
    { content: data?.title ? data?.title : '' },
  ]

  const LastArticlesContent = () => (
    <>
      <Heading6>Últimos podcasts</Heading6>
      <Divider />
      <LastArticles>
        {data?.latest_items?.map(({ id, image, title, players, published_at, slug }) => (
          <CardPodcast
            key={id}
            imgSrc={image}
            title={title}
            players={players}
            date={new Date(published_at)}
            url={`/${ChannelSection.SLUG}/${ChannelSlug.PODCASTS}/${slug}`}
          />
        ))}
      </LastArticles>
    </>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${title} - ${data.title}`} />

      <ChannelSingleContent
        data={data}
        title={title}
        channel={ChannelSlug.PODCASTS}
        lastContent={data?.latest_items?.length && <LastArticlesContent />}
        breadcrumbs={breadcrumbs}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const { params } = context
    const session = await getSession(context)
    const bearerToken = session?.user?.token

    const data = await getPost(params['all'][0], false, { Authorization: `Bearer ${bearerToken}` })

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination:
          error.code === 4033 ? '/acesso-restrito' : `/conteudo-exclusivo?callbackUrl=${context.resolvedUrl}`,
      },
    }
  }
}

export default SinglePodcast
