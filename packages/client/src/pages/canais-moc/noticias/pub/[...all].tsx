import React from 'react'
import { Heading6, CardNews, Divider } from 'designsystem'
import { LastArticles } from '../../../../containers/StylesContainer/single-artigo/_styles'
import { GetServerSideProps, NextPage } from 'next'
import { IBreadcrumb, IChannelData } from '../../../../types'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../../enums'
import { HeadWrapper, ChannelSingleContent } from '../../../../components'
import { getPost } from '../../../../services/data/moc-content.service'

const SingleNoticia: NextPage<{ data: IChannelData }> = ({ data }) => {
  const title = ChannelName.NEWS
  const breadcrumbs: IBreadcrumb[] = [
    { content: ChannelSection.NAME, href: '/' },
    { content: title, href: `/${ChannelSection.SLUG}/${ChannelSlug.NEWS}` },
    { content: data?.title },
  ]

  const LastArticlesContent = () => (
    <>
      <Heading6>Últimas notícias</Heading6>
      <Divider />
      <LastArticles>
        {data?.latest_items?.map(({ id, image, categories, title, reading_time, published_at, slug }) => (
          <CardNews
            key={id}
            imgSrc={image}
            imgAlt={title}
            title={title}
            category={categories.map((category) => category?.name).join(', ')}
            readingTime={reading_time?.toString()}
            date={new Date(published_at)}
            align="column"
            url={`/${ChannelSection.SLUG}/${ChannelSlug.NEWS}/${slug}`}
          />
        ))}
      </LastArticles>
    </>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${title} - ${data.title}`} />

      <ChannelSingleContent
        data={data}
        title={title}
        channel={ChannelSlug.NEWS}
        lastContent={<LastArticlesContent />}
        breadcrumbs={breadcrumbs}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ params, resolvedUrl }) => {
  try {
    const data = await getPost(params['all'][0], true)

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination: error.code === 4033 ? '/acesso-restrito' : `/conteudo-exclusivo?callbackUrl=${resolvedUrl}`,
      },
    }
  }
}

export default SingleNoticia
