import React from 'react'

import { serverURLPath } from '../../../lib/config'
import fetcher from '../../../hooks/api/fetcher'
import { GetStaticProps, NextPage } from 'next'
import ChannelWrapper from '../../../containers/Channel/ChannelWrapper'
import { IChannel, IGenericItem } from '../../../types'
import { MainContainer } from 'designsystem'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { ClientBreadcrumbs } from '../../../components'

import { HeadWrapper } from '../../../components'

const News: NextPage<{ news: IChannel; categories: IGenericItem[] }> = (props) => {
  const { news, categories } = props
  const breadcrumbs = [
    { content: ChannelSection.NAME, href: '/' },
    { content: ChannelName.NEWS, href: `/${ChannelSection.SLUG}/${ChannelSlug.NEWS}` },
  ]

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${ChannelName.NEWS}`} />

      <MainContainer direction="column">
        <ClientBreadcrumbs breadcrumbs={breadcrumbs} />
        <ChannelWrapper
          title={ChannelName.NEWS}
          subtitle="Fique por dentro de todas as novidades"
          channel={ChannelSlug.NEWS}
          notFoundText="Nenhuma notícia encontrada"
          categories={categories}
          content={news}
        />
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const news: IChannel = await fetcher(`${serverURLPath}/api/content/${ChannelSlug.NEWS}`)
    const categories: IGenericItem[] = await fetcher(`${serverURLPath}/api/categories?channel=${ChannelSlug.NEWS}`)

    return {
      props: {
        news,
        categories,
      },
      revalidate: 60 * 15,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default News
