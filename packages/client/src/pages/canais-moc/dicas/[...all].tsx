import React from 'react'
import { Heading6, CardTips, Divider } from 'designsystem'
import { LastArticles } from '../../../containers/StylesContainer/single-artigo/_styles'
import { GetServerSideProps, NextPage } from 'next'
import { IBreadcrumb, IChannelData } from '../../../types'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { getPost } from '../../../services/data/moc-content.service'
import { HeadWrapper, ChannelSingleContent } from '../../../components'
import { getSession } from 'next-auth/react'

const SingleDicas: NextPage<{ data: IChannelData }> = ({ data }) => {
  const title = ChannelName.TIPS
  const breadcrumbs: IBreadcrumb[] = [
    { content: ChannelSection.NAME, href: '/' },
    { content: title, href: `/${ChannelSection.SLUG}/${ChannelSlug.TIPS}` },
    { content: data?.title },
  ]

  const LastArticlesContent = () => (
    <>
      <Heading6>Últimas dicas</Heading6>
      <Divider />
      <LastArticles>
        {data?.latest_items?.map(({ id, title, published_at, slug, categories }) => (
          <CardTips
            key={id}
            category={categories['ame']}
            title={title}
            date={new Date(published_at)}
            url={`/${ChannelSection.SLUG}/${ChannelSlug.TIPS}/${slug}`}
          />
        ))}
      </LastArticles>
    </>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${title} - ${data.title}`} />

      <ChannelSingleContent
        data={data}
        title={title}
        channel={ChannelSlug.TIPS}
        lastContent={<LastArticlesContent />}
        breadcrumbs={breadcrumbs}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const { params } = context
    const session = await getSession(context)
    const bearerToken = session?.user?.token

    const data = await getPost(params['all'][0], false, { Authorization: `Bearer ${bearerToken}` })

    if (!data) {
      return {
        notFound: true,
        props: {},
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination:
          error.code === 4033 ? '/acesso-restrito' : `/conteudo-exclusivo?callbackUrl=${context.resolvedUrl}`,
      },
    }
  }
}

export default SingleDicas
