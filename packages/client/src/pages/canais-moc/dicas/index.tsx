import React from 'react'

import { serverURLPath } from '../../../lib/config'
import fetcher from '../../../hooks/api/fetcher'
import { GetStaticProps, NextPage } from 'next'

import { IChannel, IGenericItem } from '../../../types'
import ChannelWrapper from '../../../containers/Channel/ChannelWrapper'
import { MainContainer } from 'designsystem'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { ClientBreadcrumbs } from '../../../components'

import { HeadWrapper } from '../../../components'

const Dicas: NextPage<{ tips: IChannel; categories: IGenericItem[] }> = (props) => {
  const { tips, categories } = props
  const breadcrumbs = [
    { content: ChannelSection.NAME, href: '/' },
    { content: ChannelName.TIPS, href: `/${ChannelSection.SLUG}/${ChannelSlug.TIPS}` },
  ]

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${ChannelName.TIPS}`} />

      <MainContainer direction="column">
        <ClientBreadcrumbs breadcrumbs={breadcrumbs} />
        <ChannelWrapper
          title={ChannelName.TIPS}
          subtitle="Dicas novas todo mês"
          channel={ChannelSlug.TIPS}
          notFoundText="Nenhuma dica encontrada"
          categories={categories}
          content={tips}
        />
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const tips: IChannel = await fetcher(`${serverURLPath}/api/content/${ChannelSlug.TIPS}`)
    const categories: IGenericItem[] = await fetcher(`${serverURLPath}/api/categories?channel=${ChannelSlug.TIPS}`)

    return {
      props: {
        tips,
        categories,
      },
      revalidate: 60 * 15,
    }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Dicas
