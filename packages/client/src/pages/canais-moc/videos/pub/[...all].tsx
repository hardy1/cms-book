import React from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { Heading6, CardThumbVideo, Divider } from 'designsystem'
import { LastArticles } from '../../../../containers/StylesContainer/single-artigo/_styles'
import { serverURLPath } from '../../../../lib/config'
import fetcher from '../../../../hooks/api/fetcher'
import { IBreadcrumb, IChannelData } from '../../../../types'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../../enums'
import { HeadWrapper, ChannelSingleContent } from '../../../../components'

const SingleVideo: NextPage<{ data: IChannelData }> = ({ data }) => {
  const title = ChannelName.VIDEOS
  const breadcrumbs: IBreadcrumb[] = [
    { content: ChannelSection.NAME, href: '/' },
    { content: title, href: `/${ChannelSection.SLUG}/${ChannelSlug.VIDEOS}` },
    { content: data?.title },
  ]

  const LastArticlesContent = () => (
    <>
      <Heading6>Últimos Vídeos-MOC</Heading6>
      <Divider />
      <LastArticles>
        {data?.latest_items?.map(({ id, image, title, subtitle, published_at, slug, sponsor }) => (
          <CardThumbVideo
            key={id}
            imgSrc={image}
            title={title}
            subtitle={subtitle}
            date={new Date(published_at)}
            footerImg={sponsor}
            url={`/${ChannelSection.SLUG}/${ChannelSlug.VIDEOS}/${slug}`}
          />
        ))}
      </LastArticles>
    </>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${title} - ${data.title}`} />

      <ChannelSingleContent
        data={data}
        title={title}
        channel={ChannelSlug.VIDEOS}
        lastContent={<LastArticlesContent />}
        breadcrumbs={breadcrumbs}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ params, resolvedUrl }) => {
  try {
    const data: IChannelData = await fetcher(`${serverURLPath}/api/post/${params['all'][0]}`, true)

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination: error.code === 4033 ? '/acesso-restrito' : `/conteudo-exclusivo?callbackUrl=${resolvedUrl}`,
      },
    }
  }
}

export default SingleVideo
