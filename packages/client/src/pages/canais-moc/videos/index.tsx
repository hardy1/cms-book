import React from 'react'

import { serverURLPath } from '../../../lib/config'
import fetcher from '../../../hooks/api/fetcher'
import { GetStaticProps, NextPage } from 'next'
import { IChannel, IGenericItem } from '../../../types'
import ChannelWrapper from '../../../containers/Channel/ChannelWrapper'
import { MainContainer } from 'designsystem'
import { ChannelName, ChannelSection, ChannelSlug } from '../../../enums'
import { ClientBreadcrumbs } from '../../../components'

import { HeadWrapper } from '../../../components'

const Videos: NextPage<{ videos: IChannel; categories: IGenericItem[] }> = (props) => {
  const { videos, categories } = props
  const breadcrumbs = [
    { content: ChannelSection.NAME, href: '/' },
    { content: ChannelName.VIDEOS, href: `/${ChannelSection.SLUG}/${ChannelSlug.VIDEOS}` },
  ]

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${ChannelName.VIDEOS}`} />

      <MainContainer direction="column">
        <ClientBreadcrumbs breadcrumbs={breadcrumbs} />

        <ChannelWrapper
          title={ChannelName.VIDEOS}
          subtitle="Assista às principais novidades em vídeo"
          channel={ChannelSlug.VIDEOS}
          notFoundText="Nenhum vídeo encontrado"
          categories={categories}
          content={videos}
        />
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const videos: IChannel = await fetcher(`${serverURLPath}/api/content/${ChannelSlug.VIDEOS}`)
    const categories: IGenericItem[] = await fetcher(`${serverURLPath}/api/categories?channel=${ChannelSlug.VIDEOS}`)

    return {
      props: {
        videos,
        categories,
      },
      revalidate: 60,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Videos
