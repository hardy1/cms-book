import { GetServerSideProps } from 'next'
import Link from 'next/link'
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'

import {
  LeftContainer,
  TitlesContainer,
  SubTitle,
  ResultsContainer,
  ButtonContainer,
  PartTitle,
  ChapterTitle,
  ChapterContent,
  Part,
  Chapter,
  BookTitle,
} from '../../containers/StylesContainer/livro-pesquisa/_styles'
import { BooksContainer } from '../../containers/Books'
import { Button, MainContainer } from 'designsystem'
import HighlightBookTitleSearch from '../../containers/Books/HighlightBookTitleSearch'
import { NextPageWithAuth } from '../../types/auth.type'
import { getBooksSearchList } from '../../services'
import { BooksToolbar, HeadWrapper } from '../../components'
import { getSession } from 'next-auth/react'

const BooksSearch: NextPageWithAuth<{ query; data; auth; path; session }> = ({ query, data, path, session }) => {
  const [results, setResults] = useState(data?.contents?.data)
  const [page, setPage] = useState(1)
  const isFirstRender = useRef(true)

  const loadMore = () => {
    if (page < data?.contents?.last_page) {
      setPage(page + 1)
    }
  }

  useEffect(() => {
    setPage(1)
    setResults(data?.contents?.data)
  }, [query, data])

  useLayoutEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false
      return
    }

    if (page >= 2) {
      const getNewResults = async () => {
        const data = await getBooksSearchList(path, query, session, page)
        const concat = results.concat(data)
        setResults(concat)
      }

      getNewResults()
    }
  }, [page])

  function RenderMark(content) {
    const regexTerm = new RegExp(query.q, 'g')
    const sliceContent = '<p>' + content.slice(40, -3200) + '</p>'
    const newContent = sliceContent.replace(regexTerm, `<b style="color: #252525;">${query.q}</b>`)

    return <ChapterContent dangerouslySetInnerHTML={{ __html: newContent }} />
  }

  return (
    <>
      <HeadWrapper title={`MOC Brasil - Busca por ${query.q}`} />

      <BooksToolbar />

      <HighlightBookTitleSearch title={query.q} />
      <BooksContainer>
        <MainContainer>
          <LeftContainer>
            <TitlesContainer>
              <SubTitle>
                Sua pesquisa teve <strong>{data?.contents?.total} resultados</strong>
              </SubTitle>
            </TitlesContainer>
            <ResultsContainer>
              {results &&
                results.map((result) => (
                  <Link href={`/livro/${result.chapter.part.book.slug}`} key={result.id}>
                    <a>
                      <BookTitle>{result.chapter.part.book.name}</BookTitle>

                      <Part>
                        <Link href={`/livro/${result.chapter.part.book.slug}/${result.chapter.part.slug}`}>
                          <a>
                            <PartTitle>{result.chapter.part.name}</PartTitle>

                            <Chapter>
                              <Link
                                href={`/livro/${result.chapter.part.book.slug}/${result.chapter.part.slug}/${result.chapter.slug}`}
                              >
                                <a>
                                  <ChapterTitle>{result.title}</ChapterTitle>
                                  {RenderMark(result.content)}
                                </a>
                              </Link>
                            </Chapter>
                          </a>
                        </Link>
                      </Part>
                    </a>
                  </Link>
                ))}
            </ResultsContainer>
            <ButtonContainer>
              {page < data?.contents?.last_page && <Button onClick={loadMore}>Veja mais</Button>}
            </ButtonContainer>
          </LeftContainer>
        </MainContainer>
      </BooksContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req, query } = context
  const session = await getSession({ req })
  const path = process.env.BASE_API_URL

  const data = await getBooksSearchList(path, query, session)

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      data,
      query: context.query,
      path,
      session,
    },
  }
}

export default BooksSearch
BooksSearch.auth = true
