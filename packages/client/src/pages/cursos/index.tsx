import React, { useState } from 'react'
import { Heading1, Heading2, CardCourse, MainContainer } from 'designsystem'
import parse from 'html-react-parser'
import { TitleContainer, CursosContainer } from '../../containers/StylesContainer/cursos/_styles'

import { serverURLPath } from '../../lib/config'
import fetcher from '../../hooks/api/fetcher'
import { GetServerSideProps, GetStaticProps, NextPage } from 'next'

import { HeadWrapper } from '../../components'
import { getSession } from 'next-auth/react'

interface course {
  id: number
  title: string
  description: string
  url: string
  price: string
  sponsor: string
  category_list: string
  thumbnail_path: string
}

const Cursos: NextPage<{ courses: course[] }> = (props) => {
  const { courses } = props
  return (
    <>
      <HeadWrapper title="MOC Brasil - Cursos" />

      <MainContainer direction="column">
        <TitleContainer>
          <Heading1 color="primaryColor">Cursos</Heading1>
          <Heading2 color="disabledColor">Cursos para você se aperfeiçoar!</Heading2>
        </TitleContainer>

        <CursosContainer>
          {courses.length > 0 &&
            courses?.map(({ id, title, description, url, price, thumbnail_path }) => (
              <CardCourse
                key={id}
                imgSrc={thumbnail_path}
                title={title}
                description={parse(description)}
                value={price}
                url={url}
                target="_blank"
              />
            ))}
        </CursosContainer>
      </MainContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const session = await getSession(context)
    const bearerToken = session?.user?.token
    const options = {
      method: 'GET',
      headers: new Headers({
        Authorization: `Bearer ${bearerToken}`,
      }),
    }
    const courses: course[] = await fetcher(`${serverURLPath}/api/courses`, options)

    if (!courses) {
      return {
        redirect: {
          destination: '/404',
          permanent: false,
        },
        notFound: true,
      }
    }

    return {
      props: { courses },
    }
  } catch {
    return {
      props: {
        courses: [],
      },
    }
  }
}

export default Cursos
