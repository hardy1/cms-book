import React from 'react'
import { NextPage, GetStaticProps } from 'next'
import { Heading2, Heading3, Paragraph, InternalHeader, MainContainer } from 'designsystem'
import {
  TextContainer,
  ParagraphContainer,
  FormulasContainer,
  Formulas,
} from '../../containers/StylesContainer/formulas-medicas/_styles'
import { CardFormula, HeadWrapper } from '../../components'

import { serverURLPath } from '../../lib/config'
import fetcher from '../../hooks/api/fetcher'
import { MedicalFormula } from '../../types'
import { Spacing } from '../_app'

const FormulasMedicas: NextPage<{ medicalFormulas: MedicalFormula[] }> = (props) => {
  const { medicalFormulas } = props

  return (
    <>
      <HeadWrapper title="MOC Brasil - Fórmulas médicas" />
      <InternalHeader>
        <Heading2 color="primaryColor">Fórmulas médicas</Heading2>
      </InternalHeader>
      <Spacing />
      <MainContainer direction="column">
        <TextContainer>
          <Heading3 color="primaryColor">Fórmulas</Heading3>
          <ParagraphContainer>
            <Paragraph>
              Estão disponíveis para os usuários cadastrados no site algumas calculadoras e ferramentas que facilitarão
              o dia a dia dos profissionais de saúde
            </Paragraph>
          </ParagraphContainer>
        </TextContainer>
        <FormulasContainer>
          <Formulas>
            {medicalFormulas?.map(({ id, title, slug }) => (
              <CardFormula key={id} title={title} category="Fórmulas Médicas" url={`/formulas-medicas/${slug}`} />
            ))}
          </Formulas>
        </FormulasContainer>
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const medicalFormulas: MedicalFormula[] = await fetcher(`${serverURLPath}/api/medical-formulas/`)

    if (!medicalFormulas) {
      return {
        redirect: {
          destination: '/404',
          permanent: false,
        },
        notFound: true,
      }
    }

    return { props: { medicalFormulas }, revalidate: 60 }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default FormulasMedicas
