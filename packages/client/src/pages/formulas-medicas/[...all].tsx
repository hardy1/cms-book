import React from 'react'
import parse from 'html-react-parser'
import { CardRelated, Heading1, Heading2, InternalHeader, MainContainer } from 'designsystem'
import {
  TextContainer,
  FormulaContainer,
  Formulas,
  RelatedFormulaContainer,
  ContainerWrapper,
} from '../../containers/StylesContainer/formulas-medicas/_styles'

import { GetServerSideProps, NextPage } from 'next'

import { HeadWrapper } from '../../components'
import { withAuthSsr } from '../../../utils/withAuthSSR'
import { getMedicalFormula } from '../../services/data/moc-content.service'

const SingleMedicalFormula: NextPage<{ data }> = (props) => {
  const { data } = props
  const payload = data

  const relatedItems = payload?.related_items?.map((item) => ({ ...item, url: item.slug }))

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${data?.title}`} />

      <InternalHeader>
        <Heading2 color="primaryColor">Fórmulas médicas</Heading2>
      </InternalHeader>
      <MainContainer direction="column">
        <ContainerWrapper>
          <TextContainer>
            <Heading1 color="primaryColor">{payload && payload.title}</Heading1>
          </TextContainer>
          <FormulaContainer>
            <Formulas>{payload && parse(payload.content || '')}</Formulas>
          </FormulaContainer>
          <RelatedFormulaContainer>
            <CardRelated title="Selecione uma fórmula" items={relatedItems} />
          </RelatedFormulaContainer>
        </ContainerWrapper>
      </MainContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = withAuthSsr(async ({ params }) => {
  try {
    const data = await getMedicalFormula(params['all'][0])

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data } }
  } catch (error) {
    return {
      props: { data: {} },
      redirect: {
        destination: '/403',
        statusCode: 307,
      },
    }
  }
})

export default SingleMedicalFormula
