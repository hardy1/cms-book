import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function coursesHandler(req, res) {
  const source = 'api/search'
  const { query } = req

  const baseUri = `${serverURLPath}/${source}`

  const queries = Object.entries(query).map(([key, value]) => `${key}=${value}`)

  const queriesStringified = queries.length && `?${queries.join('&')}`

  const endpoint = `${baseUri}${queriesStringified}`

  try {
    const response = await axios.get(endpoint)

    res.status(200).json(response?.data)
  } catch (e) {
    console.error(e)
    res.status(405).end(`Not Allowed`)
  }
}
