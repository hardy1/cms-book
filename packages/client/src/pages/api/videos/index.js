import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function videosMocHandler(req, res) {
  const source = 'api/content/videos'

  try {
    const response = await axios.get(`${serverURLPath}/${source}`)

    res.status(200).json(response?.data?.payload)
  } catch (e) {
    res.status(405).end(`Not Allowed`)
  }
}
