import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function mocChannelsHandler(req, res) {
  const { query } = req
  const queryArray = Object.entries(query)?.map(([key, value]) => `${key}=${value}`)
  const source = 'api/categories'
  const queryString = queryArray.length ? `?${queryArray.join('&')}` : ''

  try {
    const response = await axios.get(`${serverURLPath}/${source}${queryString}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end(`Not Allowed`)
  }
}
