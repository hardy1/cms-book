import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function conatctUsHandler(req, res) {
  const source = 'api/contact-us'

  try {
    const response = await axios.get(`${serverURLPath}/${source}`)

    res.status(200).json(response?.data?.payload)
  } catch (e) {
    res.status(404).end(`Not Found`)
  }
}
