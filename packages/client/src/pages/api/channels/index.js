import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function contactsHandler(req, res) {
  const source = 'api/channels'

  try {
    const response = await axios.get(`${serverURLPath}/${source}`)

    res.status(200).json(response.data)
  } catch (e) {
    res.status(405).end(`Not Allowed`)
  }
}
