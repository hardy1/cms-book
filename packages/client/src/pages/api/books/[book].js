import { apiCall } from '../../../services'

export default async function indexBookHandler(req, res) {
  const source = 'api/books'
  const {
    query: { book },
  } = req

  try {
    const response = await apiCall.get(`/${source}/${book}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end(book)
  }
}
