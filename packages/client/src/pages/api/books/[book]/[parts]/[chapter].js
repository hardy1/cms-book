import axios from 'axios'
import { serverURLPath } from '../../../../../lib/config'

export default async function chapterBookHandler(req, res) {
  const source = 'api/books'
  const {
    query: { book, parts, chapter },
  } = req

  try {
    const response = await axios.get(`${serverURLPath}/${source}/${book}/${parts}/${chapter}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end(parts)
  }
}
