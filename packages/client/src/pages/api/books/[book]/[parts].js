import axios from 'axios'
import { serverURLPath } from '../../../../lib/config'

export default async function partsBookHandler(req, res) {
  const source = 'api/books'
  const {
    query: { book, parts },
  } = req

  try {
    const response = await axios.get(`${serverURLPath}/${source}/${book}/${parts}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end(parts)
  }
}
