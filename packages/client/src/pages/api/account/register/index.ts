import { apiCall } from '../../../../services/api'

export default async function registerHandler(req, res) {
  const source = '/api/account/register'
  const { body } = req

  const response = await apiCall({
    method: 'post',
    url: source,
    data: body,
  })

  if (response?.data?.status) {
    res.status(200).json(response?.data)
  } else {
    res.status(200).json({ message: response?.data?.message })
  }
}
