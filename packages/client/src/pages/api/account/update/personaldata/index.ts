import { apiCall } from '../../../../../services'

export default async function updatePersonalDataHandler(req, res) {
  const source = '/api/account/update/personaldata'
  const { body } = req

  if (req.method === 'PUT') {
    const response = await apiCall({
      method: 'put',
      url: source,
      data: body,
    })

    res.status(200).json(response?.data)
    if (response?.data?.status === 200) {
      res.status(200).json(response?.data)
    } else if (response?.data?.status === 401) {
      res.status(401).json({ message: response?.data?.message })
    } else {
      res.status(response?.data?.status).json({ message: response?.data?.message })
    }
  } else {
    res.status(501)
  }
}
