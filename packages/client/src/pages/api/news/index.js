import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function newsHandler(req, res) {
  const source = 'api/content/noticias'

  try {
    const response = await axios.get(`${serverURLPath}/${source}`)

    res.status(200).json(response?.data?.payload)
  } catch (e) {
    res.status(405).end(`Not Allowed`)
  }
}
