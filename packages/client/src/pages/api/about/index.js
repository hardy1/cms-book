import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function aboutHandler(req, res) {
  const source = '/api/pages/sobre'

  try {
    const response = await axios.get(`${serverURLPath}${source}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end(`Not Allowed`)
  }
}
