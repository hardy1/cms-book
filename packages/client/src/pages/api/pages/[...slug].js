import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function singlePageItemHandler(req, res) {
  const source = 'api/pages'
  const {
    query: { slug },
  } = req

  try {
    const response = await axios.get(`${serverURLPath}/${source}/${slug}`)

    res.status(200).json(response.data)
  } catch (e) {
    res.status(405).end(slug)
  }
}
