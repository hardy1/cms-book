import axios from 'axios'
import { serverURLPath } from '../../../lib/config'

export default async function indexDrugHandler(req, res) {
  const source = 'api/drugs/details'
  const {
    query: { slug },
  } = req

  try {
    const response = await axios.get(`${serverURLPath}/${source}/${slug}`)

    res.status(200).json(response.data?.payload)
  } catch (e) {
    res.status(405).end()
  }
}
