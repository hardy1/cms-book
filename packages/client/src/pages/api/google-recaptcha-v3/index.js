import axios from 'axios'

export default async function googleRecaptchaV3Handler(req, res) {
  const fetchUrl = req.headers.url
  try {
    const response = await axios.post(fetchUrl)
    res.status(200).json(response.data)
  } catch (e) {
    res.status(405)
  }
}
