import NextAuth, { JWT } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'
import { apiCall } from '../../../services'
import login, { refreshToken } from '../../../services/auth'
import jwt from 'jsonwebtoken'
import { IGenericItem } from '../../../types'

interface IDecryptedToken extends JWT {
  firstname?: string | null
  exp?: string | null
}

async function refreshAccessToken(token: string): Promise<JWT> {
  try {
    const response = token && (await refreshToken({ token }))
    const refreshedToken = response

    if (!response.token || !response) {
      throw refreshedToken
    }

    return { jwt: refreshedToken?.token }
  } catch (error) {
    return {
      jwt: token,
      error: 'RefreshAccessTokenError',
    }
  }
}

export default NextAuth({
  //debug: true,
  providers: [
    CredentialsProvider({
      name: 'Login',
      credentials: {
        login: { label: 'Email', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        try {
          const loginResponse = await login({ login: credentials.login, password: credentials.password })

          if (loginResponse) {
            return { ...loginResponse }
          }
        } catch (e) {
          throw new Error(e.message)
        }
      },
    }),
  ],
  callbacks: {
    jwt: async ({ token, user }) => {
      if (user) {
        const decryptedToken: unknown = jwt.verify(user.token as string, process.env.NEXTAUTH_SECRET, {
          algorithms: ['HS256'],
        })

        token.jwt = user.token
        token.role = (<IGenericItem>(<IDecryptedToken>decryptedToken)?.role).code
        token.permissions = (<IDecryptedToken>decryptedToken)?.permissions
        token.name = (<IDecryptedToken>decryptedToken)?.firstname
        token.email = (<IDecryptedToken>decryptedToken)?.email
        token.picture = 'https://mocbrasil.com/favicon.png'
        token.accessTokenExpires = Date.now() + (<IDecryptedToken>decryptedToken)?.exp

        token.error = !token.jwt && 'authorizationError'
        apiCall.defaults.headers.common['Authorization'] = `Bearer ${user.token}`
        return token
      }

      if (Date.now() < token.accessTokenExpires) {
        return token
      }

      if (token.jwt) {
        const refreshResponse = await refreshAccessToken(token.jwt as string)
        return { ...refreshResponse }
      }
    },
    session: async ({ session, token }) => {
      if (token) {
        session.user.role = token.role.toString()
        session.user.permissions = <[]>token.permissions
        session.user.token = token.jwt.toString()
        apiCall.defaults.headers.common['Authorization'] = `Bearer ${session.jwt}`
      }
      return session
    },
    redirect: async ({ url, baseUrl }) => {
      // Allows relative callback URLs
      if (url.startsWith('/')) return `${baseUrl}${url}`
      // Allows callback URLs on the same origin
      else if (new URL(url).origin === baseUrl) return url
      return baseUrl
    },
  },
  pages: {
    signIn: '/conteudo-exclusivo',
    error: '/conteudo-exclusivo',
  },
})
