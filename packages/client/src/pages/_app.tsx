import React, { useEffect, useState } from 'react'
import { Router, useRouter } from 'next/router'
import Cookies from 'js-cookie'
import styled, { ThemeProvider as BookThemeProvider } from 'styled-components'
import Loader from '../components/Loader'
import { getSession, SessionProvider, useSession, SessionContextValue } from 'next-auth/react'

import {
  BooksContent,
  BooksTopHeader,
  ClientTopHeader,
  ClientBooks,
  ClientHeader,
  ClientFooter,
  CookiesPolicyTermsAndConditions,
  SimpleHeader,
} from '../components'

import theme from './../lib/theme'

import GlobalContext from '../contexts'
import GlobalStyle from './../lib/globalSyles'

import media from '../resources/media'
import { mockCommon } from '../resources/mocks/common'
import { ToastContainer, toastSetup } from '../components/Toast/Toast'
import DashboardContextProvider from '../contexts/dashboard'
import { getCommons } from '../services'
import { ICommons } from '../types'
import Axios, { AxiosResponse } from 'axios'

export default function App({ Component, session, pageProps }) {
  const router = useRouter()
  const [loading, setLoading] = useState({ active: false, fullscreen: false })
  const [common, setCommon] = useState<ICommons>(mockCommon)
  const [logged, setLogged] = useState<boolean>(false)
  const [updatedSession, setUpdatedSession] = useState<SessionContextValue['data']>()

  const isCookieUsageAllowed = Cookies.get('allowCookieUsage') === 'yes'

  useEffect(() => {
    const getUpdatedSession = async () => {
      if (!updatedSession) {
        const response: AxiosResponse = await Axios.get('/api/auth/session')
        setUpdatedSession(response?.data)
      }

      setLogged(Boolean(updatedSession?.user))
    }
    getUpdatedSession()
  }, [updatedSession])

  useEffect(() => {
    const updateCommon = async () => {
      const response = await getCommons()
      setCommon(response)
    }

    updateCommon()

    const start = () => {
      setLoading({
        active: true,
        fullscreen: true,
      })
    }
    const end = () => {
      setLoading({
        active: false,
        fullscreen: false,
      })
    }

    Router.events.on('routeChangeStart', start)
    Router.events.on('routeChangeComplete', end)
    Router.events.on('routeChangeError', end)

    return () => {
      Router.events.off('routeChangeStart', start)
      Router.events.off('routeChangeComplete', end)
      Router.events.off('routeChangeError', end)
    }
  }, [])

  const getLayout = () => {
    if (
      router.pathname.indexOf('/cadastro/dados-iniciais') === 0 ||
      router.pathname.indexOf('/cadastro/dados-pessoais') === 0 ||
      router.pathname.indexOf('/cadastro/dados-profissionais') === 0 ||
      router.pathname.indexOf('/cadastro/dados-de-pagamento') === 0 ||
      router.pathname.indexOf('/cadastro/sucesso') === 0
    ) {
      return (
        <>
          <ToastContainer id={toastSetup.containerId} />
          <SimpleHeader />
          <ClientHeader items={common.header} books={common.books} hideDesktop={true} session={session} />
          <Component {...pageProps} />
          <ClientFooter items={common.footer} />
          <GlobalStyle />
        </>
      )
    }

    if (router.pathname.indexOf('/livro') === -1) {
      if (router.pathname.indexOf('/minha-conta') === -1) {
        return (
          <>
            <ToastContainer id={toastSetup.containerId} />
            <ClientTopHeader logged={logged || Boolean(session)} />
            <ClientHeader items={common.header} books={common.books} session={session} />
            <ClientBooks items={common.books} />
            {Component.auth ? (
              <Auth>
                <Component {...pageProps} />
              </Auth>
            ) : (
              <Component {...pageProps} />
            )}
            <ClientFooter items={common.footer} />
            {!isCookieUsageAllowed && <CookiesPolicyTermsAndConditions />}
            <GlobalStyle />
          </>
        )
      } else {
        return (
          <DashboardContextProvider>
            <ToastContainer id={toastSetup.containerId} />
            {Component.auth ? (
              <Auth>
                <Component {...pageProps} />
              </Auth>
            ) : (
              <Component {...pageProps} />
            )}
          </DashboardContextProvider>
        )
      }
    } else {
      return (
        <GlobalContext>
          <BookThemeProvider theme={theme}>
            <ToastContainer id={toastSetup.containerId} />
            <Loader active={loading.active} fullscreen={loading.fullscreen} />
            <BooksTopHeader />
            <ClientBooks items={common.books} />
            <BooksContent>
              {Component.auth ? (
                <Auth>
                  <Component {...pageProps} />
                </Auth>
              ) : (
                <Component {...pageProps} />
              )}
            </BooksContent>
            <GlobalStyle />
          </BookThemeProvider>
        </GlobalContext>
      )
    }
  }

  return <SessionProvider session={session}>{getLayout()}</SessionProvider>
}

export const Spacing = styled.div`
  height: 3.5rem;
  display: block;

  @media ${media.tabletL} {
    display: none;
  }
`

function Auth({ children }) {
  const { data: session } = useSession({
    required: true,
  })

  const isUser = session?.user

  if (isUser) {
    return children
  }

  return <Loader active />
}

// TODO - Avoid using getInitialProps, it slow down alls pages load
App.getInitialProps = async ({ ctx }) => {
  const session = await getSession(ctx)
  return { session }
}
