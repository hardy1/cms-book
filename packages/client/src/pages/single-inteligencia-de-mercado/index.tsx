import React from 'react'
import { GridContainer, Heading2, Heading3, Paragraph, Button, InternalHeader } from 'designsystem'

import {
  TextContainer,
  ParagraphContainer,
  ButtonContainer,
} from '../../containers/StylesContainer/single-inteligencia-de-mercado/_styles'

import { HeadWrapper } from '../../components'

const Inteligencia: React.FC = () => {
  return (
    <>
      <HeadWrapper title="Moc Brasil - Pesquisa ONCO Censo 2020" />

      <InternalHeader>
        <Heading2 color="primaryColor">Pesquisa ONCO Censo 2020</Heading2>
      </InternalHeader>
      <GridContainer>
        <TextContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Bem-vindo à Pesquisa de Mercado ONCO Censo 2020</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra,
              condimentum. Aliquet aliquet egestas feugiat fringilla commodo rutrum vitae neque, magna. Nisi, velit in
              commodo, risus lacus scelerisque enim et. Consequat congue sit scelerisque urna. Eget nisl nulla vitae
              turpis tincidunt imperdiet eget aliquet porttitor.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Objetivo</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra,
              condimentum. Aliquet aliquet egestas feugiat fringilla commodo rutrum vitae neque, magna. Nisi, velit in
              commodo, risus lacus scelerisque enim et. Consequat congue sit scelerisque urna. Eget nisl nulla vitae
              turpis tincidunt imperdiet eget aliquet porttitor.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Público</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Remuneração</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Formato</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Sobre</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra,
              condimentum. Aliquet aliquet egestas feugiat fringilla commodo rutrum vitae neque, magna. Nisi, velit in
              commodo, risus lacus scelerisque enim et. Consequat congue sit scelerisque urna. Eget nisl nulla vitae
              turpis tincidunt imperdiet eget aliquet porttitor.
            </Paragraph>
          </ParagraphContainer>
          <ParagraphContainer>
            <Heading3 color="primaryColor">Termos e Condições</Heading3>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus convallis lorem pharetra,
              condimentum. Aliquet aliquet egestas feugiat fringilla commodo rutrum vitae neque, magna. Nisi, velit in
              commodo, risus lacus scelerisque enim et. Consequat congue sit scelerisque urna. Eget nisl nulla vitae
              turpis tincidunt imperdiet eget aliquet porttitor.
            </Paragraph>
          </ParagraphContainer>
        </TextContainer>
        <ButtonContainer>
          <Button onClick={() => null}>Quero participar</Button>
        </ButtonContainer>
      </GridContainer>
    </>
  )
}

export default Inteligencia
