import React from 'react'
import parse from 'html-react-parser'
import { Heading2, Button, InternalHeader, HtmlWrapper, MainContainer } from 'designsystem'
import { TextContainer, ButtonContainer } from '../../containers/StylesContainer/sobre/_styles'
import { NextPage } from 'next'
import fetcher from '../../hooks/api/fetcher'
import { serverURLPath } from '../../lib/config'
import { useRouter } from 'next/router'
import { AboutPageType, AboutType, AuthorsBooksType } from '../../types'
import { BooksCollaborators, HeadWrapper } from '../../components'
import { useSession } from 'next-auth/react'

const Sobre: NextPage<AboutPageType> = (props: { data: AboutType; books: AuthorsBooksType[] }) => {
  const router = useRouter()
  const { status } = useSession({ required: false })
  const { title, cta_text, cta_url, content } = props.data
  const books = props.books

  const DynamicButton = () => (
    <ButtonContainer>
      <Button onClick={() => router.push(cta_url)}>{cta_text}</Button>
    </ButtonContainer>
  )

  return (
    <>
      <HeadWrapper title="MOC Brasil - Sobre" />

      <InternalHeader>
        <Heading2 color="primaryColor">{title}</Heading2>
      </InternalHeader>

      <MainContainer direction="column">
        <TextContainer>
          <HtmlWrapper>{content && parse(content)}</HtmlWrapper>
        </TextContainer>

        {cta_url ? <DynamicButton /> : ''}
      </MainContainer>

      <BooksCollaborators data={books} />
    </>
  )
}

export const getStaticProps = async () => {
  try {
    const data: AboutType = await fetcher(`${serverURLPath}/api/pages/sobre`)
    const books: AuthorsBooksType[] = await fetcher(`${serverURLPath}/api/books/resume`)

    if (!data && !books) {
      return {
        notFound: true,
      }
    }

    return {
      props: { data: data, books: books },
      revalidate: 60,
    }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Sobre
