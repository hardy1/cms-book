import React from 'react'
import Link from 'next/link'
import { GridContainer, Heading2, Paragraph, Button } from 'designsystem'
import { TitleContainer, ButtonContainer } from '../../containers/StylesContainer/404/_styles'

import { HeadWrapper } from '../../components'

const Page404: React.FC = () => {
  return (
    <>
      <HeadWrapper title="MOC Brasil - Página não encontrada" />
      <GridContainer>
        <TitleContainer>
          <Heading2 color="primaryColor">404: Página não encontrada!</Heading2>
          <Paragraph>O link que você clicou pode estar quebrado ou esta página não está mais disponível.</Paragraph>
        </TitleContainer>
        <ButtonContainer>
          <Link href="/" passHref>
            <Button as="a" onClick={() => null}>
              Voltar para página inicial
            </Button>
          </Link>
        </ButtonContainer>
      </GridContainer>
    </>
  )
}

export default Page404
