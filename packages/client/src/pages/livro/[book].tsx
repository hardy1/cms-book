import React, { useContext, useEffect } from 'react'
import { GetStaticProps } from 'next'

import { BooksContainer } from '../../containers/Books'
import BookContext from '../../contexts/book'
import { BookFullType, BookScopeDataType, CurrentBookDataType, PathsVM } from '../../types'
import { fetchBookCommons, fetchBooks } from '../../services'
import { validateTheme } from '../../lib/theme'
import { BooksToolbar, CardBiography, GenericList, Presentation } from '../../components'
import { CollaboratorsGroupType, CollaboratorType } from '../../types/collaborator.model'
import { HeadWrapper } from '../../components'
import { NextPageWithAuth } from '../../types/auth.type'

const Index: NextPageWithAuth<{ book: BookFullType; current: CurrentBookDataType; auth: boolean }> = (props) => {
  const { book, current } = props
  const { collaborators_groups } = book
  const mainCollaborators = ['Editor', 'Editor convidado', 'Coeditor']
  const { state, setState: setGlobalState } = useContext(BookContext)

  const editorsGroup: CollaboratorsGroupType[] = collaborators_groups?.filter((collaborator) =>
    mainCollaborators.includes(collaborator.group),
  )
  const reviewersGroup: CollaboratorsGroupType[] = collaborators_groups?.filter(
    (collaborator) => !mainCollaborators.includes(collaborator.group),
  )
  const editors: CollaboratorType[] = editorsGroup
    ?.map((editor) => editor.collaborators)
    .reduce((acc, curr) => acc.concat(curr), [])
  const reviewers: CollaboratorType[] = reviewersGroup
    ?.map((reviewer) => reviewer.collaborators)
    .reduce((acc, curr) => acc.concat(curr), [])

  useEffect(() => {
    if (current.bookSlug !== state.current?.bookSlug) {
      setGlobalState({
        ...state,
        current,
      })
    }

    if (!Object.keys(state.book).length) {
      setGlobalState({
        ...state,
        book: { ...book },
      })
    }
  }, [state, current, book, setGlobalState])

  const selectedTheme: string = validateTheme(book.theme_color)

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${book.name}`} />

      <BooksToolbar book={book} current={current} />
      <BooksContainer colorTheme={selectedTheme} fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode}>
        <Presentation
          book={book}
          colorTheme={selectedTheme}
          fontSizeModifier={state?.fontModifier}
          nightMode={state?.nightMode}
        />

        <h4 className="CardMainTitle">Índice do Livro</h4>
        <GenericList
          items={book?.parts}
          baseUrl={`/livro/${book.slug}`}
          colorTheme={selectedTheme}
          nightMode={state?.nightMode}
          fontSizeModifier={state?.fontModifier}
        />

        <h4 className="CardMainTitle">Editores e autores</h4>

        <h5 className="CardTitle">Editores do MOC {book.name}</h5>
        <CardBiography collaborators={editors} fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode} />

        {reviewers?.length ? <h5 className="CardTitle">Autores</h5> : <></>}
        <CardBiography collaborators={reviewers} fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode} />
      </BooksContainer>
    </>
  )
}

export const getStaticPaths = async () => {
  const books = await fetchBooks()
  const paths: PathsVM[] = books.map((book) => {
    return { params: { book: book.slug } }
  })

  return {
    paths,
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const book: BookScopeDataType = await fetchBookCommons({ bookSlug: params['book'] })

  return {
    props: book,
    revalidate: 60 * 5,
  }
}

export default Index
Index.auth = true
