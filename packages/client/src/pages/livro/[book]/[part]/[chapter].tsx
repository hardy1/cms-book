import React, { useContext, useEffect, useState } from 'react'
import { NextPage } from 'next'
import parse, { domToReact } from 'html-react-parser'
import { Element } from 'domhandler/lib/node'

import {
  BooksContainer,
  BookChapterWrapper,
  ChapterSectionContainer,
  HighlightBookTitle,
  BookChapterTitle,
} from '../../../../containers/Books'
import BookContext from '../../../../contexts/book'
import {
  BookFullType,
  BookScopeDataType,
  ChapterType,
  ContentType,
  CurrentBookDataType,
  ICidChapter,
  PathsVM,
} from '../../../../types'
import { fetchBookCommons, fetchBooks } from '../../../../services'
import { Modal, DrugsText } from 'designsystem'
import { BooksToolbar } from '../../../../components'
import { validateTheme } from '../../../../lib/theme'
import { CustomModalContainer, ModalContainer, IModalState } from '../../../../containers/Modal'
import { internalServerURLPath } from '../../../../lib/config'
import { Drug } from '../../../../types/drug.model'

import Table from '../../../../components/Table'
import { tableParserCleaner } from '../../../../../utils'
import Loader from '../../../../components/Loader'

import { HeadWrapper } from '../../../../components'
import { NextPageWithAuth } from '../../../../types/auth.type'

interface ChapterProps {
  chapter: ChapterType
  toolbarData: BookFullType
  current: CurrentBookDataType
  auth?: boolean
}

const Chapter: NextPageWithAuth<ChapterProps> = (props) => {
  const { chapter, toolbarData, current } = props
  const modalInitialState = { isOpen: false, content: { title: '', body: '', type: '' } }
  const { state, setState: setGlobalState } = useContext(BookContext)
  const [modal, setModal] = useState<IModalState>(modalInitialState)
  const [drug, setDrug] = useState<Drug>()

  const handleModal = (e) => {
    const { target } = e

    setModal({
      ...modal,
      isOpen: !modal?.isOpen,
      content: {
        body: target?.dataset?.content,
        type: target?.dataset?.type,
      },
    })
  }

  const customHandleModal = ({ name, code, content }: ICidChapter) => {
    const parsedContent = <CustomModalContainer>{parse(content)}</CustomModalContainer>

    setModal({
      ...modal,
      isOpen: !modal?.isOpen,
      content: {
        title: `${code} - ${name}`,
        body: parsedContent,
        type: '',
      },
    })
  }

  const handleClose = () => {
    setModal(modalInitialState)
    setDrug(null)
  }

  const parseAll = (rawHTML: string) => {
    const HTMLRawList = rawHTML.split(/\s*<?\/?table\s*>?\s*/g)

    const HTMLparsedList = HTMLRawList?.map((content) => {
      if (content?.match(/(tcols)/)) {
        return <Table tableRaw={`<table ${content}</table>`} colorTheme={selectedTheme} />
      } else {
        return parse(content, {
          trim: true,
          replace: (domNode: Element) => {
            if (domNode && domNode?.name === 'a' && domNode?.attribs['data-target'] === 'modal') {
              return (
                <a
                  className={domNode?.attribs?.class}
                  onClick={handleModal}
                  id={domNode?.attribs?.id}
                  data-content={domNode?.attribs['data-content']}
                  data-type={domNode?.attribs['data-type']}
                >
                  {domToReact(domNode?.children)}
                </a>
              )
            }
          },
        })
      }
    })

    return HTMLparsedList
  }

  const getDrugText = async () => {
    const slug = modal?.content?.body.toString()

    if (!drug) {
      const response = await fetch(`${internalServerURLPath}/api/drug/${slug}`)
      let content = await response.json()
      content = content[0]
      parseDrugContents(content)
      setDrug(content)
    }
  }

  const parseDrugContents = (drugData: Drug) => ({
    ...drugData,
    tabs: drugData?.tabs?.map((tab) => tab.entries?.map((entry) => (entry.parsedContent = parse(entry.content)))),
  })

  const modalContent = (type: string) => {
    switch (type) {
      case 'image':
        return <img src={modal?.content?.body?.toString()} />

      case 'text':
      case 'iframe':
        return <iframe src={modal?.content?.body.toString()} />

      case 'drug':
        getDrugText()
        return <DrugsText name={drug?.name} alternativeNames={drug?.short_name?.split(/\W/)} sections={drug?.tabs} />

      default:
        return modal?.content?.body
    }
  }

  const selectedTheme = validateTheme(toolbarData?.theme_color)

  useEffect(() => {
    if (current?.chapterSlug !== state?.current?.chapterSlug) {
      setGlobalState({
        ...state,
        current,
      })
    }

    if (modal?.isOpen) {
      document.body.style.overflow = 'hidden'
      return () => {
        document.body.style.overflow = 'visible'
      }
    }

    if (!Object.keys(state?.book).length) {
      setGlobalState({
        ...state,
        book: { ...toolbarData },
      })
    }
  }, [modal, state, current, chapter, toolbarData, setGlobalState])

  const listAuthors = chapter?.collaborators?.map(({ id, firstName, lastName }) => (
    <h3 key={id} className="Chapter__Subtitle">
      {firstName} {lastName}
    </h3>
  ))

  const pageContent = chapter?.content?.map(({ id, title, formatted_content }: ContentType) => {
    const cleanedContent = tableParserCleaner(formatted_content)
    const parsedContent = formatted_content && parseAll(cleanedContent)

    return (
      <div key={id}>
        <BookChapterTitle fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode}>
          {parse(title)}
        </BookChapterTitle>
        <BookChapterWrapper fontSizeModifier={state?.fontModifier}>{parsedContent}</BookChapterWrapper>
      </div>
    )
  })

  return (
    <>
      <HeadWrapper title={`MOC Brasil ${chapter?.name ? `- ${chapter.name}` : ''}`} />

      <Modal title={modal?.content?.title} isOpen={modal.isOpen} onClose={() => handleClose()}>
        <ModalContainer>
          {modalContent(modal?.content?.type)}
          <Loader active={modal?.content?.type === 'drug' && !drug?.slug} />
        </ModalContainer>
      </Modal>

      <BooksToolbar book={toolbarData} current={current} />

      <HighlightBookTitle colorTheme={selectedTheme} title={chapter?.part?.book?.name} part={chapter?.part?.name} />

      <BooksContainer colorTheme={selectedTheme} fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode}>
        <h1 className="Chapter__Title">{chapter?.name}</h1>
        <p className="Chapter__Date">{chapter?.last_update}</p>

        {listAuthors}

        <ChapterSectionContainer>
          {chapter?.cids?.map((curr, index) => (
            <h4 key={index} className="Chapter__CID" onClick={() => customHandleModal(curr)}>
              {curr.code}
            </h4>
          ))}
        </ChapterSectionContainer>
        {pageContent}
      </BooksContainer>
    </>
  )
}

export const getStaticPaths = async () => {
  const books = await fetchBooks()
  const paths: PathsVM[] = []

  books?.forEach((book) => {
    book?.parts?.forEach((part) => {
      part?.chapters?.forEach((chapter) => {
        paths.push({
          params: {
            book: book?.slug,
            part: part?.slug,
            chapter: chapter?.slug,
          },
        })
      })
    })
  })

  return {
    paths,
    fallback: true,
  }
}

export const getStaticProps = async ({ params }) => {
  const book: BookScopeDataType = await fetchBookCommons({
    bookSlug: params['book'],
    partSlug: params['part'],
    chapterSlug: params['chapter'],
  })

  return {
    props: book,
    revalidate: 60,
  }
}

export default Chapter
Chapter.auth = true
