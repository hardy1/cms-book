import React, { useContext, useEffect } from 'react'
import { NextPage } from 'next'

import { BooksContainer, HighlightBookTitle } from '../../../containers/Books'
import BookContext from '../../../contexts/book'
import Link from 'next/link'
import { BookFullType, BookScopeDataType, CurrentBookDataType, PartType, PathsVM } from '../../../types'
import { fetchBookCommons, fetchBooks } from '../../../services'
import { BooksToolbar } from '../../../components'
import { validateTheme } from '../../../lib/theme'

import { HeadWrapper } from '../../../components'
import { NextPageWithAuth } from '../../../types/auth.type'

type PartProps = {
  part: PartType
  toolbarData: BookFullType
  current: CurrentBookDataType
}

const Part: NextPageWithAuth<PartProps> = (props) => {
  const { part, toolbarData, current } = props
  const { state, setState: setGlobalState } = useContext(BookContext)

  useEffect(() => {
    if (current?.partSlug !== state?.current?.partSlug) {
      setGlobalState({
        ...state,
        current,
      })
    }

    if (!Object.keys(state?.book).length) {
      setGlobalState({
        ...state,
        book: { ...toolbarData },
      })
    }
  }, [state, current, toolbarData, setGlobalState])

  const chapterList = part?.chapters?.map(({ id, name, content, slug }) => {
    let subContent

    if (content.length) {
      subContent = content.map((item) => {
        return (
          <li key={item.id} className="Part__List__Sublist__Item">
            <Link href={`/livro/${part?.book?.slug}/${part?.slug}/${slug}#${item.slug}`}>
              <a>{item.title}</a>
            </Link>
          </li>
        )
      })
    }

    return (
      <li key={id} className="Part__List__Item">
        <Link href={`/livro/${part?.book?.slug}/${part?.slug}/${slug}`}>
          <a>{name}</a>
        </Link>
        <ul className="Part__List__Sublist">{subContent}</ul>
      </li>
    )
  })

  const selectedTheme = validateTheme(toolbarData?.theme_color)

  return (
    <>
      <HeadWrapper title={`MOC Brasil ${part?.name ? `- ${part.name}` : ''}`} />
      <HighlightBookTitle colorTheme={selectedTheme} title={part?.book?.name} part={part?.name} />

      <BooksToolbar book={toolbarData} current={current} />

      <BooksContainer colorTheme={selectedTheme} fontSizeModifier={state?.fontModifier} nightMode={state?.nightMode}>
        <div className="Part">
          <h1 className="Part__Title">{part?.name}</h1>
          <p className="Part__Date">Atualizado em {part?.last_update}</p>

          <ul className="Part__List">{chapterList}</ul>
        </div>
      </BooksContainer>
    </>
  )
}

export const getStaticPaths = async () => {
  const books = await fetchBooks()
  const paths: PathsVM[] = []

  books?.forEach((book) => {
    book?.parts?.forEach((part) => {
      paths.push({
        params: {
          book: book?.slug,
          part: part?.slug,
        },
      })
    })
  })

  return {
    paths,
    fallback: true,
  }
}

export const getStaticProps = async ({ params }) => {
  const book: BookScopeDataType = await fetchBookCommons({
    bookSlug: params['book'],
    partSlug: params['part'],
  })

  return {
    props: book,
    revalidate: 60,
  }
}

export default Part
Part.auth = true
