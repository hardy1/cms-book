import React, { useState, useEffect, useContext, ReactNode } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { signOut } from 'next-auth/react'

import { NavBar } from '../../components'
import { DashboardContainer, SelectedContentContainer } from '../../containers/StylesContainer/minha-conta/_styles'

import {
  menuOptions,
  dashboardInfo,
  institutionsInfo,
  dashboardMenu,
  dashboardPreferences,
  dashboardSubscription,
} from '../../resources/dashboardSetup'

import {
  GeneralInformation,
  PersonalData,
  ProfessionalData,
  PlanSignUpData,
  ChangePassword,
  Preferences,
} from '../../containers/MyAccount'

import { HeadWrapper } from '../../components'
import { toast } from '../../components/Toast'

import { IInfoBlock, IUser, IProfession } from '../../types'
import { DashboardContext } from '../../contexts/dashboard'
import { withAuthSsr } from '../../../utils/withAuthSSR'
import { userProfileInfo } from '../../services/data/user.service'
import { getProfessions } from '../../services/data/common.service'

const MinhaConta: NextPage<{ user: IUser; children?: ReactNode }> = ({ user, children }) => {
  const { state, setState: setDashboardState } = useContext(DashboardContext)
  const [boards, setBoards] = useState<IInfoBlock>()
  const [professions, setProfessions] = useState<IProfession[]>()

  const dashboardData = (currentMenu: string) => {
    switch (currentMenu) {
      case dashboardMenu.INITIAL:
        return dashboardInfo(user)

      case dashboardMenu.PLANSIGNUP:
        return dashboardSubscription(user)

      case dashboardMenu.PREFERENCES:
        return dashboardPreferences(user)

      default:
        return dashboardInfo(user)
    }
  }

  const updateBoards = (currentMenu: string) => {
    const populatedBoards = dashboardData(currentMenu)
    setBoards(populatedBoards)

    return populatedBoards
  }

  useEffect(() => {
    updateBoards(state?.currentMenu)
    setDashboardState({ ...state, professions })
  }, [state.currentMenu, professions])

  useEffect(() => {
    const fetchProfessions = async () => {
      const response = await getProfessions(true)
      setProfessions(response)
    }

    if (user) {
      const populatedBoards = updateBoards(state?.currentMenu)
      !state.professions && fetchProfessions()

      setDashboardState({ ...state, user, boards: populatedBoards, professions })
    }
  }, [])

  const getTitle = (value: string): string => {
    const selectedOption = menuOptions.find((option) => option.value === value)
    return selectedOption.name
  }

  const selectedContent = (selectedMenu: string) => {
    switch (selectedMenu) {
      case dashboardMenu.INITIAL:
        return (
          <GeneralInformation
            title={getTitle(state?.currentMenu)}
            boards={boards}
            institutions={institutionsInfo(user)}
            user={user}
          />
        )

      case dashboardMenu.PERSONAL:
        return <PersonalData title={getTitle(state?.currentMenu)} user={user} />

      case dashboardMenu.PROFESSIONAL:
        return <ProfessionalData title={getTitle(state?.currentMenu)} userProfession={user?.profession} />

      case dashboardMenu.PLANSIGNUP:
        return <PlanSignUpData user={user} title={getTitle(state?.currentMenu)} boards={boards} />

      case dashboardMenu.CHANGE_PASSWORD:
        return <ChangePassword title={getTitle(state?.currentMenu)} />

      case dashboardMenu.PREFERENCES:
        return <Preferences title={getTitle(state?.currentMenu)} user={user} boards={boards} />

      case dashboardMenu.GOBACKTOSITE:
        window.location.replace('/')
        break

      case dashboardMenu.LOGOUT:
        toast.notify('Saindo', { type: 'warning' })
        signOut({ callbackUrl: '/' })
        break

      default:
        return (
          <GeneralInformation
            title={getTitle(state?.currentMenu)}
            boards={boards}
            institutions={institutionsInfo(user)}
            user={user}
          />
        )
    }
  }

  return (
    <>
      <HeadWrapper title="MOC Brasil - Minha Conta" />
      <DashboardContext.Consumer>
        {({ state, setState }) => (
          <DashboardContainer>
            <NavBar
              userName={state?.user?.name}
              menuOptions={menuOptions}
              onClick={(e) => {
                setState({ ...state, currentMenu: e.target.value })
              }}
            />

            <SelectedContentContainer>{selectedContent(state?.currentMenu)}</SelectedContentContainer>
          </DashboardContainer>
        )}
      </DashboardContext.Consumer>
    </>
  )
}

export default MinhaConta

export const getServerSideProps: GetServerSideProps = withAuthSsr(async () => {
  try {
    const user: IUser = await userProfileInfo()

    return {
      props: {
        user,
      },
    }
  } catch {
    return {
      props: {
        user: {},
      },
      redirect: {
        destination: '/login',
      },
    }
  }
})
