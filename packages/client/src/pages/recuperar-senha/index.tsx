import React, { useState } from 'react'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { Button, FormGroup, Heading1, Input } from 'designsystem'
import { HeadingContainer } from '../../containers/cadastro'
import { Form, Label } from '../../containers/Form'
import { forgotPassword } from '../../services/auth'
import { toast } from '../../components/Toast'
import { Box, ContentContainer } from '../../components'
import { size } from '../../resources/media'
import { useRouter } from 'next/router'

import { HeadWrapper } from '../../components'

const validationSchema = yup.object({
  email: yup.string().required('Campo obrigatório').email('E-mail inválido'),
})

const ForgotPassword: React.FC = () => {
  const router = useRouter()
  const [sendStatus, setSendStatus] = useState(false)
  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit: async (values) => {
      try {
        await forgotPassword(values)
        setSendStatus(true)
      } catch (error) {
        setSendStatus(false)
        toast.notify(`Erro: ${error.message || error}`, { type: 'danger' })
      }
    },
  })

  return (
    <>
      <HeadWrapper title="MOC Brasil - Recuperar senha" />

      <ContentContainer type="flex" direction="column" maxWidth={size.tabletM}>
        {sendStatus ? (
          <>
            <HeadingContainer>
              <Heading1>Enviamos um link de redefinição de senha para o seu email.</Heading1>
            </HeadingContainer>
            <FormGroup gridColumn="1 / -1" textAlign="center">
              <Button
                onClick={() => {
                  router.push('/')
                }}
              >
                Voltar para o site
              </Button>
            </FormGroup>
          </>
        ) : (
          <>
            <HeadingContainer>
              <Heading1>Por favor, digite seu email. Vamos enviar um link para redefinir sua senha.</Heading1>
            </HeadingContainer>

            <Box width="395px" padding="2rem" overrideButton shadow>
              <Form display="block" onSubmit={formik.handleSubmit}>
                <FormGroup gridColumn="1 / -1">
                  <Label>E-mail</Label>
                  <Input
                    type="email"
                    id="email"
                    autoComplete="email"
                    placeholder="meunome@email.com"
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    message={formik.touched.email && formik.errors.email}
                    {...formik.getFieldProps('email')}
                  />
                </FormGroup>

                <FormGroup gridColumn="1 / -1" textAlign="center">
                  <Button type="submit">Enviar</Button>
                </FormGroup>
              </Form>
            </Box>
          </>
        )}
      </ContentContainer>
    </>
  )
}

export default ForgotPassword
