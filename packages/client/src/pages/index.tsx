import React from 'react'

import fetcher from '../hooks/api/fetcher'
import { NextPage } from 'next'

import Highlight from '../containers/Highlight'
import Banner from '../containers/Banner'
import Podcasts from '../containers/Podcasts'
import Partners from '../containers/Partners'
import News from '../containers/News'
import Congresses from '../containers/Congresses'
import { serverURLPath } from '../lib/config'
import { MainContainer } from 'designsystem'

import { HeadWrapper } from '../components'
import { IHome } from '../types'

const Index: NextPage<{ data: IHome }> = ({ data }) => (
  <>
    <HeadWrapper title="MOC Brasil" />

    <MainContainer direction="column">
      {data && <Highlight featuring={data.featuring} news={data.news_featured} tip={data.tip} video={data.video} />}

      <Banner />

      {data && <Podcasts podcasts={data.podcasts} />}

      <Partners />

      {data && <News news={data.news_latest} />}
      {data && <Congresses congresses={data.congressos} />}
    </MainContainer>
  </>
)

export const getStaticProps = async () => {
  try {
    const data: IHome = await fetcher(`${serverURLPath}/api/home`)

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
      props: { data },
      revalidate: 60 * 15,
    }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Index
