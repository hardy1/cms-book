import React, { useEffect, useState } from 'react'
import parse from 'html-react-parser'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { v4 as uuidv4 } from 'uuid'
import { GetStaticProps, NextPage } from 'next'
import Link from 'next/link'

import {
  Heading2,
  Input,
  CheckBox,
  Textarea,
  Button,
  InternalHeader,
  Select,
  MainContainer,
  FormGroup,
} from 'designsystem'

import { TextContainer } from '../../containers/StylesContainer/fale-conosco/_styles'
import { Form, Label } from '../../containers/Form'

import { serverURLPath } from '../../lib/config'
import fetcher from '../../hooks/api/fetcher'
import { ContactUsData, IGenericItem } from '../../types'
import { useRouter } from 'next/router'
import { ContactUsInitalValues } from '../../resources/formInitialValues'
import { ContactUsSchema } from '../../resources/formValidationSchemas'
import { toast } from '../../components/Toast'
import MASK from '../../resources/mask'

import { HeadWrapper } from '../../components'
import { sendContact } from '../../services'

const subjectOptions: IGenericItem[] = [
  {
    id: uuidv4(),
    value: 'geral',
    name: 'Geral',
  },
  {
    id: uuidv4(),
    value: 'duvidas',
    name: 'Dúvidas',
  },
  {
    id: uuidv4(),
    value: 'sugestoes',
    name: 'Sugestões',
  },
  {
    id: uuidv4(),
    value: 'assinatura-em-grupo',
    name: 'Assinatura em grupo',
  },
  {
    id: uuidv4(),
    value: 'industria-farmaceutica',
    name: 'Indústria farmacêutica',
  },
]

const FaleConosco: NextPage<ContactUsData> = (props) => {
  const { content } = props
  const router = useRouter()
  const { query } = router
  const [subjectFromQuery, setSubjectFromQuery] = useState<boolean>(!!query?.subject)
  const [initialValues, setInitialValues] = useState<typeof ContactUsInitalValues>({
    ...ContactUsInitalValues,
    subject: (query?.subject as string) || '',
  })

  useEffect(() => {
    if (router.isReady) {
      setInitialValues({ ...ContactUsInitalValues, subject: query?.subject as string })
    }
  }, [query?.subject, router?.isReady])

  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    validationSchema: Yup.object(ContactUsSchema),
    onSubmit: async (values) => {
      try {
        await sendContact({
          ...values,
          subject: values.subject as string,
          phone: values.phone?.replaceAll(/[\s()-]/g, ''),
        })
        formik.resetForm()
        toast.notify('Mensagem enviada')
        router.push('/fale-conosco')
      } catch (error) {
        toast.notify('Falha ao enviar a mensagem', { type: 'danger' })
        throw new Error(error)
      }
    },
  })

  useEffect(() => {
    if (router.isReady) {
      if (query?.subject && subjectFromQuery && !formik?.values?.subject) {
        formik.setFieldValue('subject', query?.subject)
        const selectSubject = document.getElementById('subject') as HTMLInputElement
        if (selectSubject) selectSubject.value = query?.subject as string
      }
      setSubjectFromQuery(!!query?.subject)
    }
  }, [router?.isReady, formik, query?.subject, subjectFromQuery])

  return (
    <>
      <HeadWrapper title="MOC Brasil - Fale conosco" />

      <InternalHeader>
        <Heading2>Fale conosco</Heading2>
      </InternalHeader>

      <MainContainer direction="column">
        <TextContainer>{content && parse(content)}</TextContainer>

        <Form onSubmit={formik.handleSubmit} marginTop="20px">
          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps('name')}
              id="name"
              type="text"
              label="Nome completo"
              error={formik.touched.name && Boolean(formik.errors.name)}
              message={formik.touched.name && formik.errors.name}
              required
            />
          </FormGroup>

          <FormGroup gridColumn="1 / 7">
            <Input
              {...formik.getFieldProps('email')}
              id="email"
              type="text"
              label="Email"
              error={formik.touched.email && Boolean(formik.errors.email)}
              message={formik.touched.email && formik.errors.email}
              required
            />
          </FormGroup>

          <FormGroup gridColumn="7 / -1">
            <Input
              {...formik.getFieldProps('phone')}
              id="phone"
              type="tel"
              label="Telefone"
              error={formik.touched.phone && Boolean(formik.errors.phone)}
              mask={MASK.genericPhone}
              message={formik.touched.phone && formik.errors.phone}
              required
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Select
              {...formik.getFieldProps('subject')}
              id="subject"
              label="Assunto"
              placeholder="Selecione um assunto"
              items={subjectOptions}
              error={formik.touched.subject && Boolean(formik.errors.subject)}
              message={formik.touched.subject && formik.errors.subject}
              disabled={subjectFromQuery}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Textarea
              {...formik.getFieldProps('message')}
              id="message"
              label="Mensagem"
              rows={15}
              error={formik.touched.message && Boolean(formik.errors.message)}
              message={formik.touched.message && formik.errors.message}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <CheckBox
              {...formik.getFieldProps('termsOfUse')}
              checkImage="/img/Check.svg"
              id="termsOfUse"
              name="termsOfUse"
              checked={Boolean(formik.values.termsOfUse)}
              error={formik.touched.termsOfUse && Boolean(formik.errors.termsOfUse)}
            />

            <Label htmlFor="termsOfUse">
              Aceito receber informações via e-mail sobre o MOC Brasil e também estou de acordo com os &nbsp;
              <Link passHref href="/page/termo-de-uso-politica-de-privacidade-e-assinatura">
                <a target="_blank">termos e políticas de privacidade</a>
              </Link>
              &nbsp; de uso deste site.
            </Label>
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Button type="submit" onClick={formik.handleSubmit} disabled={!formik.isValid}>
              Enviar mensagem
            </Button>
          </FormGroup>
        </Form>
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const data: ContactUsData = await fetcher(`${serverURLPath}/api/contact-us`)

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
      props: data,
      revalidate: 60,
    }
  } catch {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default FaleConosco
