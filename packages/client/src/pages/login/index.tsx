import React, { ReactNode, useEffect, useState } from 'react'
import Link from 'next/link'
import { getProviders, signIn, getSession } from 'next-auth/react'
import { useFormik } from 'formik'
import * as yup from 'yup'

import { Box, FormGroup, Heading1, IconArrowButton } from 'designsystem'

import { HeadingContainer, SimpleContainer } from '../../containers/cadastro'
import { toast } from '../../components/Toast'
import { useRouter } from 'next/router'
import { HeadWrapper, FormLogin } from '../../components'
import { GetServerSideProps } from 'next'
import { AuthError } from '../../enums'

const validationSchema = yup.object({
  login: yup.string().required('Campo obrigatório').email('E-mail inválido'),
  password: yup.string().required('Campo obrigatório'),
})

const Login: React.FC<{ children?: ReactNode; providers }> = ({ providers }) => {
  const router = useRouter()
  const { query } = router
  const [error] = useState<string | string[]>(query?.error)

  useEffect(() => {
    if (error) {
      toast.notify(`Erro no login: ${AuthError[error as string] ? AuthError[error as string] : error}`, {
        type: 'danger',
      })
    }
  }, [error])

  const formik = useFormik({
    initialValues: {
      login: '',
      password: '',
    },
    validationSchema,
    onSubmit: async (values) => {
      signIn(providers.credentials.id, { ...values, callbackUrl: '/' })
    },
  })

  return (
    <>
      <HeadWrapper title="MOC Brasil - Login" />

      <HeadingContainer marginBottomDesktop="0" marginBottomMobile="0">
        <Heading1>Login</Heading1>
      </HeadingContainer>

      <FormGroup textAlign="center" gridColumn="1 / -1">
        <SimpleContainer>
          <p className="no-login" style={{ minHeight: 'initial' }}>
            Ainda não possui cadastro?
            <Link href="/cadastro">
              <span className="link-with-icon">
                Crie sua conta &nbsp;
                <IconArrowButton />
              </span>
            </Link>
          </p>
        </SimpleContainer>
      </FormGroup>

      <Box width="395px" gridColumn="1 / -1">
        <FormLogin formik={formik} />
      </Box>
    </>
  )
}

export default Login

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context
  const session = await getSession({ req })

  return {
    props: {
      session,
      providers: await getProviders(),
    },
  }
}
