import React, { useState, useRef, useLayoutEffect, useEffect } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { Heading2, Button, Select, CardResult, InternalHeader, MainContainer } from 'designsystem'

import { serverURLPath } from '../../lib/config'
import fetcher from '../../hooks/api/fetcher'
import {
  LeftContainer,
  TitlesContainer,
  Title,
  SubTitle,
  SelectContainer,
  ResultsContainer,
  ButtonContainer,
} from '../../containers/StylesContainer/pesquisa/_styles'

import { HeadWrapper } from '../../components'

const Pesquisa: NextPage<{ data; query; channels }> = (props) => {
  const { data, channels, query } = props
  const payload = data

  const [results, setResults] = useState(payload.data)
  const [page, setPage] = useState(1)
  const [term, setTerm] = useState(query.q)
  const [filters, setFilters] = useState([])
  const isFirstRender = useRef(true)

  const loadMore = () => {
    if (page < payload.last_page) {
      setPage(page + 1)
    }
  }

  useEffect(() => {
    const newFilters = Object.entries(channels).map(([key, value]) => ({ id: 1, name: value, value: key }))
    newFilters.forEach((item, i) => {
      item.id = i + 1
    })
    setFilters(newFilters)
  }, [channels])

  useLayoutEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false
      return
    }

    const data = fetcher(`${serverURLPath}/api/search?q=${term}&p=${page}`)
    setResults([...results, data])
  }, [page, results, term])

  useLayoutEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false
      return
    }

    setTerm(query.q)
    setResults(payload.data)
  }, [term, query, payload])

  const searchTerm = decodeURIComponent(term)

  return (
    <>
      <HeadWrapper title={`MOC Brasil - Resultado da pesquisa: "${searchTerm}"`} />

      <InternalHeader>
        <Heading2 color="primaryColor">{`Resultado da pesquisa: "${searchTerm}"`}</Heading2>
      </InternalHeader>

      <MainContainer>
        <LeftContainer>
          <TitlesContainer>
            <Title>Resultado no MOC</Title>
            <SubTitle>
              Sua pesquisa teve <strong>{payload.total} resultados</strong>
            </SubTitle>
          </TitlesContainer>
          {payload.total !== 0 && (
            <SelectContainer>
              <Select id="selectId" placeholder="Canal" items={filters} />
            </SelectContainer>
          )}
          <ResultsContainer>
            {results &&
              results.map(({ id, channel, published_at, excerpt, title, slug }) => (
                <CardResult
                  key={id}
                  category={channel}
                  date={new Date(published_at)}
                  description={excerpt}
                  title={title}
                  url={`/canais-moc/${channel}/${slug}`}
                />
              ))}
          </ResultsContainer>
          <ButtonContainer>{page < payload.last_page && <Button onClick={loadMore}>Veja mais</Button>}</ButtonContainer>
        </LeftContainer>
      </MainContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ res, query }) => {
  try {
    const data = await fetcher(`${serverURLPath}/api/search?q=${query.q}`)
    const channels = await fetcher(`${serverURLPath}/api/channels/`)

    if (!data) {
      return {
        notFound: true,
      }
    }

    return { props: { data, query, channels } }
  } catch {
    res.statusCode = 404
    return {
      props: {},
    }
  }
}

export default Pesquisa
