/* eslint-disable */
import React from 'react'
import fetcher from '../../hooks/api/fetcher'
import { GetServerSideProps, NextPage } from 'next'
import { useRouter } from 'next/router'
import parse from 'html-react-parser'
import { serverURLPath } from '../../lib/config'

import { Heading2, InternalHeader, HtmlWrapper, Button, MainContainer } from 'designsystem'
import { ButtonContainer, TextContainer } from '../../containers/StylesContainer/pesquisas/_styles'

import { HeadWrapper } from '../../components'

const SingleIntelicenciaDeMercado: NextPage<{ data }> = (props) => {
  const router = useRouter()
  const { data } = props
  const payload = data || { ...data }

  const DynamicButton = () => (
    <ButtonContainer>
      <Button onClick={() => router.push(payload.cta_url)}>{payload.cta_text}</Button>
    </ButtonContainer>
  )

  return (
    <>
      <HeadWrapper title={`MOC Brasil - ${payload.title}`} />
      <InternalHeader>
        <Heading2 color="primaryColor">{payload && payload.title}</Heading2>
      </InternalHeader>
      <MainContainer direction="column">
        <TextContainer>
          <HtmlWrapper>{payload && parse(payload.content)}</HtmlWrapper>
        </TextContainer>
        {payload && payload.cta_url ? <DynamicButton /> : ''}
      </MainContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ res, params }) => {
  try {
    const data = await fetcher(`${serverURLPath}/api/post/${params['all'][0]}`)

    if (!data) {
      return {
        notFound: true,
        props: {},
      }
    }

    return { props: { data } }
  } catch {
    res.statusCode = 404
    return {
      props: {},
    }
  }
}

export default SingleIntelicenciaDeMercado
