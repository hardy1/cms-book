import React, { useState } from 'react'
import { useFormik } from 'formik'
import Link from 'next/link'
import { useRouter } from 'next/router'
import * as Yup from 'yup'

import {
  Button,
  CheckBox,
  FormGroup,
  Heading1,
  Input,
  InternalHeader,
  MainContainer,
  Paragraph,
  Radio,
  Textarea,
} from 'designsystem'

import { sendContact } from '../../../services'

import { groupOrLegalEntityInitialValues } from '../../../resources/formInitialValues'
import { groupOrLegalEntitySchema } from '../../../resources/formValidationSchemas'
import MASK from '../../../resources/mask'

import { purchaseMethod } from '../../../enums'

import { toast } from '../../../components/Toast'
import { Form, Label } from '../../../containers/Form'

const GrupoOuPessoaJuridica: React.FC = () => {
  const [purchaseType, setPurchaseType] = useState(purchaseMethod.GROUP)

  const router = useRouter()

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: groupOrLegalEntityInitialValues,
    validationSchema: Yup.object(groupOrLegalEntitySchema),
    onSubmit: async (values) => {
      try {
        await sendContact({
          ...values,
          subject: 'Formulário: Grupo ou pessoa jurídica',
          phone: values.phone?.replaceAll(/[\s()-]/g, ''),
        })
        formik.resetForm()
        toast.notify('Mensagem enviada')
        router.push('/cadastro/grupo-ou-pessoa-juridica')
      } catch (error) {
        toast.notify('Falha ao enviar a mensagem', { type: 'danger' })
        throw new Error(error)
      }
    },
  })

  const formContent = (
    <>
      <FormGroup gridColumn="1 / -1">
        <Paragraph>
          Se o seu objetivo é adquirir <strong>Acesso em Grupo</strong>, por favor, preencha os campos abaixo.
          Entraremos em contato!
        </Paragraph>
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Input
          {...formik.getFieldProps('name')}
          id="name"
          type="text"
          label="Nome completo"
          required
          error={formik.touched.name && Boolean(formik.errors.name)}
          message={formik.touched.name && formik.errors.name}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Input
          {...formik.getFieldProps('email')}
          id="email"
          type="text"
          label="Email"
          required
          error={formik.touched.email && Boolean(formik.errors.email)}
          message={formik.touched.email && formik.errors.email}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          mask={MASK.cnpj}
          id="cnpjDocument"
          label="CNPJ"
          placeholder="Ex: 00.000.000/0000-00"
          type="text"
          required
          error={formik.touched.cnpjDocument && Boolean(formik.errors.cnpjDocument)}
          message={formik.touched.cnpjDocument && formik.errors.cnpjDocument}
          {...formik.getFieldProps('cnpjDocument')}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('phone')}
          id="phone"
          type="text"
          label="Telefone (celular)"
          placeholder="Ex: +551199999876"
          required
          error={formik.touched.phone && Boolean(formik.errors.phone)}
          mask={MASK.genericPhone}
          message={formik.touched.phone && formik.errors.phone}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('institutionName')}
          id="institutionName"
          type="text"
          label="Nome da instituição"
          required
          error={formik.touched.institutionName && Boolean(formik.errors.institutionName)}
          message={formik.touched.institutionName && formik.errors.institutionName}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('totalAccess')}
          id="totalAccess"
          type="text"
          label="Número de acessos"
          required
          error={formik.touched.totalAccess && Boolean(formik.errors.totalAccess)}
          message={formik.touched.totalAccess && formik.errors.totalAccess}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Textarea
          {...formik.getFieldProps('message')}
          id="message"
          label="Observações (opcional)"
          rows={15}
          error={formik.touched.message && Boolean(formik.errors.message)}
          message={formik.touched.message && formik.errors.message}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <CheckBox
          {...formik.getFieldProps('termsOfUse')}
          checkImage="/img/Check.svg"
          id="termsOfUse"
          name="termsOfUse"
          checked={Boolean(formik.values.termsOfUse)}
          error={formik.touched.termsOfUse && Boolean(formik.errors.termsOfUse)}
        />

        <Label htmlFor="termsOfUse">
          Aceito receber informações via e-mail sobre o MOC Brasil e também estou de acordo com os &nbsp;
          <Link passHref href="/page/termo-de-uso-politica-de-privacidade-e-assinatura">
            <a target="_blank">termos e políticas de privacidade</a>
          </Link>
          &nbsp; de uso deste site.
        </Label>
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Button type="submit" onClick={formik.handleSubmit} disabled={!formik.isValid}>
          Enviar mensagem
        </Button>
      </FormGroup>
    </>
  )

  const redirectContent = (
    <>
      <FormGroup gridColumn="1 / -1">
        <Paragraph>
          Se você precisa de um <strong>Acesso Individual</strong>, basta cadastrar-se com seus dados pessoais e
          informar os dados de Pessoa Jurídica na <strong>etapa de pagamento</strong>. Assim, embora o cadastro fique em
          seu nome, toda a transação de faturamento ficará com os dados da sua instituição. Nesse caso, clique no link
          abaixo, selecione um perfil e preencha o seu cadastro
        </Paragraph>
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Link href="/cadastro" passHref>
          <Button>Continuar meu cadastro</Button>
        </Link>
      </FormGroup>
    </>
  )

  const contentPage = purchaseType === purchaseMethod.GROUP ? formContent : redirectContent

  return (
    <>
      <InternalHeader>
        <Heading1>Grupo ou pessoa jurídica</Heading1>
      </InternalHeader>

      <MainContainer direction="column">
        <Form onSubmit={formik.handleSubmit} marginTop="48px">
          <FormGroup gridColumn="1 / -1" marginBottom="24px">
            <Radio
              name="person-type"
              label="Compra em grupo"
              id="person"
              checked={purchaseType === purchaseMethod.GROUP}
              onChange={() => setPurchaseType(purchaseMethod.GROUP)}
            />

            <Radio
              name="person-type"
              label="Compra individual"
              id="company"
              checked={purchaseType === purchaseMethod.SINGLE}
              onChange={() => setPurchaseType(purchaseMethod.SINGLE)}
            />
          </FormGroup>

          {contentPage}
        </Form>
      </MainContainer>
    </>
  )
}

export default GrupoOuPessoaJuridica
