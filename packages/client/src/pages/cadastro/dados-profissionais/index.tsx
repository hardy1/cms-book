import React, { useState, useEffect } from 'react'
import { GetServerSideProps } from 'next'
import { useRouter, NextRouter } from 'next/router'
import { getSession } from 'next-auth/react'
import { DefaultSession } from 'next-auth'
import styled from 'styled-components'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { Box, Button, FormGroup, GridContainer, Heading1 } from 'designsystem'

import { UserProfessionalSchema } from '../../../resources/formValidationSchemas'
import { UserProfessionalInitialValues } from '../../../resources/formInitialValues'
import { handleSpecializationId, handleProfessionRegister } from '../../../resources'
import { withAuthSsr } from '../../../../utils/withAuthSSR'
import { getProfessions } from '../../../services/data/common.service'
import { userProfessionalDataUpdate } from '../../../services/data/user.service'
import { IProfession, IUserProfessionalData } from '../../../types'

import { HeadingContainer } from '../../../containers/cadastro'
import { Form, ProfessionForm } from '../../../containers/Form'
import { HeadWrapper } from '../../../components'
import { toast } from '../../../components/Toast'
import Loader from '../../../components/Loader'

const validationSchema = Yup.object({
  ...UserProfessionalSchema,
})

const StyledBox = styled(Box)`
  padding: 30px 29px 10px !important;
`
const StyledButton = styled(Button)`
  margin-top: 2px !important;
`

const LoaderContainer = styled.div`
  margin-top: 39px;
`

const DadosProfissionais: React.FC<{ professions: IProfession[] }> = ({ professions }) => {
  const router: NextRouter = useRouter()
  const [error, setError] = useState<string>()

  useEffect(() => {
    error && toast.notify(error, { type: 'danger' })
  }, [error])

  const formikProps = {
    initialValues: {
      ...UserProfessionalInitialValues,
    },
    validationSchema,
    onSubmit: async (values: IUserProfessionalData) => {
      const specializationId = await handleSpecializationId({
        profession: Number(values.profession),
        specialization: values.specialization,
        professions,
      })
      const professionRegister = await handleProfessionRegister(values.professionRegister)

      try {
        const response = await userProfessionalDataUpdate({
          ...values,
          specialization: specializationId,
          professionRegister: professionRegister,
        })

        if (response?.status) {
          router.push('/cadastro/dados-de-pagamento')
          toast.notify(response?.message)
        } else {
          throw new Error(response?.message)
        }
      } catch (e) {
        setError(e.message)
      }
    },
  }

  return (
    <>
      <HeadWrapper title="MOC Brasil - Cadastro - Dados profissionais" />

      <Formik {...formikProps}>
        {(formik) => (
          <GridContainer>
            <HeadingContainer>
              <Heading1>Dados Profissionais</Heading1>
            </HeadingContainer>

            <StyledBox className="box-form" width="765px" gridColumn="1 / -1">
              <Form onReset={formik.handleReset} onSubmit={formik.handleSubmit}>
                <ProfessionForm formik={formik} professions={professions} />
                <FormGroup textAlign="center" gridColumn="1 / -1">
                  {!formik.isSubmitting && (
                    <StyledButton type="submit" disabled={!formik.isValid}>
                      Continuar
                    </StyledButton>
                  )}

                  {formik.isSubmitting && (
                    <LoaderContainer>
                      <Loader active size="small" />
                    </LoaderContainer>
                  )}
                </FormGroup>
              </Form>
            </StyledBox>
          </GridContainer>
        )}
      </Formik>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = withAuthSsr(async ({ req }) => {
  try {
    const currentSession = await getSession({ req })
    const professions = await getProfessions()

    return {
      props: {
        professions,
        session: currentSession,
      },
    }
  } catch (e) {
    return {
      props: {
        professions: {},
      },
      redirect: {
        permanent: false,
        destination: '/login',
      },
    }
  }
}, '/cadastro/dados-iniciais')

export default DadosProfissionais
