import React from 'react'
import { v4 as uuidv4 } from 'uuid'

import { Button } from 'designsystem'

import { Table as TableComponent } from '../../../containers/cadastro'
import { IconCheck, IconDanger } from '../../../containers/Icons'
import { planCode } from '../../../enums'
import { IPlan } from '../../../types'

const TableList = [
  {
    id: uuidv4(),
    description: 'Acesso a notícias sobre as principais evidências publicadas na área da oncologia',
    info: null,
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso a cursos patrocinados',
    info: null,
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso a Vídeo-MOC',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso a MOC-Dica',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso a MOC-Podcast',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Newsletter Semanal',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso ao app',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'Acesso ao MOC IA (em breve)',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: false,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: false,
    },
  },
  {
    id: uuidv4(),
    description: 'Consulta à calculadora: Superfície corporal',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: true,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description:
      'Consulta às calculadoras: Cinética do PSA; Dose de carboplatina; Declínio do marcador após BEP, Dessensibilização de droga',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: false,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: false,
    },
  },
  {
    id: uuidv4(),
    description: 'MOC-Enfermagem; MOC-Drogas; MOC-Manejo de Toxicidades; MOC-Cuidados de Suporte',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: false,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: true,
    },
  },
  {
    id: uuidv4(),
    description: 'MOC-Diagnóstico Diferencial, MOC-Tumores Sólidos, MOC-Tumores Raros, MOC-Hemato',
    info: 'Informacão adicional',
    options: {
      [planCode.LIMITED]: false,
      [planCode.MAX]: true,
      [planCode.PREMIUM]: false,
    },
  },
]

type TableProps = {
  activeColumn?: string
  plans: IPlan[]
  onClick?: (offerId?: number) => void
}

const Table: React.FC<TableProps> = ({ activeColumn, plans, onClick }) => {
  const tableList = TableList.map(({ description, id, options }) => {
    const iconCheck = <IconCheck width={14} height={14} fill="#2ecc71" />
    const iconDanger = <IconDanger />

    const verifyOption = (param: string) => (options[param] ? iconCheck : iconDanger)

    const setActiveColumn = (columnName: string) => (columnName === activeColumn ? 'options show' : 'options')

    return (
      <tr key={id}>
        <td className="description">
          <p>{description}</p>
        </td>

        {plans?.map((plan: IPlan, jIndex: number) => (
          <td key={jIndex} className={setActiveColumn(plan?.product?.code)}>
            {verifyOption(plan?.product?.code)}
          </td>
        ))}
      </tr>
    )
  })

  return (
    <TableComponent>
      <tbody>
        {tableList}

        <tr className="row-action">
          <td className="description--empty"></td>
          {plans?.map((plan: IPlan, index: number) => (
            <td key={index}>
              <Button onClick={() => onClick(plan?.offerId)} color={plan?.color}>
                Comece agora
              </Button>
            </td>
          ))}
        </tr>
      </tbody>
    </TableComponent>
  )
}

export default Table
