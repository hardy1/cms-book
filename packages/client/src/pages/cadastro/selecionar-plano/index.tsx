import React, { useState, useEffect, useCallback } from 'react'
import { GetStaticProps, NextPage } from 'next'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import Slider from 'react-slick'
import {
  Button,
  GridContainer,
  Heading1,
  MainContainer,
  IconGearPercentage,
  Input,
  Paragraph,
  Heading3,
  IconClose,
} from 'designsystem'
import Link from 'next/link'
import { useRouter } from 'next/router'

import {
  HeadWrapper,
  PlanBox,
  FixedPlanHeaderContainer,
  FixedPlanHeaderBox,
  ContentContainer,
} from '../../../components'

import { HeadingContainer, TableContainer, TogglePlans, TogglePlayAndBoxContainer } from '../../../containers/cadastro'
import { setCookiesKey, clearCookiesByKey, getCookiesKey } from '../../../services/browser-cookies'
import { cookies, planCode } from '../../../enums'
import { IPlan, IPlanComparisonMap, PlansPageType, SubscriptionPeriod } from '../../../types'
import fetcher from '../../../hooks/api/fetcher'
import { serverURLPath } from '../../../lib/config'
import { formatCurrency } from '../../../../utils/currecyParser'
import colors from '../../../resources/colors'

import Table from './Table'
import styled from 'styled-components'
import ReactModal from 'react-modal'
import { postOfferCoupon } from '../../../services'
import { handleDecimal } from '../../../../utils/handleDecimal'

const ModalContainer = styled.div`
  color: ${colors.neutralColor};
  font-family: 'Montserrat';
  font-size: 1em;
`

const CloseButton = styled.span`
  cursor: pointer;
  position: absolute;
  right: 31px;
  top: 23px;
`

const StyledCouponPlan = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;

  .Coupon__Discount {
    width: 15rem;
    height: 2rem;
    padding: 0.2rem 1rem;
    background: ${colors.primaryColor};
    border: 2px solid ${colors.primaryColor};
    color: white;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 0.7rem;
    text-align: center;
    border-radius: 10px;
    margin-bottom: 0.2rem;
  }

  .Coupon__Details {
    display: flex;
    padding: 1rem;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    border: 2px solid ${colors.primaryColor};
    border-radius: 10px;
    width: 15rem;
    height: 15rem;
  }

  .Coupon__Oldvalue {
    text-decoration: line-through;
    color: ${colors.mediumGray};
    margin-bottom: 0;
    margin-top: 2rem;
  }
`

const StyledCouponPlanModal = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  svg {
    width: 3rem;
    height: 3rem;
  }

  .Coupon__Title {
    display: flex;
    align-items: center;
    width: 70%;
    gap: 1rem;
  }
`

export const ModalOverlayStyles = {
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  backgroundColor: 'rgb(255 255 255 / 90%)',
  zIndex: 2,
}

export const ModalContentStyles = {
  backgroundColor: '#fff',
  boxShadow: '2px 2px 4px rgba(0, 0, 0, 0.25)',
  position: 'absolute',
  top: '40px',
  left: '40px',
  right: '40px',
  bottom: '40px',
  overflow: 'auto',
  WebkitOverflowScrolling: 'touch',
  border: 'none',
  borderRadius: '10px',
  outline: 'none',
  padding: '46px 26px',
  width: 'max-content',
  height: 'max-content',
  maxWidth: '80%',
  maxHeight: '70%',
  margin: 'auto',
}

const SelecionarPlano: NextPage<PlansPageType> = ({ planPeriods }) => {
  const router = useRouter()
  const [selectedPeriod, setSelectedPeriod] = useState<SubscriptionPeriod>(planPeriods[0])
  const [activeColumn, setActiveColumn] = useState<string>(planCode.MAX)
  const [displayScrollableContainer, setDisplayScrollableContainer] = useState<boolean>(false)
  const [modal, setModal] = useState<boolean>(false)
  const [coupon, setCoupon] = useState(null)
  const [offer, setOffer] = useState(null)
  const [priceData, setPriceData] = useState(null)

  const [selectedProfile, setSelectedProfile] = useState<string>('MED')

  const formik = useFormik({
    isInitialValid: false,
    initialValues: { coupon: '' },
    validationSchema: Yup.object({
      coupon: Yup.string().required('Campo obrigatório'),
    }),
    onSubmit: async (values) => {
      console.info('Validando o cupom', values)
    },
  })

  useEffect(() => {
    const setAvailablePeriods = () => {
      planPeriods.length && setSelectedPeriod(planPeriods[0])
    }

    setAvailablePeriods()
  }, [planPeriods])

  useEffect(() => {
    setSelectedProfile(getCookiesKey(cookies.SELECTED_USER_PROFILE))
    clearCookiesByKey(cookies.COUPON_VERIFICATION)
  }, [])

  const handleClick = (offerId: number, period?: string) => {
    setCookiesKey(cookies.SELECTED_OFFER, offerId)
    if (offerId === 7 && period) {
      setCookiesKey(cookies.SELECTED_PERIOD, period)
    } else {
      clearCookiesByKey(cookies.SELECTED_PERIOD)
    }
    router.push('/cadastro/dados-de-pagamento')
  }

  const planBoxes = selectedPeriod.plans?.map(
    ({ offerId, product, period, priceValue, monthlyPriceValue, comparison, color }) => {
      return (
        <PlanBox
          key={offerId}
          plan={product}
          name={product.name}
          subtitle={product?.description}
          price={priceValue.toString()}
          monthlyPrice={monthlyPriceValue.toString()}
          recommended={selectedProfile !== 'MED' ? false : true}
          periodicity={period.period}
          moreInfo={product?.code !== planCode.LIMITED}
          details={product?.code === planCode.LIMITED ? 'Acesso a todas as notícias do site' : null}
          comparison={comparison}
          color={color}
          onClick={() => handleClick(offerId, period.name)}
        />
      )
    },
  )

  const activeButtonClass = 'TogglePlansList__Item__Button--Active'

  const setToggleCondition = (time: string): string => (selectedPeriod?.period === time ? activeButtonClass : '')

  const showColumnMobile = (index: number) => {
    if (selectedPeriod.plans.length) {
      setActiveColumn(selectedPeriod.plans[index]?.product?.code)
    } else {
      setActiveColumn(planCode.MAX)
    }
  }

  const sliderSettings = {
    afterChange: (index: number) => showColumnMobile(index),
    className: 'Slider',
    arrows: false,
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          dots: true,
          swipe: true,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          dots: false,
          swipe: false,
        },
      },
    ],
  }

  const updatePlansByPeriod = (period: SubscriptionPeriod) => {
    setSelectedPeriod(period)
  }

  const PlanPeriodsList = planPeriods?.map((period) => {
    const hasDiscount = period.name.toLocaleLowerCase() === 'anual' && <small>Desconto de 20%</small>

    return (
      <li key={period.id} className="TogglePlansList__Item">
        <button
          className={`TogglePlansList__Item__Button ${setToggleCondition(period.period)}`}
          onClick={() => updatePlansByPeriod(period)}
        >
          {period.name}
          {hasDiscount}
        </button>
      </li>
    )
  })

  const handleScrollableContainer = useCallback(() => {
    const y = window.scrollY
    y >= 850 && y <= 1750 ? setDisplayScrollableContainer(true) : setDisplayScrollableContainer(false)
  }, [])

  useEffect(() => {
    window.addEventListener('scroll', handleScrollableContainer)

    return () => {
      window.removeEventListener('scroll', handleScrollableContainer)
    }
  }, [handleScrollableContainer])

  const handleCouponButton = () => {
    setModal(true)
  }

  const handleSubmit = async () => {
    try {
      const planData = await postOfferCoupon({ promoCode: formik.values.coupon }, true)
      const selectedCoupon = planData.coupons?.find((entry) => entry.code === formik.values.coupon)
      setCookiesKey(cookies.COUPON_VERIFICATION, selectedCoupon)
      setCookiesKey(cookies.SELECTED_OFFER, planData?.offer?.id)
      setCookiesKey(cookies.SELECTED_PERIOD, planData?.offer?.subscription_period)
      selectedCoupon && setCoupon(selectedCoupon)
      planData && setOffer(planData.offer)
      planData && setPriceData(planData.price_data)
    } catch {
      formik.setFieldError('coupon', 'Cupom inválido')
    }
  }

  return (
    <>
      <ReactModal
        isOpen={modal}
        style={{
          overlay: ModalOverlayStyles as React.CSSProperties,
          content: ModalContentStyles as React.CSSProperties,
        }}
        onRequestClose={() => setModal(false)}
        appElement={undefined}
      >
        <ModalContainer>
          {coupon ? (
            <>
              <ContentContainer maxWidth="750px" gap="1rem">
                <StyledCouponPlanModal>
                  <div className="Coupon__Title">
                    <span>
                      <IconGearPercentage fill={colors.primaryColor} />
                    </span>
                    <Heading1>Comprar com Cupom</Heading1>
                  </div>
                  <Paragraph>
                    Para continuar a compra da assinatura do plano abaixo clique em avançar, faça seu login ou
                    cadastre-se. O desconto do cupom será aplicado na tela de dados de pagamento.
                  </Paragraph>
                  <StyledCouponPlan>
                    <div className="Coupon__Discount">
                      <p>{`plano com ${coupon?.coupon_group?.mechanism?.discount_value}% de desconto`}</p>
                    </div>
                    <div className="Coupon__Details">
                      <Heading3>{offer?.product?.name}</Heading3>
                      <Paragraph>{offer?.name}</Paragraph>
                      <h3 className="Coupon__Oldvalue">{`R$${handleDecimal(priceData?.old_price || 0)}`}</h3>
                      <Heading1>{`R$${handleDecimal(priceData?.price || 0)}`}</Heading1>
                      <Button color={colors.primaryColor} onClick={() => router.push('/cadastro/dados-iniciais')}>
                        Avançar
                      </Button>
                    </div>
                  </StyledCouponPlan>
                </StyledCouponPlanModal>
              </ContentContainer>
            </>
          ) : (
            <>
              <ContentContainer maxWidth="450px" gap="1rem">
                <Heading1>Cupom de Desconto</Heading1>
                <Input
                  {...formik.getFieldProps('coupon')}
                  id="coupon"
                  type="text"
                  autoComplete="off"
                  error={formik.touched.coupon && Boolean(formik.errors.coupon)}
                  message={formik.touched.coupon && formik.errors.coupon}
                />
                <Paragraph>*Respeite o uso de letras maiúsculas e minúsculas.</Paragraph>
                <Paragraph>*O desconto do cupom promocional não são cumulativos.</Paragraph>
                <Paragraph>
                  *O cupom só pode ser aplicado na aquisição de uma nova assinatura de plano, não é válido para planos
                  vigentes.
                </Paragraph>
              </ContentContainer>
              <ContentContainer gridColumns={2} gap="1rem" margins="1.5rem 0 0 0">
                <Button color={colors.disabledColor} type="button" onClick={() => setModal(false)}>
                  Cancelar
                </Button>
                {!formik.isSubmitting && (
                  <Button type="submit" onClick={() => handleSubmit()} disabled={!formik.isValid}>
                    Aplicar cupom
                  </Button>
                )}
              </ContentContainer>
            </>
          )}
        </ModalContainer>
        <CloseButton onClick={() => setModal(false)}>
          <IconClose />
        </CloseButton>
      </ReactModal>
      <HeadWrapper title="MOC Brasil - Cadastro - Selecionar Plano" />

      <MainContainer direction="column">
        <GridContainer>
          <HeadingContainer>
            <Heading1>Escolha um dos planos abaixo ou experimente gratuitamente.</Heading1>
          </HeadingContainer>
        </GridContainer>

        <TogglePlayAndBoxContainer sliderPlans={selectedPeriod.plans.length}>
          <div className="toggle-plan-container">
            <div className="TogglePlan">
              <p className="HighlightAnnualPlan">
                <img src="/img/Label.svg" alt="Label" width={24} height={24} />
                Estudantes e Residentes: 50% de desconto automático em todos os planos.
              </p>

              <TogglePlans>
                <h5 className="TogglePlansTitle">Selecione o período do seu plano abaixo:</h5>
                <ul className="TogglePlansList">{PlanPeriodsList}</ul>

                <Link passHref href="#">
                  <Button
                    className="TogglePlans__BuyWithCupom"
                    icon={<IconGearPercentage />}
                    onClick={() => handleCouponButton()}
                  >
                    Comprar com cupom
                  </Button>
                </Link>
              </TogglePlans>
            </div>
          </div>

          <div className="boxes">
            <Slider {...sliderSettings}>{planBoxes}</Slider>
          </div>
        </TogglePlayAndBoxContainer>

        {displayScrollableContainer && (
          <FixedPlanHeaderContainer>
            <MainContainer>
              {selectedPeriod.plans &&
                selectedPeriod.plans.map((plan) => (
                  <FixedPlanHeaderBox key={plan.code}>
                    <p className="plan-name">{plan.product.name}</p>
                    <p className="plan-price">
                      {plan.priceValue === '0' ? ' Gratuito' : `R$${plan.priceValue}/${plan.period.name.toLowerCase()}`}
                    </p>

                    <Button onClick={() => handleClick(plan.offerId, plan.period.name)}>Comece agora</Button>
                  </FixedPlanHeaderBox>
                ))}
            </MainContainer>
          </FixedPlanHeaderContainer>
        )}

        <TableContainer>
          <Table plans={selectedPeriod.plans} activeColumn={activeColumn} onClick={handleClick} />
        </TableContainer>
      </MainContainer>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const periodsResponse = await fetcher(`${serverURLPath}/api/checkout/periods`)
    const colorsRemap = ['#66B8FF', '#144590', '#216DE1']

    const planPeriods: SubscriptionPeriod[] = await Promise.all(
      periodsResponse.map(async (payloadPeriod) => {
        // periods handler
        const currentPeriod: SubscriptionPeriod = {
          id: payloadPeriod.id,
          name: payloadPeriod.name,
          period: payloadPeriod.period,
          plans: [],
        }

        // plans of period handler
        const plansResponse = await fetcher(`${serverURLPath}/api/checkout/periods/${currentPeriod.id}`)
        currentPeriod.plans = plansResponse.map((payloadPlan, index) => {
          const currentPlan: IPlan = {
            name: payloadPlan?.name,
            slug: payloadPlan?.slug,
            code: payloadPlan?.code,
            months: payloadPlan?.months,
            priceValue: formatCurrency(payloadPlan?.price_value),
            monthlyPriceValue: formatCurrency(payloadPlan?.monthly_price_value),
            offerId: payloadPlan.offer_id,
            product: payloadPlan?.product,
            period: payloadPlan?.period,
            color: payloadPlan?.color || colorsRemap[index] || colors.primaryColor,
            comparison: payloadPlan?.comparison?.map((item: IPlanComparisonMap) => ({
              name: item.name,
              periodId: item.period_id,
              months: item.months,
              monthlyPriceValue: formatCurrency(item.monthly_price_value),
              monthlyDiffPercent: item.monthly_diff_percent,
              priceValue: formatCurrency(item.price_value),
            })),
          }
          return currentPlan
        })

        return currentPeriod
      }),
    )

    return {
      props: {
        planPeriods,
      },
      revalidate: 120,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default SelecionarPlano
