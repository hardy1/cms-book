import React from 'react'
import Link from 'next/link'
import { useFormik } from 'formik'
import { useRouter } from 'next/router'
import * as Yup from 'yup'

import { pharmaceuticalIndustryInitialValues } from '../../../resources/formInitialValues'
import { pharmaceuticalIndustrySchema } from '../../../resources/formValidationSchemas'
import MASK from '../../../resources/mask'

import {
  Button,
  CheckBox,
  FormGroup,
  Heading1,
  Input,
  InternalHeader,
  MainContainer,
  Paragraph,
  Textarea,
} from 'designsystem'

import { sendContact } from '../../../services'

import { Form, Label } from '../../../containers/Form'
import { toast } from '../../../components/Toast'

const IndustriaFarmaceutica: React.FC = () => {
  const router = useRouter()

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: pharmaceuticalIndustryInitialValues,
    validationSchema: Yup.object(pharmaceuticalIndustrySchema),
    onSubmit: async (values) => {
      try {
        await sendContact({
          ...values,
          subject: 'Formulário: Indústria farmacêutica',
          phone: values.phone?.replaceAll(/[\s()-]/g, ''),
          cpfDocument: values.cpfDocument?.replace(/\D/g, ''),
        })
        formik.resetForm()
        toast.notify('Mensagem enviada')
        router.push('/cadastro/industria-farmaceutica')
      } catch (error) {
        toast.notify('Falha ao enviar a mensagem', { type: 'danger' })
        throw new Error(error)
      }
    },
  })

  return (
    <>
      <InternalHeader>
        <Heading1>Indústria farmacêutica</Heading1>
      </InternalHeader>

      <MainContainer direction="column">
        <Form onSubmit={formik.handleSubmit} marginTop="48px">
          <FormGroup gridColumn="1 / -1">
            <Paragraph>
              Agradecemos a confiança no conteúdo do MOC! Por favor, preencha os campos abaixo. Entraremos em contato!
            </Paragraph>
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps('name')}
              id="name"
              type="text"
              label="Nome completo"
              autoComplete="name"
              required
              error={formik.touched.name && Boolean(formik.errors.name)}
              message={formik.touched.name && formik.errors.name}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Input
              {...formik.getFieldProps('email')}
              id="email"
              type="text"
              label="Email"
              required
              error={formik.touched.email && Boolean(formik.errors.email)}
              message={formik.touched.email && formik.errors.email}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / 7">
            <Input
              mask={MASK.cpf}
              id="cpfDocument"
              label="CPF"
              placeholder="Ex: 111.222.333-44"
              type="text"
              required
              error={formik.touched.cpfDocument && Boolean(formik.errors.cpfDocument)}
              message={formik.touched.cpfDocument && formik.errors.cpfDocument}
              {...formik.getFieldProps('cpfDocument')}
            />
          </FormGroup>

          <FormGroup gridColumn="7 / -1">
            <Input
              {...formik.getFieldProps('phone')}
              id="phone"
              type="text"
              label="Telefone (Celular)"
              placeholder="Ex: +551199999876"
              required
              error={formik.touched.phone && Boolean(formik.errors.phone)}
              mask={MASK.genericPhone}
              message={formik.touched.phone && formik.errors.phone}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / 7">
            <Input
              {...formik.getFieldProps('institutionName')}
              id="institutionName"
              type="text"
              label="Nome da instituição"
              required
              error={formik.touched.institutionName && Boolean(formik.errors.institutionName)}
              message={formik.touched.phone && formik.errors.phone}
            />
          </FormGroup>

          <FormGroup gridColumn="7 / -1">
            <Input
              {...formik.getFieldProps('totalAccess')}
              id="totalAccess"
              type="text"
              label="Número total de acessos"
              required
              error={formik.touched.totalAccess && Boolean(formik.errors.totalAccess)}
              message={formik.touched.totalAccess && formik.errors.totalAccess}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Textarea
              {...formik.getFieldProps('message')}
              id="message"
              label="Observações (opcional)"
              rows={15}
              error={formik.touched.message && Boolean(formik.errors.message)}
              message={formik.touched.message && formik.errors.message}
            />
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <CheckBox
              {...formik.getFieldProps('termsOfUse')}
              checkImage="/img/Check.svg"
              id="termsOfUse"
              name="termsOfUse"
              checked={Boolean(formik.values.termsOfUse)}
              error={formik.touched.termsOfUse && Boolean(formik.errors.termsOfUse)}
            />

            <Label htmlFor="termsOfUse">
              Aceito receber informações via e-mail sobre o MOC Brasil e também estou de acordo com os &nbsp;
              <Link passHref href="/page/termo-de-uso-politica-de-privacidade-e-assinatura">
                <a target="_blank">termos e políticas de privacidade</a>
              </Link>
              &nbsp; de uso deste site.
            </Label>
          </FormGroup>

          <FormGroup gridColumn="1 / -1">
            <Button type="submit" onClick={formik.handleSubmit} disabled={!formik.isValid}>
              Enviar mensagem
            </Button>
          </FormGroup>
        </Form>
      </MainContainer>
    </>
  )
}

export default IndustriaFarmaceutica
