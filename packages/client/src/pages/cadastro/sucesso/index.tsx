import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { Button, GridContainer, Heading1 } from 'designsystem'

import { CardImage, HeadingContainer } from '../../../containers/cadastro'
import { IconGmail, IconInvoice, IconOutlook, IconYahoo } from '../../../containers/Icons'
import { GetServerSideProps } from 'next'
import { withAuthSsr } from '../../../../utils/withAuthSSR'

import { IUser } from '../../../types'
import { userProfileInfo } from '../../../services/data/user.service'

const Sucesso: React.FC<{ boleto_url: string | null }> = ({ boleto_url }) => {
  const [customMessage, setCustomMessage] = useState<string>('Seu cadastro foi concluído com sucesso')

  const router = useRouter()

  const defaultMessage = (
    <div className="Card__Message">
      <p>Em breve você receberá um email de confirmação da criação da sua conta e do seu pagamento.</p>
      <p>Por favor confira se o email não caiu em sua caixa de Spam.</p>
    </div>
  )

  const setContent = () => {
    const { type } = router.query

    switch (type) {
      case 'card':
        return defaultMessage

      case 'invoice':
        return (
          <>
            {defaultMessage}
            {boleto_url && (
              <Link href={boleto_url} passHref>
                <Button as="a" target="_blank" className="Card__ButtonPrintInvoice">
                  <IconInvoice />
                  Imprimir seu boleto
                </Button>
              </Link>
            )}
          </>
        )

      default:
        return (
          <div className="Card__Message">
            <p>
              Em breve você receberá um email de confirmação da criação da sua conta com todos os detalhes do que você
              pode acessar em nossa plataforma.
            </p>
            <p>Por favor confira se o email não caiu em sua caixa de Spam.</p>
          </div>
        )
    }
  }

  useEffect(() => {
    if (router.query.type === 'card') {
      setCustomMessage('Sua compra foi concluída com sucesso')
    } else if (router.query.type === 'invoice') {
      setCustomMessage('Seu boleto foi gerado com sucesso')
    }
  }, [router.query.type])

  return (
    <GridContainer>
      <HeadingContainer>
        <Heading1>Sucesso</Heading1>
      </HeadingContainer>

      <CardImage imagePosition="right" imageURL="/img/doctor.jpeg" gridColumn="1 / -1">
        <div className="Card__Container">
          <h2 className="Card__Title">
            Parabéns!
            <span>{customMessage}</span>
          </h2>

          {setContent()}

          <div className="Card__Links">
            <p>
              <Link href="/minha-conta" passHref>
                <a>Ir para minha conta</a>
              </Link>
            </p>

            <p>
              <Link href="/" passHref>
                <a>Voltar para o site</a>
              </Link>
            </p>
          </div>

          <div className="Card__Actions">
            <p>Ou vá direto para seu email:</p>

            <Link href="https://gmail.com" passHref>
              <Button as="a" target="_blank" icon={<IconGmail />}>
                <span className="sr-only">Gmail</span>
              </Button>
            </Link>

            <Link href="https://login.yahoo.com/" passHref>
              <Button as="a" target="_blank" icon={<IconYahoo />}>
                <span className="sr-only">Yahoo</span>
              </Button>
            </Link>

            <Link href="https://login.live.com/" passHref>
              <Button as="a" target="_blank" icon={<IconOutlook />}>
                <span className="sr-only">Outlook</span>
              </Button>
            </Link>
          </div>
        </div>
      </CardImage>
    </GridContainer>
  )
}

export const getServerSideProps: GetServerSideProps = withAuthSsr(async () => {
  const user: IUser = await userProfileInfo()

  const boletoUrl = user.subscription?.gateway.currentTransaction.boletoUrl

  return {
    props: {
      boleto_url: boletoUrl === undefined ? null : boletoUrl,
    },
  }
})

export default Sucesso
