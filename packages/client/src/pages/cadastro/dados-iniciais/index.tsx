import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { Box, Button, FormGroup, GridContainer, Heading1, IconArrowButton } from 'designsystem'

import { HeadingContainer, SimpleContainer } from '../../../containers/cadastro'
import { Form } from '../../../containers/Form'
import UserRegistrationForm from '../../../containers/User/UserRegistrationForm'
import {
  UserRegistrationSchema,
  UserOptionsSchema,
  PasswordSchema,
  addressSchema as AddressSchema,
} from '../../../resources/formValidationSchemas'
import {
  UserOptionsInitialValues,
  UserRegistrationInitialValues,
  PasswordInitialValues,
  addressSchema as AddressInitialValues,
} from '../../../resources/formInitialValues'
import UserOptionsForm from '../../../containers/User/UserOptionsForm'
import { userRegistration } from '../../../services/data/user.service'
import { toast } from '../../../components/Toast'
import { HeadWrapper } from '../../../components'
import Loader from '../../../components/Loader'

import { GoogleRecaptchaV3LoadScript, GoogleRecaptchaV3 } from '../../../hooks/google-recaptcha-v3'
import { getProviders, signIn, LiteralUnion, ClientSafeProvider } from 'next-auth/react'
import { GetServerSideProps } from 'next'
import { BuiltInProviderType } from 'next-auth/providers'

const LoaderContainer = styled.div`
  margin-top: 39px;
`

const initialValues = {
  ...UserRegistrationInitialValues,
  ...PasswordInitialValues,
  ...UserOptionsInitialValues,
  state: AddressInitialValues.state,
  city: AddressInitialValues.city,
}

const validationSchema = Yup.object({
  ...UserRegistrationSchema,
  ...PasswordSchema,
  ...UserOptionsSchema,
  state: AddressSchema.state,
  city: AddressSchema.city,
})

const DadosIniciais: React.FC<{
  providers: Record<LiteralUnion<BuiltInProviderType, string>, ClientSafeProvider> | null
}> = ({ providers }) => {
  const [error, setError] = useState<string>()

  useEffect(() => {
    error && toast.notify(error, { type: 'danger' })
    setError('')
  }, [error])

  useEffect(() => {
    GoogleRecaptchaV3LoadScript()
  }, [])

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      const recaptchaResult = await GoogleRecaptchaV3()
      if (recaptchaResult) {
        try {
          const response = await userRegistration({
            ...values,
            cpfDocument: values.cpfDocument?.replace(/\D/g, ''),
          })

          if (response?.status) {
            signIn(providers.credentials.id, {
              login: values.email,
              password: values.password,
              callbackUrl: `/cadastro/dados-profissionais`,
            })
          } else {
            setError(response?.message)
            throw new Error(response?.message)
          }
        } catch (e) {
          setError(e.message)
        }
      } else {
        setError('O Google Recaptcha bloqueou a sua requisição.')
      }
    },
  })

  return (
    <>
      <HeadWrapper title="MOC Brasil - Cadastro - Dados iniciais" />

      <GridContainer>
        <HeadingContainer marginBottomDesktop="0" marginBottomMobile="0">
          <Heading1>Cadastre-se</Heading1>
        </HeadingContainer>

        <FormGroup textAlign="center" gridColumn="1 / -1">
          <SimpleContainer>
            <p className="no-login" style={{ minHeight: 'initial' }}>
              Já possui cadastro?
              <Link href="/login">
                <span className="link-with-icon">
                  Faça login em sua conta &nbsp;
                  <IconArrowButton />
                </span>
              </Link>
            </p>
          </SimpleContainer>
        </FormGroup>

        <Box className="box-form" width="765px" gridColumn="1 / -1">
          <Form onSubmit={formik.handleSubmit}>
            <UserRegistrationForm formik={formik} />

            <FormGroup gridColumn="1 / 13">
              <p className="required-fields">* Todos os campos são obrigatórios</p>
            </FormGroup>

            <UserOptionsForm formik={formik} />

            <FormGroup textAlign="center" gridColumn="1 / -1">
              {!formik.isSubmitting && (
                <Button type="submit" disabled={!formik.isValid}>
                  Continuar
                </Button>
              )}

              {formik.isSubmitting && (
                <LoaderContainer>
                  <Loader active size="small" />
                </LoaderContainer>
              )}
            </FormGroup>
          </Form>
        </Box>
      </GridContainer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    props: {
      providers: await getProviders(),
    },
  }
}

export default DadosIniciais
