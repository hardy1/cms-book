import React from 'react'
import styled from 'styled-components'
import { GetStaticProps } from 'next'
import { useRouter } from 'next/router'
import { Box, Button, GridContainer, Heading1, MainContainer, Paragraph } from 'designsystem'

import fetcher from '../../hooks/api/fetcher'
import { HeadingContainer, BoxAccessContainer } from '../../containers/cadastro'
import { serverURLPath } from '../../lib/config'
import { UserProfile } from '../../types/user-profile.model'
import { setCookiesKey, clearCookies } from '../../services/browser-cookies'
import { cookies } from '../../enums'

import { HeadWrapper } from '../../components'
import media from '../../resources/media'

enum iconPath {
  MED = '/img/access-doctor.svg',
  PS = '/img/access-health-professionals.svg',
  ASG = '/img/access-group-assign.svg',
  IF = '/img/access-pharmaceutical-industry.svg',
}

enum profileColor {
  MED = '#216DE1',
  PS = '#FF5A00',
  ASG = '#2EB43B',
  IF = '#CE4E9B',
}

const StyledMobileControlDiv = styled.div`
  @media ${media.tabletL} {
    > div {
      display: unset;
    }
  }
`
const StyledBox = styled(Box)`
  p {
    min-height: 114px;
  }
`

const Acesso: React.FC<{ userProfiles: UserProfile[] }> = (props) => {
  const { userProfiles } = props
  const router = useRouter()

  const handleClick = (selected: UserProfile): void => {
    clearCookies([cookies.SELECTED_USER_PROFILE, cookies.SELECTED_OFFER, cookies.COUPON_VERIFICATION])
    setCookiesKey(cookies.SELECTED_USER_PROFILE, selected.code || 'MED')
    switch (selected.code) {
      case 'ASG':
        router.push('/cadastro/grupo-ou-pessoa-juridica')
        break

      case 'IF':
        router.push('/cadastro/industria-farmaceutica')
        break

      default:
        router.push('/cadastro/selecionar-plano')
        break
    }
  }

  const profiles = userProfiles.map(({ id, code, name, description }) => (
    <StyledBox
      key={id}
      color={profileColor[code]}
      titleColor={profileColor[code]}
      buttonColor={profileColor[code]}
      className="BoxItem"
      boxShadowMobile
    >
      <img className="image-access" src={iconPath[code]} alt={name} />
      <h3>{name}</h3>
      <p>{description}</p>

      <Button color={profileColor[code]} onClick={() => handleClick({ id, code, name })}>
        Selecionar
      </Button>
    </StyledBox>
  ))

  return (
    <>
      <HeadWrapper title="MOC Brasil - Grupos profissionais" />
      <StyledMobileControlDiv>
        <MainContainer direction="column">
          <GridContainer>
            <HeadingContainer>
              <Heading1>Tenha acesso ao conteúdo exclusivo MOC</Heading1>
              <Paragraph>
                Escolha um perfil abaixo e conheça a melhor opção para acompanhar os assuntos em evidência na Oncologia.
              </Paragraph>
            </HeadingContainer>
          </GridContainer>

          <BoxAccessContainer className="BoxAccessContainer">{profiles}</BoxAccessContainer>
        </MainContainer>
      </StyledMobileControlDiv>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const userProfiles: UserProfile[] = await fetcher(`${serverURLPath}/api/user-profiles`)

    const userProfileMED = userProfiles.filter(({ code }) => code.toLowerCase() === 'med')[0]
    const userProfilePS = userProfiles.filter(({ code }) => code.toLowerCase() === 'ps')[0]
    const userProfileASG = userProfiles.filter(({ code }) => code.toLowerCase() === 'asg')[0]
    const userProfileIF = userProfiles.filter(({ code }) => code.toLowerCase() === 'if')[0]

    return {
      props: {
        userProfiles: [userProfileMED, userProfilePS, userProfileASG, userProfileIF],
      },
      revalidate: 60,
    }
  } catch (error) {
    return {
      props: {},
      revalidate: true,
    }
  }
}

export default Acesso
