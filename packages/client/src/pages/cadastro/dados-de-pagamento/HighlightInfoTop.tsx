import React from 'react'

import { HighlightInfo } from '../../../containers/cadastro'
import { handleDecimal } from '../../../../utils/handleDecimal'

interface HighlightInfoTopProps {
  planName: string
  value: number
  period?: string
  discount?: number
  totalPrice: number
  children?: React.ReactNode
  incomeTax?: number
  retention?: number
  netPrice?: number
}

const HighlightInfoTop: React.FC<HighlightInfoTopProps> = ({
  planName,
  value,
  period,
  discount,
  totalPrice,
  children,
  incomeTax,
  retention,
  netPrice,
}) => {
  return (
    <HighlightInfo marginBottom="40px" bottomSeparator>
      <h3 className="HighlightInfo__Title">Detalhes do pedido</h3>

      <div className="HighlightInfo__Details">
        <ul className="HighlightInfo__Details__Plan">
          <li>
            <h4>Plano</h4>
            <p className="highLight">{planName}</p>
          </li>

          <li className="HighlightInfo__Details__Plan__Type">
            <h4>Período</h4>
            <p title={period} className="highLight">
              {period}
            </p>
          </li>
        </ul>

        <ul className="HighlightInfo__Details__Values">
          <li>
            <h4>Subtotal</h4>
            <p>R$ {handleDecimal(value)}</p>
          </li>

          <li>
            <h4>Desconto do cupom</h4>
            <p>R$ {handleDecimal(discount)}</p>
          </li>

          <li>
            <h4>Total</h4>
            <p>R$ {handleDecimal(totalPrice)}</p>
          </li>
        </ul>

        {incomeTax && retention && netPrice && (
          <ul className="HighlightInfo__Details__Values">
            <li>
              <h4>Retenção (4,65%)</h4>
              <p>R$ {retention}</p>
            </li>

            <li>
              <h4>IR (1,5%)</h4>
              <p>R$ {incomeTax}</p>
            </li>

            <li>
              <h4>Total líquido</h4>
              <p className="highLight">R$ {netPrice}</p>
            </li>
          </ul>
        )}
      </div>

      <div className="HighlightInfo__Actions">{children}</div>
    </HighlightInfo>
  )
}

export default HighlightInfoTop
