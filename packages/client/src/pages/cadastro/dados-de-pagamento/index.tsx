/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { Box, Button, FormGroup, Input, Switch, Radio } from 'designsystem'
import Router from 'next/router'

import { Form, Label } from '../../../containers/Form'

import CreditCardFields from './CreditCardFields'
import HighlightInfoTop from './HighlightInfoTop'
import PaymentMethods from './PaymentMethods'
import PaymentDataWrapper from './PaymentDataWrapper'
import BillingDataFields from './BillingDataFields'

import { cookies } from '../../../enums'
import { default as nextCookies } from 'next-cookies'
import {
  addToCart,
  checkoutByBankSlip,
  checkoutByCreditCard,
  getCart,
  getOfferPeriod,
  postCouponValidate,
} from '../../../services/data/checkout.service'
import { ICart, IUser } from '../../../types'
import { paymentMethod } from '../../../enums'

import { HeadWrapper } from '../../../components'

import paymentDataSchema from './_paymentDataSchema'
import initialValuesObj from './_initialValuesObj'
import { GetServerSideProps } from 'next'
import { withAuthSsr } from '../../../../utils/withAuthSSR'
import { userProfileInfo } from '../../../services'
import { toast } from '../../../components/Toast'
import { useRouter } from 'next/router'
import { getCookiesKey, clearCookies, setCookiesKey } from '../../../services/browser-cookies'

const StyledFormGroup = styled(FormGroup)`
  align-items: unset !important;
`
const StyledButton = styled(Button)`
  margin-top: 31px !important;
`
const StyledButtonContainer = styled.div`
  grid-column: 1 / -1;
  display: flex;
  justify-content: center;

  button {
    margin-top: -2px !important;
  }
`

const DadosPagamento: React.FC<{ cart: ICart; user: IUser }> = ({ cart, user }) => {
  const [payment, setPayment] = useState(paymentMethod.CREDIT_CARD)
  const [validationSchema, setValidationSchema] = useState({})
  const [selectedPlan, setSelectedPlan] = useState<ICart>(cart)
  const [checkoutError, setCheckoutError] = useState<string>()
  const [isValidCoupon, setIsValidCoupon] = useState<boolean | undefined>(undefined)
  const [billingDataType, setBillingDataType] = useState<string>('person')
  const [incomeTax, setIncomeTax] = useState(null)
  const [retention, setRetention] = useState(null)
  const [netPrice, setNetPrice] = useState(null)
  const [preloadedCoupon] = useState(getCookiesKey(cookies.COUPON_VERIFICATION))

  const [initialValues] = useState({
    ...initialValuesObj(billingDataType, payment),
    phone: user?.phone || '',
    email: user?.email || '',
  })

  const router = useRouter()
  const formik = useFormik({
    initialValues,
    validationSchema: cart.id !== 7 ? Yup.object(validationSchema) : undefined,
    onSubmit: async (values) => {
      const checkoutData = {
        payment_method: payment,
        billing_data: {
          document_type: billingDataType === 'person' ? 'person' : 'company',
          document: billingDataType === 'person' ? values.cpfDocument : values.cnpjDocument,
          name: values.name,
          email: values.email,
          phone: values.phone,
          address: {
            country: values.country,
            state: values.state,
            city: values.city,
            neighborhood: values.neighborhood,
            street: values.street,
            street_number: values.number,
            postcode: values.zipcode,
            complement: values.complement,
          },
        },
      }
      try {
        if (payment === paymentMethod.BANK_SLIP) {
          await checkoutByBankSlip(checkoutData)
          router.push('/cadastro/sucesso?type=invoice')
        } else if (payment === paymentMethod.CREDIT_CARD) {
          await checkoutByCreditCard(checkoutData, {
            cardName: values.cardName,
            cardCVV: values.cardCVV,
            cardNumber: values.cardNumber,
            cardValidationMonth: values.cardValidationMonth,
            cardValidationYear: values.cardValidationYear,
          })

          clearCookies([cookies.COUPON_VERIFICATION])
          router.push('/cadastro/sucesso?type=card')
        }
      } catch (error) {
        setCheckoutError(error.message)
      }
    },
  })

  const addCouponToCart = async (coupon: string | undefined) => {
    try {
      const planData = await postCouponValidate({ promoCode: coupon }, true)
      if (planData) {
        setCookiesKey(cookies.COUPON_VERIFICATION, coupon)
        setCookiesKey(cookies.SELECTED_OFFER, cart.offer.id)
        setCookiesKey(cookies.SELECTED_PERIOD, cart.period)
        setIsValidCoupon(true)

        const response = await addToCart(
          { documentType: billingDataType !== 'CNPJ' ? 'CPF' : 'CNPJ', promoCode: coupon, offerId: cart.offer.id },
          true,
        )

        if (response.status) {
          setSelectedPlan(response)
        }
      }
    } catch {
      setIsValidCoupon(false)
      setSelectedPlan({
        ...selectedPlan,
        discountTotalPrice: 0,
        totalPrice: selectedPlan.oldTotalPrice,
      })
    }
  }

  useEffect(() => {
    const validationSchema = paymentDataSchema(payment, billingDataType)
    setValidationSchema(validationSchema)
  }, [payment, billingDataType])

  useEffect(() => {
    formik.resetForm({
      values: {
        promoCode: formik.values?.promoCode,
        name: formik.values?.name,
        email: formik.values?.email,
        phone: formik.values?.phone,
        zipcode: formik.values?.zipcode,
        country: formik.values?.country,
        state: formik.values?.state,
        city: formik.values?.city,
        neighborhood: formik.values?.neighborhood,
        street: formik.values?.street,
        number: formik.values?.number,
        complement: formik.values?.complement,
      },
    })
  }, [payment])

  useEffect(() => {
    const resetFiedDocument = () => {
      if (billingDataType === 'person') {
        return {
          cpfDocument: '',
        }
      }

      if (billingDataType === 'company') {
        return {
          cnpjDocument: '',
        }
      }
      return {}
    }

    formik.resetForm({
      values: {
        ...resetFiedDocument(),
        promoCode: formik.values?.promoCode,
        name: formik.values?.name,
        email: formik.values?.email,
        phone: formik.values?.phone,
        zipcode: formik.values?.zipcode,
        country: formik.values?.country,
        state: formik.values?.state,
        city: formik.values?.city,
        neighborhood: formik.values?.neighborhood,
        street: formik.values?.street,
        number: formik.values?.number,
        complement: formik.values?.complement,
      },
    })
  }, [billingDataType])

  useEffect(() => {
    checkoutError && toast.notify(checkoutError, { type: 'danger' })
    setCheckoutError('')
  }, [checkoutError])

  const switchButtonClasses = 'Switch__Button PaymentDataWrapper__Switch__Button'

  const setHighlightInfoTop = ({ selectedPlan, children }) => (
    <HighlightInfoTop
      planName={selectedPlan?.name}
      value={selectedPlan?.oldTotalPrice}
      discount={selectedPlan?.discountTotalPrice}
      totalPrice={selectedPlan?.totalPrice}
      period={selectedPlan?.period}
      incomeTax={incomeTax}
      retention={retention}
      netPrice={netPrice}
    >
      {children}
    </HighlightInfoTop>
  )

  const defaultHighlightTop = setHighlightInfoTop({
    selectedPlan,
    children: (
      <Button className="HighlightInfo__Actions__Button" onClick={() => Router.push('/cadastro/selecionar-plano')}>
        Trocar Plano
      </Button>
    ),
  })

  const highlightInfoBottom = setHighlightInfoTop({
    selectedPlan,
    children: (
      <Button type="submit" className="HighlightInfo__Actions__Button">
        Finalizar
      </Button>
    ),
  })

  const getIRData = async () => {
    const response = await addToCart({ documentType: 'CNPJ', offerId: cart.id }, true)

    const { discounts } = response

    const adjustValue = (value) => parseFloat(value).toFixed(2).replace('.', ',')

    const newIncomeTax = adjustValue(discounts?.ir?.value)
    const newRetention = adjustValue(discounts?.retencao_unificada?.value)
    const newNetPrice = adjustValue(response.price_data?.price_with_tax_value)

    setIncomeTax(newIncomeTax)
    setRetention(newRetention)
    setNetPrice(newNetPrice)
    setBillingDataType('company')
  }

  const resetToPerson = () => {
    setIncomeTax(null)
    setRetention(null)
    setNetPrice(null)
    setBillingDataType('person')
  }

  useEffect(() => {
    if (formik.values.promoCode.length === 0) {
      setIsValidCoupon(undefined)
    }
  }, [formik.values.promoCode.length])

  useEffect(() => {
    if (preloadedCoupon) {
      formik.setFieldValue('promoCode', preloadedCoupon)
      setIsValidCoupon(true)
    }
  }, [])

  return (
    <>
      <HeadWrapper title="MOC Brasil - Cadastro - Dados de pagamento" />

      <PaymentDataWrapper>
        <Box className="box-form" width="765px" gridColumn="1 / -1">
          {cart.id === 7 && (
            <>
              {defaultHighlightTop}

              <StyledButtonContainer>
                <Button type="button" onClick={() => router.push('/cadastro/sucesso')}>
                  Finalizar
                </Button>
              </StyledButtonContainer>
            </>
          )}

          {cart.id !== 7 && (
            <>
              <Form onSubmit={formik.handleSubmit} className="PaymentDataWrapper__Form">
                {defaultHighlightTop}

                <StyledFormGroup gridColumn="1 / 7" className="PaymentDataWrapper__PromoCode">
                  <Input
                    {...formik.getFieldProps('promoCode')}
                    label="Cupom de desconto"
                    id="promoCode"
                    type="text"
                    hasValidPromoCode={formik.values.promoCode.length > 0 && isValidCoupon !== undefined ? true : false}
                    isValidPromoCode={isValidCoupon ? 'isValidCoupon' : 'isNotValidCoupon'}
                    error={formik.values.promoCode.length > 0 ? true : false}
                    onBlur={() => addCouponToCart(formik.values.promoCode)}
                  />

                  <StyledButton
                    className="PaymentDataWrapper__PromoCode__Button"
                    onClick={() => addCouponToCart(formik.values.promoCode)}
                  >
                    Aplicar Cupom
                  </StyledButton>
                </StyledFormGroup>

                <h4 className="PaymentDataWrapper__TitleSection">
                  <span>Tipo de pagamento</span>
                </h4>

                <FormGroup gridColumn="1 / 9" className="PaymentDataWrapper__SwitchWrapper">
                  <Label htmlFor="paymentMethod">Forma de pagamento</Label>
                  <Switch className="PaymentDataWrapper__Switch">
                    <button
                      type="button"
                      className={`${switchButtonClasses} ${payment === paymentMethod.CREDIT_CARD ? 'active' : ''}`}
                      onClick={() => setPayment(paymentMethod.CREDIT_CARD)}
                    >
                      <span>Pagar com</span> Cartão
                    </button>

                    <button
                      type="button"
                      className={`${switchButtonClasses} ${payment === paymentMethod.BANK_SLIP ? 'active' : ''}`}
                      onClick={() => setPayment(paymentMethod.BANK_SLIP)}
                    >
                      <span>Pagar com</span> Boleto
                    </button>
                  </Switch>
                </FormGroup>

                {payment === paymentMethod.CREDIT_CARD && <CreditCardFields formik={formik} />}

                <h4 className="PaymentDataWrapper__TitleSection">
                  <span>Dados do titular do cartão | Dados de cobrança do boleto</span>
                </h4>

                <p className="PaymentDataWrapper__Paragraph">A nota fiscal será emitida com os dados abaixo.</p>

                <FormGroup gridColumn="1 / -1">
                  <Radio
                    name="person-type"
                    label="Pessoa Fisica"
                    id="person"
                    checked={billingDataType === 'person'}
                    onChange={() => resetToPerson()}
                  />

                  <Radio
                    name="person-type"
                    label="Pessoa Jurídica"
                    id="company"
                    checked={billingDataType === 'company'}
                    onChange={() => getIRData()}
                  />
                </FormGroup>

                <BillingDataFields formik={formik} formType={billingDataType} />

                {highlightInfoBottom}
              </Form>

              {checkoutError && <p>{checkoutError}</p>}
            </>
          )}
        </Box>

        <PaymentMethods />
      </PaymentDataWrapper>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = withAuthSsr(async (ctx) => {
  const serverCookies = nextCookies(ctx)
  const selectedOfferId = serverCookies[cookies.SELECTED_OFFER] || null
  const selectedPeriod = serverCookies[cookies.SELECTED_PERIOD] || undefined
  const user: IUser = await userProfileInfo()

  if (selectedOfferId) {
    const responseCart: ICart = await addToCart({ documentType: 'CPF', offerId: selectedOfferId })

    if (responseCart?.isUnlimited) {
      return {
        props: {
          cart: {},
          user: user || {},
        },
        redirect: {
          permanent: false,
          destination: '/cadastro/sucesso',
        },
      }
    }
  } else {
    return {
      props: {
        cart: {},
        user: user || {},
      },
      redirect: {
        permanent: false,
        destination: '/cadastro/selecionar-plano',
      },
    }
  }

  try {
    const getCartFunc = await getCart()
    const offerPeriod = await getOfferPeriod(selectedOfferId)

    const cart = {
      ...getCartFunc,
      ...offerPeriod,
    }

    return {
      props: {
        cart,
        user: user || {},
      },
    }
  } catch {
    if (selectedOfferId === '7' && selectedPeriod) {
      const cart = {
        id: 7,
        name: 'Plano Limitado',
        period: selectedPeriod,
        totalPrice: 0.0,
        oldTotalPrice: 0.0,
        discountTotalPrice: 0.0,
        monthly_price: 0.0,
      }
      return {
        props: {
          cart,
          user: user || {},
        },
      }
    } else {
      return {
        props: {
          cart: {},
          user: user || {},
        },
        redirect: {
          permanent: false,
          destination: '/login',
        },
      }
    }
  }
}, '/cadastro/dados-iniciais?callbackUrl=/cadastro/dados-de-pagamento')

export default DadosPagamento
