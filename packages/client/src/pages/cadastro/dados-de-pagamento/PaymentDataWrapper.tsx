import styled from 'styled-components'

const PaymentDataWrapper = styled.div`
  * {
    box-sizing: border-box;
  }

  .PaymentDataWrapper {
    &__Form {
      padding-top: 50px;
      padding-right: 20px;
      padding-left: 20px;

      @media screen and (min-width: 768px) {
        padding-top: 0;
        padding-right: 0;
        padding-left: 0;
      }
    }

    &__PromoCode {
      display: flex;
      flex-wrap: wrap;
      align-items: flex-end;

      @media screen and (min-width: 768px) {
        flex-wrap: nowrap;
      }

      > div {
        width: 100%;

        @media screen and (min-width: 768px) {
          width: 300px;
          padding-right: 10px;
        }
      }

      label {
        ~ div {
          @media screen and (min-width: 768px) {
            width: 290px;
          }
        }
      }

      &__Button {
        width: 100%;
        height: 50px;
        font-size: 16px;
        margin-top: 20px;

        @media screen and (min-width: 768px) {
          width: auto;
        }
      }
    }

    &__TitleSection {
      position: relative;
      grid-column: 1 / -1;
      width: 100%;
      color: #144590;
      font-family: 'Lato', sans-serif;
      font-size: 14px;
      font-weight: 400;
      line-height: 17px;
      letter-spacing: 0.5px;
      text-align: left;
      text-transform: uppercase;
      margin-top: 0;
      margin-bottom: 16px;

      span {
        position: relative;
        display: inline-block;
        background-color: #fff;
        z-index: 1;
        padding-right: 30px;
      }

      ::before {
        content: '';
        position: absolute;
        right: 0;
        bottom: 2px;
        width: 100%;
        height: 1px;
        background-color: #eee;
        pointer-events: none;
      }
    }

    &__Paragraph {
      min-height: initial;
      grid-column: 1 / -1;
      margin-top: 0;
      margin-bottom: 16px;
      text-align: left;
    }

    &__Switch {
      &__Button {
        min-width: initial;
      }
    }
  }
`

export default PaymentDataWrapper
