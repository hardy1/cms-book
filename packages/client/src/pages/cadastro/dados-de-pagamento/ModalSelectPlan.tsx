import React, { useState } from 'react'
import { Box, Button, Switch } from 'designsystem'

import { SelectPlanModal } from '../../../containers/cadastro'

import planItems from '../../../../utils/planItems'

const ModalSelectPlan: React.FC = () => {
  const MONTHLY = 'monthly'
  const YEARLY = 'yearly'

  const [plan, setPlan] = useState(MONTHLY)

  const plans = planItems[plan].map(({ id, title, subtitle, price, discount, details, recommended, color }) => {
    const isRecomended = recommended && <h4 className="recommended">O mais recomendado</h4>

    return (
      <Box
        key={id}
        width="100%"
        className="Modal__PlansList__Item"
        color={color}
        titleColor={color}
        buttonColor={color}
        boxShadowMobile
      >
        {isRecomended}
        <h3 className="Modal__PlansList__Item__Title">
          {title}
          <small>{subtitle}</small>
        </h3>

        <p className="price Modal__PlansList__Item__Price">
          R${price}
          <small>/mês</small>
        </p>

        <p className="details Modal__PlansList__Item__Details">
          <strong>{discount}</strong>
          {details}
        </p>

        <Button>Comece agora</Button>
      </Box>
    )
  })

  return (
    <SelectPlanModal className="ModalChoosePlan">
      <div className="Modal__Body">
        <Button className="Modal__Close">
          <span>&times;</span>
        </Button>

        <div className="Modal__SwitchContainer">
          <Switch className="Switch">
            <button className={`Switch__Button ${plan === MONTHLY ? 'active' : ''}`} onClick={() => setPlan(MONTHLY)}>
              Mensal
            </button>

            <button className={`Switch__Button ${plan === YEARLY ? 'active' : ''}`} onClick={() => setPlan(YEARLY)}>
              Anual
            </button>
          </Switch>
        </div>

        <div className="Modal__PlansList" dir="ltr">
          {plans}
        </div>

        <div className="Modal__FreePlan">
          <Box
            className="Modal__FreePlan__Item"
            width="100%"
            color="#3e3f42"
            titleColor="#3e3f42"
            buttonColor="#3e3f42"
            boxShadowMobile
          >
            <div className="Modal__FreePlan__Item__Title__Container">
              <h3 className="Modal__FreePlan__Item__Title">
                Plano Experiência
                <small>
                  Ideal para consultas pontuais, médicos de outras especialidades e profissionais que querem uma
                  primeira experiência no projeto.
                </small>
              </h3>
            </div>

            <div className="Modal__FreePlan__Item__PriceAndAction">
              <p className="price Modal__FreePlan__Item__Price">
                R$ 30
                <small>/7 dias</small>
              </p>

              <Button>Comece agora</Button>
            </div>

            <p className="Modal__FreePlan__Item__Info">
              *Teste a nossa plataforma com <em>todas as funcionalidades</em> liberadas por 7 dias.
            </p>
          </Box>
        </div>
      </div>
    </SelectPlanModal>
  )
}

export default ModalSelectPlan
