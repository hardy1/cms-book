import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { FormGroup, Input, Select } from 'designsystem'

import { Tooltip } from '../../../containers/cadastro'
import { Label } from '../../../containers/Form'
import { IconInformation } from '../../../containers/Icons'
import { GenericFormSection } from '../../../types/generic-form-section.model'
import ButtonWrapperTooltip from './ButtonWrapperTooltip'

import Visa from '../../../assets/img/credit-cards/visa.png'
import Master from '../../../assets/img/credit-cards/master.png'
import DinersClub from '../../../assets/img/credit-cards/diners-club.png'
import AmericanExpress from '../../../assets/img/credit-cards/american-express.png'
import creditCardType from 'credit-card-type'

const StyledAlignFieldWithNoLabel = styled.div`
  @media screen and (min-width: 768px) {
    margin-top: 31px;
  } ;
`
const StyledCreditCardFlag = styled.div`
  display: flex;
  align-items: baseline;
  padding-top: 30px;
  height: 81px;
`

const selectMonth = [
  {
    id: 1,
    name: '1 - Janeiro',
    value: '01',
  },
  {
    id: 2,
    name: '2 - Fevereiro',
    value: '02',
  },
  {
    id: 3,
    name: '3 - Março',
    value: '03',
  },
  {
    id: 4,
    name: '4 - Abril',
    value: '04',
  },
  {
    id: 5,
    name: '5 - Maio',
    value: '05',
  },
  {
    id: 6,
    name: '6 - Junho',
    value: '06',
  },
  {
    id: 7,
    name: '7 - Julho',
    value: '07',
  },
  {
    id: 8,
    name: '8 - Agosto',
    value: '08',
  },
  {
    id: 9,
    name: '9 - Setembro',
    value: '09',
  },
  {
    id: 10,
    name: '10 - Outubro',
    value: '10',
  },
  {
    id: 11,
    name: '11 - Novembro',
    value: '11',
  },
  {
    id: 12,
    name: '12 - Dezembro',
    value: '12',
  },
]

interface ISelectObject {
  id: number
  name: number
  value: number
}

const CreditCardFields: React.FC<GenericFormSection> = ({ formik }) => {
  const [showTooltip, toggleTooltip] = useState(false)
  const [listOfYears, setListOfYears] = useState<ISelectObject[]>([])

  useEffect(() => {
    const currentYear = new Date().getFullYear()

    for (let i = 0; i <= 10; i++) {
      setListOfYears((listOfYears) => [
        ...listOfYears,
        {
          id: currentYear + i,
          name: currentYear + i,
          value: currentYear + i,
        },
      ])
    }
  }, [])

  const [numberValidation, setNumberValidation] = useState<string>('')

  const cardType = (value: string) => {
    const cct = creditCardType(value)

    setNumberValidation(cct[0]?.type)

    formik.setFieldValue('cardNumber', value)

    if (value === '') {
      setNumberValidation('')
    }
  }

  const handleCreditCardType = (cardType: string) => {
    switch (cardType) {
      case 'visa':
        return <img src={Visa} alt="Visa" width={48} />
      case 'mastercard':
        return <img src={Master} alt="Master" width={48} />
      case 'diners-club':
        return <img src={DinersClub} alt="Diners Club" width={48} />
      case 'american-express':
        return <img src={AmericanExpress} alt="American Express" width={48} />
      default:
        return <p>{cardType}</p>
    }
  }

  return (
    <>
      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('cardNumber')}
          type="text"
          id="cardNumber"
          label="Número do cartão"
          onChange={(e) => cardType(e.target.value)}
          required
          error={formik.touched.cardNumber && Boolean(formik.errors.cardNumber)}
          message={formik.touched.cardNumber && formik.errors.cardNumber}
          mask={'#### #### #### ####'}
        />
      </FormGroup>
      <FormGroup gridColumn="7 / 9">
        <StyledCreditCardFlag>{handleCreditCardType(numberValidation)}</StyledCreditCardFlag>
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('cardName')}
          type="text"
          id="cardName"
          label="Nome (Igual ao do cartão)"
          required
          error={Boolean(formik.errors.cardName)}
          message={formik.errors.cardName}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 2">
        <Select
          {...formik.getFieldProps('cardValidationMonth')}
          placeholder="Mês"
          id="cardValidationMonth"
          label="Validade"
          items={selectMonth}
          required
          error={Boolean(formik.errors.cardValidationMonth)}
          message={formik.errors.cardValidationMonth}
        />
      </FormGroup>

      <FormGroup gridColumn="2 / 4">
        <StyledAlignFieldWithNoLabel>
          <Select
            {...formik.getFieldProps('cardValidationYear')}
            placeholder="Ano"
            id="cardValidationYear"
            label=""
            items={listOfYears}
            required
            error={Boolean(formik.errors.cardValidationYear)}
            message={formik.errors.cardValidationYear}
          />
        </StyledAlignFieldWithNoLabel>
      </FormGroup>

      <FormGroup gridColumn="4 / 7">
        <Label htmlFor="cardCVV">
          <span>CVV</span>
          <ButtonWrapperTooltip type="button" onClick={() => toggleTooltip(!showTooltip)}>
            <span>Saber mais</span>

            <Tooltip show={showTooltip}>Número de 3 a 6 dígitos no verso do seu cartão</Tooltip>

            <IconInformation />
          </ButtonWrapperTooltip>
        </Label>

        <Input
          {...formik.getFieldProps('cardCVV')}
          type="text"
          id="cardCVV"
          error={formik.touched.cardCVV && Boolean(formik.errors.cardCVV)}
          message={formik.errors.cardCVV}
          mask={'######'}
          required
        />
      </FormGroup>

      <FormGroup gridColumn="1 / -1">
        <p className="required-fields">* Todos os campos são obrigatórios</p>
      </FormGroup>
    </>
  )
}

export default CreditCardFields
