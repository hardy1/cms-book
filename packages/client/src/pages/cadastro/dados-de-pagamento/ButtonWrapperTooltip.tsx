import styled from 'styled-components'

const ButtonWrapperTooltip = styled.button`
  position: relative;
  top: -2px;
  min-width: auto !important;
  height: 15px;
  background-color: transparent;
  border-width: 0;
  font-size: 16px !important;
  vertical-align: text-bottom;
  cursor: pointer;
  padding: 0 !important;
  margin: 0 0 0 10px !important;

  span {
    position: absolute;
    width: 0;
    height: 0;
    overflow: hidden;
  }
`

export default ButtonWrapperTooltip
