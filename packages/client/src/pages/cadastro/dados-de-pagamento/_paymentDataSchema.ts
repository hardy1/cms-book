import {
  addressSchema,
  CreditCardSchema,
  documentsSchema,
  UserPersonalSchema,
  UserRegistrationSchema,
} from '../../../resources/formValidationSchemas'

import { paymentMethod } from '../../../enums'

const paymentDataSchema = (paymentType, billingDataType) => {
  const AddressFields = {
    city: addressSchema.city,
    country: addressSchema.country,
    neighborhood: addressSchema.neighborhood,
    number: addressSchema.number,
    state: addressSchema.state,
    street: addressSchema.street,
    zipcode: addressSchema.zipcode,
  }

  const creditCardFields = {
    cardCVV: CreditCardSchema.cardCVV,
    cardName: CreditCardSchema.cardName,
    cardNumber: CreditCardSchema.cardNumber,
    cardValidationMonth: CreditCardSchema.cardValidationMonth,
    cardValidationYear: CreditCardSchema.cardValidationYear,
  }

  const documentCompanyField = {
    cnpjDocument: documentsSchema.cnpjDocument,
  }

  const documentPersonField = {
    cpfDocument: documentsSchema.cpfDocument,
  }

  const userFields = {
    email: UserRegistrationSchema.email,
    name: UserRegistrationSchema.name,
    phone: UserPersonalSchema.phone,
  }

  const setPaymentType = () => {
    if (paymentType === paymentMethod.CREDIT_CARD) {
      return creditCardFields
    }

    return {}
  }

  const setDocumentType = () => {
    if (billingDataType === 'person') {
      return documentPersonField
    }

    if (billingDataType === 'company') {
      return documentCompanyField
    }

    return {}
  }

  return {
    ...AddressFields,
    ...setDocumentType(),
    ...setPaymentType(),
    ...userFields,
  }
}

export default paymentDataSchema
