import React from 'react'
import { Button } from 'designsystem'

import { HighlightInfo } from '../../../containers/cadastro'
import { GenericFormSection } from '../../../types/generic-form-section.model'
import { handleDecimal } from '../../../../utils/handleDecimal'

interface HighlightInfoBottomProps extends GenericFormSection {
  payment: string
  value?: number
}

const HighlightInfoBottom: React.FC<HighlightInfoBottomProps> = ({ value, formik, formType, payment }) => {
  const {
    city,
    country,
    cpfDocument,
    cnpjDocument,
    email,
    name,
    neighborhood,
    number,
    phone,
    promocode,
    state,
    street,
    zipcode,
    cardCVV,
    cardName,
    cardNumber,
    cardValidationMonth,
    cardValidationYear,
  } = formik.values

  return (
    <HighlightInfo padding="37px 29px 40px">
      <div className="HighlightInfo__Details">
        <ul className="HighlightInfo__Details__Values">
          <li>
            <h4>Total</h4>
            <p>R$ {handleDecimal(value)}</p>
          </li>
        </ul>
      </div>

      <div className="HighlightInfo__Actions">
        <Button
          type="submit"
          className="HighlightInfo__Actions__Button LargeButton"
          disabled={
            city === '' ||
            country === '' ||
            email === '' ||
            name === '' ||
            neighborhood === '' ||
            number === '' ||
            phone === '' ||
            promocode === '' ||
            state === '' ||
            street === '' ||
            zipcode === '' ||
            (payment === 'credit_card' && cardCVV === undefined) ||
            (payment === 'credit_card' && cardCVV === '') ||
            (payment === 'credit_card' && cardName === undefined) ||
            (payment === 'credit_card' && cardName === '') ||
            (payment === 'credit_card' && cardValidationMonth === undefined) ||
            (payment === 'credit_card' && cardValidationMonth === '') ||
            (payment === 'credit_card' && cardValidationYear === undefined) ||
            (payment === 'credit_card' && cardValidationYear === '') ||
            (payment === 'credit_card' && cardNumber === undefined) ||
            (payment === 'credit_card' && cardNumber === '') ||
            (formType === 'person' && cpfDocument === '') ||
            (formType === 'company' && cnpjDocument === '')
          }
        >
          Finalizar
        </Button>
      </div>
    </HighlightInfo>
  )
}

export default HighlightInfoBottom
