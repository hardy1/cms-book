const initialValuesObj = (billingDataType: string, payment: string) => {
  const setFieldDocument = () => {
    if (billingDataType === 'person') {
      return {
        cpfDocument: '',
      }
    }

    if (billingDataType === 'company') {
      return {
        cnpjDocument: '',
      }
    }

    return {}
  }

  const setFieldCard = () => {
    if (payment === 'credit_card') {
      return {
        cardNumber: '',
        cardCVV: '',
        cardName: '',
        cardValidationMonth: '',
        cardValidationYear: '',
      }
    }
    return {}
  }

  return {
    ...setFieldCard(),
    ...setFieldDocument(),
    promoCode: '',
    name: '',
    email: '',
    phone: '',
    zipcode: '',
    country: 'BR',
    state: '',
    city: '',
    neighborhood: '',
    street: '',
    number: '',
    complement: '',
  }
}

export default initialValuesObj
