import styled from 'styled-components'
import { FormGroup } from 'designsystem'

const _FormGroupWithZipcode = styled(FormGroup)`
  .FormGroupWithZipcode {
    &__Link {
      position: absolute;
      top: 0;
      right: 0;
      width: auto;
      min-width: initial;
      color: #216de1;
      font-family: 'Lato', sans-serif;
      font-size: 12px;
      line-height: 19px;
      text-decoration: none;
      padding-top: 0;
      padding-right: 0;
      padding-bottom: 0;
      padding-left: 0;
      margin-top: 0;
      box-sizing: border-box;
      z-index: 1;
      transition: opacity 0.2s ease-out;

      &:hover,
      &:focus {
        opacity: 0.8;
      }

      @media screen and (min-width: 1024px) {
        top: 46px;
        right: 19px;
      }
    }
  }
`

export default _FormGroupWithZipcode
