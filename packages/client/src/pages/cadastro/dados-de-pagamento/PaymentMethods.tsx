import React from 'react'
import Image from 'next/image'

import { PaymentMethodsContainer } from '../../../containers/cadastro'

const PaymentMethods: React.FC = () => {
  const cardImageProps = (source, alt, width, height, className) => ({
    src: `/img/${source}.svg`,
    alt,
    className: className || 'Container__Cards__Image',
    width: width || '85px',
    height: height || '50px',
  })

  return (
    <PaymentMethodsContainer>
      <div className="Container__Cards">
        <h6 className="Container__Title">Cartões aceitos</h6>

        <Image {...cardImageProps('visa', 'Visa', null, null, null)} />
        <Image {...cardImageProps('mastercard', 'Mastercard', null, null, null)} />
        <Image {...cardImageProps('maestro', 'Maestro', null, null, null)} />
        <Image {...cardImageProps('elo', 'Elo', null, null, null)} />
        <Image {...cardImageProps('alelo', 'Alelo', null, null, null)} />
        <Image {...cardImageProps('amex', 'American Express', null, null, null)} />
        <Image {...cardImageProps('bb', 'Banco do Brasil', null, null, null)} />
        <Image {...cardImageProps('hipercard', 'Hipercard', null, null, null)} />
        <Image {...cardImageProps('diners-club', 'Diners Club', null, null, null)} />
      </div>

      <div className="Container__SecureSite">
        <h6 className="Container__Title">Site seguro</h6>

        <Image {...cardImageProps('cloudfare', 'Cloudfare', '200px', '80px', 'Container__SecureSite__Image')} />
      </div>
    </PaymentMethodsContainer>
  )
}

export default PaymentMethods
