import React, { useState, useEffect } from 'react'

import { FormGroup, Input, Select } from 'designsystem'
import cep from 'cep-promise'

import { GenericFormSection } from '../../../types/generic-form-section.model'
import { countries } from '../../../resources/mocks/common/index'

import MASK from '../../../resources/mask'

import FormGroupWithZipcode from './_FormGroupWithZipcode'

interface IAddressByCep {
  neighborhood: string
  city: string
  country: string
  mobile: string
  phone: string
  state: string
  zipcode: string
  street: string
  complement?: string
}

const BillingDataFields: React.FC<GenericFormSection> = ({ formType, formik }) => {
  const [currentAddress, setCurrentAddress] = useState<IAddressByCep>()

  const cpfDocumentField = (
    <Input
      {...formik.getFieldProps('cpfDocument')}
      type="text"
      id="cpfDocument"
      label="CPF *"
      required
      error={Boolean(formik.errors.cpfDocument)}
      message={formik.errors.cpfDocument}
    />
  )

  const companyDocumentField = (
    <Input
      {...formik.getFieldProps('cnpjDocument')}
      type="text"
      id="cnpjDocument"
      label="CNPJ *"
      required
      error={Boolean(formik.errors.cnpjDocument)}
      message={formik.errors.cnpjDocument}
    />
  )

  const setDocumentType = () => {
    switch (formType) {
      case 'person':
        return cpfDocumentField
      case 'company':
        return companyDocumentField
      default:
        return null
    }
  }

  const addressByCep = async (postalCode: string) => {
    try {
      const address = postalCode ? await cep(postalCode) : {}
      setCurrentAddress(address)
      return address
    } catch (error) {
      formik.setFieldValue('city', '', true)
      formik.setFieldValue('state', '', true)
    }
  }

  const customHandleBlur = async (e) => {
    const { value, id } = e.target
    if (!formik.errors[id] && formik.values[id]) {
      await addressByCep(value)
    }

    if (!formik.values[id]) {
      formik.setFieldValue('city', '', false)
      formik.setFieldValue('state', '', false)
    }
  }

  useEffect(() => {
    formik.setFieldError('zipcode', '')
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (currentAddress?.city) {
      formik.setFieldValue('city', currentAddress?.city, false)
      formik.setFieldTouched('city', true, false)
      formik.setFieldError('city', undefined)
    }

    if (currentAddress?.state) {
      formik.setFieldValue('state', currentAddress?.state, false)
      formik.setFieldTouched('state', true, false)
      formik.setFieldError('state', undefined)
    }

    if (currentAddress?.neighborhood) {
      formik.setFieldValue('neighborhood', currentAddress?.neighborhood, false)
      formik.setFieldTouched('neighborhood', true, false)
      formik.setFieldError('neighborhood', undefined)
    }

    if (currentAddress?.street) {
      formik.setFieldValue('street', currentAddress?.street, false)
      formik.setFieldTouched('street', true, false)
      formik.setFieldError('street', undefined)
    }
    if (currentAddress?.complement) {
      formik.setFieldValue('complement', currentAddress?.complement, false)
      formik.setFieldTouched('complement', true, false)
      formik.setFieldError('complement', undefined)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentAddress])

  return (
    <>
      <FormGroup gridColumn="1 / 7">{setDocumentType()}</FormGroup>

      <FormGroup gridColumn="1 / -1">
        <Input
          {...formik.getFieldProps('name')}
          type="text"
          id="name"
          label="Nome completo ou razão social *"
          required
          error={Boolean(formik.errors.name)}
          message={formik.errors.name}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('email')}
          type="email"
          id="email"
          label="Email *"
          required
          autoComplete="email"
          error={Boolean(formik.errors.email)}
          message={formik.errors.email}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('phone')}
          mask={MASK.genericPhone}
          type="text"
          id="phone"
          label="Telefone *"
          placeholder="Ex: +551199999876"
          required
          error={Boolean(formik.errors.phone)}
          message={formik.errors.phone}
        />
      </FormGroup>

      <FormGroupWithZipcode gridColumn="1 / 7">
        <a
          href="https://buscacepinter.correios.com.br/app/endereco/index.php"
          className="FormGroupWithZipcode__Link"
          target="_blank"
          rel="noreferrer"
        >
          Não sei meu CEP
        </a>
        <Input
          {...formik.getFieldProps('zipcode')}
          mask={MASK.zipcodeBr}
          type="text"
          id="zipcode"
          label="CEP *"
          autoComplete="zipcode"
          required
          onBlur={customHandleBlur}
          error={Boolean(formik.errors.zipcode)}
          message={formik.errors.zipcode}
        />
      </FormGroupWithZipcode>

      <FormGroup gridColumn="7 / -1">
        <Select
          id="country"
          label="País *"
          placeholder="Selecione um país"
          items={countries}
          value="BR"
          onChange={(e) => formik.setFieldValue('country', e?.target?.value, false)}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('state')}
          type="text"
          id="state"
          label="Estado *"
          autoComplete="address-level2"
          required
          disabled
          error={Boolean(formik.errors.state)}
          message={formik.errors.state}
        />
      </FormGroup>

      <FormGroup gridColumn="7 / -1">
        <Input
          {...formik.getFieldProps('city')}
          type="text"
          id="city"
          label="Cidade *"
          autoComplete="address-level2"
          required
          disabled
          error={Boolean(formik.errors.city)}
          message={formik.errors.city}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input
          {...formik.getFieldProps('neighborhood')}
          type="text"
          id="neighborhood"
          label="Bairro *"
          required
          disabled
          error={Boolean(formik.errors.neighborhood)}
          message={formik.errors.neighborhood}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 10">
        <Input
          {...formik.getFieldProps('street')}
          type="text"
          id="street"
          label="Endereço *"
          required
          disabled
          error={Boolean(formik.errors.street)}
          message={formik.errors.street}
        />
      </FormGroup>

      <FormGroup gridColumn="10 / -1">
        <Input
          {...formik.getFieldProps('number')}
          type="text"
          id="number"
          label="Número *"
          required
          error={Boolean(formik.errors.number)}
          message={formik.errors.number}
        />
      </FormGroup>

      <FormGroup gridColumn="1 / 7">
        <Input {...formik.getFieldProps('complement')} type="text" id="complement" label="Complemento (opcional)" />
      </FormGroup>
    </>
  )
}

export default BillingDataFields
