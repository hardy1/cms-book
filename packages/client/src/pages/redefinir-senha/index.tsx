import React, { useState } from 'react'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { Button, FormGroup, Heading1, Input } from 'designsystem'
import { HeadingContainer } from '../../containers/cadastro'
import { Form, Label } from '../../containers/Form'
import { resetPassword } from '../../services/auth'
import { toast } from '../../components/Toast'
import { Box, ContentContainer } from '../../components'
import { size } from '../../resources/media'
import { useRouter } from 'next/router'
import { PasswordSchema } from '../../resources/formValidationSchemas'
import { PasswordInitialValues } from '../../resources/formInitialValues'

import { HeadWrapper } from '../../components'

const validationSchema = yup.object(PasswordSchema)

const ResetPassword: React.FC = () => {
  const router = useRouter()
  const [resetStatus, setResetStatus] = useState(false)
  const formik = useFormik({
    initialValues: PasswordInitialValues,
    validationSchema,
    onSubmit: async (values) => {
      try {
        await resetPassword({
          ...values,
          code: router.query?.code,
        })
        setResetStatus(true)
      } catch (error) {
        setResetStatus(false)
        if (router.query?.code) {
          toast.notify(`Erro: ${error.message || error}`, { type: 'danger' })
        } else {
          toast.notify(`Erro: Código inválido`, { type: 'danger' })
        }
      }
    },
  })

  return (
    <>
      <HeadWrapper title="MOC Brasil - Redefinir senha" />

      <ContentContainer type="flex" direction="column" maxWidth={size.tabletM}>
        {resetStatus ? (
          <>
            <HeadingContainer>
              <Heading1>Senha redefinida com sucesso! Agora você pode fazer seu login com sua nova senha.</Heading1>
            </HeadingContainer>
            <FormGroup gridColumn="1 / -1" textAlign="center">
              <Button
                onClick={() => {
                  router.push('/login')
                }}
              >
                Fazer login
              </Button>
            </FormGroup>
          </>
        ) : (
          <>
            <HeadingContainer>
              <Heading1>Redefinir senha</Heading1>
            </HeadingContainer>

            <Box width="395px" padding="2rem" overrideButton shadow>
              <Form display="block" onSubmit={formik.handleSubmit}>
                <FormGroup gridColumn="1 / -1">
                  <Label>Nova senha</Label>
                  <Input
                    type="password"
                    id="password"
                    autoComplete="password"
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    message={formik.touched.password && formik.errors.password}
                    {...formik.getFieldProps('password')}
                  />
                </FormGroup>
                <FormGroup gridColumn="1 / -1">
                  <Label>Repita a nova senha</Label>
                  <Input
                    type="password"
                    id="confirmPassword"
                    error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                    message={formik.touched.confirmPassword && formik.errors.confirmPassword}
                    {...formik.getFieldProps('confirmPassword')}
                  />
                </FormGroup>

                <FormGroup gridColumn="1 / -1" textAlign="center">
                  <Button type="submit">Enviar</Button>
                </FormGroup>
              </Form>
            </Box>
          </>
        )}
      </ContentContainer>
    </>
  )
}

export default ResetPassword
