import fetch from 'isomorphic-unfetch'

const fetcher = async (url, options?) => {
  const res = await fetch(url, options)
  const data = await res.json()

  return data?.payload
}

export default fetcher
