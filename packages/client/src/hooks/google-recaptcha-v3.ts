const siteKey = '6LeerQEfAAAAAP5qeGkMq8CUOOVc6LXp-_qQq6fW'
const secretKey = '6LeerQEfAAAAAEux2ct8YUzclgfTjopNclAFRPSb'

export const GoogleRecaptchaV3LoadScript = () => {
  const script = document.createElement('script')
  script.src = `https://www.google.com/recaptcha/api.js?render=${siteKey}`

  return document.body.appendChild(script)
}

export const GoogleRecaptchaV3 = () => {
  return new Promise((resolve) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    grecaptcha.ready(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      grecaptcha.execute(siteKey, { action: 'submit' }).then(async (token: string) => {
        const fetchApi = await fetch('/api/google-recaptcha-v3', {
          method: 'POST',
          headers: {
            url: `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`,
          },
        })

        const data = await fetchApi.json()

        resolve(data.success)
      })
    })
  })
}
