import React, { createContext, Dispatch, FC, SetStateAction, useState } from 'react'
import { BookScopeDataType } from '../types'

type BookContextProps = {
  state: BookScopeDataType
  setState: Dispatch<SetStateAction<BookScopeDataType>>
}

/* eslint-disable */
const DEFAULT_VALUE = {
  state: {
    fullScreen: false,
    nightMode: false,
    bookIndex: false,
    searchIn: false,
    fontModifier: 1,
    book: {},
    loading: {
      active: false,
      fullscreen: false,
    },
  },
  setState: () => { },
}
/* eslint-enable */

const BookContext = createContext<BookContextProps>(DEFAULT_VALUE)

const BookContextProvider: FC = ({ children }) => {
  const [state, setState] = useState(DEFAULT_VALUE.state)

  return (
    <BookContext.Provider
      value={{
        state,
        setState,
      }}
    >
      {children}
    </BookContext.Provider>
  )
}

export { BookContextProvider }
export default BookContext
