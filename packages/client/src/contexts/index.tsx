import React, { FC } from 'react'
import { BookContextProvider } from './book'

const GlobalContext: FC = ({ children }) => <BookContextProvider>{children}</BookContextProvider>

export * from './dashboard'
export default GlobalContext
