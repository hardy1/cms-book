import React, { useState, createContext, Dispatch, SetStateAction } from 'react'
import { IUser, IInfoBlock, IProfession } from '../types'
import { dashboardMenu } from '../resources/dashboardSetup'

interface IDashboardScope {
  user?: IUser
  boards?: IInfoBlock
  professions?: IProfession[]
  currentMenu: string
}

interface IDashboardContextProps {
  state: IDashboardScope
  setState?: Dispatch<SetStateAction<IDashboardScope>>
}

const DEFAULT_VALUE: IDashboardContextProps = {
  state: {
    user: undefined,
    boards: undefined,
    professions: undefined,
    currentMenu: dashboardMenu.INITIAL,
  },
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setState: (): void => {},
}

export const DashboardContext = createContext<IDashboardContextProps>(DEFAULT_VALUE)

const DashboardContextProvider: React.FC = ({ children }) => {
  const [state, setState] = useState(DEFAULT_VALUE.state)

  return (
    <DashboardContext.Provider
      value={{
        state,
        setState,
      }}
    >
      {children}
    </DashboardContext.Provider>
  )
}

export default DashboardContextProvider
