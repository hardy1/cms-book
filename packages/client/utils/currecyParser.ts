export const formatCurrency = (value: number): string => {
  const limitDecimals = Math.round(value * 100) / 100
  const formattedValue = limitDecimals.toString().replace('.', ',')
  return formattedValue
}
