const tableParserCleaner = (htmlRaw) =>
  htmlRaw
    ?.replace(/\s((aid:))?((ccolwidth)|(table)|(xmlns))?(:aid)?(="\w*\W*\d*(\w*\.*\/*)*")?/g, ' ')
    .replace(/\s{2,}/g, ' ')
    .replace(/\\r/g, '')
    .replace(/(crows)/g, 'rowspan')
    .replace(/(ccols)/g, 'colspan')
    .replace(/(theader="")/g, 'th="1"')
    .replace(/(Table|recommendation_table)/g, 'table')
    .replace(/(recommendation_cell)/g, 'td')
    .replace(/(Cell|cell)/g, 'td')
    .replace(/(<\/?foreign_term>)/g, ' ')

const tableChildrenTagRemover = (tableRaw) => tableRaw.replace(/<\/?(a|span)((\s?(\w*-?\w*="\D*\d*\W*\w*"))*)>/g, '')

export { tableParserCleaner, tableChildrenTagRemover }
