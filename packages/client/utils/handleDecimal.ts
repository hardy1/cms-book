export const handleDecimal = (value) => {
  if (Number.isInteger(Number(value))) {
    return `${value},00`
  } else {
    return `${Number(value).toFixed(2).replace('.', ',')}`
  }
}
