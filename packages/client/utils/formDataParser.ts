export const createFormData = (dataObject) => {
  const formData: FormData = new FormData()
  Object.entries(dataObject).forEach(([key, value]) => {
    formData.append(key, typeof value === 'string' ? <string>value : <Blob>value)
  })
  return formData
}

export const destructureArrayToObject = (dataArray, entryName?: string) => {
  const objectData = new Object()
  dataArray.forEach((item, index) => {
    Object.entries(item).forEach(([key, value]) => (objectData[`${entryName}[${index}][${key}]`] = value))
  })
  return objectData
}
