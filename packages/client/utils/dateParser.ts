export const datePipe = (UTCDate: string) => {
  const parsedDate = new Date(UTCDate)
  const day = parsedDate.getDate()
  const month = parsedDate.getMonth() < 10 ? `0${parsedDate.getMonth()}` : parsedDate.getMonth()
  const year = parsedDate.getFullYear()

  return `${day}/${month}/${year}`
}
