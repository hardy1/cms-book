const withImages = require('next-images')

module.exports = {
  ...withImages(),
  future: {
    webpack5: true,
  },
  async rewrites() {
    return [
      {
        source: '/proxy/:path*',
        destination: `${process.env.BASE_API_URL}/:path*`,
      },
    ]
  },
}
