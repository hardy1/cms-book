# MOC Design System

## Run the project locally

Clone the project and install the dependencies:

```sh
$ git clone git@bitbucket.org:coffeecup/moc-designsystem.git
$ cd moc-designsystem
$ npm install
```

## Scripts

### Test:
```sh
$ npm test
```

### Lint:
```sh
$ npm run lint
```

### Generate new component:
```sh
$ npm run generate
# Enter component name
$ FooBar
# Check if it's a Stateless Component
$ y/n
```

#### Storybook:
```sh
$ npm run storybook
```
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

#### Build Storybook:
```sh
$ npm run build-storybook
```

### Branches
Choose short and descriptive names.

```sh
# Good
$ git checkout -b feature/oauth

# Bad
$ git checkout -b login_fix
```

Use dashes to separate words.

```sh
# Good
$ git checkout -b feature/add-webhook

# Bad
$ git checkout -b feature/add_webhook
```

Use default prefixes for new branches, to allow automated workflows and make branch types clearer.

```sh
# Bugfix
$ bugfix/foo-bar

# Feature
feature/foo-bar

# Hotfix
hotfix/foo-bar

# Release
release/foo-bar
```

## Merges
Merges should be always _squashed_ to turn the commit message into the pull request title.
