import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import svg from 'rollup-plugin-svg'
import { sizeSnapshot } from 'rollup-plugin-size-snapshot'
import { terser } from 'rollup-plugin-terser'
import visualizer from 'rollup-plugin-visualizer'
import svgr from '@svgr/rollup'
import image from '@rollup/plugin-image'

import pkg from './package.json'

export const commons = [
  svg(),
  sizeSnapshot(),
  terser({
    output: {
      comments: false,
    },
  }),
]

export default {
  input: 'src/index.ts',
  external: ['react', 'react-dom', 'prop-types', 'styled-components', 'react-modal'],
  output: [
    {
      name: 'moc-designsystem',
      exports: 'named',
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
    {
      name: 'moc-designsystem',
      exports: 'named',
      file: pkg.module,
      format: 'esm',
      sourcemap: true,
    },
  ],
  plugins: [
    resolve({
      extensions: ['.js', '.jsx', 'ts', 'tsx'],
      jsnext: true,
      browser: true,
    }),
    typescript({
      module: 'ESNext',
    }),
    ...commons,
    visualizer({
      title: 'Bundle size',
      template: 'treemap',
      filename: 'dist/stats.html',
    }),
    svgr({ typescript: true }),
    image(),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react/react.js': ['Children', 'Component', 'PropTypes', 'createElement'],
        'node_modules/react-dom/index.js': ['render'],
        'react-is': ['isForwardRef', 'isValidElementType'],
      },
    }),
  ],
}
