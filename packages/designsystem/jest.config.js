module.exports = {
  preset: 'ts-jest',
  rootDir: '.',
  testEnvironment: 'jsdom',
  setupTestFrameworkScriptFile: '<rootDir>/src/internal/jest-setup.ts',
  moduleNameMapper: {
    '\\.svg$': '<rootDir>/src/internal/fileTransformer.ts',
    '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/src/internal/fileMock.js',
  },
}
