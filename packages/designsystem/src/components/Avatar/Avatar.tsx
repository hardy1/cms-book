import * as React from 'react'
import { AvatarContainer, Img } from './Avatar.styles'

export type AvatarProps = {
  imgAlt?: string
  imgSrc?: string
  size?: 'large' | 'medium' | 'small'
}

const Avatar: React.FC<AvatarProps> = ({ imgSrc = '', imgAlt = '', size = 'small' }: AvatarProps) => (
  <AvatarContainer size={size}>{imgSrc && <Img src={imgSrc} alt={imgAlt} />}</AvatarContainer>
)

export default Avatar
