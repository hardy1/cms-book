import * as React from 'react'
import { render } from '@testing-library/react'
import Avatar from './Avatar'

describe('Avatar', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Avatar />)

    expect(component).toBeTruthy()
  })
})
