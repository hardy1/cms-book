import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Avatar, { AvatarProps } from './Avatar'

export default {
  title: 'Components/Avatar',
  component: Avatar,
} as Meta

const Template: Story<AvatarProps> = (args) => <Avatar {...args} />

export const Default = Template.bind({})
Default.args = {
  imgSrc: 'https://picsum.photos/100/100',
  imgAlt: 'Image alt',
}

export const Medium = Template.bind({})
Medium.args = {
  imgSrc: 'https://picsum.photos/100/100',
  imgAlt: 'Image alt',
  size: 'medium',
}

export const Large = Template.bind({})
Large.args = {
  imgSrc: 'https://picsum.photos/100/100',
  imgAlt: 'Image alt',
  size: 'large',
}
