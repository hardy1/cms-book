import styled from 'styled-components'
import colors from '../../resources/colors'

type AvatarProps = {
  size: 'large' | 'medium' | 'small'
}

export const AvatarContainer = styled.div<AvatarProps>`
  background-color: ${colors.lightGray};
  border-radius: 50%;
  overflow: hidden;

  ${(props) =>
    props.size === 'small' &&
    `
      height: 30px;
      width: 30px;
    `};

  ${(props) =>
    props.size === 'medium' &&
    `
      height: 60px;
      width: 60px;
    `};

  ${(props) =>
    props.size === 'large' &&
    `
      height: 100px;
      width: 100px;
    `};
`

export const Img = styled.img`
  object-fit: cover;
  height: 100%;
  width: 100%;
`
