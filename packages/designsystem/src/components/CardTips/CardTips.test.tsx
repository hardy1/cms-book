import * as React from 'react'
import { render } from '@testing-library/react'
import CardTips from './CardTips'

describe('CardTips', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardTips date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
