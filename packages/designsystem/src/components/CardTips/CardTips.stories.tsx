import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardTips, { CardTipsProps } from './CardTips'

export default {
  title: 'Components/CardTips',
  component: CardTips,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
  },
} as Meta

const Template: Story<CardTipsProps> = (args) => (
  <StoryContainer width={350}>
    <CardTips {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Dica – Acesso ao tratamento para dor do paciente oncológico',
  category: 'Dica de oncologia',
  date: new Date('Jul 13 2020'),
  url: '#',
}
