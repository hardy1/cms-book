import * as React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { IconPlayTip } from '../../icons'
import { Link, Container, IconContainer, Category, Title, Date } from './CardTips.styles'

export type CardTipsProps = {
  category?: string
  date: number | Date | undefined
  title?: string
  url?: string
}

const CardTips: React.FC<CardTipsProps> = ({ category = '', date, title = '', url = '' }: CardTipsProps) => {
  const hasCategory = category && <Category>{category}</Category>

  const hasTitle = title && (
    <Title className={!category ? 'noCategory' : ''} title={title}>
      {title}
    </Title>
  )

  const hasDate = date && <Date>{format(date, 'PPP', { locale: ptBR })}</Date>

  return (
    <Link href={url}>
      <Container>
        {hasCategory}
        {hasTitle}
        {hasDate}

        <IconContainer>
          <IconPlayTip />
        </IconContainer>
      </Container>
    </Link>
  )
}

export default CardTips
