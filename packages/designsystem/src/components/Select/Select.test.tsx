import * as React from 'react'
import { render } from '@testing-library/react'
import Select from './Select'

describe('Select', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Select id="selectId" />)

    expect(component).toBeTruthy()
  })
})
