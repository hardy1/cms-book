import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Select, { SelectProps } from './Select'

export default {
  title: 'Components/Select',
  component: Select,
} as Meta

const Template: Story<SelectProps> = (args) => <Select {...args} />

export const Default = Template.bind({})
Default.args = {
  id: 'selectId',
  placeholder: 'Select',
  internalValue: true,
  items: [
    { id: 1, value: 'item1', name: 'Item 1' },
    { id: 2, value: 'item2', name: 'Item 2' },
    { id: 3, value: 'item3', name: 'Item 3' },
  ],
}

export const WithLabel = Template.bind({})
WithLabel.args = {
  id: 'selectId',
  label: 'Label',
  placeholder: 'Select',
  internalValue: true,
  items: [
    { id: 1, value: 'item1', name: 'Item 1' },
    { id: 2, value: 'item2', name: 'Item 2' },
    { id: 3, value: 'item3', name: 'Item 3' },
  ],
}

export const Error = Template.bind({})
Error.args = {
  id: 'selectId',
  label: 'Label',
  placeholder: 'Select',
  internalValue: true,
  items: [
    { id: 1, value: 'item1', name: 'Item 1' },
    { id: 2, value: 'item2', name: 'Item 2' },
    { id: 3, value: 'item3', name: 'Item 3' },
  ],
  error: true,
  message: 'Message',
}
