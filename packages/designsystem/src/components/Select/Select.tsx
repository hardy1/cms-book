import * as React from 'react'
import { IconSelect } from '../../icons'
import { SelectStyled, SelectLabel, SelectContainer, SelectComponent, SelectIcon, SelectMessage } from './Select.styles'

export type ListProps = {
  id?: number | string
  name: string
  value?: string
}

export type SelectProps = {
  error?: boolean
  id: string
  items?: ListProps[] | undefined
  label?: string
  message?: string
  placeholder?: string
  handleChange?: (e: React.ChangeEvent<HTMLSelectElement>) => void
  onChange?: (e: React.ChangeEvent<HTMLSelectElement>) => void
  autoComplete?: string
  value?: string
  disabled?: boolean
  internalValue?: boolean
}

const Select: React.FC<SelectProps> = ({
  error = false,
  id,
  items,
  label = '',
  message = '',
  placeholder = '',
  onChange = undefined,
  handleChange = undefined,
  autoComplete = '',
  value = '',
  disabled = false,
  internalValue = false,
}: SelectProps) => {
  const [currentValue, setCurrentValue] = React.useState<string>(value)

  const handleValue = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    setCurrentValue(e?.target?.value)
  }

  return (
    <SelectStyled>
      {label && <SelectLabel htmlFor={id}>{label}</SelectLabel>}
      <SelectContainer error={error} disabled={disabled}>
        <SelectComponent
          id={id}
          error={error}
          onChange={internalValue ? handleValue : onChange || handleChange}
          autoComplete={autoComplete}
          defaultValue={currentValue}
          disabled={disabled}
        >
          {placeholder && (
            <option value="" disabled>
              {placeholder}
            </option>
          )}

          {items &&
            items.map(({ id, value, name }) => (
              <option key={id} value={value}>
                {name}
              </option>
            ))}
        </SelectComponent>
        <SelectIcon error={error} disabled={disabled}>
          <IconSelect />
        </SelectIcon>
      </SelectContainer>
      {error && <SelectMessage className="SelectErrorMessage">{message}</SelectMessage>}
    </SelectStyled>
  )
}

export default Select
