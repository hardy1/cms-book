import styled from 'styled-components'
import colors from '../../resources/colors'

type SelectProps = {
  error: boolean
  disabled: boolean
}

export const SelectStyled = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  justify-content: center;
  width: auto;
`

export const SelectLabel = styled.label`
  color: ${colors.disabledColor};
  font-size: 1em;
  font-weight: 400;
  line-height: 19px;
  margin-bottom: 12px;
  text-transform: uppercase;
  letter-spacing: 0.05em;
`

export const SelectContainer = styled.div<SelectProps>`
  height: 50px;
  position: relative;
  width: 100%;
  background-color: ${(props) => (props.disabled ? colors.lightColor : colors.white)};
  border-color: ${(props) => (props.error ? `${colors.errorColor}` : `${colors.disabledColor}`)};
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
`

export const SelectComponent = styled.select<SelectProps>`
  appearance: none;
  border: none;
  background: transparent;
  color: ${(props) => (props.error ? `${colors.errorColor}` : `${colors.neutralColor}`)};
  font-size: 1em;
  height: 50px;
  outline: none;
  padding: 0 30px 0 10px;
  width: 100%;

  &::placeholder {
    color: ${colors.disabledColor};
  }

  &:focus {
    border-color: ${colors.primaryColor};
    color: ${colors.primaryColor};
  }
`

export const SelectIcon = styled.span<SelectProps>`
  align-items: center;
  display: flex;
  height: 50px;
  justify-content: center;
  right: 0;
  margin-top: -25px;
  pointer-events: none;
  position: absolute;
  top: 50%;
  width: 30px;

  & svg path {
    fill: ${(props) => (props.error ? colors.errorColor : props.disabled ? colors.disabledColor : colors.primaryColor)};
  }
`

export const SelectMessage = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.errorColor};
  font-size: 1em;
  margin-top: 12px;
`
