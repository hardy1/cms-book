import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ContactSection from './ContactSection'

export default {
  title: 'Components/ContactSection',
  component: ContactSection,
  parameters: {
    controls: { hideNoControlsWarning: true },
    viewport: {
      defaultViewport: 'mobile1',
    },
  },
} as Meta

export const Default: React.FC = () => <ContactSection />
