import * as React from 'react'
import { render } from '@testing-library/react'
import ContactSection from './ContactSection'

describe('ContactSection', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<ContactSection>{children}</ContactSection>)

    expect(component).toBeTruthy()
  })
})
