import styled from 'styled-components'
import colors from '../../resources/colors'

export const ContactSectionContainer = styled.div`
  width: 264px;
  text-align: left;
  color: ${colors.neutralColor};

  a {
    color: ${colors.secondaryColor};
    text-decoration: none;
  }
`
