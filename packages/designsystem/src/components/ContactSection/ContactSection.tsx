import React from 'react'
import { ContactSectionContainer } from './ContactSection.styles'

type ContactSectionProps = {
  children?: React.ReactNode
}

const ContactSection: React.FC = ({ children }: ContactSectionProps) => (
  <ContactSectionContainer>{children}</ContactSectionContainer>
)

export default ContactSection
