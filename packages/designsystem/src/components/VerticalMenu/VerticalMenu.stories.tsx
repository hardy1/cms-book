import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import VerticalMenu, { VerticalMenuProps } from './VerticalMenu'

export default {
  title: 'Components/VerticalMenu',
  component: VerticalMenu,
} as Meta

const Template: Story<VerticalMenuProps> = (args) => <VerticalMenu {...args} />

export const Default = Template.bind({})
Default.args = {
  label: 'Livros',
  items: [
    { id: 1, value: 'Tumores Sólidos' },
    { id: 2, value: 'Hemato' },
    { id: 3, value: 'Drogas' },
    { id: 4, value: 'Tumores Raros' },
    { id: 5, value: 'Manejo de Toxicidades' },
    { id: 6, value: 'Cuidados de Suporte' },
    { id: 7, value: 'Diagnóstico Diferencial' },
    { id: 8, value: 'Enfermagem' },
  ],
}
