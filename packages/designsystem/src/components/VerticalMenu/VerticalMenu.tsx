import * as React from 'react'
import { StyledVerticalMenu } from './VerticalMenu.styles'
import VerticalMenuItem from '../VerticalMenuItem'

type VerticalMenuItemProps = {
  id: number
  url?: string
  value: string
}

export type VerticalMenuProps = {
  items?: VerticalMenuItemProps[] | undefined
  label?: string
}

const VerticalMenu: React.FC<VerticalMenuProps> = ({ items, label }: VerticalMenuProps) => (
  <StyledVerticalMenu>
    <strong>{label}</strong>
    {items && items.map(({ id, url = '', value }) => <VerticalMenuItem key={id} value={value} url={url} />)}
  </StyledVerticalMenu>
)

export default VerticalMenu
