import styled from 'styled-components'
import colors from '../../resources/colors'

export const StyledVerticalMenu = styled.ul`
  font-family: 'Lato', sans-serif;
  text-align: left;
  color: ${colors.neutralColor};
  font-size: 1em;
  line-height: 170%;
  list-style: none;
  padding: 0;
  width: 180px;
  margin-top: 0;
  margin-bottom: 10px;

  & > strong {
    display: inline-block;
    margin-bottom: 10px;
  }
`
