import * as React from 'react'
import { render } from '@testing-library/react'
import VerticalMenu from './VerticalMenu'

describe('VerticalMenu', () => {
  it('SHOULD render correctly', () => {
    const component = render(<VerticalMenu label="Test">Test</VerticalMenu>)

    expect(component).toBeTruthy()
  })
})
