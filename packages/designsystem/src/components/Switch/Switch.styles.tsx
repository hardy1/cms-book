import styled from 'styled-components'

type StyledSwitchProps = {
  width?: string
}

const StyledSwitch = styled.div<StyledSwitchProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: ${({ width }) => width || '100%'};
  height: 55px;
  border-width: 1px;
  border-style: solid;
  border-color: #9ea0a5;
  border-radius: 4px;
  box-sizing: border-box;
  padding: 4px;

  .Switch__Button {
    margin: 0;
    width: calc(50% - 2px);
    height: 47px;
    background-color: transparent;
    border-width: 0;
    border-radius: 4px;
    box-sizing: border-box;
    color: #3e3f42;
    font-family: 'Lato', sans-serif;
    font-size: 0.875rem;
    line-height: 1.4;
    padding: 10px 10px;
    cursor: pointer;
    transition: background-color 0.2s ease-out, color 0.2s ease-out;

    span {
      display: none;

      @media screen and (min-width: 768px) {
        display: inline-block;
      }
    }

    @media screen and (min-width: 768px) {
      font-size: 1rem;
      padding: 10px 15px;
    }

    &.active {
      background-color: #216de1;
      color: #fff;
      pointer-events: none;
    }
  }
`

export { StyledSwitch }
