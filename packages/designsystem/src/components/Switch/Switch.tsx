import * as React from 'react'
import { StyledSwitch } from './Switch.styles'

export type SwitchProps = {
  as?: React.ElementType
  className?: string
  children?: React.ReactNode
  id?: string
  width?: string
}

const Box: React.FC<SwitchProps> = ({ as = 'div', className, children, id, width }: SwitchProps) => (
  <StyledSwitch as={as} className={className} id={id} width={width}>
    {children}
  </StyledSwitch>
)

export default Box
