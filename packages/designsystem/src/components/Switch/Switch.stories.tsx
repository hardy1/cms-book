import React, { useState } from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Switch from './Switch'

export default {
  title: 'Components/Switch',
  component: Switch,
} as Meta

const Template: Story = (args) => {
  const BUTTON_ONE = 'BUTTON_ONE'
  const BUTTON_TWO = 'BUTTON_TWO'

  const [activeButton, setActiveButton] = useState(BUTTON_ONE)

  return (
    <Switch {...args}>
      <button
        type="button"
        className={`Switch__Button ${activeButton === BUTTON_ONE ? 'active' : ''}`}
        onClick={() => setActiveButton(BUTTON_ONE)}
      >
        Button one
      </button>

      <button
        type="button"
        className={`Switch__Button ${activeButton === BUTTON_TWO ? 'active' : ''}`}
        onClick={() => setActiveButton(BUTTON_TWO)}
      >
        Button two
      </button>
    </Switch>
  )
}

export const Default = Template.bind({})

export const WithWidth = Template.bind({})

WithWidth.args = {
  width: '350px',
}
