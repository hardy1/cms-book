import * as React from 'react'
import { render } from '@testing-library/react'
import Switch from './Switch'

describe('Switch', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Switch id="selectId" />)

    expect(component).toBeTruthy()
  })

  it('SHOULD render as section', () => {
    const component = render(<Switch as="section" id="selectId" />)
    const componentType = component.container.querySelector('#selectId')?.tagName

    expect(componentType).toBe('SECTION')
  })

  it('SHOULD render as article', () => {
    const component = render(<Switch as="article" id="selectId" />)
    const componentType = component.container.querySelector('#selectId')?.tagName

    expect(componentType).toBe('ARTICLE')
  })

  it('SHOULD create based on id prop', () => {
    const component = render(<Switch id="selectId" />)
    const componentType = component.container.querySelector('#selectId')

    expect(componentType).toBeTruthy()
  })

  it('SHOULD have width prop', () => {
    const component = render(<Switch width="100px" id="selectId" />)
    const componentById = component.container.querySelector('#selectId')?.getAttribute('width')

    expect(componentById).toBe('100px')
  })

  it('SHOULD have width prop in em, rem or px', () => {
    const component = render(<Switch width="100" id="selectId" />)
    const componentWithUnity = render(<Switch width="100px" id="selectId" />)
    const componentById = component.container.querySelector('#selectId')?.getAttribute('width')
    const componentWithUnityById = componentWithUnity.container.querySelector('#selectId')?.getAttribute('width')

    expect(componentById).not.toMatch(/(em)|(rem)|(px)/g)
    expect(componentWithUnityById).toMatch(/(em)|(rem)|(px)/g)
  })
})
