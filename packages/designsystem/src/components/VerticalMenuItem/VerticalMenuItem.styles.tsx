import styled from 'styled-components'
import colors from '../../resources/colors'

export const VerticalMenuItemContainer = styled.div`
  font-family: 'Lato', sans-serif;
  color: ${colors.neutralColor};
  line-height: 170%;
  font-size: 1em;
  cursor: pointer;
`

export const VerticalMenuItemLink = styled.a`
  color: ${colors.neutralColor};
  text-decoration: none;
`
