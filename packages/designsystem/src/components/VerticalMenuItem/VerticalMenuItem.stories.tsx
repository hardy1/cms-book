import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import VerticalMenuItem, { VerticalMenuItemProps } from './VerticalMenuItem'

export default {
  title: 'Components/VerticalMenuItem',
  component: VerticalMenuItem,
} as Meta

const Template: Story<VerticalMenuItemProps> = (args) => <VerticalMenuItem {...args} />

export const Default = Template.bind({})
Default.args = {
  url: '#',
  value: 'Item',
}
