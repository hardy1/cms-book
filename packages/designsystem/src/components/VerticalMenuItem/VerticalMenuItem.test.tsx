import * as React from 'react'
import { render } from '@testing-library/react'
import VerticalMenuItem from './VerticalMenuItem'

describe('VerticalMenuItem', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<VerticalMenuItem value={children} />)

    expect(component).toBeTruthy()
  })
})
