import * as React from 'react'
import { VerticalMenuItemContainer, VerticalMenuItemLink } from './VerticalMenuItem.styles'

export type VerticalMenuItemProps = {
  url?: string
  value: string
}

const VerticalMenuItem: React.FC<VerticalMenuItemProps> = ({ url = '', value }: VerticalMenuItemProps) => (
  <VerticalMenuItemContainer>
    <VerticalMenuItemLink href={url}>{value}</VerticalMenuItemLink>
  </VerticalMenuItemContainer>
)

export default VerticalMenuItem
