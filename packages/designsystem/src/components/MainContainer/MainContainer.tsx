import * as React from 'react'

import { MainContainerStyled } from './MainContainer.styles'

type MainContainerProps = {
  align?: string
  as?: React.ElementType
  children: React.ReactNode
  fluid?: boolean
  direction?: string
  noSpacement?: boolean
}

const MainContainer: React.FC<MainContainerProps> = ({
  align = 'flex-start',
  as = 'div',
  children,
  fluid = false,
  direction = 'row',
  noSpacement = false,
}: MainContainerProps) => (
  <MainContainerStyled as={as} align={align} fluid={fluid} direction={direction} noSpacement={noSpacement}>
    {children}
  </MainContainerStyled>
)

export default MainContainer

export { MainContainerProps }
