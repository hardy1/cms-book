import styled, { css } from 'styled-components'

type MainContainer = {
  align: 'center' | 'flex-start' | 'flex-end'
  fluid?: boolean
  direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse'
  gridPosition?: string
  noSpacement?: boolean
}

interface ISetPaddingProps {
  noSpacement?: boolean
  padding?: string
}

interface ISetIsFluidProps {
  fluid?: boolean
  maxWidth?: string
}

interface ISetDirectionProps {
  direction: string
}

const setPadding = ({ noSpacement, padding }: ISetPaddingProps) => {
  const currentPadding = noSpacement ? 0 : padding

  return css`
    padding-right: ${currentPadding};
    padding-left: ${currentPadding};
  `
}

const setIsFluid = ({ fluid, maxWidth }: ISetIsFluidProps) => {
  if (fluid) {
    return css`
      width: 100%;
    `
  } else {
    return css`
      max-width: ${maxWidth};
    `
  }
}

const setDirection = ({ direction }: ISetDirectionProps) =>
  direction &&
  css`
    flex-direction: ${direction};
  `

const MainContainerStyled = styled.div<MainContainer>`
  position: relative;
  display: flex;
  width: 100%;
  align-items: ${({ align }) => align || 'flex-start'};
  ${({ direction = '' }) => setDirection({ direction })};
  box-sizing: border-box;
  ${({ noSpacement }) => setPadding({ noSpacement, padding: '16px' })};
  margin-right: auto;
  margin-left: auto;

  ::before,
  ::after {
    position: inherit;
    box-sizing: inherit;
  }

  @media screen and (min-width: 1024px) {
    ${({ fluid }) => setIsFluid({ fluid, maxWidth: '960px' })}
    ${({ noSpacement }) => setPadding({ noSpacement, padding: '24px' })};
  }

  @media screen and (min-width: 1366px) {
    ${({ fluid }) => setIsFluid({ fluid, maxWidth: '1140px' })}
  }

  @media screen and (min-width: 1920px) {
    ${({ fluid }) => setIsFluid({ fluid, maxWidth: '1320px' })}
  }
`

export { MainContainerStyled }
