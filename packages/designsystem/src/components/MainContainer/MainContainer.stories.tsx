import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import MainContainer, { MainContainerProps } from './MainContainer'

const alignOptions = ['center', 'flex-start', 'flex-end']

const directionOptions = ['row', 'row-reverse', 'column', 'column-reverse']

export default {
  title: 'Components/MainContainer',
  component: MainContainer,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
  argTypes: {
    align: {
      options: alignOptions,
      control: { type: 'select' },
      table: {
        defaultValue: { summary: 'flex-start' },
      },
    },
    fluid: {
      table: {
        defaultValue: { summary: 'false' },
      },
    },
    direction: {
      options: directionOptions,
      control: { type: 'select' },
      table: {
        defaultValue: { summary: 'row' },
      },
    },
    noSpacement: {
      table: {
        defaultValue: { summary: 'false' },
      },
    },
  },
} as Meta

const paragraphStyle = {
  width: '50%',
  border: '1px solid #333',
  color: '#222',
  fontFamily: 'sans-serif',
  fontSize: '20px',
  padding: '8px',
  margin: 0,
}

const Template: Story<MainContainerProps> = (args) => (
  <MainContainer {...args}>
    <p style={paragraphStyle}>Primeiro Texto</p>
    <p style={paragraphStyle}>Segundo Texto</p>
  </MainContainer>
)

export const Default = Template.bind({})
Default.args = {}
