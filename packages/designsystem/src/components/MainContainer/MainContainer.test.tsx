import React from 'react'
import { render } from '@testing-library/react'
import MainContainer from './MainContainer'

describe('MainContainer', () => {
  it('SHOULD render correctly', () => {
    const component = render(<MainContainer>Text</MainContainer>)

    expect(component).toBeTruthy()
  })
})
