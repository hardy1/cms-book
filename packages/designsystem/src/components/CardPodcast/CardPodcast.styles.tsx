import styled from 'styled-components'
import colors from '../../resources/colors'

type CardPodcastProps = {
  imgSrc?: string
}

export const Link = styled.a`
  text-decoration: none;
`

export const Container = styled.div<CardPodcastProps>`
  align-items: flex-end;
  background-color: ${colors.neutralColor};
  background-image: url(${(props) => props.imgSrc});
  background-position: center center;
  background-size: cover;
  border-radius: 15px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  cursor: pointer;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  height: 335px;
  justify-content: center;
  padding: 15px;
`

export const IconContainer = styled.div`
  flex: 1;
`

export const Content = styled.div`
  align-self: stretch;
  background-color: ${colors.white};
  border-radius: 15px;
  display: flex;
  flex-direction: column;
  height: auto;
  justify-content: flex-end;
  min-height: 100px;
  padding: 15px;
`

export const Title = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.neutralColor};
  display: -webkit-box;
  font-size: 0.875em;
  line-height: 17px;
  margin: 0;
  flex: 1;
  overflow: hidden;
  width: 100%;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 4;
`

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 16px;
`

export const Date = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.disabledColor};
  font-size: 0.75em;
  text-transform: uppercase;
`

export const PlayersContainer = styled.div``

export const Players = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.disabledColor};
  font-size: 0.75em;
  margin-left: 6px;
`
