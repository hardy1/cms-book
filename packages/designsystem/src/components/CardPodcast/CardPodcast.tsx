import * as React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { IconHeadphone, IconPlay } from '../../icons'
import {
  Link,
  Container,
  IconContainer,
  Content,
  Title,
  PlayersContainer,
  Players,
  Date,
  Footer,
} from './CardPodcast.styles'
import { PlaceholderPodcast } from '../../assets'

export type CardPodcastProps = {
  date: number | Date | undefined
  imgSrc?: string
  players?: number | undefined
  title?: string
  url?: string
}

const CardPodcast: React.FC<CardPodcastProps> = ({
  date,
  imgSrc = '',
  players,
  title = '',
  url = '',
}: CardPodcastProps) => (
  <Link href={url}>
    <Container imgSrc={imgSrc || PlaceholderPodcast}>
      <IconContainer>
        <IconHeadphone />
      </IconContainer>
      <Content>
        {title && <Title title={title}>{title}</Title>}
        {(date || players) && (
          <Footer>
            {date && <Date>{format(date, 'PPP', { locale: ptBR })}</Date>}
            {players && (
              <PlayersContainer>
                <IconPlay />
                <Players>{players}</Players>
              </PlayersContainer>
            )}
          </Footer>
        )}
      </Content>
    </Container>
  </Link>
)

export default CardPodcast
