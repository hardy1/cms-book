import * as React from 'react'
import { render } from '@testing-library/react'
import CardPodcast from './CardPodcast'

describe('CardPodcast', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardPodcast date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
