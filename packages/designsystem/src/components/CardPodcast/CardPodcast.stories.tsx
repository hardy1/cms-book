import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardPodcast, { CardPodcastProps } from './CardPodcast'

export default {
  title: 'Components/CardPodcast',
  component: CardPodcast,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
    players: {
      control: {
        type: 'number',
      },
    },
  },
} as Meta

const Template: Story<CardPodcastProps> = (args) => (
  <StoryContainer width={350}>
    <CardPodcast {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title:
    'Podcast #033 – Associação de DNA tumoral circulante e células tumorais circulantes no câncer de mama triplo-negativo',
  date: new Date('Jul 13 2020'),
  players: 300,
  imgSrc: 'https://picsum.photos/300/330',
  url: '#',
}
