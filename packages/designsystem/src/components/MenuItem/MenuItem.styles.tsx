import styled from 'styled-components'
import colors from '../../resources/colors'

export const MenuItemContainer = styled.li`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  text-align: left;
  font-size: 15px;
  line-height: 170%;
  cursor: pointer;
`

export const MenuItemLink = styled.a`
  color: ${colors.neutralColor};
  text-decoration: none;
`
