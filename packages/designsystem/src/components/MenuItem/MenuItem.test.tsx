import * as React from 'react'
import { render } from '@testing-library/react'
import MenuItem from './MenuItem'

describe('MenuItem', () => {
  const label = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<MenuItem value={label} />)

    expect(component).toBeTruthy()
  })
})
