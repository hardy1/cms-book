import * as React from 'react'
import { MenuItemContainer, MenuItemLink } from './MenuItem.styles'

export type MenuItemProps = {
  url?: string
  value: string
}

const MenuItem: React.FC<MenuItemProps> = ({ url = '', value = '' }: MenuItemProps) => (
  <MenuItemContainer>{value && <MenuItemLink href={url}>{value}</MenuItemLink>}</MenuItemContainer>
)

export default MenuItem
