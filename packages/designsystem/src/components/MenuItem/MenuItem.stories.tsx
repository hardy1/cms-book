import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import MenuItem, { MenuItemProps } from './MenuItem'

export default {
  title: 'Components/MenuItem',
  component: MenuItem,
} as Meta

const Template: Story<MenuItemProps> = (args) => <MenuItem {...args} />

export const Default = Template.bind({})
Default.args = {
  value: 'Item',
}
