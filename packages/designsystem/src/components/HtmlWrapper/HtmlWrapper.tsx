import * as React from 'react'
import { HtmlWrapperContainer } from './HtmlWrapper.styles'

export type HtmlWrapperProps = {
  children: React.ReactNode
}

const HtmlWrapper: React.FC<HtmlWrapperProps> = ({ children }: HtmlWrapperProps) => (
  <HtmlWrapperContainer>{children}</HtmlWrapperContainer>
)

export default HtmlWrapper
