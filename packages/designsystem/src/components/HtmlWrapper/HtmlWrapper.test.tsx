import * as React from 'react'
import { render } from '@testing-library/react'
import HtmlWrapper from './HtmlWrapper'

describe('HtmlWrapper', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<HtmlWrapper>{children}</HtmlWrapper>)

    expect(component).toBeTruthy()
  })
})
