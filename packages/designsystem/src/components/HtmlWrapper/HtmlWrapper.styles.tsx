import { size } from '../../resources/media'
import styled from 'styled-components'
import colors from '../../resources/colors'

export const HtmlWrapperContainer = styled.div`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;

  /* img {
    width: 100% !important;
  } */

  & h1 {
    font-size: 2.375rem;
    line-height: 1.5rem;
    margin-bottom: 0;
  }

  & h2 {
    font-size: 1.875rem;
    line-height: 1.2rem;
    margin-bottom: 0;
  }

  & h3 {
    font-size: 1.5rem;
    line-height: 1.2rem;
    margin-bottom: 0;
  }

  & p,
  div {
    font-size: 1rem;
    line-height: 1.8rem;
    margin-bottom: 0;
  }

  & a {
    color: ${colors.primaryColor};
    font-size: 1rem;
  }

  & ul {
    margin-bottom: 0;
  }

  & iframe {
    display: block;
    width: 100% !important;
    padding-top: 1rem;
    padding-bottom: 1.5rem;

    @media screen and (min-width: ${size.tablet}) {
      width: 640px;
      height: 360px;
    }
  }
`
