import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import HtmlWrapper, { HtmlWrapperProps } from './HtmlWrapper'

export default {
  title: 'Components/HtmlWrapper',
  component: HtmlWrapper,
} as Meta

const Template: Story<HtmlWrapperProps> = (args) => <HtmlWrapper {...args} />

export const Default = Template.bind({})
Default.args = {
  children: (
    <>
      <h1>Lorem ipsum</h1>
      <h2>Lorem ipsum</h2>
      <h3>Lorem ipsum</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.<a href="#">Vestibulum</a> suscipit quis neque quis
        blandit. Pellentesque quis suscipit quam, et mollis nunc.
      </p>
      <ul>
        <li>Lorem ipsum</li>
        <li>Lorem ipsum</li>
      </ul>
    </>
  ),
}
