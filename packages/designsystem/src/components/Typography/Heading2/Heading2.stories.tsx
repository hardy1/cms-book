import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Heading2, { Heading2Props } from './Heading2'

export default {
  title: 'Components/Typography/Heading2',
  component: Heading2,
} as Meta

const Template: Story<Heading2Props> = (args) => <Heading2 {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Canais Moc',
  color: 'primaryColor',
}
