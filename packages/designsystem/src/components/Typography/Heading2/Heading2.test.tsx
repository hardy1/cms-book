import * as React from 'react'
import { render } from '@testing-library/react'
import Heading2 from './Heading2'

describe('Typography/Heading2', () => {
  const text = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<Heading2>{text}</Heading2>)

    expect(component).toBeTruthy()
  })
})
