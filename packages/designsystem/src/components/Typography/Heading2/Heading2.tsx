import * as React from 'react'
import styled from 'styled-components'
import colors from '../../../resources/colors'

const Title = styled.h2`
  display: block;
  font-family: 'Montserrat', sans-serif;
  font-size: 1.875rem;
  font-style: normal;
  font-weight: bold;
  line-height: 1.2;
  margin: 0;
  padding: 0;

  ${(props) => props.color === 'primaryColor' && `color: ${colors.primaryColor};`};
  ${(props) => props.color === 'disabledColor' && `color: ${colors.disabledColor};`};
`

export type Heading2Props = {
  children: string
  color?: 'primaryColor' | 'disabledColor'
}

const Heading2: React.FC<Heading2Props> = ({ color = 'primaryColor', children }: Heading2Props) => (
  <Title color={color}>{children}</Title>
)

export default Heading2
