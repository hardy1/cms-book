import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Heading6, { Heading6Props } from './Heading6'

export default {
  title: 'Components/Typography/Heading6',
  component: Heading6,
} as Meta

const Template: Story<Heading6Props> = (args) => <Heading6 {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Canais Moc',
  color: 'primaryColor',
}
