import * as React from 'react'
import { render } from '@testing-library/react'
import Heading6 from './Heading6'

describe('Typography/Heading6', () => {
  const text = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<Heading6>{text}</Heading6>)

    expect(component).toBeTruthy()
  })
})
