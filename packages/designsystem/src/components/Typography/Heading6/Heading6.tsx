import * as React from 'react'
import styled from 'styled-components'
import colors from '../../../resources/colors'

const Title = styled.h6`
  display: block;
  font-family: 'Montserrat', sans-serif;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: bold;
  line-height: 1.2;
  margin: 0;
  padding: 0;
  text-transform: uppercase;

  ${(props) => props.color === 'primaryColor' && `color: ${colors.primaryColor};`};
  ${(props) => props.color === 'disabledColor' && `color: ${colors.disabledColor};`};
`

export type Heading6Props = {
  children: string
  color?: 'primaryColor' | 'disabledColor'
}

const Heading6: React.FC<Heading6Props> = ({ color = 'primaryColor', children }: Heading6Props) => (
  <Title color={color}>{children}</Title>
)

export default Heading6
