import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Heading3, { Heading3Props } from './Heading3'

export default {
  title: 'Components/Typography/Heading3',
  component: Heading3,
} as Meta

const Template: Story<Heading3Props> = (args) => <Heading3 {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Canais Moc',
  color: 'primaryColor',
}
