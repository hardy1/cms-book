import * as React from 'react'
import styled from 'styled-components'
import colors from '../../../resources/colors'

const Title = styled.h3`
  display: block;
  font-family: 'Montserrat', sans-serif;
  font-size: 1.5rem;
  font-style: normal;
  font-weight: bold;
  line-height: 1.2;
  margin: 0;
  padding: 0;

  ${(props) => props.color === 'primaryColor' && `color: ${colors.primaryColor};`};
  ${(props) => props.color === 'disabledColor' && `color: ${colors.disabledColor};`};
`

export type Heading3Props = {
  children: string
  color?: 'primaryColor' | 'disabledColor'
}

const Heading3: React.FC<Heading3Props> = ({ color = 'primaryColor', children }: Heading3Props) => (
  <Title color={color}>{children}</Title>
)

export default Heading3
