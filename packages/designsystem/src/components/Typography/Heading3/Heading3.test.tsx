import * as React from 'react'
import { render } from '@testing-library/react'
import Heading3 from './Heading3'

describe('Typography/Heading3', () => {
  const text = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<Heading3>{text}</Heading3>)

    expect(component).toBeTruthy()
  })
})
