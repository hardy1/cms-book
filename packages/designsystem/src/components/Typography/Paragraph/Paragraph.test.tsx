import * as React from 'react'
import { render } from '@testing-library/react'
import Paragraph from './Paragraph'

describe('Typography/Paragraph', () => {
  const text = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<Paragraph>{text}</Paragraph>)

    expect(component).toBeTruthy()
  })
})
