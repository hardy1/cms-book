import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Paragraph, { ParagraphProps } from './Paragraph'

export default {
  title: 'Components/Typography/Paragraph',
  component: Paragraph,
} as Meta

const Template: Story<ParagraphProps> = (args) => <Paragraph {...args} />

export const Default = Template.bind({})
Default.args = {
  children:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget ante sollicitudin, pulvinar enim sit amet, efficitur mauris. Donec vel laoreet leo, ac volutpat metus. Nunc in quam quis justo auctor laoreet nec vitae odio. Aenean sed porta lectus. Sed at odio velit. Proin sed pulvinar magna. Etiam ut neque elementum, iaculis erat sit amet, aliquet felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
}

export const PrimaryColor = Template.bind({})
PrimaryColor.args = {
  color: 'primaryColor',
  children:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget ante sollicitudin, pulvinar enim sit amet, efficitur mauris. Donec vel laoreet leo, ac volutpat metus. Nunc in quam quis justo auctor laoreet nec vitae odio. Aenean sed porta lectus. Sed at odio velit. Proin sed pulvinar magna. Etiam ut neque elementum, iaculis erat sit amet, aliquet felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
}
