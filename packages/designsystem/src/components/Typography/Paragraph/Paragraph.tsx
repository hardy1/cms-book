import * as React from 'react'
import styled from 'styled-components'
import colors from '../../../resources/colors'

const StyledParagraph = styled.p`
  color: ${colors.neutralColor};
  display: block;
  font-family: 'Lato', sans-serif;
  margin: 0;
  padding: 0;

  ${(props) => props.color === 'neutralColor' && `color: ${colors.neutralColor};`};
  ${(props) => props.color === 'primaryColor' && `color: ${colors.primaryColor};`};
`

export type ParagraphProps = {
  children: React.ReactNode
  color?: 'neutralColor' | 'primaryColor'
}

const Paragraph: React.FC<ParagraphProps> = ({ children, color = 'neutralColor' }: ParagraphProps) => (
  <StyledParagraph color={color}>{children}</StyledParagraph>
)

export default Paragraph
