import * as React from 'react'
import styled from 'styled-components'

import colors from '../../../resources/colors'
import media from '../../../resources/media'

const Title = styled.h1`
  display: block;
  font-family: 'Montserrat', sans-serif;
  font-size: 2.375rem;
  font-style: normal;
  font-weight: bold;
  line-height: 1.5;
  margin: 0;
  padding: 0;

  ${({ color }) => color === 'black' && `color: ${colors.black};`};

  ${({ color }) => color === 'primaryColor' && `color: ${colors.primaryColor};`};

  ${({ color }) => color === 'disabledColor' && `color: ${colors.disabledColor};`};

  @media ${media.tabletM} {
    font-size: 2rem;
  }
`

export type Heading1Props = {
  as?: React.ElementType
  className?: string
  children: string | undefined
  color?: 'primaryColor' | 'disabledColor' | 'black'
}

const Heading1: React.FC<Heading1Props> = ({
  as = 'h1',
  className,
  color = 'primaryColor',
  children,
}: Heading1Props) => (
  <Title as={as} className={className} color={color}>
    {children}
  </Title>
)

export default Heading1
