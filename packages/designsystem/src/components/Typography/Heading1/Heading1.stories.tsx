import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Heading1, { Heading1Props } from './Heading1'

export default {
  title: 'Components/Typography/Heading1',
  component: Heading1,
} as Meta

const Template: Story<Heading1Props> = (args) => <Heading1 {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Canais Moc',
  color: 'primaryColor',
}
