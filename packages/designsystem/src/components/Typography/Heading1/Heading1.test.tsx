import * as React from 'react'
import { render } from '@testing-library/react'
import Heading1 from './Heading1'

describe('Typography/Heading1', () => {
  const text = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<Heading1>{text}</Heading1>)

    expect(component).toBeTruthy()
  })
})
