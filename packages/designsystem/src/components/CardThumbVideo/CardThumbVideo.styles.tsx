import styled from 'styled-components'
import colors from '../../resources/colors'

interface CardThumbVideoProps {
  backgroundUrl?: string
  href?: string
}

export interface IText {
  titleUppercase?: boolean
  titleLowercase?: boolean
  uppercase?: boolean
  lowercase?: boolean
}

export const Wrapper = styled.div`
  display: flex;
  margin-bottom: 24px;
`

export const Container = styled.div`
  background-color: ${colors.primaryColor};
  cursor: pointer;
  display: flex;
  font-family: 'Lato', sans-serif;
  flex-direction: column;
  height: 100%;
  width: 100%;
  justify-content: space-between;
`

export const Header = styled.div<CardThumbVideoProps>`
  background-color: ${colors.neutralColor};
  background-image: url(${(props) => props.backgroundUrl});
  background-position: center center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 150px;
`

export const Content = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  height: auto;
  padding: 15px;
`

export const Link = styled.a`
  text-decoration: none;
`

export const Subtitle = styled.h5<IText>`
  color: ${colors.quaternaryColor};
  font-family: 'Lato', sans-serif;
  margin-bottom: 10px;
  font-size: 0.765em;
  font-weight: 400;
  margin: 0;
  ${({ uppercase }) => uppercase && 'text-transform: uppercase;'}
  ${({ lowercase }) => lowercase && 'text-transform: lowercase;'}
`

export const Title = styled.h4<IText>`
  color: ${colors.white};
  display: -webkit-box;
  font-family: 'Montserrat', sans-serif;
  font-size: 0.875em;
  flex: 1;
  line-height: 17px;
  margin-bottom: 5px;
  margin: 16px 0 0 0;
  max-width: 270px;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 5;
  ${({ titleUppercase }) => titleUppercase && 'text-transform: uppercase;'}
  ${({ titleLowercase }) => titleLowercase && 'text-transform: lowercase;'}
`

export const Date = styled.span<IText>`
  font-family: 'Lato', sans-serif;
  color: ${colors.quaternaryColor};
  font-size: 0.625em;
  margin-top: 16px;
  ${({ uppercase }) => uppercase && 'text-transform: uppercase;'}
  ${({ lowercase }) => lowercase && 'text-transform: lowercase;'}
`

export const Footer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #eee;
  height: 48px;
  padding: 5px;
  box-sizing: border-box;
  align-items: center;

  a + a {
    margin-left: 4px;
  }
`

export const Img = styled.img`
  max-height: 40px;
  width: auto;
  height: auto;
`
