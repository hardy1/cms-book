import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardThumbVideo, { CardThumbVideoProps } from './CardThumbVideo'

export default {
  title: 'Components/CardThumbVideo',
  component: CardThumbVideo,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
  },
} as Meta

const Template: Story<CardThumbVideoProps> = (args) => (
  <StoryContainer width={350}>
    <CardThumbVideo {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Inibidores de ICDK4/6 não são todos iguais (foco em Abemaciclibe).',
  subtitle: 'Volume 11 - Número 06',
  date: new Date('Jul 13 2020'),
  imgSrc: 'https://picsum.photos/310/140',
  footerImg: 'https://fera.ag/wp-content/themes/fera-2020/assets/images/logo-icon.svg',
  url: '#',
}
