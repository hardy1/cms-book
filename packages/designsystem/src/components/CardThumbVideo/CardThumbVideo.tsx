import React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import {
  Wrapper,
  Container,
  Header,
  Content,
  Link,
  Title,
  Subtitle,
  Date,
  Footer,
  Img,
  IText,
} from './CardThumbVideo.styles'
import { IconPlayVideo } from '../../icons'
import { PlaceholderVideo } from '../../assets'

export interface CardThumbVideoProps extends IText {
  date: number | Date | undefined
  footerImg?: string
  imgSrc?: string
  subtitle?: string
  title?: string
  url?: string
}

const CardThumbVideo: React.FC<CardThumbVideoProps> = ({
  date,
  footerImg,
  imgSrc = '',
  subtitle = '',
  title = '',
  url = '',
  titleUppercase = false,
  titleLowercase = false,
  uppercase = false,
  lowercase = false,
}: CardThumbVideoProps) => (
  <Wrapper>
    <Container>
      <Link href={url}>
        <Header backgroundUrl={imgSrc || PlaceholderVideo}>
          <IconPlayVideo />
        </Header>
      </Link>

      <Link href={url}>
        <Content>
          {subtitle && (
            <Subtitle uppercase={uppercase} lowercase={lowercase}>
              {subtitle}
            </Subtitle>
          )}
          {title && (
            <Title title={title} titleUppercase={titleUppercase} titleLowercase={titleLowercase}>
              {title}
            </Title>
          )}
          {date && <Date>{format(date, 'PPP', { locale: ptBR })}</Date>}
        </Content>
        <Footer>{footerImg && <Img src={footerImg} />}</Footer>
      </Link>
    </Container>
  </Wrapper>
)

export default CardThumbVideo
