import * as React from 'react'
import { render } from '@testing-library/react'
import CardThumbVideo from './CardThumbVideo'

describe('CardThumbVideo', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardThumbVideo date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
