import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import Divider from './Divider'

export default {
  title: 'Components/Divider',
  component: Divider,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <Divider />
