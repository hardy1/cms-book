import styled from 'styled-components'
import colors from '../../resources/colors'

export const DividerStyled = styled.hr`
  background-color: ${colors.secondaryColor};
  border: 0;
  height: 4px;
  margin: 16px 0 0 0;
  width: 100%;
`
