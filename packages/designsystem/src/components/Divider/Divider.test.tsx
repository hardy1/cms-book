import * as React from 'react'
import { render } from '@testing-library/react'
import Divider from './Divider'

describe('Divider', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Divider />)

    expect(component).toBeTruthy()
  })
})
