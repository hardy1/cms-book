import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import CardRelated, { CardRelatedProps } from './CardRelated'

export default {
  title: 'Components/CardRelated',
  component: CardRelated,
} as Meta

const Template: Story<CardRelatedProps> = (args) => <CardRelated {...args} />

export const Default = Template.bind({})

Default.args = {
  title: 'Título do card',
  items: [
    {
      id: 1,
      title: 'O que muda no MOC após a ASCO 2020',
      category: 'Novidades',
      date: new Date('Jul 13 2020'),
      readingTime: '5 min. de leitura',
      url: '#',
    },
    {
      id: 2,
      title: 'Estendida aprovação de pembrolizumabe para o tratamento do Linfoma de Hodgkin nos EUA',
      category: 'Novidades',
      date: new Date('Jul 13 2020'),
      readingTime: '5 min. de leitura',
      url: '#',
    },
    {
      id: 3,
      title: 'EVENTOS | Meeting the Minds – Hematologia e Oncologia',
      category: 'Novidades',
      date: new Date('Jul 13 2020'),
      readingTime: '5 min. de leitura',
      url: '#',
    },
  ],
}
