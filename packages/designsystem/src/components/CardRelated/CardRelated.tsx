import * as React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import {
  Container,
  Divider,
  Content,
  Item,
  Title,
  ArticleTitle,
  Footer,
  Category,
  Date,
  ReadingTime,
  ContentWrapper,
} from './CardRelated.styles'

type ListProps = {
  date?: number | Date | undefined
  id?: number
  title?: string
  url?: string
  slug?: string
  readingTime?: number
  subtitle?: string
  categories?: [
    {
      id: number
      name: string
      slug: string
    },
  ]
}

export type CardRelatedProps = {
  items?: ListProps[] | undefined
  title?: string
}

const CardRelated: React.FC<CardRelatedProps> = ({ items, title }: CardRelatedProps) => (
  <div>
    <Container>
      {title && (
        <>
          <Title>{title}</Title>
          <Divider />
        </>
      )}

      <Content>
        {items &&
          items.map(({ categories = null, date, id, readingTime = '', title = '', url = '' }) => (
            <ContentWrapper key={id}>
              <ArticleTitle>
                <Item href={url}>{title}</Item>
              </ArticleTitle>
              {(categories || date || readingTime) && (
                <Footer>
                  {categories &&
                    categories.map((value, i) => {
                      if (categories.length > i + 1) {
                        return (
                          <Item key={value.id} href={`/canais-moc/noticias?category=${value.slug}`}>
                            <Category>{value.name}, </Category>
                          </Item>
                        )
                      } else {
                        return (
                          <Item key={value.id} href={`/canais-moc/noticias?category=${value.slug}`}>
                            <Category>{value.name}</Category>
                          </Item>
                        )
                      }
                    })}
                  {date && <Date>- {format(date, 'PPP', { locale: ptBR })}</Date>}
                  {readingTime && <ReadingTime>- {readingTime}</ReadingTime>}
                </Footer>
              )}
            </ContentWrapper>
          ))}
      </Content>
    </Container>
  </div>
)

export default CardRelated
