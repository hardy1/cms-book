import * as React from 'react'
import { render } from '@testing-library/react'
import CardRelated from './CardRelated'

describe('CardRelated', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardRelated />)

    expect(component).toBeTruthy()
  })
})
