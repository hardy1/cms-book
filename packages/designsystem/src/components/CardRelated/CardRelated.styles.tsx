import styled from 'styled-components'
import colors from '../../resources/colors'

export const Container = styled.div`
  background-color: #eee;
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  display: flex;
  flex-direction: column;
  padding: 32px 30px;
`

export const Title = styled.h3`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1.125rem;
  margin: 0;
  text-transform: uppercase;
  width: 100%;
`

export const Divider = styled.hr`
  background-color: ${colors.secondaryColor};
  border: 0;
  height: 4px;
  margin: 16px 0 0 0;
  width: 100%;
`

export const Content = styled.div`
  display: flex;
  margin-top: 32px;
  flex: 1;
  flex-direction: column;
  width: auto;
`

export const ContentWrapper = styled.div`
  & + div {
    margin-top: 32px;
  }
`

export const Item = styled.a`
  color: ${colors.primaryColor};
  text-decoration: none;

  &:not(:first-child) {
    margin-top: 32px;
  }
`

export const Footer = styled.div`
  margin-top: 10px;
`

export const ArticleTitle = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1rem;
  margin: 0;
  width: 100%;

  &:hover {
    color: ${colors.secondaryColor};
    text-decoration: underline;
  }
`

export const Category = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.secondaryColor};
  font-size: 0.75rem;
  font-weight: 600;
`

export const Date = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75rem;
  font-weight: 600;
  margin-left: 5px;
`

export const ReadingTime = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75rem;
  font-weight: 400;
  margin-left: 5px;
`
