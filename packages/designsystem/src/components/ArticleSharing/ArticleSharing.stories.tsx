import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import ArticleSharing from './ArticleSharing'

export default {
  title: 'Components/ArticleSharing',
  component: ArticleSharing,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <ArticleSharing />
