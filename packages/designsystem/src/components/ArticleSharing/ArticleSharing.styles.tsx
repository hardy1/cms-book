import * as React from 'react'
import styled from 'styled-components'

export const ArticleSharingContainer = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  margin-top: 32px;
`

export const Logos = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  margin-top: 16px;
`

export const SocialButtonStyle = styled.a`
  &:not(:first-child) {
    margin-left: 10px;
  }
`

export const SocialLogo = styled.img`
  width: auto;
`

export const SocialButton: React.FC<{ shareUrl: string; src: string; alt?: string }> = ({ shareUrl, src, alt }) => (
  <SocialButtonStyle href={shareUrl} target="_blank" rel="noreferrer">
    <SocialLogo src={src} alt={alt} />
  </SocialButtonStyle>
)
