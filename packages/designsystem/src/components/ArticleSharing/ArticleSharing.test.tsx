import * as React from 'react'
import { render } from '@testing-library/react'
import ArticleSharing from './ArticleSharing'

describe('ArticleSharing', () => {
  it('SHOULD render correctly', () => {
    const component = render(<ArticleSharing url="https://moc.com.br" />)

    expect(component).toBeTruthy()
  })
})
