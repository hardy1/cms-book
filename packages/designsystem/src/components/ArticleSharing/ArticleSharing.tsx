import * as React from 'react'
import { Heading6 } from '../Typography'
import whatsappLogo from '../../assets/img/logo-whatsapp.png'
import twitterLogo from '../../assets/img/logo-twitter.png'
import linkedinLogo from '../../assets/img/logo-linkedin.png'
import facebookLogo from '../../assets/img/logo-facebook.png'
import gmailLogo from '../../assets/img/logo-gmail.png'

import { ArticleSharingContainer, Logos, SocialButton } from './ArticleSharing.styles'

const ArticleSharing: React.FC<{ url: string; title?: string }> = ({ url, title }) => (
  <ArticleSharingContainer>
    <Heading6>Compartilhe</Heading6>
    <Logos>
      <SocialButton
        shareUrl={`https://api.whatsapp.com/send?text=${title || ''}%20${url}`}
        src={whatsappLogo}
        alt="WhatsApp Logo"
      />
      <SocialButton shareUrl={`https://twitter.com/intent/tweet?text=${url}`} src={twitterLogo} alt="Twitter Logo" />
      <SocialButton
        shareUrl={`https://www.linkedin.com/sharing/share-offsite/?url=${url}`}
        src={linkedinLogo}
        alt="LinkedIn Logo"
      />
      <SocialButton shareUrl={`https://www.facebook.com/sharer.php?u=${url}`} src={facebookLogo} alt="Facebook Logo" />
      <SocialButton shareUrl={`mailto:?subject=${title || url}&body=${url}`} src={gmailLogo} alt="GMail Logo" />
    </Logos>
  </ArticleSharingContainer>
)

export default ArticleSharing
