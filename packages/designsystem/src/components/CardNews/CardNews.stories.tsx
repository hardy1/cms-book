import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardNews, { CardNewsProps } from './CardNews'

export default {
  title: 'Components/CardNews',
  component: CardNews,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
  },
} as Meta

const Template: Story<CardNewsProps> = (args) => (
  <StoryContainer width={350}>
    <CardNews {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Aprovado novo tratamento para leucemia de células pilosas no Brasil',
  category: 'Novidades',
  date: new Date('Jul 13 2020'),
  readingTime: '5',
  imgSrc: 'https://picsum.photos/200/100',
  imgAlt: 'Image alternative text',
  url: '#',
  align: 'row',
}

export const Column = Template.bind({})
Column.args = {
  title: 'Aprovado novo tratamento para leucemia de células pilosas no Brasil',
  category: 'Novidades',
  date: new Date('Jul 13 2020'),
  readingTime: '5',
  imgSrc: 'https://picsum.photos/200/100',
  imgAlt: 'Image alternative text',
  url: '#',
  align: 'column',
}
