import * as React from 'react'
import { render } from '@testing-library/react'
import CardNews from './CardNews'

describe('CardNews', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardNews date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
