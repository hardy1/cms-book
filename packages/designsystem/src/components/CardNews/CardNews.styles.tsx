import styled, { css } from 'styled-components'

import colors from '../../resources/colors'
import media from '../../resources/media'
import { CardNewsProps } from './CardNews'

interface ContainerProps extends CardNewsProps {
  size: string
}

const thumbnail = {
  width: {
    sm: '130px',
    md: '170px',
    lg: '260px',
    full: '100%',
  },
  height: {
    sm: '80px',
    md: '100px',
    lg: '125px',
    full: '100%',
  },
}

export const StyledLink = styled.a`
  text-decoration: none;
`

export const Container = styled.div<ContainerProps>`
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  display: flex;
  flex-direction: ${(props) => `${props.align}`};
  max-width: ${(props) => (props.align === 'column' ? thumbnail.width[props.size] : 'unset')};

  @media ${media.mobileL} {
    flex-direction: ${(props) => props.align === 'row' && 'column'};
  }
`

export const ImgContainer = styled.div<ContainerProps>`
  position: relative;
  width: 100%;
  height: 150px;
  overflow: hidden;
  border-radius: 4px;

  @media screen and (min-width: 1024px) {
    ${({ align, imgContainerHeight }) =>
      align === 'column' &&
      !imgContainerHeight &&
      css`
        height: 102px;
      `}

    ${({ align, imgContainerHeight }) =>
      align === 'column' &&
      imgContainerHeight &&
      css`
        height: ${imgContainerHeight};
      `}
  }

  ${({ align }) =>
    align === 'row' &&
    css`
      @media screen and (min-width: 768px) {
        max-width: 130px;
        height: 80px;
      }
    `}
`

export const StyledImg = styled.img`
  object-fit: cover;
  object-position: center;
  width: 100%;
  height: 100%;
`

export const Content = styled.div<CardNewsProps>`
  margin: ${(props) => (props.align === 'row' ? '0 0 0 20px' : '10px 0 0 0')};
  flex: 1;
  width: auto;

  @media ${media.mobileL} {
    margin: ${(props) => props.align === 'row' && '10px 0 0 0'};
  }
`

export const StyledTitle = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1rem;
  margin: 0;
  width: 100%;
`

export const StyledFooter = styled.div`
  margin-top: 0.75rem;
  font-family: 'Lato', sans-serif;
  font-size: 0.75rem;
  font-weight: 600;
`

export const FooterWrapper = styled.div<{ inline?: boolean }>`
  display: flex;
  flex-direction: ${(props) => (props.inline ? 'row' : 'column')};
  margin: 0.75rem 0 0 0;

  & .separator {
    color: ${colors.disabledColor};
    margin: 0;
  }
`

export const StyledCategory = styled.p`
  color: ${colors.secondaryColor};
  margin: 0;
`

export const StyledDate = styled.p<{ inline?: boolean }>`
  color: ${colors.disabledColor};
  margin: ${(props) => (props.inline ? '0' : '0.25rem 0 0.25rem 0')};
  white-space: nowrap;
`

export const StyledReadingTime = styled.p`
  color: ${colors.disabledColor};
  font-weight: 400;
  margin-top: 0;
`
