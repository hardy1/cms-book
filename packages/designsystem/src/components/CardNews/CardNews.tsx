import React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import PlaceholderNews from '../../assets/img/placeholder-news.png'

import {
  StyledLink,
  Container,
  ImgContainer,
  Content,
  StyledTitle,
  FooterWrapper,
  StyledReadingTime,
  StyledDate,
  StyledCategory,
  StyledImg,
  StyledFooter,
} from './CardNews.styles'

export interface CardNewsProps {
  align?: 'row' | 'column'
  category?: string
  date?: number | Date | undefined
  imgAlt?: string
  imgSrc?: string
  readingTime?: string
  readingTimeText?: string
  title?: string
  url?: string
  inlineFooter?: boolean
  thumbnailSize?: 'sm' | 'md' | 'lg' | 'full'
  imgContainerHeight?: string
}

const CardNews: React.FC<CardNewsProps> = ({
  align = 'row',
  category = '',
  date,
  imgAlt = '',
  imgSrc = '',
  readingTime = '',
  readingTimeText = 'min. de leitura',
  title = '',
  url = '',
  inlineFooter = false,
  thumbnailSize = 'full',
  imgContainerHeight,
}: CardNewsProps) => {
  const Image = (
    <ImgContainer align={align} size={thumbnailSize} imgContainerHeight={imgContainerHeight}>
      <StyledImg src={imgSrc || PlaceholderNews} alt={imgAlt} />
    </ImgContainer>
  )

  const Title = title && <StyledTitle>{title}</StyledTitle>

  const Category = category && <StyledCategory>{category}</StyledCategory>

  const InlineFooter = inlineFooter && <p className="separator">&nbsp;-&nbsp;</p>

  const Date = date && <StyledDate inline={inlineFooter}>{format(date, 'PPP', { locale: ptBR })}</StyledDate>

  const ReadingTime = readingTime && <StyledReadingTime>{`${readingTime} ${readingTimeText}`}</StyledReadingTime>

  const footerSection = (category || date || readingTime) && (
    <StyledFooter>
      <FooterWrapper inline={inlineFooter}>
        {Category}
        {InlineFooter}
        {Date}
      </FooterWrapper>
      {ReadingTime}
    </StyledFooter>
  )

  return (
    <StyledLink href={url}>
      <Container align={align} size={thumbnailSize}>
        {Image}

        <Content align={align}>
          {Title}
          {footerSection}
        </Content>
      </Container>
    </StyledLink>
  )
}

export default CardNews
