import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Button from '../Button'
import Modal from './Modal'

export default {
  title: 'Components/Modal',
  component: Modal,
} as Meta

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const Template = ({ title, onClose, isOpen, ...args }) => {
  const [isOpenModal, setIsOpenModal] = React.useState(false)

  const handleClose = () => setIsOpenModal(!isOpenModal)

  return (
    <div>
      <Button onClick={handleClose} size="medium">
        Open Modal
      </Button>
      <Modal title={title} isOpen={isOpenModal} onClose={handleClose} {...args}>
        Lorem ipsum
      </Modal>
    </div>
  )
}

export const Default = Template.bind({})
Default.args = {
  title: 'Título da modal',
}
