import * as React from 'react'
import ReactModal from 'react-modal'
import { IconClose } from '../../icons'
import { Heading1 } from '../Typography'
import { ModalContainer, ModalOverlayStyles, ModalContentStyles, CloseButton } from './Modal.styles'

export interface ModalProps {
  className?: string
  children: React.ReactNode
  title?: string
  isOpen: boolean
  onClose: () => void
}

const Modal: React.FC<ModalProps> = ({ className, children, title, isOpen, onClose }: ModalProps) => (
  <ReactModal
    className={className}
    isOpen={isOpen}
    style={{
      overlay: ModalOverlayStyles as React.CSSProperties,
      content: ModalContentStyles as React.CSSProperties,
    }}
    onRequestClose={onClose}
    appElement={document.getElementsByTagName('body') || undefined}
  >
    {title && <Heading1 color="black">{title}</Heading1>}
    <ModalContainer>{children}</ModalContainer>
    <CloseButton onClick={onClose}>
      <IconClose />
    </CloseButton>
  </ReactModal>
)

export default Modal
