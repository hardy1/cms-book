import styled from 'styled-components'
import colors from '../../resources/colors'

export const ModalContainer = styled.div`
  color: ${colors.neutralColor};
  font-family: 'Montserrat';
  font-size: 1em;
`

export const CloseButton = styled.span`
  cursor: pointer;
  position: absolute;
  right: 31px;
  top: 23px;
`

export const ModalOverlayStyles = {
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  backgroundColor: 'rgb(255 255 255 / 90%)',
  zIndex: 2,
}

export const ModalContentStyles = {
  backgroundColor: '#fff',
  boxShadow: '2px 2px 4px rgba(0, 0, 0, 0.25)',
  position: 'absolute',
  top: '40px',
  left: '40px',
  right: '40px',
  bottom: '40px',
  overflow: 'auto',
  WebkitOverflowScrolling: 'touch',
  border: 'none',
  borderRadius: '10px',
  outline: 'none',
  padding: '46px 26px',
  width: 'max-content',
  height: 'max-content',
  maxWidth: '80%',
  maxHeight: '70%',
  margin: 'auto',
}
