import * as React from 'react'
import { render } from '@testing-library/react'
import Modal from './Modal'

describe('Modal', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(
      <Modal isOpen onClose={() => null}>
        {children}
      </Modal>,
    )

    expect(component).toBeTruthy()
  })
})
