import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import colors from '../../resources/colors'

import Color, { ColorProps } from './Color'

export default {
  title: 'Theme/Colors',
  component: Color,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

const colorsMap = Object.keys(colors).map((key) => <Color key={key} name={key} color={colors[key]} />)

export const List: React.FC<ColorProps> = () => <React.Fragment>{colorsMap}</React.Fragment>
