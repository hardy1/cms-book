import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import FormGroup from './FormGroup'
import Input from '../Input'

export default {
  title: 'Components/FormGroup',
  component: FormGroup,
} as Meta

const Template: Story = (args) => {
  return (
    <FormGroup {...args}>
      <Input label="Label Example" type="text" id="input" onChange={() => null} />
    </FormGroup>
  )
}

export const Default = Template.bind({})
