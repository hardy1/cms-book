import * as React from 'react'
import { render } from '@testing-library/react'
import FormGroup from './FormGroup'

describe('FormGroup', () => {
  const children = 'Form group text'

  it('SHOULD render correctly', () => {
    const component = render(<FormGroup>{children}</FormGroup>)

    expect(component).toBeTruthy()
  })
})
