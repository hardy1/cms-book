import * as React from 'react'
import { StyledFormGroup } from './FormGroup.styles'

type FormGroupProps = {
  as?: React.ElementType
  children: React.ReactNode
  textAlign?: string
  className?: string
  gridColumn?: string
  marginBottom?: string
  marginTop?: string
}

const FormGroup: React.FC<FormGroupProps> = ({
  as = 'div',
  className,
  children,
  gridColumn,
  marginBottom,
  marginTop,
  textAlign,
}: FormGroupProps) => (
  <StyledFormGroup
    as={as}
    className={className}
    gridColumn={gridColumn}
    marginBottom={marginBottom}
    marginTop={marginTop}
    textAlign={textAlign}
  >
    {children}
  </StyledFormGroup>
)

export default FormGroup
