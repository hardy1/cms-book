import styled, { css } from 'styled-components'
import colors from '../../resources/colors'

type StyledFormGroupProps = {
  textAlign?: string
  className?: string
  gridColumn?: string
  marginBottom?: string
  marginTop?: string
}

const StyledFormGroup = styled.div<StyledFormGroupProps>`
  position: relative;
  padding-bottom: 24px;
  text-align: ${({ textAlign }) => textAlign || 'left'};
  margin-bottom: ${({ marginBottom }) => marginBottom || '0'};

  ${({ marginTop }) =>
    marginTop &&
    css`
      margin-top: ${marginTop};
    `}

  ${({ gridColumn }) =>
    gridColumn &&
    css`
      grid-column: 1 / -1;

      @media screen and (min-width: 768px) {
        grid-column: ${gridColumn};
      }
    `}

  .add-institutions {
    position: relative;
    display: inline-block;
    color: #9ea0a5;
    font-family: 'Lato';
    font-size: 1rem;
    font-weight: 100;
    line-height: 19px;
    text-align: left;
    text-decoration: none;
    margin-bottom: 12px;

    :hover,
    :focus {
      ::before {
        transform: scale(0.8);
      }
    }

    ::before {
      content: '';
      position: absolute;
      right: 0;
      bottom: -2px;
      left: 0;
      width: 100%;
      height: 1px;
      background-color: #9ea0a5;
      transition: transform 0.2s ease-out;
    }
  }

  .forgot-password {
    min-height: initial;
    text-align: left;
    padding-bottom: 30px;
    margin-top: 0;
    margin-bottom: 0;

    a {
      color: ${colors.secondaryColor};
      font-family: 'Lato';
      font-size: 1rem;
      line-height: 19px;
      padding: 0;
      margin-top: 0;
    }
  }

  .required-fields {
    min-height: initial;
    color: ${colors.disabledColor};
    font-family: 'Lato';
    font-size: 12px;
    line-height: 14px;
    margin-top: 0;
    margin-bottom: 0;
  }

  &.submit {
    display: flex;
    justify-content: center;
  }

  .InputErrorMessage,
  .SelectErrorMessage,
  .TextareaErrorMessage {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    font-size: 0.875rem;
    margin-top: 0;
    line-height: 24px;
  }
`

export { StyledFormGroup }
