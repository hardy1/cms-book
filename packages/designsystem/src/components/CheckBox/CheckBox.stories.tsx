import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import CheckBox from './CheckBox'
import FormGroup from '../FormGroup'

export default {
  title: 'Components/CheckBox',
  component: CheckBox,
} as Meta

const Template: Story = (args) => (
  <FormGroup>
    <CheckBox {...args} />
    <label htmlFor="default-checkbox">Checkbox with label</label>
  </FormGroup>
)

export const Default = Template.bind({})

Default.args = {
  className: 'checkbox-class',
  id: 'default-checkbox',
  name: 'default-checkbox',
}
