import * as React from 'react'
import StyledCheckBox from './CheckBox.styles'

export type CheckBoxProps = {
  checkImage?: string
  className?: string
  id?: string
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void
  value?: any
  name?: string
  error?: boolean
  checked?: boolean
}

const CheckBox: React.FC<CheckBoxProps> = ({
  checkImage,
  onChange,
  onClick,
  value,
  className,
  id,
  name,
  checked,
  error,
}: CheckBoxProps) => (
  <>
    <StyledCheckBox
      checkImage={checkImage}
      className={className}
      id={id}
      type="checkbox"
      name={name}
      checked={checked}
      value={value}
      onChange={onChange}
      onClick={onClick}
      error={error}
    />
  </>
)

export default CheckBox
