import * as React from 'react'
import { render } from '@testing-library/react'
import CheckBox from './CheckBox'

describe('CheckBox', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CheckBox id="selectId" />)

    expect(component).toBeTruthy()
  })
})
