import styled, { css } from 'styled-components'
import colors from '../../resources/colors'

type StyledCheckBoxProps = {
  checkImage?: string
  error?: boolean
}

const StyledCheckBox = styled.input<StyledCheckBoxProps>`
  position: absolute;
  top: -1px;
  left: 0;
  display: inline-block;
  width: 20px;
  height: 20px;
  border-width: 2px;
  border-style: solid;
  border-color: ${(props) => (props.error ? `${colors.errorColor}` : `${colors.neutralColor}`)};
  border-radius: 4px;
  margin: 0;
  cursor: pointer;
  transition-property: background-color, border-color;
  transition-duration: 0.2s;
  transition-timing-function: ease-out;
  appearance: none;
  margin-right: 12px;
  vertical-align: bottom;
  cursor: pointer;

  ::before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;

    ${({ checkImage }) =>
      checkImage &&
      css`
        background-image: url(${checkImage});
        background-repeat: no-repeat;
        background-position: center;
        background-size: 12px 10px;
      `}
  }

  &:checked {
    background-color: #144590;
    border-color: #144590;
  }

  + label {
    display: inline-block;
    width: 100%;
    color: #3e3f42;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    line-height: 19px;
    text-transform: none;
    padding-left: 30px;
    margin-bottom: 0;
    box-sizing: border-box;
    cursor: pointer;

    a {
      color: #216de1;
      text-decoration: none;
    }
  }
`

export default StyledCheckBox
