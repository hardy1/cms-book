import styled from 'styled-components'
import colors from '../../resources/colors'

export const MenuContainer = styled.ul`
  display: flex;
  font-family: Lato;
  justify-content: space-between;
  max-width: 800px;
  width: 100%;
  color: ${colors.neutralColor};
  font-size: 1em;
  line-height: 170%;
  list-style: none;
  margin: 0;
  padding: 0;
`
