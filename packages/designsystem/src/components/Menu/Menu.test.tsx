import * as React from 'react'
import { render } from '@testing-library/react'
import Menu from './Menu'

describe('Menu', () => {
  const items = [
    { id: 1, value: 'Home' },
    { id: 2, value: 'Sobre' },
    { id: 3, value: 'Cursos' },
    { id: 4, value: 'CanaisMOC' },
    { id: 5, value: 'Podcast' },
    { id: 6, value: 'Inteligência de Mercado' },
    { id: 7, value: 'Fórmulas médicas' },
    { id: 8, value: 'Fale conosco' },
  ]

  it('SHOULD render correctly', () => {
    const component = render(<Menu items={items} />)

    expect(component).toBeTruthy()
  })
})
