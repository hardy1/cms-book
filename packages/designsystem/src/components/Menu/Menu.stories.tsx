import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Menu, { MenuProps } from './Menu'

export default {
  title: 'Components/Menu',
  component: Menu,
} as Meta

const Template: Story<MenuProps> = (args) => <Menu {...args} />

export const Default = Template.bind({})
Default.args = {
  items: [
    { id: 1, value: 'Home' },
    { id: 2, value: 'Sobre' },
    { id: 3, value: 'Cursos' },
    { id: 4, value: 'CanaisMOC' },
    { id: 5, value: 'Podcast' },
    { id: 6, value: 'Inteligência de Mercado' },
    { id: 7, value: 'Fórmulas médicas' },
    { id: 8, value: 'Fale conosco' },
  ],
}
