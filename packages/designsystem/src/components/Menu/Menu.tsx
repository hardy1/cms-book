import * as React from 'react'
import { MenuContainer } from './Menu.styles'
import MenuItem from '../MenuItem'

type MenuItemProps = {
  id: number
  url?: string
  value?: string
}

export type MenuProps = {
  items: MenuItemProps[] | undefined
}

const Menu: React.FC<MenuProps> = ({ items }: MenuProps) => (
  <MenuContainer>
    {items && items.map(({ id, url = '', value = '' }) => <MenuItem key={id} value={value} url={url} />)}
  </MenuContainer>
)

export default Menu
