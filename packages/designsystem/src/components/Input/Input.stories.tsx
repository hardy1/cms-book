import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import { IconSearch } from '../../icons'

import Input, { InputProps } from './Input'

export default {
  title: 'Components/Input',
  component: Input,
  argTypes: {
    icon: {
      control: {
        disable: true,
      },
    },
  },
  parameters: {
    viewport: {
      defaultViewport: 'mobile2',
    },
  },
} as Meta

const Template: Story<InputProps> = (args) => <Input {...args} />

export const Default = Template.bind({})
Default.args = {
  id: 'inputId',
  type: 'text',
  placeholder: 'Placeholder',
}

export const Email = Template.bind({})
Email.args = {
  id: 'inputId',
  type: 'email',
  placeholder: 'Placeholder',
}

export const Tel = Template.bind({})
Tel.args = {
  id: 'inputId',
  type: 'tel',
  placeholder: 'Placeholder',
}

export const Password = Template.bind({})
Password.args = {
  id: 'inputId',
  type: 'password',
  placeholder: 'Placeholder',
}

export const WithLabel = Template.bind({})
WithLabel.args = {
  id: 'inputId',
  type: 'text',
  label: 'Label',
  message: 'Message',
  placeholder: 'Placeholder',
}

export const WithIcon = Template.bind({})
WithIcon.args = {
  id: 'inputId',
  type: 'text',
  label: 'Label',
  message: 'Message',
  placeholder: 'Placeholder',
  icon: <IconSearch />,
}

export const Error = Template.bind({})
Error.args = {
  id: 'inputId',
  type: 'text',
  message: 'Message',
  placeholder: 'Placeholder',
  error: true,
}
