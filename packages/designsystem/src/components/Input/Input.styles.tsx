import styled from 'styled-components'
import colors from '../../resources/colors'

type InputProps = {
  isValidPromoCode?: 'isValidCoupon' | 'isNotValidCoupon'
  error?: boolean
  icon?: React.ReactNode
}

type LabelProps = {
  required?: boolean
}

const setRequired = () => `
  color: #144590;
  margin-left: 5px;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`

export const InputStyled = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  justify-content: center;
  width: 100%;
`

export const InputLabel = styled.label<LabelProps>`
  color: ${colors.disabledColor};
  font-size: 1em;
  font-weight: 400;
  line-height: 19px;
  margin-bottom: 12px;
  text-transform: uppercase;
  letter-spacing: 0.05em;

  .required {
    display: inline-block;
    ${setRequired()}
  }
`

export const InputContainer = styled.div`
  display: flex;
  height: 50px;
  height: ${(props) => (props.className === 'datepicker' ? '75px' : '50px')};
  position: relative;
  width: 100%;
`

export const InputComponentStyle = styled.div<InputProps>`
  width: 100%;

  input.isValidCoupon {
    border-color: ${(props) => (props.error ? `${colors.successColor}` : `${colors.disabledColor}`)};
    border-color: ${colors.successColor};
  }
  input.isNotValidCoupon {
    border-color: ${colors.errorColor};
  }
  input {
    border-radius: 4px;
    border-style: solid;
    border-width: 1px;
    color: ${colors.neutralColor};
    font-size: 1em;
    flex: 1;
    height: 50px;
    outline: none;
    padding: ${(props) => (props.icon ? '0 16px 0 46px' : '0 16px')};
    width: 100%;
    box-sizing: border-box;

    &::placeholder {
      color: ${colors.disabledColor};
    }

    &:focus {
      border-color: ${colors.primaryColor};
      color: ${colors.primaryColor};
    }
  }
`

export const InputIcon = styled.span`
  align-items: center;
  display: flex;
  height: 50px;
  justify-content: center;
  left: 0;
  margin-top: -25px;
  position: absolute;
  top: 50%;
  width: 50px;
`

export const InputMessage = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${(props) => (props.className === 'isValidCoupon' ? colors.successColor : colors.errorColor)};
  font-size: 1em;
  margin-top: 12px;
`
