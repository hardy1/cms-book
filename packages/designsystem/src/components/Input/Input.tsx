import * as React from 'react'
import NumberFormat from 'react-number-format'
import {
  InputStyled,
  InputLabel,
  InputContainer,
  InputComponentStyle,
  InputIcon,
  InputMessage,
  Container,
} from './Input.styles'

export type InputProps = {
  isValidPromoCode?: 'isValidCoupon' | 'isNotValidCoupon'
  hasValidPromoCode?: boolean
  isDatePicker?: boolean
  isLoading?: boolean
  error?: boolean
  icon?: React.ReactNode
  id: string
  label?: string
  message?: string
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>) => void
  placeholder?: string
  ref?: React.RefObject<HTMLInputElement>
  type: 'text' | 'email' | 'tel' | 'password' | 'search' | undefined
  autoComplete?: string
  value?: string
  maxLength?: number
  disabled?: boolean
  required?: boolean
  mask?: string
  numberSeparators?: boolean
  currency?: boolean
  currencyPrefix?: string
  className?: string
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(function input(
  {
    isValidPromoCode,
    hasValidPromoCode,
    isDatePicker,
    isLoading,
    error = false,
    icon,
    id,
    value,
    label = '',
    message = '',
    onChange,
    onBlur,
    placeholder = '',
    type = 'text',
    autoComplete = 'on',
    maxLength,
    disabled = false,
    required = false,
    mask,
    numberSeparators,
    currency,
    currencyPrefix,
    className,
  },
  ref,
) {
  return (
    <Container>
      <InputStyled>
        {label && (
          <InputLabel htmlFor={id} required={required}>
            {label}
          </InputLabel>
        )}
        <InputContainer className={isDatePicker ? 'datepicker' : ''}>
          <InputComponentStyle error={error} icon={icon}>
            {mask || numberSeparators || currency ? (
              <NumberFormat
                value={value}
                format={mask ? mask : undefined}
                prefix={currency ? (currencyPrefix ? `${currencyPrefix} ` : 'R$ ') : undefined}
                thousandSeparator={currency || numberSeparators ? '.' : undefined}
                decimalSeparator={currency || numberSeparators ? ',' : undefined}
                autoComplete={autoComplete}
                id={id}
                placeholder={placeholder}
                onChange={onChange}
                onBlur={onBlur}
                maxLength={maxLength}
                disabled={disabled}
                className={className}
              />
            ) : (
              <input
                autoComplete={autoComplete}
                type={type}
                id={id}
                placeholder={placeholder}
                onChange={onChange}
                onBlur={onBlur}
                ref={ref}
                value={value}
                maxLength={maxLength}
                disabled={disabled}
                className={isValidPromoCode && hasValidPromoCode && !isLoading ? isValidPromoCode : className}
              />
            )}
          </InputComponentStyle>
          {icon && <InputIcon>{icon}</InputIcon>}
        </InputContainer>
      </InputStyled>
      {isValidPromoCode === 'isValidCoupon' && hasValidPromoCode && !isLoading && (
        <InputMessage className="isValidCoupon">Cupom de desconto aplicado!</InputMessage>
      )}
      {isValidPromoCode === 'isNotValidCoupon' && hasValidPromoCode && !isLoading && (
        <InputMessage>Cupom de desconto inválido!</InputMessage>
      )}
      {error && <InputMessage className="InputErrorMessage">{message}</InputMessage>}
    </Container>
  )
})

export default Input
