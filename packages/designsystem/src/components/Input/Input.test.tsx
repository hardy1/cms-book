import * as React from 'react'
import { render } from '@testing-library/react'
import Input from './Input'

describe('Input', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Input type="text" id="InputId" onChange={jest.fn()} />)

    expect(component).toBeTruthy()
  })
})
