import styled from 'styled-components'

import colors from '../../resources/colors'
import media from '../../resources/media'

type StyledAlertProps = {
  type?: 'success' | 'warning' | 'danger'
}

const setBackgroundColor = (type?: string) => {
  const { errorColor, successColor, warningColor } = colors

  switch (type) {
    case 'success':
      return successColor
    case 'warning':
      return warningColor
    case 'danger':
      return errorColor
    default:
      return successColor
  }
}

const StyledAlert = styled.div<StyledAlertProps>`
  position: relative;
  display: flex;
  width: fit-content;
  min-width: 450px;
  max-width: 450px;
  align-items: center;
  min-height: 50px;
  background-color: ${({ type }) => setBackgroundColor(type)};
  border-radius: 4px;
  padding: 14px 40px 14px 45px;
  box-sizing: border-box;
  margin-bottom: 20px;

  @media ${media.mobileL} {
    min-width: 100%;
    max-width: 100%;
    width: 100%;
  }

  .Alert {
    &__Text {
      display: inline-block;
      width: inherit;
      color: #fff;
      font-family: 'Lato', sans-serif;
      font-size: 16px;
      font-weight: bold;
      line-height: 19px;
      margin-top: 0;
      margin-bottom: 0;
    }

    &__SVGTypeIcon,
    &__ButtonClose {
      position: absolute;
      top: 16px;
      display: inline-flex;
    }

    &__SVGTypeIcon {
      position: absolute;
      left: 18px;
      width: 18px;
      height: auto;
    }

    &__ButtonClose {
      top: 18px;
      right: 18px;
      align-items: center;
      justify-content: center;
      width: 15px;
      height: 15px;
      background-color: transparent;
      border-width: 0;
      box-sizing: inherit;
      cursor: pointer;
      padding: 0;
      transition: opacity 0.2s ease-out;

      &:hover,
      &:focus {
        opacity: 0.8;
      }

      &:focus {
        outline: 1px dashed #fff;
      }

      &:active {
        opacity: 0.6;
      }

      span {
        position: inherit;
        width: 0;
        height: 0;
        overflow: hidden;
      }
    }
  }
`

export { StyledAlert }
