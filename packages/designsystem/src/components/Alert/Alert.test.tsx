import * as React from 'react'
import { render } from '@testing-library/react'
import Alert from './Alert'

describe('Alert', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Alert type="success" text="Text alert" />)

    expect(component).toBeTruthy()
  })
})
