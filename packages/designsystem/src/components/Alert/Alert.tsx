import React from 'react'

import { StyledAlert } from './Alert.styles'

import colors from '../../resources/colors'

import SvgIconClose from '../../icons/IconClose'
import SvgIconCheckCircle from '../../icons/IconCheckCircle'
import SvgIconBell from '../../icons/IconBell'
import SvgIconExclamationRounded from '../../icons/IconExclamationRounded'

export type AlertProps = {
  ariaLabelText?: string
  buttonClose?: boolean
  className?: string
  id?: string
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  text: string
  type?: 'success' | 'warning' | 'danger'
}

const Alert: React.FC<AlertProps> = ({
  ariaLabelText = 'Fechar Alerta',
  buttonClose,
  className,
  id,
  onClick,
  text,
  type,
}: AlertProps) => {
  const { white } = colors

  const iconClose = <SvgIconClose width={12} height={12} fill={white} />

  const svgProps = {
    className: 'Alert__SVGTypeIcon',
    fill: white,
  }

  const svgIconCheckCircle = <SvgIconCheckCircle {...svgProps} />

  const setIconType = (type?: string) => {
    switch (type) {
      case 'success':
        return svgIconCheckCircle
      case 'warning':
        return <SvgIconBell {...svgProps} />
      case 'danger':
        return <SvgIconExclamationRounded {...svgProps} />
      default:
        return svgIconCheckCircle
    }
  }

  const showButtonClose = buttonClose && (
    <button aria-label={ariaLabelText} className="Alert__ButtonClose" onClick={onClick}>
      <span>Fechar</span>
      {iconClose}
    </button>
  )

  const alertText = <p className="Alert__Text">{text}</p>

  return (
    <StyledAlert className={className} id={id} type={type}>
      {setIconType(type)}
      {alertText}
      {showButtonClose}
    </StyledAlert>
  )
}

export default Alert
