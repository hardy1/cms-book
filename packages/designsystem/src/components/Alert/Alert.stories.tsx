import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Alert, { AlertProps } from './Alert'

export default {
  title: 'Components/Alert',
  component: Alert,
  parameters: {},
  argTypes: {
    type: {
      options: ['success', 'warning', 'danger'],
      control: { type: 'select' },
      table: {
        defaultValue: { summary: 'success' },
      },
    },
  },
} as Meta

const Template: Story<AlertProps> = (args) => <Alert {...args} />

export const Default = Template.bind({})
Default.args = {
  text: 'Alert text',
}
