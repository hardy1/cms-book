import styled from 'styled-components'

const StyledRadio = styled.input`
  display: none;
  box-sizing: border-box;

  :checked {
    + label {
      ::before {
        border-width: 7px;
        border-color: #144590;
      }
    }
  }

  + label {
    position: relative;
    display: inline-block;
    width: 100%;
    color: #3e3f42;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    font-weight: 400;
    line-height: 20px;
    padding-right: 20px;
    padding-left: 34px;
    margin-bottom: 20px;
    box-sizing: border-box;
    cursor: pointer;

    @media screen and (min-width: 768px) {
      width: auto;
      margin-bottom: 0;
    }

    ::before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 20px;
      height: 20px;
      background-color: transparent;
      border-width: 2px;
      border-style: solid;
      border-color: #3e3f42;
      border-radius: 50%;
      box-sizing: border-box;
    }
  }
`

export default StyledRadio
