import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Radio, { RadioProps } from './Radio'

export default {
  title: 'Components/Radio',
  component: Radio,
  argTypes: {
    checked: {
      table: {
        defaultValue: { summary: 'false' },
      },
    },
  },
} as Meta

const Template: Story<RadioProps> = (args) => <Radio {...args} />

export const Default = Template.bind({})

Default.args = {
  id: 'input-radio',
  label: 'Input radio label',
  name: 'input-radio-group',
  checked: false,
}
