import * as React from 'react'
import { render } from '@testing-library/react'
import Radio from './Radio'

describe('Radio', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Radio id="RadioId" name="radio" label="Radio" />)

    expect(component).toBeTruthy()
  })
})
