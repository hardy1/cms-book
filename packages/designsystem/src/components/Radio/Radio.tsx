import React from 'react'

import StyledRadio from './Radio.styles'

export type RadioProps = {
  checked?: boolean
  className?: string
  disabled?: boolean
  id: string
  label: string
  name: string
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void
  onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
  ref?: React.RefObject<HTMLInputElement>
  required?: boolean
}

const Radio: React.FC<RadioProps> = ({
  checked,
  className,
  disabled,
  id,
  label,
  name,
  onClick,
  onBlur,
  onChange,
  ref,
  required,
}) => {
  return (
    <>
      <StyledRadio
        type="radio"
        id={id}
        className={className}
        onClick={onClick}
        onChange={onChange}
        onBlur={onBlur}
        name={name}
        ref={ref}
        disabled={disabled}
        required={required}
        checked={checked}
      />

      <label htmlFor={id}>{label}</label>
    </>
  )
}

export default Radio
