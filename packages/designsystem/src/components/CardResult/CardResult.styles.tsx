import styled from 'styled-components'
import colors from '../../resources/colors'

export const Link = styled.a`
  text-decoration: none;
`

export const Container = styled.div`
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  display: flex;
  max-width: 630px;
  width: 100%;
`

export const Content = styled.div`
  flex: 1;
  width: auto;
`

export const Title = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1em;
  margin: 0;
  width: 100%;
`

export const Description = styled.p`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  line-height: 130%;
  margin: 16px 0 0;
  width: 100%;
`

export const Footer = styled.div`
  margin-top: 10px;
`

export const Category = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.secondaryColor};
  font-size: 0.75em;
  font-weight: 600;
`

export const Date = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  font-weight: 600;
  margin-left: 5px;
`
