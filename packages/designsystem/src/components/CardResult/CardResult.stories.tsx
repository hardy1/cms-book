import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardResult, { CardResultProps } from './CardResult'

export default {
  title: 'Components/CardResult',
  component: CardResult,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
  },
} as Meta

const Template: Story<CardResultProps> = (args) => (
  <StoryContainer width={450}>
    <CardResult {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Tumores de células gigantes (TCG)',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque libero, et nunc dignissim nibh porttitor id senectus. Eget odio mattis et non mi id fermentum, et blandit. Et, pretium diam urna Eget odio mattis et non mi id fermentum, et blandit...',
  category: 'Vídeos',
  date: new Date('Jul 13 2020'),
  url: '#',
}
