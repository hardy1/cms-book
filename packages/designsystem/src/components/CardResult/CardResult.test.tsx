import * as React from 'react'
import { render } from '@testing-library/react'
import CardResult from './CardResult'

describe('CardResult', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardResult date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
