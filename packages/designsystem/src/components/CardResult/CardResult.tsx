import * as React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { Link, Container, Content, Description, Title, Category, Date, Footer } from './CardResult.styles'

export type CardResultProps = {
  category?: string
  date: number | Date | undefined
  description?: string
  title?: string
  url?: string
}

const CardResult: React.FC<CardResultProps> = ({
  category = '',
  date,
  description = '',
  title = '',
  url = '',
}: CardResultProps) => (
  <Link href={url}>
    <Container>
      <Content>
        {title && <Title>{title}</Title>}
        {description && <Description>{description}</Description>}
        {(category || date) && (
          <Footer>
            {category && <Category>{category}</Category>}
            {date && <Date>- {format(date, 'PPP', { locale: ptBR })}</Date>}
          </Footer>
        )}
      </Content>
    </Container>
  </Link>
)

export default CardResult
