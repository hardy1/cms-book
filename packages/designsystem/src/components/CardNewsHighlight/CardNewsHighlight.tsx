import * as React from 'react'
import { format } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import {
  Link,
  Container,
  ImgContainer,
  Img,
  Content,
  Description,
  Title,
  Category,
  Date,
  Footer,
  ReadingTime,
} from './CardNewsHighlight.styles'
import { PlaceholderNews } from '../../assets'

export type CardNewsHighlightProps = {
  category?: string
  date: number | Date | undefined
  description?: string
  imgAlt?: string
  imgSrc?: string
  readingTime?: string
  title?: string
  url?: string
}

const CardNewsHighlight: React.FC<CardNewsHighlightProps> = ({
  category = '',
  date,
  description = '',
  imgAlt = '',
  imgSrc = '',
  readingTime = '',
  title = '',
  url = '',
}: CardNewsHighlightProps) => (
  <Link href={url}>
    <Container>
      <ImgContainer>
        <Img src={imgSrc || PlaceholderNews} alt={imgAlt} />
      </ImgContainer>
      <Content>
        {title && <Title>{title}</Title>}
        {description && <Description>{description}</Description>}
        {(category || date || readingTime) && (
          <Footer>
            {category && <Category>{category}</Category>}
            {date && <Date>- {format(date, 'PPP', { locale: ptBR })}</Date>}
            {readingTime && <ReadingTime>- {readingTime}</ReadingTime>}
          </Footer>
        )}
      </Content>
    </Container>
  </Link>
)

export default CardNewsHighlight
