import styled from 'styled-components'
import colors from '../../resources/colors'

export const Link = styled.a`
  text-decoration: none;
`

export const Container = styled.div`
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  display: flex;
  max-width: 630px;
  width: 100%;
`

export const ImgContainer = styled.div`
  height: 100%;
  width: 200px;
`

export const Img = styled.img`
  border-radius: 4px;
  width: 100%;
`

export const Content = styled.div`
  margin-left: 16px;
  flex: 1;
  width: auto;
`

export const Title = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1.25em;
  margin: 0;
  width: 100%;
`

export const Description = styled.p`
  color: ${colors.neutralColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  letter-spacing: 0.05em;
  line-height: 14px;
  margin-top: 6px;
  width: 100%;
`

export const Footer = styled.div`
  margin-top: 12px;
`

export const Category = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.secondaryColor};
  font-size: 0.75em;
  font-weight: 600;
`

export const Date = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  font-weight: 600;
  margin-left: 5px;
`

export const ReadingTime = styled.span`
  color: ${colors.disabledColor};
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  font-weight: 400;
  margin-left: 5px;
`
