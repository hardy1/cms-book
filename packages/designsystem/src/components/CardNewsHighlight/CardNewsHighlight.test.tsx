import * as React from 'react'
import { render } from '@testing-library/react'
import CardNewsHighlight from './CardNewsHighlight'

describe('CardNewsHighlight', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardNewsHighlight date={new Date('Jul 13 2020')} />)

    expect(component).toBeTruthy()
  })
})
