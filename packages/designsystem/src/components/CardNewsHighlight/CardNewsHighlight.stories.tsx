import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import CardNewsHighlight, { CardNewsHighlightProps } from './CardNewsHighlight'

export default {
  title: 'Components/CardNewsHighlight',
  component: CardNewsHighlight,
  argTypes: {
    date: {
      control: {
        type: 'date',
      },
    },
  },
} as Meta

const Template: Story<CardNewsHighlightProps> = (args) => <CardNewsHighlight {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Aprovado novo tratamento para leucemia de células pilosas no Brasil',
  description:
    'A ANVISA (Agência Nacional de Vigilância Sanitária) aprovou em 03 de agosto de 2020 a imunotoxina moxetumomabe pasudotox para o tratamento de pacientes',
  category: 'Novidades',
  date: new Date('Jul 13 2020'),
  readingTime: '5 min. de leitura',
  imgSrc: 'https://picsum.photos/200/100',
  imgAlt: 'Image alternative text',
  url: '#',
}
