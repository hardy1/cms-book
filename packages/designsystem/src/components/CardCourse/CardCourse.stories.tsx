import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardCourse, { CardCourseProps } from './CardCourse'

export default {
  title: 'Components/CardCourse',
  component: CardCourse,
  argTypes: {
    stars: {
      control: {
        type: 'number',
      },
    },
  },
} as Meta

const Template: Story<CardCourseProps> = (args) => (
  <StoryContainer width={350}>
    <CardCourse {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'IV Curso Intensivo de Oncologia',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non vestibulum erat. Nam ullamcorper enim lacus, a lacinia eros rutrum ut.',
  subtitle: 'Realização: MastoWeb',
  value: 'R$ 0,00',
  stars: 5,
  imgSrc: 'https://picsum.photos/305/230',
  url: '#',
}
