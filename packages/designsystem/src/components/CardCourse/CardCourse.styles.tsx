import styled from 'styled-components'
import colors from '../../resources/colors'

type CardCourseProps = {
  backgroundUrl?: string
}

export const Link = styled.a`
  display: flex;
  text-decoration: none;
  border: 1px solid ${colors.lightColor};
`

export const Container = styled.div`
  background-color: ${colors.white};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  height: 100%;
  transition: all 0.5s ease;
  width: 100%;

  &:hover {
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.25);
  }
`

export const Header = styled.div<CardCourseProps>`
  align-items: center;
  background-color: ${colors.disabledColor};
  background-image: url(${(props) => props.backgroundUrl});
  background-position: center center;
  background-size: cover;
  border-color: ${colors.lightColor};
  border-style: solid;
  border-width: 1px;
  display: flex;
  justify-content: center;
  height: 230px;
`

export const Content = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  height: auto;
  padding: 14px;
`

export const ContainerStars = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: auto;

  & svg:not(:first-child) {
    margin-left: 2px;
  }
`

export const Title = styled.h4`
  color: ${colors.primaryColor};
  font-family: 'Montserrat', sans-serif;
  font-size: 1.5em;
  font-weight: 600;
  line-height: 29px;
  margin: 16px 0 0 0;
  text-transform: uppercase;
`

export const Description = styled.p`
  color: ${colors.black};
  display: -webkit-box;
  font-family: 'Lato', sans-serif;
  font-size: 1em;
  flex: 1;
  line-height: 1.2rem;
  margin-bottom: 5px;
  margin: 10px 0 0 0;
  overflow: hidden;
  -webkit-box-orient: vertical;
`

export const Subtitle = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.disabledColor};
  font-size: 0.75em;
  margin-top: 20px;
  text-transform: uppercase;
`

export const Footer = styled.div`
  align-items: center;
  background-color: ${colors.primaryColor};
  color: ${colors.white};
  display: flex;
  justify-content: space-between;
  padding: 14px;
`

export const Value = styled.span`
  font-family: 'Lato', sans-serif;
  color: ${colors.white};
  font-size: 1.125em;
  font-weight: 500;
`

export const Button = styled.span`
  align-items: center;
  color: ${colors.white};
  display: flex;
  justify-content: center;
  font-family: 'Lato', sans-serif;
  font-size: 1.125em;
  font-weight: 800;

  & svg {
    margin-left: 14px;
  }

  & svg path {
    fill: ${colors.white};
  }
`
