import * as React from 'react'
import { render } from '@testing-library/react'
import CardCourse from './CardCourse'

describe('CardCourse', () => {
  it('SHOULD render correctly', () => {
    const component = render(<CardCourse url="#" />)

    expect(component).toBeTruthy()
  })
})
