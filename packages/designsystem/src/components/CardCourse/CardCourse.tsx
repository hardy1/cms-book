import * as React from 'react'
import { IconArrowButton, IconStar } from '../../icons'
import {
  Link,
  Container,
  Header,
  Content,
  ContainerStars,
  Title,
  Description,
  Subtitle,
  Footer,
  Value,
  Button,
} from './CardCourse.styles'
import { PlaceholderCourse } from '../../assets'

export type CardCourseProps = {
  description?: string | JSX.Element[] | JSX.Element
  imgSrc?: string
  stars?: number | undefined
  subtitle?: string
  title?: string
  url?: string
  value?: string
  target?: string
}

const CardCourse: React.FC<CardCourseProps> = ({
  description = '',
  imgSrc = '',
  stars,
  subtitle = '',
  title = '',
  url = '',
  value = '',
  target = '_self',
}: CardCourseProps) => (
  <Link href={url} target={target}>
    <Container>
      <Header backgroundUrl={imgSrc || PlaceholderCourse} />
      <Content>
        {stars && (
          <ContainerStars>
            {[...Array(stars)].map((_, i) => (
              <IconStar key={i} />
            ))}
          </ContainerStars>
        )}
        {title && <Title title={title}>{title}</Title>}
        {description && <Description>{description}</Description>}
        {subtitle && <Subtitle>{subtitle}</Subtitle>}
      </Content>
      <Footer>
        {value && <Value>{value}</Value>}
        <Button>
          Ver curso
          <IconArrowButton />
        </Button>
      </Footer>
    </Container>
  </Link>
)

export default CardCourse
