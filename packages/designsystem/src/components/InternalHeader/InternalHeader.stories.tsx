import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import InternalHeader from './InternalHeader'
import Heading2 from '../Typography/Heading2'

export default {
  title: 'Components/InternalHeader',
  component: InternalHeader,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => (
  <InternalHeader>
    <Heading2 color="primaryColor">Default</Heading2>
  </InternalHeader>
)
