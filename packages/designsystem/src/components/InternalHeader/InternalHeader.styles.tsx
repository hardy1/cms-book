import styled from 'styled-components'
import media from '../../resources/media'

export const InternalHeaderContainer = styled.div`
  width: 100%;
  text-align: center;

  & * {
    margin: 1rem 0;
  }

  @media ${media.tabletL} {
    margin-top: 32px;
  }
`

export const FullDivider = styled.hr`
  border: 0;
  background-color: #eeeeee;
  display: block;
  height: 1px;
  margin-top: 16px;
`
