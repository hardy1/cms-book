import * as React from 'react'
import { render } from '@testing-library/react'
import InternalHeader from './InternalHeader'

describe('InternalHeader', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<InternalHeader>{children}</InternalHeader>)

    expect(component).toBeTruthy()
  })
})
