import * as React from 'react'

import { InternalHeaderContainer, FullDivider } from './InternalHeader.styles'

export type InternalHeaderProps = {
  children: React.ReactNode
}

const InternalHeader: React.FC<InternalHeaderProps> = ({ children }: InternalHeaderProps) => (
  <InternalHeaderContainer>
    {children}
    <FullDivider />
  </InternalHeaderContainer>
)

export default InternalHeader
