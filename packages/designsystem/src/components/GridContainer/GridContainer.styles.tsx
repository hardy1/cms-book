import styled from 'styled-components'
import media from '../../resources/media'

interface StyledGridContainerProps {
  margins?: string
}

export const StyledGridContainer = styled.div<StyledGridContainerProps>`
  display: grid;
  column-gap: 2.188rem;
  grid-template-columns: repeat(12, 1fr);
  margin: ${(props) => (props.margins ? props.margins : '0px auto')};
  max-width: 1280px;
  position: relative;

  @media ${media.laptopM} {
    max-width: 1200px;
    padding: 0 20px;
  }
`
