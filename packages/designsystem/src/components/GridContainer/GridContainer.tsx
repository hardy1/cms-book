import * as React from 'react'
import { StyledGridContainer } from './GridContainer.styles'

export type GridContainerProps = {
  children: React.ReactNode
  margins?: string
}

const GridContainer: React.FC<GridContainerProps> = ({ children, margins }: GridContainerProps) => (
  <StyledGridContainer margins={margins}>{children}</StyledGridContainer>
)

export default GridContainer
