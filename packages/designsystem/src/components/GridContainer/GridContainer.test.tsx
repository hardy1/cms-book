import * as React from 'react'
import { render } from '@testing-library/react'
import GridContainer from './GridContainer'

describe('GridContainer', () => {
  const children = 'test'

  it('SHOULD render correctly', () => {
    const component = render(<GridContainer>{children}</GridContainer>)

    expect(component).toBeTruthy()
  })
})
