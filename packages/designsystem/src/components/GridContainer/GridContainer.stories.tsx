import * as React from 'react'
import { Meta, Story } from '@storybook/react/types-6-0'

import GridContainer, { GridContainerProps } from './GridContainer'

export default {
  title: 'Components/GridContainer',
  component: GridContainer,
} as Meta

const Template: Story<GridContainerProps> = (args) => <GridContainer {...args}>Default</GridContainer>

export const Default = Template.bind({})
Default.args = {}

export const Margins = Template.bind({})
Margins.args = {
  margins: '75px 0',
}
