import * as React from 'react'
import { StyledButton } from './Button.styles'

export type ButtonProps = {
  as?: React.ElementType
  children?: React.ReactNode
  /** Renders a disabled button and prevents onClick */
  disabled?: boolean
  fill?: 'flat' | 'outline' | 'text'
  icon?: React.ReactNode
  /** Callback function to be called when user clicks on button */
  onClick?: () => void
  size?: 'large' | 'normal' | 'medium' | 'small'
  type?: 'button' | 'submit' | 'reset'
  color?: string
  className?: string
  href?: string
  target?: string
}

const Button: React.FC<ButtonProps> = ({
  as = 'button',
  children,
  disabled = false,
  fill = 'flat',
  icon,
  onClick,
  size = 'normal',
  type = 'button',
  color = '',
  className,
  href,
  target,
}: ButtonProps) => (
  <StyledButton
    as={as}
    className={className}
    disabled={disabled}
    onClick={onClick}
    fill={fill}
    size={size}
    type={type}
    hasChildren={children}
    data-testid="StyledButton"
    color={color}
    href={href}
    target={target}
  >
    {children && <span>{children}</span>}
    {icon && icon}
  </StyledButton>
)

export default Button
