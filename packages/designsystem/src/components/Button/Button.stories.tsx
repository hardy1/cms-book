import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import { IconArrowButton, IconHeadphone } from '../../icons'

import Button, { ButtonProps } from './Button'

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    icon: {
      control: {
        disable: true,
      },
    },
  },
} as Meta

const Template: Story<ButtonProps> = (args) => <Button {...args} />

export const Default = Template.bind({})
Default.args = {
  children: 'Button',
}

export const Disabled = Template.bind({})
Disabled.args = {
  children: 'Button',
  disabled: true,
}

export const Outline = Template.bind({})
Outline.args = {
  children: 'Button',
  fill: 'outline',
}

export const Text = Template.bind({})
Text.args = {
  children: 'Button',
  fill: 'text',
}

export const Small = Template.bind({})
Small.args = {
  children: 'Button',
  size: 'small',
}

export const Medium = Template.bind({})
Medium.args = {
  children: 'Button',
  size: 'medium',
}

export const Large = Template.bind({})
Large.args = {
  children: 'Button',
  size: 'large',
}

export const WithIcon = Template.bind({})
WithIcon.args = {
  children: 'Button',
  icon: <IconHeadphone />,
}

export const TextWithIcon = Template.bind({})
TextWithIcon.args = {
  children: 'Button',
  fill: 'text',
  icon: <IconArrowButton />,
}

export const Circle = Template.bind({})
Circle.args = {
  icon: <IconHeadphone />,
}
