import styled from 'styled-components'
import colors from '../../resources/colors'

type ButtonProps = {
  fill: 'flat' | 'outline' | 'text'
  size: 'large' | 'normal' | 'medium' | 'small'
  color?: string
  hasChildren: React.ReactNode
}

export const StyledButton = styled.button<ButtonProps>`
  align-items: center;
  border-width: 2px;
  border-style: solid;
  display: inline-flex;
  height: auto;
  justify-content: center;
  margin: 0;
  outline: 0;
  transition: all 0.5s ease;
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  text-decoration: none;

  ${(props) =>
    props.fill === 'flat' &&
    `
      background-color: ${props.color || colors.secondaryColor};
      border-radius: 4px;
      border-color: ${props.color || colors.secondaryColor};
      color: ${colors.white};
    `};

  ${(props) =>
    props.fill === 'outline' &&
    `
      border-radius: 4px;
      background-color: transparent;
      border-color: ${props.color || colors.secondaryColor};
      color: ${props.color || colors.secondaryColor};
    `};

  ${(props) =>
    props.fill === 'text' &&
    `
      background-color: transparent;
      border: none;
      color: ${props.color || colors.secondaryColor};
      font-weight: 900;
      padding: 0 !important;
    `};

  ${({ disabled }) =>
    disabled &&
    `
      background-color: #E5E5E5;
      border-color: #E5E5E5;
      color: ${colors.disabledColor};
  `};

  ${(props) =>
    props.size === 'small' &&
    `
      font-size: 0.875em;
      padding: 5px 32px;
    `};

  ${(props) =>
    props.size === 'medium' &&
    `
      font-size: 1.125em;
      padding: 8px 42px;
    `};

  ${(props) =>
    props.size === 'normal' &&
    `
      font-size: 1.125em;
      padding: 15px 40px;
    `};

  ${(props) =>
    props.size === 'large' &&
    `
      font-size: 1.250em;
      padding: 16px 80px;
    `};

  ${(props) =>
    !props.hasChildren &&
    `
      border-radius: 50%;
      padding: 0;
      height: 55px;
      width: 55px;
    `};

  &:hover {
    ${(props) =>
      props.fill === 'flat' &&
      `
        box-shadow: 0px 4px 5px rgba(0, 105, 153, 0.25);
    `};

    ${(props) =>
      props.fill === 'outline' &&
      `
        background-color: ${props.color || colors.secondaryColor};
        border-color: ${props.color || colors.secondaryColor};
        color: ${colors.white};
    `};

    ${({ disabled }) => disabled && 'box-shadow: none;'};
  }

  & svg:first-child {
    margin-right: 0;
  }

  & svg:last-child {
    margin-left: 10px;
  }

  & svg:only-child {
    margin: 0;
  }
`
