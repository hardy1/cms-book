import * as React from 'react'
import { fireEvent, render } from '@testing-library/react'
import Button from './Button'
import { act } from 'react-dom/test-utils'

describe('Button', () => {
  const children = 'Button text'

  it('SHOULD render correctly', () => {
    const component = render(<Button onClick={jest.fn}>{children}</Button>)

    expect(component).toBeTruthy()
  })

  it.skip('SHOULD call onClick function WHEN component is clicked', () => {
    jest.useFakeTimers()

    const buttonHandler = jest.fn()

    const component = render(<Button onClick={buttonHandler}>{children}</Button>)

    const button = component.getByTestId('StyledButton')

    act(() => {
      fireEvent.click(button)
    })

    expect(buttonHandler).toBeCalled()
  })

  it.skip('SHOULD not be disabled', () => {
    const component = render(<Button onClick={jest.fn}>{children}</Button>)

    const button = component.getByTestId('StyledButton')

    expect(button).not.toBeDisabled()
  })

  describe('WHEN disabled is true', () => {
    it.skip('THEN it SHOULD render button disabled', () => {
      const component = render(
        <Button disabled onClick={jest.fn}>
          {children}
        </Button>,
      )

      const button = component.getByTestId('StyledButton')

      expect(button).toBeDisabled()
    })
  })
})
