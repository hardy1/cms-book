interface DrugsTextSectionEntry {
  name: string
  content?: string
  parsedContent?: string | Element | Element[] | JSX.Element | JSX.Element[]
}

interface DrugsTextSection {
  name: string
  entries?: DrugsTextSectionEntry[]
}

export interface DrugsTextProps {
  name: string
  alternativeNames?: string[]
  sections?: DrugsTextSection[]
  category?: string
}
