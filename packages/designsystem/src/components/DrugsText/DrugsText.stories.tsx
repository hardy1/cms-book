import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import DrugsText from './DrugsText'
import { DrugsTextProps } from './DrugsText.model'
import { DrugsTextMock } from './DrugsText.mock'

export default {
  title: 'Components/DrugsText',
  component: DrugsText,
} as Meta

const Template: Story<DrugsTextProps> = (args) => (
  <StoryContainer width={800}>
    <DrugsText {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = DrugsTextMock
