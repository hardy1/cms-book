import * as React from 'react'
import { render } from '@testing-library/react'
import DrugsText from './DrugsText'
import { DrugsTextMock } from './DrugsText.mock'

describe('DrugsText', () => {
  it('SHOULD render correctly', () => {
    const component = render(<DrugsText {...DrugsTextMock} />)

    expect(component).toBeTruthy()
  })
})
