import React, { FC } from 'react'
import { DrugsTextProps } from './DrugsText.model'
import { DrugContainer, DrugSection, DrugSectionTitle, DrugSubtitle, DrugText, DrugTitle } from './DrugsText.styles'

const DrugsText: FC<DrugsTextProps> = (props: DrugsTextProps) => {
  const { name, alternativeNames, sections, category } = props

  const drugSections = sections?.map((section, i) => (
    <DrugSection key={i}>
      {section?.name && section?.name !== 'default' && <DrugSectionTitle>{section.name}</DrugSectionTitle>}
      {section?.entries?.map((entry, j) => (
        <DrugText key={j} topic={entry.name}>
          {entry.parsedContent || entry.content}
        </DrugText>
      ))}
    </DrugSection>
  ))

  return (
    <DrugContainer>
      <DrugSubtitle>{category}</DrugSubtitle>
      <DrugTitle>{name}</DrugTitle>

      {alternativeNames && <DrugText>({alternativeNames.join(', ')})</DrugText>}

      {drugSections}
    </DrugContainer>
  )
}

export default DrugsText
