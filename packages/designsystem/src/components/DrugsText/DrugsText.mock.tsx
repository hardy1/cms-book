export const DrugsTextMock = {
  name: 'AMITRIPTILINA',
  alternativeNames: ['Amytril', 'NiceDrug', 'Foolano'],
  sections: [
    {
      name: '',
      entries: [
        {
          name: 'presentation',
          content: 'comprimido revestido de 10 mg, 25 mg, 75 mg.',
        },
        {
          name: 'therapeutic_classification',
          content: 'antidepressivo tricíclico (TCA).',
        },
        {
          name: 'mechanism_of_action',
          content:
            'amitriptilina é um antidepressivo com propriedades ansiolítica e sedativa. Inibe o mecanismo da bomba da membrana responsável pela captação de serotonina e de norepinefrina nos neurônios serotoninérgicos e adrenérgicos.',
        },
        {
          name: 'metabolism',
          content:
            'hepático, metabolizado via N-desmetilação pelas CYP3A4, CYP2C9 e CYP2D6 em seu metabólito ativo primário, a nortriptilina.',
        },
        {
          name: 'excretion',
          content:
            'renal, principalmente como metabólitos livres ou na forma conjugada; pequenas quantidades excretadas nas fezes; não dialisável (hemodiálise e peritoneal)',
        },
        {
          name: 't1_2_of_elimination',
          content: '9 a 25 h.',
        },
        {
          name: 'dilution',
          content: '-',
        },
        {
          name: 'adjustment_for_liver_function',
          content: 'pacientes com comprometimento da função renal podem precisar de doses reduzidas. Usar com cautela.',
        },
        {
          name: 'adjustment_for_kidney_function',
          content: 'pacientes com comprometimento hepático podem exigir doses reduzidas. Usar com cautela.',
        },
        {
          name: 'pomalidomide_evaluation',
          content: '-',
        },
        {
          name: 'contraindication',
          content: '',
        },
        {
          name: 'greater_severity',
          content: '',
        },
        {
          name: 'moderate_severity',
          content: '',
        },
        {
          name: 'frequent_adverse_reactions',
          content:
            'Endocrinometabólica: ganho de peso. Gastrintestinais: obstipação; xerostomia. Neurológicas: tontura; cefaleia; sonolência. Oftalmológica: visão turva.',
        },
        {
          name: 'serious_adverse_reactions',
          content:
            'Cardiovasculares: arritmia cardíaca; alterações no ECG; infarto do miocárdio; intervalo QT prolongado; morte súbita cardíaca. Hematológica: agranulocitose. Hepática: icterícia (rara). Neurológicas: síndrome neuroléptica maligna; convulsão. Psiquiátricas: depressão; pensamentos suicidas e suicídio.',
        },
        {
          name: 'adverse_reactions_recommendations',
          content:
            'amitriptilina não é recomendada para uso durante a fase de recuperação aguda após infarto do miocárdio.',
        },
      ],
    },
    {
      name: 'Farmacocinetico',
      entries: [
        {
          name: 'absorption',
          content:
            'absorvido rapidamente (VO), o pico de concentração plasmática (Tmáx) ocorre em aproximadamente 6 h após a administração oral.',
        },
        {
          name: 'distribution',
          content: 'a ligação às proteínas plasmática é de 96%',
        },
      ],
    },
  ],
  category: 'Medicamentos para dor',
}
