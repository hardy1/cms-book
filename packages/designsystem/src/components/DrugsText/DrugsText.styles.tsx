import colors from '../../resources/colors'
import React, { FC } from 'react'
import styled from 'styled-components'

export const DrugTitle = styled.h1`
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 2rem;
  line-height: 2.2rem;
  margin-top: 0;
`

export const DrugSubtitle = styled.h2`
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 1rem;
  line-height: 2.2rem;
  margin-bottom: 0;
  color: ${colors.lightGray};
`

export const DrugLabTitle = styled.h5`
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 1rem;
  line-height: 2rem;
  margin: 0;
`

export const DrugContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

export const DrugSection = styled.section``

export const DrugSectionTitle = styled.h2`
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: bold;
  line-height: 2rem;
`

const DrugTextContent = styled.p`
  font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 1rem;
  line-height: 2rem;
  text-align: justify;
  letter-spacing: 0.05em;
  margin: 0;

  & .Drug {
    &__Topic {
      font-weight: bold;
    }
  }
`

interface DrugTextProps {
  topic?: string
  children: React.ReactNode | HTMLElement
}

export const DrugText: FC<DrugTextProps> = (props: DrugTextProps) => {
  const { topic, children } = props

  return (
    <DrugTextContent>
      {topic && <span className="Drug__Topic">{topic}: </span>}
      {children}
    </DrugTextContent>
  )
}
