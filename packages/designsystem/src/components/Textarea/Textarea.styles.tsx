import styled from 'styled-components'
import colors from '../../resources/colors'

type TextareaProps = {
  error: boolean
}

export const TextareaStyled = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  font-family: 'Lato', sans-serif;
  justify-content: center;
  width: 100%;
`

export const TextareaLabel = styled.label`
  color: ${colors.disabledColor};
  font-size: 1em;
  font-weight: 400;
  line-height: 19px;
  margin-bottom: 12px;
  text-transform: uppercase;
  letter-spacing: 0.05em;
`

export const TextareaContainer = styled.div`
  position: relative;
  display: flex;
  width: 100%;
`

export const TextareaComponent = styled.textarea<TextareaProps>`
  border-color: ${(props) => (props.error ? `${colors.errorColor}` : `${colors.disabledColor}`)};
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  color: ${(props) => (props.error ? `${colors.errorColor}` : `${colors.neutralColor}`)};
  font-family: 'Lato', sans-serif;
  font-size: 1em;
  flex: 1;
  outline: none;
  padding: 16px;
  resize: none;
  width: auto;

  &::placeholder {
    color: ${colors.disabledColor};
  }

  &:focus {
    border-color: ${colors.primaryColor};
    color: ${colors.primaryColor};
  }
`

export const TextareaMessage = styled.span`
  color: ${colors.errorColor};
  font-size: 1em;
  margin-top: 12px;
`
