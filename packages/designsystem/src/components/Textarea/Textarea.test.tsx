import * as React from 'react'
import { render } from '@testing-library/react'
import Textarea from './Textarea'

describe('Textarea', () => {
  it('SHOULD render correctly', () => {
    const component = render(<Textarea id="textareaId" onChange={jest.fn()} />)

    expect(component).toBeTruthy()
  })
})
