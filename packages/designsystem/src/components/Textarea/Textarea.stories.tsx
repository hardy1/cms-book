import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Textarea, { TextareaProps } from './Textarea'

export default {
  title: 'Components/Textarea',
  component: Textarea,
  parameters: {
    viewport: {
      defaultViewport: 'mobile2',
    },
  },
} as Meta

const Template: Story<TextareaProps> = (args) => <Textarea {...args} />

export const Default = Template.bind({})
Default.args = {
  id: 'textareaId',
  placeholder: 'Placeholder',
}

export const CustomRows = Template.bind({})
CustomRows.args = {
  id: 'textareaId',
  placeholder: 'Placeholder',
  rows: 10,
}

export const WithLabel = Template.bind({})
WithLabel.args = {
  id: 'textareaId',
  label: 'Label',
  message: 'Message',
  placeholder: 'Placeholder',
}

export const Error = Template.bind({})
Error.args = {
  id: 'textareaId',
  message: 'Message',
  placeholder: 'Placeholder',
  error: true,
}
