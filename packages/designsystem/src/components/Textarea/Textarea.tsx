import * as React from 'react'
import { TextareaStyled, TextareaLabel, TextareaContainer, TextareaComponent, TextareaMessage } from './Textarea.styles'

export type TextareaProps = {
  error?: boolean
  id: string
  label?: string
  value?: string
  message?: string
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void
  placeholder?: string
  rows?: number
}

const Textarea: React.FC<TextareaProps> = ({
  error = false,
  id,
  value,
  label = '',
  message = '',
  onChange,
  placeholder = '',
  rows = 2,
}: TextareaProps) => {
  const hasLabel = label && <TextareaLabel htmlFor={id}>{label}</TextareaLabel>
  const hasError = error && <TextareaMessage className="TextareaErrorMessage">{message}</TextareaMessage>
  return (
    <TextareaStyled>
      {hasLabel}

      <TextareaContainer>
        <TextareaComponent
          rows={rows}
          value={value}
          id={id}
          placeholder={placeholder}
          error={error}
          onChange={onChange}
        />
      </TextareaContainer>

      {hasError}
    </TextareaStyled>
  )
}

export default Textarea
