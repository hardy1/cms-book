import * as React from 'react'
import { IconFacebook, IconTwitter, IconLinkedin, IconPodcast } from '../../icons'

import { SocialSectionContainer } from './SocialSection.styles'

const SocialSection: React.FC = () => (
  <SocialSectionContainer>
    <IconFacebook />
    <IconTwitter />
    <IconLinkedin />
    <IconPodcast />
  </SocialSectionContainer>
)

export default SocialSection
