import * as React from 'react'
import { Meta } from '@storybook/react/types-6-0'

import SocialSection from './SocialSection'

export default {
  title: 'Components/SocialSection',
  component: SocialSection,
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
} as Meta

export const Default: React.FC = () => <SocialSection />
