import * as React from 'react'
import { render } from '@testing-library/react'
import SocialSection from './SocialSection'

describe('SocialSection', () => {
  it('SHOULD render correctly', () => {
    const component = render(<SocialSection />)

    expect(component).toBeTruthy()
  })
})
