import * as React from 'react'
import {
  Link,
  Container,
  ImgContainer,
  Img,
  Content,
  Title,
  Footer,
  FooterText,
  FooterTextSubtitle,
  Subtitle,
} from './CardResearch.styles'
import { PlaceholderNews } from '../../assets'

export type CardResearchProps = {
  imgAlt?: string
  imgSrc?: string
  readingTime?: number
  remuneration?: string
  title?: string
  url?: string
  subtitle?: string
}

const CardResearch: React.FC<CardResearchProps> = ({
  imgAlt = '',
  imgSrc = '',
  readingTime = 0,
  remuneration = '',
  title = '',
  url = '',
  subtitle = '',
}: CardResearchProps) => (
  <Link href={url}>
    <Container>
      <ImgContainer>
        <Img src={imgSrc || PlaceholderNews} alt={imgAlt} />
      </ImgContainer>
      <Content>
        {title && <Title>{title}</Title>}
        {(remuneration || readingTime || subtitle) && (
          <>
            <Subtitle>{subtitle && <FooterTextSubtitle>{subtitle}</FooterTextSubtitle>}</Subtitle>
            <Footer>
              {remuneration && <FooterText>Remuneração: {remuneration}</FooterText>}
              {readingTime > 0 && <FooterText>Tempo estimado: {readingTime} min</FooterText>}
            </Footer>
          </>
        )}
      </Content>
    </Container>
  </Link>
)

export default CardResearch
