import styled from 'styled-components'
import colors from '../../resources/colors'

export const Link = styled.a`
  text-decoration: none;
`

export const Container = styled.div`
  cursor: pointer;
  font-family: 'Lato', sans-serif;
  display: flex;
  height: 80px;
`

export const ImgContainer = styled.div`
  height: 80px;
  width: 160px;
  border-radius: 4px;
  overflow: hidden;
`

export const Img = styled.img`
  object-fit: cover;
  height: 100%;
  width: 100%;
`

export const Content = styled.div`
  margin: 0 0 0 20px;
  flex: 1;
  width: auto;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Title = styled.h4`
  font-family: 'Montserrat', sans-serif;
  color: ${colors.primaryColor};
  font-size: 1em;
  margin: 0;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

export const Subtitle = styled.div`
  margin-top: 16px;
`

export const Footer = styled.div`
  margin: auto 0 0 0;
`

export const FooterText = styled.p`
  margin: 0;
  color: ${colors.disabledColor};
  display: block;
  font-family: 'Lato', sans-serif;
  font-size: 0.75em;
  font-weight: 400;
`

export const FooterTextSubtitle = styled(FooterText)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: inherit;
`
