import * as React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import StoryContainer from '../../resources/StoryContainer'

import CardResearch, { CardResearchProps } from './CardResearch'

export default {
  title: 'Components/CardResearch',
  component: CardResearch,
} as Meta

const Template: Story<CardResearchProps> = (args) => (
  <StoryContainer width={450}>
    <CardResearch {...args} />
  </StoryContainer>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Pesquisa ONCO Censo 2020',
  subtitle:
    'A pesquisa ONCO CENSO é um projeto com o objetivo de captar informações que ajudarão a conhecer melhor o universo dos oncologistas e hematologistas brasileiros.',
  remuneration: '1 mês grátis de assinatura',
  readingTime: 10,
  imgSrc: 'https://picsum.photos/200/100',
  imgAlt: 'Image alternative text',
  url: '#',
}
