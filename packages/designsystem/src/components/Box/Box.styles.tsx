import styled, { css } from 'styled-components'
import colors from '../../resources/colors'

type StyledBoxProps = {
  className?: string
  color?: string
  titleColor?: string
  buttonColor?: string
  width?: string
  margin?: string
  boxShadowMobile?: boolean
  gridColumn?: string
}

const StyledBox = styled.div<StyledBoxProps>`
  position: relative;
  display: block;
  width: 100%;
  min-height: 370px;
  background-color: ${colors.white};
  border-width: 2px;
  border-style: solid;
  border-color: ${colors.white};
  border-radius: 10px;
  text-align: center;
  box-sizing: border-box;
  margin: ${({ margin }) => margin || 'auto'};
  padding: 0 1rem;

  ${({ gridColumn }) =>
    gridColumn &&
    css`
      grid-column: ${gridColumn};
    `}

  ${({ boxShadowMobile }) =>
    boxShadowMobile &&
    css`
      box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
      padding: 12px 16px;
    `}

  @media screen and (min-width: 768px) {
    background-color: ${colors.white};
    width: ${({ width }) => width || 'auto'};
    padding: 24px 16px;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.25);
  }

  @media screen and (min-width: 1280px) {
    padding: 30px 29px 62px;
  }

  :active {
    border-color: ${({ color }) => color || colors.white};
  }

  img {
    &.image-access {
      width: auto;
      height: 40px;
      margin-bottom: 18px;
    }
  }

  h3 {
    min-height: 58px;
    color: ${({ titleColor }) => titleColor};
    font-family: 'Montserrat' sans-serif;
    font-size: 24px;
    line-height: 29px;
    margin-top: 0;
    margin-bottom: 20px;

    @media screen and (min-width: 768px) {
      min-height: 87px;
    }

    @media screen and (min-width: 1280px) {
      min-height: 58px;
    }

    small {
      height: 3rem;
      display: block;
      color: ${colors.neutralColor};
      font-size: 1rem;
      font-weight: 400;
      line-height: 19px;
    }
  }

  .recommended {
    position: absolute;
    top: -45px;
    left: 0;
    width: 100%;
    height: 35px;
    background-color: ${({ color }) => color};
    border-radius: 10px;
    color: ${colors.white};
    font-size: 12px;
    font-weight: 300;
    line-height: 35px;
    text-transform: uppercase;
    margin: 0;
  }

  p {
    min-height: 76px;
    color: ${colors.neutralColor};
    font-family: 'Lato';
    font-size: 16px;
    line-height: 19px;
    margin-bottom: 24px;
  }

  .price {
    &--full {
      color: ${({ color }) => color};
    }

    &--monthly {
      color: ${({ color }) => color};
    }
  }

  .details {
    &__title {
      color: ${({ color }) => color};
    }

    &__text {
      color: ${colors.neutralColor};
    }
  }

  button,
  a {
    margin-top: 25px;
    min-width: 150px;
    font-size: 18px;
    background-color: ${({ buttonColor }) => buttonColor};
    border-color: ${({ buttonColor }) => buttonColor};
    box-sizing: border-box;
    padding: 8.5px 15px;
  }
`

export default StyledBox
export { StyledBoxProps }
