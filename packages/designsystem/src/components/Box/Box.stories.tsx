import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Box from './Box'

export default {
  title: 'Components/Box',
  component: Box,
} as Meta

const Template: Story = (args) => <Box {...args} />

export const Default = Template.bind({})
