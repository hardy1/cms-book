import React from 'react'
import { render } from '@testing-library/react'
import Box from './Box.styles'

describe('Box', () => {
  it('should render the component', () => {
    const boxText = 'Box component'
    const wrapper = render(<Box>{boxText}</Box>)
    expect(wrapper).toBeTruthy()
  })
})
