import * as React from 'react'
import StyledBox from './Box.styles'

export type BoxProps = {
  as?: React.ElementType
  buttonColor?: string
  className?: string
  children?: React.ReactNode
  color?: string
  titleColor?: string
  boxShadowMobile?: boolean
  gridColumn?: string
  width?: string
}

const Box: React.FC<BoxProps> = ({
  as = 'div',
  boxShadowMobile,
  buttonColor,
  children,
  color,
  titleColor,
  className,
  gridColumn,
  width,
}: BoxProps) => (
  <StyledBox
    as={as}
    boxShadowMobile={boxShadowMobile}
    buttonColor={buttonColor}
    className={className}
    color={color}
    titleColor={titleColor}
    gridColumn={gridColumn}
    width={width}
  >
    {children}
  </StyledBox>
)

export default Box
