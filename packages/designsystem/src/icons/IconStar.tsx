import * as React from 'react'

function SvgIconStar(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={14} height={15} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M6.024 1.346c.278-.965 1.645-.965 1.923 0l.815 2.84a1 1 0 00.962.724h2.878c.938 0 1.36 1.176.635 1.772l-2.532 2.081a1 1 0 00-.326 1.049L11.294 13c.271.944-.837 1.672-1.596 1.05L7.621 12.34a1 1 0 00-1.27 0L4.273 14.05c-.759.623-1.867-.105-1.596-1.049l.916-3.188a1 1 0 00-.327-1.049L.735 6.683C.009 6.085.43 4.91 1.369 4.91h2.879a1 1 0 00.96-.724l.816-2.84z"
        fill="#F1C40F"
      />
    </svg>
  )
}

export default SvgIconStar
