import * as React from 'react'

function SvgIconLinkedin(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={18} height={17} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M4.603 16.579H1.166V5.51h3.437v11.069zM2.883 4C1.783 4 .892 3.09.892 1.99a1.99 1.99 0 113.981 0c0 1.1-.892 2.01-1.99 2.01zM17.467 16.58h-3.43V11.19c0-1.285-.026-2.931-1.787-2.931-1.787 0-2.06 1.395-2.06 2.838v5.48H6.755V5.51h3.296v1.51h.048c.46-.87 1.58-1.788 3.252-1.788 3.479 0 4.118 2.291 4.118 5.267v6.08h-.003z"
        fill="#144590"
      />
    </svg>
  )
}

export default SvgIconLinkedin
