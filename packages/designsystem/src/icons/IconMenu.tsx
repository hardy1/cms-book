import * as React from 'react'

const SvgIconMenu = ({ className, width, height, fill }: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      className={className}
      width={width || 18}
      height={height || 16}
      viewBox={`0 0 ${width || 18} ${height || 16}`}
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M.643 2.893h16.714A.643.643 0 0018 2.25V.643A.643.643 0 0017.357 0H.643A.643.643 0 000 .643V2.25c0 .355.288.643.643.643zm0 6.428h16.714A.643.643 0 0018 8.68V7.07a.643.643 0 00-.643-.642H.643A.643.643 0 000 7.07V8.68c0 .355.288.642.643.642zm0 6.429h16.714a.643.643 0 00.643-.643V13.5a.643.643 0 00-.643-.643H.643A.643.643 0 000 13.5v1.607c0 .355.288.643.643.643z"
        fill={fill || '#144590'}
      />
    </svg>
  )
}

export default SvgIconMenu
