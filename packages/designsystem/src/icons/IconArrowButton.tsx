import * as React from 'react'

function SvgIconArrowButton(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={17} height={18} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M7.228 1.206L8.07.29a.857.857 0 011.287 0l7.376 8.008a1.046 1.046 0 010 1.398l-7.376 8.012a.857.857 0 01-1.287 0l-.842-.915a1.051 1.051 0 01.015-1.413l4.572-4.732H.911c-.505 0-.911-.441-.911-.99V8.341c0-.549.406-.99.91-.99h10.905L7.243 2.62a1.043 1.043 0 01-.015-1.413z"
        fill="#216DE1"
      />
    </svg>
  )
}

export default SvgIconArrowButton
