import * as React from 'react'

function SvgIconArrowRight(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={10} height={16} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M1.895 1l6.957 6.923-6.957 6.923"
        stroke="#216DE1"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SvgIconArrowRight
