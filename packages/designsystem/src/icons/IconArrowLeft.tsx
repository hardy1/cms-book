import * as React from 'react'

function SvgIconArrowLeft(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={10} height={16} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M8.957 14.846L2 7.923 8.957 1"
        stroke="#216DE1"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SvgIconArrowLeft
