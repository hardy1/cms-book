import * as React from 'react'

function SvgIconArrowDown(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={12} height={8} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M10.9 1.95L5.95 6.9 1 1.95"
        stroke="#3E3F42"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SvgIconArrowDown
