import * as React from 'react'

function SvgIconHeadphone(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={25} height={25} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M20.6 12.069v-1.715c0-4.464-3.496-8.201-7.793-8.33-2.214-.052-4.271.739-5.844 2.266a7.972 7.972 0 00-2.44 5.77v2.009c-1.11 0-2.01.901-2.01 2.01v4.02c0 1.108.9 2.009 2.01 2.009h2.009V10.059a5.979 5.979 0 011.83-4.327 6.006 6.006 0 014.384-1.7c3.224.097 5.844 2.933 5.844 6.322v9.754h2.01c1.109 0 2.01-.901 2.01-2.01v-4.02c0-1.108-.901-2.009-2.01-2.009z"
        fill="#fff"
      />
      <path d="M7.537 12.069h2.01v8.039h-2.01v-8.039zm8.039 0h2.01v8.039h-2.01v-8.039z" fill="#fff" />
    </svg>
  )
}

export default SvgIconHeadphone
