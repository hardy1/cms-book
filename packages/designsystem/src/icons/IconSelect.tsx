import * as React from 'react'

function SvgIconSelect(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={12} height={9} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M6.781 8.023a1 1 0 01-1.562 0L.37 1.958A1 1 0 011.151.333h9.697a1 1 0 01.782 1.625L6.78 8.023z"
        fill="#144590"
      />
    </svg>
  )
}

export default SvgIconSelect
