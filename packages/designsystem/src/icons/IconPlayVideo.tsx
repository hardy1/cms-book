import * as React from 'react'

function SvgIconPlayVideo(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={49} height={49} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <ellipse cx={24.523} cy={24.651} rx={24.017} ry={23.93} fill="#216DE1" />
      <path d="M33.694 25.086l-13.1 7.536V17.55l13.1 7.536z" fill="#fff" />
    </svg>
  )
}

export default SvgIconPlayVideo
