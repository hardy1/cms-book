import React from 'react'

const SvgIconExclamationRounded = ({ className, width, height, fill }: React.SVGProps<SVGSVGElement>) => (
  <svg
    className={className}
    width={width || 512}
    height={height || 512}
    viewBox={`0 0 ${width || 512} ${height || 512}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M496 256.004c0 132.576-107.459 239.995-239.995 239.995s-239.996-107.419-239.996-239.995c0-132.499 107.459-239.996 239.996-239.996s239.995 107.498 239.995 239.996zM256.004 304.39c-24.585 0-44.516 19.93-44.516 44.515s19.93 44.517 44.516 44.517c24.585 0 44.515-19.931 44.515-44.517s-19.93-44.515-44.515-44.515zM213.74 144.381l7.178 131.611c0.335 6.159 5.428 10.98 11.595 10.98h46.979c6.168 0 11.26-4.821 11.596-10.98l7.178-131.611c0.363-6.652-4.933-12.245-11.596-12.245h-61.338c-6.662 0-11.958 5.594-11.595 12.245z"
      fill={fill || '#fff'}
    />
  </svg>
)

export default SvgIconExclamationRounded
