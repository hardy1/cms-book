import React from 'react'

const SvgIconClose = ({ width, height, fill }: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg width={width || 19} height={height || 19} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M4.58575 5.78345L0 10.4838L1.34139 11.8588L5.92793 7.15757L10.6578 12L12 10.6259L7.26934 5.78263L11.5695 1.37496L10.2281 3.05176e-05L5.92716 4.40851L1.76572 0.148043L0.423523 1.52218L4.58575 5.78345Z"
        fill={fill || '#3e3f42'}
      />
    </svg>
  )
}

export default SvgIconClose
