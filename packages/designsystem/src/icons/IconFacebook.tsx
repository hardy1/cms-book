import * as React from 'react'

function SvgIconFacebook(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={18} height={18} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M17.155 9.525A8.577 8.577 0 008.579.947 8.577 8.577 0 000 9.525 8.581 8.581 0 007.237 18v-5.995H5.059v-2.48h2.18v-1.89c0-2.15 1.28-3.337 3.24-3.337.938 0 1.92.168 1.92.168v2.11h-1.082c-1.066 0-1.398.66-1.398 1.34v1.61h2.379l-.38 2.48H9.917V18a8.581 8.581 0 007.238-8.475z"
        fill="#144590"
      />
    </svg>
  )
}

export default SvgIconFacebook
