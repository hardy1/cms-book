import * as React from 'react'

function SvgIconPlay(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={9} height={11} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M8.83 5.768L.563 10.552V.984l8.269 4.784z" fill="#9EA0A5" />
    </svg>
  )
}

export default SvgIconPlay
