import * as React from 'react'

function SvgIconPlayTip(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={29} height={30} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <circle cx={14.791} cy={14.842} r={14.184} fill="#fff" />
      <path d="M19.519 15.367l-7.88 4.55v-9.1l7.88 4.55z" fill="#216DE1" />
    </svg>
  )
}

export default SvgIconPlayTip
