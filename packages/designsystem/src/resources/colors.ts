const colors = {
  black: '#000',
  disabledColor: '#9ea0a5',
  neutralColor: '#3e3f42',
  primaryColor: '#144590',
  secondaryColor: '#216de1',
  tertiaryColor: '#00e4b7',
  quaternaryColor: '#b1dbff',
  successColor: '#2ecc71',
  warningColor: '#f1c40f',
  errorColor: '#fe4140',
  lightColor: '#f1f1f1',
  lightBlue: '#b3dcff',
  white: '#fff',
  lightGray: '#c4c4c4',
}

export default colors
