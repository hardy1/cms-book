import * as React from 'react'
import styled from 'styled-components'

export type StoryContainerProps = {
  children: React.ReactNode
  width: number
}

export const StoryContainerStyled = styled.div<StoryContainerProps>`
  display: flex;
  flex-direction: column;
  max-width: ${(props) => `${props.width}px`};
  width: 100%;
`

const StoryContainer: React.FC<StoryContainerProps> = ({ children, width }: StoryContainerProps) => (
  <StoryContainerStyled width={width}>{children}</StoryContainerStyled>
)

export default StoryContainer
